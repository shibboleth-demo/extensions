#!/bin/bash

# Simple test if an IdP's ECP endpoint is running

# --------------------------------------------------------------------
#
# You need to set these parameters
#
#

# userid = a valid login id on your system
userid="some_userid"

# password = the user's password
userpw="good_password"

# idphost = your idp hostname
idphost="idp.example.edu"

# idpid = your idp entity id
idpid="https://idp.example.edu/"

# rpid = a valid SP entityId that is configured for ECP
rpid="https://sp.example.edu/shibboleth"

# acsurl,ascurlbinding = an AssertionConsumerService URL and binding
acsurl="https://sp.example.edu/Shibboleth.sso/SAML2/ECP"
acsurlbinding="urn:oasis:names:tc:SAML:2.0:bindings:PAOS"

#
# --------------------------------------------------------------------
#

# create a soap request

now="`date -u +%Y-%m-%dT%H:%M:%S`"
id="_`uuidgen | tr -d '-'`"
# note. possible id if you dont have uuidgen: "_a7849c4e7188d592b1a266a34332ffbe"

envelope='<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/" xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol"><S:Body><samlp:AuthnRequest xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol" AssertionConsumerServiceURL="'${acsurl}'" ID="'${id}'" IssueInstant="'${now}'" ProtocolBinding="'${acsurlbinding}'" Version="2.0"><saml:Issuer xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">'${rpid}'</saml:Issuer><samlp:NameIDPolicy AllowCreate="1"/><samlp:Scoping><samlp:IDPList><samlp:IDPEntry ProviderID="'${idpid}'"/></samlp:IDPList></samlp:Scoping></samlp:AuthnRequest></S:Body></S:Envelope>' 


# make the request

URL="https://${idphost}/idp/profile/SAML2/SOAP/ECP"
resp="`curl -k -s -d \"$envelope\" -u \"${userid}:${userpw}\" \"$URL\" `" 

# examine what we got

echo "$resp" > testecp.out

echo "$resp" | grep -q "Fault>"  
[[ $? == 0 ]] && {
   echo "ECP request failed: soap fault!"
   echo "$resp"
   exit 1
}

echo "$resp" | grep -q "Response"  
[[ $? == 0 ]] && {
   echo "ECP request successful!"
   exit 0
}

echo "ECP request failed.  response:"
echo "$resp"
exit 1

