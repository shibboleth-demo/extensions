/*
 * Copyright [2010] [University Corporation for Advanced Internet Development, Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.ecp.config.profile;

import javax.xml.namespace.QName;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.w3c.dom.Element;

import edu.internet2.middleware.shibboleth.idp.config.profile.saml2.SAML2SSOProfileHandlerBeanDefinitionParser;
import edu.internet2.middleware.shibboleth.idp.config.profile.saml2.AbstractSAML2ProfileHandlerBeanDefinitionParser;


import edu.internet2.middleware.shibboleth.idp.ext.ecp.profile.ECPProfileHandler;

/**
 * Spring bean definition parser for {@link ECPProfileHandler} profile handler.
 */
public class ECPProfileHandlerBeanDefinitionParser
        extends AbstractSAML2ProfileHandlerBeanDefinitionParser {
        // extends SAML2SSOProfileHandlerBeanDefinitionParser {

    /** Schema type. */
    public static final QName SCHEMA_TYPE = new QName(ECPProfileHandlerNamespaceHandler.NAMESPACE, "SAML2ECP");

    /** {@inheritDoc} */
    protected Class getBeanClass(Element arg0) {
        return ECPProfileHandler.class;
    }

    /** {@inheritDoc} */
    protected void doParse(Element config, BeanDefinitionBuilder builder) {
        super.doParse(config, builder);
        // see that the handler gets initialized
        builder.setInitMethodName("initialize");
    }
}

