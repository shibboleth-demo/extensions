/*
 * Copyright 2010 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.ecp.config.relyingparty;

import edu.internet2.middleware.shibboleth.common.config.relyingparty.saml.SAML2SSOProfileConfigurationFactoryBean;
import edu.internet2.middleware.shibboleth.idp.ext.ecp.relyingparty.ECPConfiguration;

/** Spring factory for ECP SAML 2 SSO profile configurations. */
public class ECPProfileConfigurationFactoryBean extends SAML2SSOProfileConfigurationFactoryBean {


    /** {@inheritDoc} */
    public Class getObjectType() {
        return ECPConfiguration.class;
    }

    /** {@inheritDoc} */
    protected Object createInstance() throws Exception {
        ECPConfiguration configuration = (ECPConfiguration) getObjectType().newInstance();
        populateBean(configuration);

        // necessary?
        configuration.setIncludeAttributeStatement(includeAttributeStatement());
        configuration.setMaximumSPSessionLifetime(getMaximumSPSessionLifetime());

        return configuration;
    }
}


