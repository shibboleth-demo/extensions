@echo off
setlocal

set ENDORSED=$INSTALL_DIR$/Shib2Idp/lib/endorsed/

REM Grab all the dependencies
if defined CLASSPATH (
  set LOCALCLASSPATH=%CLASSPATH%
)

REM add in the dependency .jar files

for %%i in ("$INSTALL_DIR$/Shib2Idp\Shib1Tools\*.jar") do (
	call "$INSTALL_DIR$/Shib2Idp\bin\cpappend.bat" %%i
)

for %%i in ("$INSTALL_DIR$/Shib2Idp\lib\*.jar") do (
	call "$INSTALL_DIR$/Shib2Idp\bin\cpappend.bat" %%i
)

REM Here we go
java -Djava.endorsed.dirs="%ENDORSED%" -cp "%LOCALCLASSPATH%" edu.internet2.middleware.shibboleth.utils.ExtKeyTool %*
