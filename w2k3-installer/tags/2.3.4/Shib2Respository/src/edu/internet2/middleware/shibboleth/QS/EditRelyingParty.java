/*
 * Copyright [2010] [University Corporation for Advanced Internet Development, Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.internet2.middleware.shibboleth.QS;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.text.ParseException;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

public class EditRelyingParty {

    /**
     * This program's job is to strip testshib out from a generated relying-party
     * and to put in metadata from a specific configuration.
     *
     * @param args
     * @throws IOException 
     * @throws SAXException 
     */
        
    private Document parseFile(String FileName) throws SAXException, IOException
    {
        DOMParser parser = new DOMParser();
        parser.parse(FileName);
        return parser.getDocument();
    }

    private String getAttribute(Node element, String AttrName)
    {
        NamedNodeMap map = element.getAttributes();
        if (null == map) {
            return "";
        }
        Node item = map.getNamedItem(AttrName);
        if (null == item) {
            return "";
        }
        return item.getNodeValue();
    }
    /***
     * Given a metadata provider, strip all the metadata descriptions which are not
     * called IdPMD.  As we go by collect the name of a file used to back the data.
     * 
     * @param metadataProvider what to look through.
     * @throws ParseException
     */
    private void stripNonSelf(Node metadataProvider) throws ParseException
    {
        boolean foundSelfMetadata = false;
        NodeList mpChildren = metadataProvider.getChildNodes();
        Node[] removeChildren = new Node[mpChildren.getLength()];
        for (int j = 0; j < removeChildren.length; j++) {
            removeChildren[j] = null;
        }
        for (int j = 0; j < removeChildren.length; j++) {
            Node mpChild = mpChildren.item(j);
            if (mpChild.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            String mpChildNameSpace = mpChild.getNamespaceURI();
            String mpChildLocalName = mpChild.getLocalName();
                        
            if (mpChildNameSpace == null || mpChildLocalName == null ||
                !mpChildNameSpace.equals("urn:mace:shibboleth:2.0:metadata") ||
                !mpChildLocalName.equals("MetadataProvider")) {
                continue;
            }
            if (getAttribute(mpChild, "id").equals("IdPMD")) {
                foundSelfMetadata = true;
                continue;
            }
            removeChildren[j] = mpChild;
            if (backingFile != null && backingFile.length() != 0) {
                continue;
            }
            backingFile = getAttribute(mpChild, "backingFile");
        }
        if (!foundSelfMetadata) {
            throw new ParseException("Cannot find own metadata", 3);
        }
        if (null == backingFile || 0 == backingFile.length()) {
            throw new ParseException("No backingfile location found", 7);
        }
        for (int j = 0; j < removeChildren.length; j++) {
            if (null != removeChildren[j]) {
                metadataProvider.removeChild(removeChildren[j]);
            }
        }
    }
        
    /***
     * Fidn the now with namespace:localname
     * @param parent
     * @param ns
     * @param ln
     * @param outerType
     * @return
     * @throws ParseException
     */
    private Node findFirstofType(Node parent, String ns, String ln, String outerType) throws ParseException
    {
        //
        // Get hold of the children
        //
        NodeList rpChildren = parent.getChildNodes();
        if (rpChildren == null) {
            throw new ParseException(outerType + " has no children", 0);
        }
                
        //
        // And iterate looking for what we were told to get
        //
        for (int i=0; i< rpChildren.getLength(); i++) {
            Node child = rpChildren.item(i);
            if (child.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            String nameSpace = child.getNamespaceURI();
            String localName = child.getLocalName();
                        
            if (nameSpace == null || localName == null ||
                !nameSpace.equals(ns) ||
                !localName.equals(ln)) {
                continue;
            }
            return child;
        }
        throw new ParseException(outerType + " :no " + ln + " found", 5);
    }
        
    private void outputDom(Document what, String OutFile) throws IOException
    {
        DOMImplementationLS impl;
        LSSerializer serializer;
        LSOutput outputter;             

        impl = (DOMImplementationLS) what.getImplementation().getFeature("LS", "3.0");
        serializer = impl.createLSSerializer();
        Writer writer = new BufferedWriter(new FileWriter(OutFile));
        outputter = impl.createLSOutput();
        outputter.setCharacterStream(writer);
                
        serializer.write(what, outputter);
    }
    
    /***
     * plug the filename into the metadata provider.  Also delete the old one
     * @param newpluginMetadata The MetadataProvider
     */
    private void setBackingFileAndDelete(Node Metadata) {
        //
        // We expect the metadata provider to look like this 
        //
        // <MetadataProvider id="xxx" 
        //            xsi:type="FileBackedHTTPMetadataProvider" 
        //            xmlns="urn:mace:shibboleth:2.0:metadata" 
        //            metadataURL="http://metadata.ukfederation.org.uk/ukfederation-metadata.xml"
        //            backingFile="C:\Program Files\Internet2\Shib2Idp/metadata/UK-metadata.xml">
        //  [...] and then some filter stuff
        //
                
        //
        // First delete the backing store
        //
        File file = new File(backingFile);
        file.delete();
                
        //
        // Now set the backing file 
        //
        Attr attr = (Attr) Metadata.getAttributes().getNamedItem("backingFile");

        attr.setValue(backingFile);
    }


    private void optionallyCreateHtmlFile(Node pluginInformation, String htmlFile) throws IOException
    {
    	//
    	// Grab the params from the config file
    	//
        String saml1Test = getAttribute(pluginInformation, "saml1TestSite");
        String saml2Test = getAttribute(pluginInformation, "saml2TestSite");
        String fedName   = getAttribute(pluginInformation, "fedName");
        
        //
        // Delete the html file - if it exists
        //
        File file = new File(htmlFile);
        file.delete();
        
        if (saml1Test.length() == 0 && saml2Test.length() == 0) {
        	//
        	// don't do anything
        	//
        	return;
        }

        file.createNewFile();
        PrintStream stream = new PrintStream(file);

        stream.println("<HTML>");
        stream.println("<Head><title>Converted to " + fedName + "</title></Head>");
        stream.println("<p>Relying-Party.xml converted to run for " + fedName + "</p>");
        stream.println("<p>Restart tomcat and then try any provided test links</p>");
        
        if (saml2Test.length() > 0) {
        	stream.println("SAML2 test - <a href=\"" + saml2Test + "\">here</a><p>");
        }
        if (saml1Test.length() > 0) {
        	stream.println("SAML1 test - <a href=\"" + saml1Test + "\">here</a><p>");
        }
        stream.close();
	}
        
    private String InFile;
    private String SaveFile;
    private String HtmlFile;
    private String ParamFile;
        
    private String backingFile = null;
        
        
    private EditRelyingParty(String In, String Param, String Html)
    {
        InFile = In;
        SaveFile = In + ".SAV";
        ParamFile = Param;
        HtmlFile = Html;
    }
        
    //
    // Do the work
    //
    private void Execute() throws SAXException, IOException, ParseException
    {
        Document metadata, plugin;
        Node relyingPartyMetadata, relyingPartyGroup, defaultRelyingParty, securityCredential;
        Node pluginInformation, pluginMetadataProvider, pluginTrustEngine;
        String entityID;
        String fedName;
                
        //
        // Grab hold of the metadata file and parameters file
        //
        metadata = parseFile(InFile);
        plugin = parseFile(ParamFile);
                
        //
        // Find the points of interest in the metadata
        //
        relyingPartyGroup = findFirstofType(metadata, "urn:mace:shibboleth:2.0:relying-party", 
                                            "RelyingPartyGroup", "RelyingParty from metadata");
        relyingPartyMetadata = findFirstofType(relyingPartyGroup, "urn:mace:shibboleth:2.0:metadata", 
                                               "MetadataProvider", "Relying Party Group from metadata");
        defaultRelyingParty =  findFirstofType(relyingPartyGroup, "urn:mace:shibboleth:2.0:relying-party", 
                                               "DefaultRelyingParty", "Relying Party Group from metdata");
        securityCredential = findFirstofType(relyingPartyGroup, "urn:mace:shibboleth:2.0:security", 
                                             "Credential", "Relying Party Group from metdata");
        entityID = getAttribute(defaultRelyingParty, "provider");
                
        //
        // Get the metadata ready for the insertion
        //
        System.out.println("\n\nEntity ID = " + entityID + "\n");
        stripNonSelf(relyingPartyMetadata);

        //
        // Now get the info from the plugin file
        //
        pluginInformation = findFirstofType(plugin, "urn:mace:shibboleth:2.0:QSinstall", 
                                            "PluginInformation", "Plugin Information");
        pluginMetadataProvider = findFirstofType(pluginInformation, "urn:mace:shibboleth:2.0:metadata", 
                                                 "MetadataProvider", "Plugin");
        pluginInformation.removeChild(pluginMetadataProvider);
        Node newpluginMetadata = metadata.importNode(pluginMetadataProvider, true);
        setBackingFileAndDelete(newpluginMetadata);
                
        pluginTrustEngine = findFirstofType(pluginInformation, "urn:mace:shibboleth:2.0:security", 
                                            "TrustEngine", "Plugin");
        
        fedName = getAttribute(pluginInformation, "fedName");
        if (fedName.length() == 0) {
        	throw new ParseException("Federation identifier not found in plugin file", 0);
        }
        
        //
        // Get the trustEngine read for insertion
        //

        pluginInformation.removeChild(pluginTrustEngine);
        Node newpluginTrustEngine= metadata.importNode(pluginTrustEngine, true);
                
        //
        // Now wire it in
        //
        relyingPartyMetadata.appendChild(newpluginMetadata);
                
        relyingPartyGroup.insertBefore(newpluginTrustEngine, securityCredential.getNextSibling());
                
        File file = new File(InFile);
        File dest = new File(SaveFile);
        file.renameTo(dest);
        outputDom(metadata, InFile);
        
        optionallyCreateHtmlFile(pluginInformation, HtmlFile);
        
        System.out.println("Relying Party configured for " + fedName);
        
    }

    public static void main(String[] args) {

        EditRelyingParty prog;
                
        if (args.length != 3) {
            System.out.println("Three params - input/output file, config file, HTML file");
            System.exit(1);
        }
                
        prog = new EditRelyingParty(args[0], args[1], args[2]);
                
        try {
            prog.Execute();
                        
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
        
}
package edu.internet2.middleware.shiiboleth.QS;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

public class MainProgram {

	/**
	 * 
	 * @param args
	 * @throws IOException 
	 * @throws SAXException 
	 */
	
	private Document parseFile(String FileName) throws SAXException, IOException
	{
		DOMParser parser = new DOMParser();
		parser.parse(FileName);
		return parser.getDocument();
	}

	private String getAttribute(Node element, String AttrName)
	{
		NamedNodeMap map = element.getAttributes();
		if (null == map) {
			return "";
		}
		Node item = map.getNamedItem(AttrName);
		if (null == item) {
			return "";
		}
		return item.getNodeValue();
	}
	/***
	 * Given a metadata provider, strip all the metadata descriptions which are not
	 * called IdPMD.  As we go by collect the name of a file used to back the data.
	 * 
	 * @param metadataProvider what to look through.
	 * @throws ParseException
	 */
	private void stripNonSelf(Node metadataProvider) throws ParseException
	{
		boolean foundSelfMetadata = false;
		NodeList mpChildren = metadataProvider.getChildNodes();
		Node[] removeChildren = new Node[mpChildren.getLength()];
		for (int j = 0; j < removeChildren.length; j++) {
			removeChildren[j] = null;
		}
		for (int j = 0; j < removeChildren.length; j++) {
			Node mpChild = mpChildren.item(j);
			if (mpChild.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}
			String mpChildNameSpace = mpChild.getNamespaceURI();
			String mpChildLocalName = mpChild.getLocalName();
			
			if (mpChildNameSpace == null || mpChildLocalName == null ||
			    !mpChildNameSpace.equals("urn:mace:shibboleth:2.0:metadata") ||
			    !mpChildLocalName.equals("MetadataProvider")) {
				continue;
			}
			if (getAttribute(mpChild, "id").equals("IdPMD")) {
				foundSelfMetadata = true;
				continue;
			}
			removeChildren[j] = mpChild;
			if (backingFile != null && backingFile.length() != 0) {
				continue;
			}
			backingFile = getAttribute(mpChild, "backingFile");
		}
		if (!foundSelfMetadata) {
			throw new ParseException("Cannot find own metadata", 3);
		}
		if (null == backingFile || 0 == backingFile.length()) {
			throw new ParseException("No backingfile location found", 7);
		}
		for (int j = 0; j < removeChildren.length; j++) {
			if (null != removeChildren[j]) {
				metadataProvider.removeChild(removeChildren[j]);
			}
		}
	}
	
	/***
	 * Fidn the now with namespace:localname
	 * @param parent
	 * @param ns
	 * @param ln
	 * @param outerType
	 * @return
	 * @throws ParseException
	 */
	private Node findFirstofType(Node parent, String ns, String ln, String outerType) throws ParseException
	{
		//
		// Get hold of the children
		//
		NodeList rpChildren = parent.getChildNodes();
		if (rpChildren == null) {
			throw new ParseException(outerType + " has no children", 0);
		}
		
		//
		// And iterate looking for what we were told to get
		//
		for (int i=0; i< rpChildren.getLength(); i++) {
			Node child = rpChildren.item(i);
			if (child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}
			String nameSpace = child.getNamespaceURI();
			String localName = child.getLocalName();
			
			if (nameSpace == null || localName == null ||
			    !nameSpace.equals(ns) ||
			    !localName.equals(ln)) {
				continue;
			}
			return child;
		}
		throw new ParseException(outerType + " :no " + ln + " found", 5);
	}
	
	private Node findTrustEngineInsertPoint(Document relyingParty)
	{
		return null;
	}
	
	private void outputDom(Document what, String OutFile) throws IOException
	{
		DOMImplementationLS impl;
		LSSerializer serializer;
		LSOutput outputter;		

		impl = (DOMImplementationLS) what.getImplementation().getFeature("LS", "3.0");
		serializer = impl.createLSSerializer();
		Writer writer = new BufferedWriter(new FileWriter(OutFile));
		outputter = impl.createLSOutput();
		outputter.setCharacterStream(writer);
		
		serializer.write(what, outputter);
	}
	
	private String InFile;
	private String SaveFile;
	private String OutFile;
	private String ParamFile;
	
	private String backingFile = null;
	
	
	private MainProgram(String In, String Param, String Out)
	{
		InFile = In;
		SaveFile = In + ".SAV";
		ParamFile = Param;
		OutFile = Out;
	}
	
	//
	// Do the work
	//
	private void Execute() throws SAXException, IOException, ParseException
	{
		Document metadata, plugin;
		Node relyingPartyMetadata, relyingPartyGroup, defaultRelyingParty, securityCredential;
		Node pluginInformation, pluginMetadataProvider, pluginTrustEngine;
		String entityID;
		
		//
		// Grab hold of the metadata file and parameters file
		//
		metadata = parseFile(InFile);
		plugin = parseFile(ParamFile);
		outputDom(metadata, SaveFile);
		
		//
		// Find the points of interest in the metadata
		//
		relyingPartyGroup = findFirstofType(metadata, "urn:mace:shibboleth:2.0:relying-party", 
				                            "RelyingPartyGroup", "RelyingParty from metadata");
		relyingPartyMetadata = findFirstofType(relyingPartyGroup, "urn:mace:shibboleth:2.0:metadata", 
				                               "MetadataProvider", "Relying Party Group from metadata");
		defaultRelyingParty =  findFirstofType(relyingPartyGroup, "urn:mace:shibboleth:2.0:relying-party", 
				                               "DefaultRelyingParty", "Relying Party Group from metdata");
		securityCredential = findFirstofType(relyingPartyGroup, "urn:mace:shibboleth:2.0:security", 
				                               "Credential", "Relying Party Group from metdata");
		entityID = getAttribute(defaultRelyingParty, "provider");
		
		//
		// Get the metadata ready for the insertion
		//
		System.out.println("\n\nEntity ID = " + entityID + "\n");
		stripNonSelf(relyingPartyMetadata);

		//
		// Now get the info from the plugin file
		//
		pluginInformation = findFirstofType(plugin, "urn:mace:shibboleth:2.0:QSinstall", 
				                            "PluginInformation", "Plugin Information");
		pluginMetadataProvider = findFirstofType(pluginInformation, "urn:mace:shibboleth:2.0:metadata", 
				                            "MetadataProvider", "Plugin");
		pluginInformation.removeChild(pluginMetadataProvider);
		Node newpluginMetadata = metadata.importNode(pluginMetadataProvider, true);
		setBackingFileAndDelete(newpluginMetadata);
		
		pluginTrustEngine = findFirstofType(pluginInformation, "urn:mace:shibboleth:2.0:security", 
                                            "TrustEngine", "Plugin");

		pluginInformation.removeChild(pluginTrustEngine);
		Node newpluginTrustEngine= metadata.importNode(pluginTrustEngine, true);
		
		//
		// Now wire it in
		//
		relyingPartyMetadata.appendChild(newpluginMetadata);
		
		relyingPartyGroup.insertBefore(newpluginTrustEngine, securityCredential.getNextSibling());
		
		outputDom(metadata, OutFile);
	}
	
	/***
	 * plug the filename into the metadata provider.  Also delete the old one
	 * @param newpluginMetadata The MetadataProvider
	 */
	private void setBackingFileAndDelete(Node Metadata) {
		//
		// We expect the metadata provider to look like this 
		//
		// <MetadataProvider id="xxx" 
		//            xsi:type="FileBackedHTTPMetadataProvider" 
		//            xmlns="urn:mace:shibboleth:2.0:metadata" 
        //            metadataURL="http://metadata.ukfederation.org.uk/ukfederation-metadata.xml"
        //            backingFile="C:\Program Files\Internet2\Shib2Idp/metadata/UK-metadata.xml">
		//  [...] and then some filter stuff
		//
		
		//
		// First delete the backing store
		//
		File file = new File(backingFile);
		file.delete();
		
		//
		// Now check to see what value the backing file currently has
		//
		String type = getAttribute(Metadata, "type");
		Attr attr = (Attr) Metadata.getAttributes().getNamedItem("backingFile");

		attr.setValue(backingFile);
	}

	public static void main(String[] args) {

		MainProgram prog;
		
		if (args.length != 3) {
			System.out.println("Thre params - input file, config file, output file");
			System.exit(1);
		}
		
		prog = new MainProgram(args[0], args[1], args[2]);
		
		
		try {
			prog.Execute();
			
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	
}
