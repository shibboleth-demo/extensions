'
' Code taken from the Shib SP install
'

Function ReadFile( filePath )
   Dim theFile

   'OpenTextFile args: <path>, 1 = ForReading
   'If you read an empty file, VBScript throws an error for some reason
   if (FileSystemObj.FileExists(filePath)) then
     Set theFile = FileSystemObj.GetFile(filePath)
     if (theFile.size > 0) then
       Set theFile = FileSystemObj.OpenTextFile(filePath, 1)
       ReadFile = theFile.ReadAll
     else
       ReadFile = ""
     end if
   else
     ReadFile = ""
   end if
End Function

Sub WriteFile( filePath, contents )
   Dim theFile

   'OpenTextFile args: <path>, 2 = ForWriting, True = create if not exist
   Set theFile = FileSystemObj.OpenTextFile(filePath, 2, True)
   theFile.Write contents
End Sub

Sub ReplaceInFile( filePath, lookForStr, replaceWithStr )
  Dim buffer

  buffer = ReadFile(filePath)
  if (buffer <> "") then
    buffer = Replace(buffer, lookForStr, replaceWithStr)
    WriteFile filePath, buffer
  end if
End Sub


Dim FileSystemObj, ConfigFile, theLog
Dim customData, msiProperties, InstallDir, TypeLib

Set TypeLib = CreateObject("Scriptlet.TypeLib")

on error resume next
Set FileSystemObj = CreateObject("Scripting.FileSystemObject")
  if (Err = 0) then

  'Get the Parameters values via CustomActionData
  customData = Session.Property("CustomActionData")
  msiProperties = split(customData,";@;")
  InstallDir = msiProperties(0)

  'Remove all trailing backslashes to normalize
  do while (mid(InstallDir,Len(InstallDir),1) = "\")
    InstallDir = mid(InstallDir,1,Len(InstallDir)-1)
  loop
  InstallDirJava = Replace(InstallDir, "\", "/")
  InstallDirWindows = Replace(InstallDirJava, "/", "\\")

  IdPHostName = LCase(msiProperties(1))
  IdPDomain = LCase(msiProperties(2))
  IdPScope = LCase(msiProperties(3))
  LdapAddr = msiProperties(4)
  LdapUser = msiProperties(5)
  LdapPassword = msiProperties(6)
  LdapPort = msiProperties(7)
  BrowserPort = msiProperties(8)
  SSOPort = msiProperties(9)
  Version = msiProperties(10)

  idpExpandedDomain = Replace(IdpDomain, ".", ",DC=")
  if (LdapPort <> "3268") then
    ldapSearchPath = "CN=Users,DC=" & idpExpandedDomain
  else 
    ldapSearchPath = "DC=" & idpExpandedDomain
  end if
	
  set theLog = FileSystemObj.OpenTextFile(InstallDir & "\Shib2IdP\logs\EditConfig.log" , 2, True)

  theLog.WriteLine Session.Property("EditConfig")
  theLog.WriteLine customData

  theLog.WriteLine "Install dir " & InstallDir & "(java: " & InstallDirJava & ")"

  configFile = InstallDir & "\CaptiveTomcat 6.0\conf\Server.xml"

  theLog.WriteLine configFile & ": Replace $INSTALL_DIR$ with " & InstallDirJava & "/Shib2IdP"
  ReplaceInFile configFile, "$INSTALL_DIR$", InstallDirJava & "/Shib2IdP"

  theLog.WriteLine configFile & ": Replace $BROWSER_PORT$ with " & BrowserPort
  ReplaceInFile configFile, "$BROWSER_PORT$", BrowserPort

  theLog.WriteLine configFile & ": Replace $SSO_PORT$ with " & SSOPort
  ReplaceInFile configFile, "$SSO_PORT$", SSOPort

  extKeyToolFile = InstallDir & "\Shib2IdP\Shib1Tools\extkeytool.bat"
  theLog.WriteLine extKeyToolFile & ": Replace $INSTALL_DIR$ with " & InstallDirJava 
  ReplaceInFile extKeyToolFile, "$INSTALL_DIR$", InstallDirJava

  deployfile = InstallDir & "\CaptiveTomcat 6.0\conf\Catalina\localhost\idp.xml"

  theLog.WriteLine deployfile & " : Replace $INSTALL_DIR$ with " & InstallDirJava & "/Shib2IdP"
  ReplaceInFile deployfile, "$INSTALL_DIR$", InstallDirJava & "/Shib2IdP"
  theLog.WriteLine "Random comment"

  '
  ' If a port is 443 then its not specified
  '
  if (BrowserPort <> "443") then
    BrowserPort = ":" & BrowserPort
  else
    BrowserPort = ""
  end if

  if (SSOPort <> "443") then
    SSOPort = ":" & SSOPort
  else
    SSOPort = ""
  end if

  '
  ' And now create the properties file
  '
  installFileName = InstallDir & "\shib2IdpInstall\src\installer\resources\install.properties"
  theLog.WriteLine "comment Random"

  theLog.WriteLine "Writing installation stuff to " & installFileName
  theLog.WriteLine "Also Writing installation stuff to " & InstallDir & "\Shib2IdpInstall\src\installer\resources\install.properties"

  theLog.WriteLine "Random comment2"

  set theInstallFile = FileSystemObj.OpenTextFile(installFileName , 2, True)

  theLog.WriteLine "Random comment4"

  theLog.WriteLine "@" & theInstallFile

  theInstallFile.WriteLine "# Installation at " & Now
  theInstallFile.WriteLine "idp.home=" & InstallDirJava & "/Shib2Idp"
  theInstallFile.WriteLine "idp.hostname=" & IdPHostName
  theInstallFile.WriteLine "install.dir=" & InstallDirJava
  theInstallFile.WriteLine "install.dir.windows=" & InstallDirWindows
  theInstallFile.WriteLine "idp.keystore.pass=SeCrEt" 
  theInstallFile.WriteLine "idp.scope=" & IdPScope
  theInstallFile.WriteLine "idp.domain=" & IdPDomain
  theInstallFile.WriteLine "ldap.search.path=" & ldapSearchPath
  theInstallFile.WriteLine "ldap.addr=" & LdapAddr
  theInstallFile.WriteLine "ldap.user=" & LdapUser
  theInstallFile.WriteLine "ldap.password=" &LdapPassword
  theInstallFile.WriteLine "ldap.port=" &LdapPort
  theInstallFile.WriteLine "browser.port=" &BrowserPort
  theInstallFile.WriteLine "sso.port=" &SSOPort
  guid = left(TypeLib.Guid, 38)
  theInstallFile.WriteLine "random=" & guid
  theInstallFile.WriteLine "now=" & Now
  theInstallFile.WriteLine "version=" & Version

  theLog.WriteLine "Replace: Done!?!"
  theLog.close

End If