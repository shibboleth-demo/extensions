package edu.internet2.middleware.shibboleth.quickInstallIdp;
/*
 *  Copyright 2007 Internet2
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.kerberos.KerberosPrincipal;
import javax.security.auth.login.LoginException;

import org.apache.catalina.realm.GenericPrincipal;

import com.sun.security.auth.module.Krb5LoginModule;

/**
 * This is a 'waffer thin' shim on top of Sun's kerberos module to allow us to declare that 
 * everyone who gets authorized has role "shib-use", thus making the config of Shib's web.xml easy.
 * 
 * TODO: This might be extended to do a look and if the user is memberOf="CN=Administrators" assert
 * that they are manager as well.
 * 
 * TODO: Probably should be recast to use encapsulation?
 * 
 * Based on code found at http://wiki.wsmoak.net/.  Thanks to Wendy Smoak.
 */
public class QuickIdPLoginModule extends Krb5LoginModule {

    private Subject subject;
    
    public void initialize( Subject subject, 
                            CallbackHandler callbackHandler, 
                            Map<String, ?> sharedState, 
                            Map<String, ?> options ) {

        super.initialize(subject, callbackHandler, sharedState, options);
        this.subject = subject;
    }
    
    public boolean commit() throws LoginException {

        boolean answer = super.commit();
        KerberosPrincipal kp;

        if (subject !=null) {
            
            Object[] array = subject.getPrincipals().toArray();
            if( array.length > 0 && array[0] instanceof KerberosPrincipal ) {
               kp = (KerberosPrincipal) array[0];
            } else {
               return false;
            }

            List<String> roles = new ArrayList<String>();
            //
            // In theory we should be able to null out the current principal and replace it with
            // a new GenericPrincipal one with the name taken from the KerberosPrincipal and the list of
            // roles.  Trouble is that Tomcat doesn't seem to want to recognise this.  So we just add
            // a new Principal for each of the roles and set up Tomcat to look for the KerberosPrincipal 
            // for the username and the GenericPrinical for the roles.
            //

            GenericPrincipal gp = new GenericPrincipal(null, "idpuser", null, roles);
            subject.getPrincipals().add(  gp );

            //
            // TODO:  This is were we would add "manager" if we wanted
            //
            //gp = new GenericPrincipal(null, "manager", null, roles);
            //subject.getPrincipals().add(  gp );
            
            

        }
        return answer;
    }
}
