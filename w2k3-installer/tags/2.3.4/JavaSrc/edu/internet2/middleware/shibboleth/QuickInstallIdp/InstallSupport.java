package edu.internet2.middleware.shibboleth.quickInstallIdp;

import jargs.gnu.CmdLineParser;
import jargs.gnu.CmdLineParser.Option;

import java.io.File;

public class InstallSupport {

    protected static String DEFAULT_IN_PATH_TRAILER = File.separator + "etc" + File.separator + "idp.xml";
    protected static String DEFAULT_OUT_PATH_TRAILER = File.separator + "etc" + File.separator + "idp.xml";
    protected static String DEFAULT_ARP_PATH_TRAILER = File.separator + "etc" + File.separator + "arps" + File.separator;
    protected static String DEFAULT_ERRLOG_TRAILER = File.separator + "logs" + File.separator + "shib-error.log";
    protected static String DEFAULT_LOG_TRAILER = File.separator + "logs" + File.separator + "certgen.log";
    protected static String DEFAULT_TXLOG_TRAILER = File.separator + "logs" + File.separator + "shib-access.log";
    protected static String DEFAULT_KEY_TRAILER = File.separator + "etc" + File.separator + "Idp.key";
    protected static String DEFAULT_JKS_TRAILER = File.separator + "etc" + File.separator + "Idp.jks";
    protected static String DEFAULT_CERT_TRAILER = File.separator + "etc" + File.separator + "Idp.crt";
    protected static String DEFAULT_SALT_TRAILER = File.separator + "etc" + File.separator + "persistent.jks";
    protected static String DEFAULT_RESOLVER_TRAILER = File.separator + "etc" + File.separator + "Resolver.xml";
    protected static String DEFAULT_METADATA_TRAILER = File.separator + "etc" + File.separator + "Metadata.xml";
    protected static String DEFAULT_ALIAS = "tomcat";
    protected static String DEFAULT_RELYING_PARTY = "http://ukfederation.org.uk";
 
    /**
     * Give the defaults and the command line, work out what the values should be.
     * @param idpHome The home directory.
     * @param prefix  The Default trailer portion of the name
     * @param parser The Command line
     * @param option The option we are looking at
     * @return The provided value (if present) or the 
     */
    private static String processArg(String prefix, String idpHome, String defValue, CmdLineParser parser, Option option) {
        
        String val = (String) parser.getOptionValue(option);
        
        if (null != val) {
            return val;
        }
        
    return  prefix + idpHome + defValue;
    }

    protected static String processUrlArg(String idpHome, String defValue, CmdLineParser parser, Option option) {

        return processArg("file:///", idpHome, defValue, parser, option);
    }

    protected static String processFileArg(String idpHome, String def, CmdLineParser parser, Option option) {
        return processArg("", idpHome, def, parser, option);
    }
    
    protected static String processArg(String defValue, CmdLineParser parser, Option option) {
        
        String val = (String) parser.getOptionValue(option);
        
        if (null != val) {
            return val;
        }
        
    return defValue;
    }

 
}
