'
' Code taken from the Shib SP install
'

Function ReadFile( filePath )
   Dim theFile

   'OpenTextFile args: <path>, 1 = ForReading
   'If you read an empty file, VBScript throws an error for some reason
   if (FileSystemObj.FileExists(filePath)) then
     Set theFile = FileSystemObj.GetFile(filePath)
     if (theFile.size > 0) then
       Set theFile = FileSystemObj.OpenTextFile(filePath, 1)
       ReadFile = theFile.ReadAll
     else
       ReadFile = ""
     end if
   else
     ReadFile = ""
   end if
   theFile.close()
End Function

Sub WriteFile( filePath, contents )
   Dim theFile

   'OpenTextFile args: <path>, 2 = ForWriting, True = create if not exist
   Set theFile = FileSystemObj.OpenTextFile(filePath, 2, True)
   theFile.Write contents
End Sub

Sub ReplaceInFileWithFile( filePath, lookForStr, replaceWithFile )
  Dim buffer
 
  repstr = ReadFile(replaceWithFile)

  if (repstr <> "") then 
    repstr = Replace(repstr, "-----BEGIN CERTIFICATE-----", "")
    repstr = Replace(repstr, "-----END CERTIFICATE-----", "")
  end if

  buffer = ReadFile(filePath)
 
  if (buffer <> "") then
    buffer = Replace(buffer, lookForStr, repstr)
    WriteFile filePath, buffer
  end if
End Sub


Dim FileSystemObj, ConfigFile, theLog
Dim customData, msiProperties, InstallDir

on error resume next
Set FileSystemObj = CreateObject("Scripting.FileSystemObject")
  if (Err = 0) then

  'Get the INSTALLDIR and IDP_NAME values via CustomActionData
  customData = Session.Property("CustomActionData")
  msiProperties = split(customData,";@;")
  InstallDir = msiProperties(0)

  set theLog = FileSystemObj.OpenTextFile(InstallDir & "\Idp\logs\EditMetadata.log" , 2, True)

  theLog.WriteLine "Install dir " & InstallDir 
  theLog.WriteLine "Replace in " & InstallDir & "IdP\etc\metadata-segment.xml : $$CERT$$ with the contents of " & InstallDir & "IdP\Etc\idp.crt"

  ReplaceInFileWithFile InstallDir & "IdP\etc\metadata-segment.xml", "$$CERT$$", InstallDir & "IdP\etc\idp.crt"

  theLog.WriteLine "Replace: Done"
  theLog.close

End If