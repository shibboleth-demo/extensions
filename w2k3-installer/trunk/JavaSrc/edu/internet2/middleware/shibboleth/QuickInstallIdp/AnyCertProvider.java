/*
 *  Copyright 2007 Internet2
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.quickInstallIdp;

import java.security.KeyStore;
import java.security.Provider;
import java.security.cert.X509Certificate;

import javax.net.ssl.ManagerFactoryParameters;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactorySpi;
import javax.net.ssl.X509TrustManager;

/**
 * Security provider that validates any cert (app must provide real validation)
 * Provides "AnyCert" algorithm.  This is taregtted at Tomcat implementations 
 * where we want to have an IDPAA port (which is SSL) but will check the keys inside Shib.
 */

public class AnyCertProvider extends Provider {

    private static final long serialVersionUID = -4785062147348874084L;

    public AnyCertProvider() {
      super( "AnyCertProvider", 1.0, "Trust any certificate");
      put( "TrustManagerFactory.AnyCert", AnyCertTrustManagerFactory.class.getName() );
 
   }

   protected static class AnyCertTrustManagerFactory extends TrustManagerFactorySpi {
      public AnyCertTrustManagerFactory() {}
      protected void engineInit(KeyStore keystore) {}
      protected void engineInit(ManagerFactoryParameters params) {}
      protected TrustManager[] engineGetTrustManagers() {
          return new TrustManager[] {
            new AnyCertX509TrustManager()
         };
      }

      protected TrustManager[] getTrustManagers() {
         return new TrustManager[] {
            new AnyCertX509TrustManager()
         };
      }
   }

   protected static class AnyCertX509TrustManager implements X509TrustManager {
      public void checkClientTrusted(X509Certificate[] certs, String auth) {
      }
      public void checkServerTrusted(X509Certificate[] certs, String auth) {
      }
      public X509Certificate[] getAcceptedIssuers() {
         return (new X509Certificate[]{});
      }
   }
}
