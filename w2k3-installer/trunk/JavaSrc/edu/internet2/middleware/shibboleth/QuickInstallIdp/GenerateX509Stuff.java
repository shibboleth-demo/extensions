/*
 *  Copyright 2007 Internet2
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.quickInstallIdp;

import jargs.gnu.CmdLineParser;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.asn1.x509.X509Name;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMWriter;
import org.bouncycastle.x509.X509V3CertificateGenerator;

public class GenerateX509Stuff extends InstallSupport {

    static PrintStream log = System.err;

    static final int YEARS_LIFE = 10;

    /**
     * @param args
     * @throws NoSuchProviderException 
     * @throws NoSuchAlgorithmException 
     * @throws IOException 
     * @throws SignatureException 
     * @throws IllegalStateException 
     * @throws InvalidKeyException 
     * @throws CertificateException 
     * @throws KeyStoreException 
     */
    public static void main(String[] args) {
                
        
        try {
            //
            // Make Bouncy Castle provider available
            //
            Security.addProvider(new BouncyCastleProvider());
            
            CmdLineParser parser = new CmdLineParser();
            CmdLineParser.Option keyOption = parser.addStringOption('k', "keyfile");
            CmdLineParser.Option certOption = parser.addStringOption('o', "certfile");
            CmdLineParser.Option saltOption = parser.addStringOption('a', "saltfile");
            CmdLineParser.Option idpHomeOption = parser.addStringOption('h', "home");
            CmdLineParser.Option subject = parser.addStringOption('s', "subjectCN");
            CmdLineParser.Option jksOption = parser.addStringOption('j', "jksfile");
            CmdLineParser.Option aliasOption = parser.addStringOption('a', "alias");
            CmdLineParser.Option passwordOption = parser.addStringOption('p', "password");
            CmdLineParser.Option logOption = parser.addStringOption('l', "log");
            
            try {
                parser.parse(args);
            } catch (CmdLineParser.OptionException e) {
                log.println(e.getMessage());
                try {
                    Thread.sleep(100); // silliness to get error to print first
                } catch (InterruptedException ie) {
                    // doesn't matter
                }
                printUsage(log);
                System.exit(-1);
            }
            
            //
            // Get the required values
            //
           String idpHome = (String) parser.getOptionValue(idpHomeOption);
           String subjectName = (String) parser.getOptionValue(subject); 
           String password = (String) parser.getOptionValue(passwordOption);
    
            //
            // Were they provided?
            //
            
            if (null == idpHome || null == subjectName || null == password) {
                printUsage(log);
                System.exit(-1);            
            }
            
            //
            // Srip trailing slash from idphome
            //
            if (idpHome.charAt(idpHome.length()-1) == File.separatorChar) {
                idpHome = idpHome.substring(0, idpHome.length()-1);
            }
    
            //
            // setup log
            //
            String logName = "<UNKNOWN>";
            try {
                logName = InstallSupport.processFileArg(idpHome, DEFAULT_LOG_TRAILER, parser, logOption);
                log = new PrintStream(logName);
            } catch (Exception e) {
                log.println("Couldn't open log " + logName);
                System.exit(-1);
            }
    
            String keyFile = InstallSupport.processFileArg(idpHome, DEFAULT_KEY_TRAILER, parser, keyOption);
            String jksFile = InstallSupport.processFileArg(idpHome, DEFAULT_JKS_TRAILER, parser, jksOption);
            String certFile = InstallSupport.processFileArg(idpHome, DEFAULT_CERT_TRAILER, parser, certOption);
            String alias = InstallSupport.processArg(DEFAULT_ALIAS, parser, aliasOption);
            String saltFile = InstallSupport.processFileArg(idpHome, DEFAULT_SALT_TRAILER, parser, saltOption);
    
            log.println("Key File - " + keyFile);
            log.println("Jks File - " + jksFile);
            log.println("Cert File - " + certFile);
            log.println("Salt File - " + saltFile);
            log.println("Alias - " + alias);
            //
            // So get the keys
            //
            
            KeyPair keys = getKeyPair();
            
            //
            // Now, get the cert
            //
            String issuerAndSubject = "O=Internet2QI, OU=InternetQuickInstall, CN=" + subjectName;
            
            Certificate cert = getCert(issuerAndSubject, issuerAndSubject, keys);
            
            //
            // Emit the keyfile
            //
    
            printKey(new FileOutputStream(keyFile), keys.getPrivate());
            
            //
            // Emit the certfile
            //
            
            printCert(new FileOutputStream(certFile), cert);
            
            //
            // Emit the JKS file
            //
            printJks(new FileOutputStream(jksFile), cert, keys.getPrivate(), alias, password.toCharArray());
            
            //
            // Emit the salt
            //
            genSalt(password, saltFile);
            
        } catch (Exception e) {
            log.println("Failed");
            log.println(e);
        }
           
    }
    
    /**
     * Generate a 2048-bit RSA key pair
     * @return The keypair
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    private static KeyPair getKeyPair() throws NoSuchAlgorithmException, NoSuchProviderException {

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", "BC");
        keyGen.initialize(2048);
        KeyPair keys = keyGen.generateKeyPair();
        return keys;
    }
    
    /**
     * Get the encoded private key in PEM format.
     * @param stream The output stream
     * @param key The key
     * @throws IOException
     */
    private static void printKey(OutputStream stream, PrivateKey key) throws IOException {       
        
        // 
        PEMWriter pWrt = new PEMWriter(new OutputStreamWriter(stream));
        pWrt.writeObject(key);
        pWrt.close();        
    }

    /**
     * create the certificate - version 3 - with extensions.
     * @param issuer
     * @param subject
     * @param keys
     * @return
     * @throws IOException
     * @throws CertificateEncodingException
     * @throws InvalidKeyException
     * @throws IllegalStateException
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     * @throws SignatureException
     */
    private static X509Certificate getCert(String issuer, String subject, KeyPair keys) throws IOException, CertificateEncodingException, InvalidKeyException, IllegalStateException, NoSuchProviderException, NoSuchAlgorithmException, SignatureException {

        X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
        certGen.setSerialNumber(BigInteger.valueOf(1));
        certGen.setIssuerDN(new X509Principal(issuer));
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MINUTE, -5);
        certGen.setNotBefore(cal.getTime());
        cal.add(Calendar.YEAR, YEARS_LIFE);
        certGen.setNotAfter(cal.getTime());
        certGen.setSubjectDN(new X509Principal(subject));
        certGen.setPublicKey(keys.getPublic());
        
        certGen.setSignatureAlgorithm("SHA1WithRSAEncryption");
        
        SubjectPublicKeyInfo spki = new SubjectPublicKeyInfo((ASN1Sequence) new ASN1InputStream(
                new ByteArrayInputStream(keys.getPublic().getEncoded())).readObject());
        SubjectKeyIdentifier ski = new SubjectKeyIdentifier(spki);
        certGen.addExtension(X509Extensions.SubjectKeyIdentifier.getId(), false, ski);
        
        AuthorityKeyIdentifier aki = new AuthorityKeyIdentifier(spki,
                new GeneralNames(new GeneralName(new X509Name(issuer))),
                BigInteger.valueOf(1)
                );
        certGen.addExtension(X509Extensions.AuthorityKeyIdentifier.getId(), false, aki);
        
        BasicConstraints bc = new BasicConstraints(true);
        certGen.addExtension(X509Extensions.BasicConstraints.getId(), false, bc);
        
        X509Certificate cert = certGen.generate(keys.getPrivate(), "BC");
        
        return cert;
    }

    /**
     * Output a certificate to a PEM file.
     * @param stream
     * @param cert
     * @throws IOException
     */
    private static void printCert(OutputStream stream, Certificate cert) throws IOException {
        PEMWriter pWrt = new PEMWriter(new OutputStreamWriter(stream));
        pWrt.writeObject(cert);
        pWrt.close();     
    }
    
    private static void printJks(OutputStream stream, Certificate cert, PrivateKey key, String alias, char[] password) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException
    {
        KeyStore ks = KeyStore.getInstance("JKS");
        ks.load(null, password);
        Certificate chain[] = {cert};
        
        ks.setKeyEntry(alias, key, password, chain);
        
        ks.store(stream, password);
    }
    
    private static void genSalt(String Password, String file) throws NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException {
        KeyGenerator gen = KeyGenerator.getInstance("DESede");
        SecretKey secret = gen.generateKey();
        File keyStorePath = new File(file);
        
        KeyStore keyStore = KeyStore.getInstance("JCEKS");
        if (keyStorePath.exists()) {
            FileInputStream fis = new FileInputStream(keyStorePath);
            keyStore.load(fis, Password.toCharArray());
        } else {
            keyStore.load(null, Password.toCharArray());
        }
        keyStore.setKeyEntry("handleKey", secret, Password.toCharArray(), null);
        keyStore.store(new FileOutputStream(keyStorePath), Password.toCharArray());

    }
    
    private static void printUsage(PrintStream out) {

        out.println("usage: ");
        out.println();
        out.println("-h, --home (Required) IdP Home directory");
        out.println("-p, --password (Required) JKS & key password");
        out.println("-s, --subjectCN (Required) CN for the certificate e.g. example.org");
        out.println("-k, --keyfile");
        out.println("-c, --certfile");
        out.println("-j, --jksfile");
        out.println("-a, --alias");
    }
}
