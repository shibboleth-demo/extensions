'
' Code taken from the Shib SP install
'

Function ReadFile( filePath )
   Dim theFile

   'OpenTextFile args: <path>, 1 = ForReading
   'If you read an empty file, VBScript throws an error for some reason
   if (FileSystemObj.FileExists(filePath)) then
     Set theFile = FileSystemObj.GetFile(filePath)
     if (theFile.size > 0) then
       Set theFile = FileSystemObj.OpenTextFile(filePath, 1)
       ReadFile = theFile.ReadAll
     else
       ReadFile = ""
     end if
   else
     ReadFile = ""
   end if
End Function

Sub WriteFile( filePath, contents )
   Dim theFile

   'OpenTextFile args: <path>, 2 = ForWriting, True = create if not exist
   Set theFile = FileSystemObj.OpenTextFile(filePath, 2, True)
   theFile.Write contents
End Sub

Sub ReplaceInFile( filePath, lookForStr, replaceWithStr )
  Dim buffer

  buffer = ReadFile(filePath)
  if (buffer <> "") then
    buffer = Replace(buffer, lookForStr, replaceWithStr)
    WriteFile filePath, buffer
  end if
End Sub


Dim FileSystemObj, ConfigFile, theLog
Dim customData, msiProperties, InstallDir, TypeLib

Set TypeLib = CreateObject("Scriptlet.TypeLib")

on error resume next
Set FileSystemObj = CreateObject("Scripting.FileSystemObject")
  if (Err = 0) then

  'Get the INSTALLDIR and IDP_NAME values via CustomActionData
  customData = Session.Property("CustomActionData")
  msiProperties = split(customData,";@;")
  InstallDir = msiProperties(0)
  AdDomainUC = msiProperties(1)
  AdDomain = LCase(AdDomainUC)
  LDAPAddr = msiProperties(2)
  IdPScope = msiProperties(3)
  IdPAddr  = msiProperties(4)

  idpHome = InstallDir & "\Idp"
  CName = Replace(AdDomain, ".", ",DC=")
	


   set theLog = FileSystemObj.OpenTextFile(InstallDir & "\Idp\logs\EditConfig.log" , 2, True)

   theLog.WriteLine Session.Property("EditConfig")
   theLog.WriteLine customData

  theLog.WriteLine "Install dir " & InstallDir & "(java: " & InstallDirJava & ")"

  theLog.WriteLine "NAME = " & AdDomainUC & " name = " & AdDomain


  'Remove all trailing backslashes to normalize
  do while (mid(InstallDir,Len(InstallDir),1) = "\")
    InstallDir = mid(InstallDir,1,Len(InstallDir)-1)
  loop
  InstallDirJava = Replace(InstallDir, "\", "/")

  configFile = InstallDir & "\CaptiveTomcat5.5\conf\Server.xml"

  theLog.WriteLine configFile & ": Replace $IDP_HOME with " & idpHome
  ReplaceInFile configFile, "$IDP_HOME$", idpHome

  theLog.WriteLine InstallDir & "\IdPInstall\build.properties : Replace $IDP_HOME with " & InstallDirJava & "/idp"
  ReplaceInFile InstallDir & "\IdPInstall\build.properties", "$IDP_HOME$", InstallDirJava & "/idp"

  theLog.WriteLine InstallDir & "\IdPInstall\build.properties : Replace $TOMCAT_HOME$ with " & InstallDirJava & "/CaptiveTomcat5.5"
  ReplaceInFile InstallDir & "\IdPInstall\build.properties", "$TOMCAT_HOME$", InstallDirJava & "/CaptiveTomcat5.5"

  theLog.WriteLine InstallDir & "\IdPInstall\src\conf\resolver.xml : Replace $IDP_HOME with " & idpHome
  ReplaceInFile InstallDir & "\IdPInstall\src\conf\resolver.xml", "$IDP_HOME$", idpHome

  theLog.WriteLine InstallDir & "\IdPInstall\src\conf\resolver.xml : Replace $IDP_SCOPE with " & IdPScope
  ReplaceInFile InstallDir & "\IdPInstall\src\conf\resolver.xml", "$IDP_SCOPE$", IdpScope

  guid = left(TypeLib.Guid, 38)

  theLog.WriteLine InstallDir & "\IdPInstall\src\conf\resolver.xml : Replace $RANDOM with " & guid
  ReplaceInFile InstallDir & "\IdPInstall\src\conf\resolver.xml", "$RANDOM$", guid

  theLog.WriteLine InstallDir & "\IdPInstall\src\conf\resolver.xml : Replace $IDP_NAME with " & AdDomain
  ReplaceInFile InstallDir & "\IdPInstall\src\conf\resolver.xml", "$IDP_NAME$", AdDomain
  
  theLog.WriteLine InstallDir & "\IdPInstall\src\conf\resolver.xml : Replace $LDAP_ADDR with " & LDAPAddr
  ReplaceInFile InstallDir & "\IdPInstall\src\conf\resolver.xml", "$LDAP_ADDR$", LDAPAddr
  
  theLog.WriteLine InstallDir & "\IdPInstall\src\conf\resolver.xml : Replace $IDP_EXPANDED_NAME with " & CName
  ReplaceInFile InstallDir & "\IdPInstall\src\conf\resolver.xml", "$IDP_EXPANDED_NAME$", CName

  theLog.WriteLine InstallDir & "\IdPInstall\src\conf\dist.idp.xml : Replace $IDP_NAME with " & AdDomain
  ReplaceInFile InstallDir & "\IdPInstall\src\conf\dist.idp.xml", "$IDP_NAME$", AdDomain

  theLog.WriteLine InstallDir & "\IdPInstall\src\conf\dist.idp.xml : Replace $IDP_ADDR with " & IdpAddr
  ReplaceInFile InstallDir & "\IdPInstall\src\conf\dist.idp.xml", "$IDP_ADDR$", IdpAddr

  theLog.WriteLine InstallDir & "\IdPInstall\WebApplication\login.jsp : Replace $IDP_NAME$ with " & AdDomain
  ReplaceInFile InstallDir & "\IdPInstall\WebApplication\login.jsp", "$IDP_NAME$", AdDomain

  theLog.WriteLine InstallDir & "\ReadMe.html : Replace $IDP_NAME$ with " & AdDomain
  ReplaceInFile InstallDir & "\ReadMe.html", "$IDP_NAME$", AdDomain

  theLog.WriteLine InstallDir & "\ReadMe.html : Replace $IDP_ADDR$ with " & IdpAddr
  ReplaceInFile InstallDir & "\ReadMe.html", "$IDP_ADDR$", IdpAddr

  theLog.WriteLine InstallDir & "\ReadMe.html : Replace $HOME$ with " & InstallDir
  ReplaceInFile InstallDir & "\ReadMe.html", "$HOME$", InstallDir
  
  theLog.WriteLine InstallDir & "\IdP\etc\metadata-segment.xml : Replace $IDP_NAME$ with " & AdDomain
  ReplaceInFile InstallDir & "\IdP\etc\metadata-segment.xml", "$IDP_NAME$", AdDomain

  theLog.WriteLine InstallDir & "\IdP\etc\metadata-segment.xml : Replace $IDP_SCOPE$ with " & IdPScope
  ReplaceInFile InstallDir & "\IdP\etc\metadata-segment.xml", "$IDP_SCOPE$", IdPScope

  theLog.WriteLine InstallDir & "\IdP\etc\metadata-segment.xml : Replace $IDP_ADDR$ with " & IdPAddr
  ReplaceInFile InstallDir & "\IdP\etc\metadata-segment.xml", "$IDP_ADDR$", IdPAddr

  if (FileSystemObj.FileExists(InstallDir & "\IdPInstall\bin\md.bat")) then
      theLog.WriteLine  InstallDir & "\IdPInstall\bin\md.bat replace $IDP_HOME$ with" & idpHome
      ReplaceInFile InstallDir & "\IdPInstall\bin\md.bat", "$IDP_HOME$", idpHome
      theLog.WriteLine  InstallDir & "\IdPInstall\bin\md.bat replace $JAVA_HOME$ with" & InstallDir & "\CaptiveJava"
      ReplaceInFile InstallDir & "\IdPInstall\bin\md.bat", "$JAVA_HOME$", InstallDir & "\CaptiveJava"
  end if

  theLog.WriteLine "Replace: Done"
  theLog.close

End If