<html xmlns="http://www.w3.org/1999/xhtml" xmlns:ic="http://schemas.xmlsoap.org/ws/2005/05/identity">
 <head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <!-- <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/infocard.css"/> -->
     <title>Information card builder - card type selections</title>
 </head>

 <body>
<tr>
  <td><img src="<%= request.getContextPath() %>/images/UWlogo.gif" align="bottom"/></td>
  <td width="20"></td>
  <td valign="bottom"><h2 style="font-family: serif; color:#808080">
        Information card builder</h2></td>
  <td width="20"></td>
</tr>
</table>

<table class="banner" border="0" width="100%">
<tr>
  <td align="center" class="bannernote"></td>
</tr>
</table>

<p/>

<hr>

  <p/>
   <h3>Choose the type of card you want to create:</h3>
   <p/>
     <ul>
  <% if(request.getAttribute("passwordAction") != null){ %>
      <li>An card protected by id and password requires you to enter your UWNetID and password whenever you use it.
        <p/>
       <form action="<%=request.getAttribute("passwordAction")%>" method="post">
       <input type="hidden" name="cardtype" value="password"/>
       Card name: <input type="text" name="cardname" value="UW InfoCard: pw"/>
       <input type="submit" value="Get it" /></td>
       </form>
      </li>
  <% } %>
    <p/>
    <hr width="100" align="left"/>
    <p/>

  <% if(request.getAttribute("personalAction") != null){ %>
       <li>An card protected by a personal card requires you to select a personal card whenever you use it.
        <p/> 
       <form action="<%=request.getAttribute("personalAction")%>" method="post">
<!-- object method
          <object type="application/x-informationCard" name="xmlToken"
           <param name="tokenType" value="urn:oasis:names:tc:SAML:1.0:assertion">
           <param name="requiredClaims" value="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/privatepersonalidentifier"/>
         </object>
  -->
<!-- xhtml method -->
          <ic:informationCard name="xmlToken" style="behavior:url(#default#informationCard)" tokenType="urn:oasis:names:tc:SAML:1.0:assertion">
           <ic:add claimType="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/privatepersonalidentifier" optional="false"/>
         </ic:informationCard>
<!-- -->
       <input type="hidden" name="cardtype" value="personal"/> 
       Card name: <input type="text" name="cardname" value="UW InfoCard: pc"/>
       <input type="submit" value="Get it" /></td>
       </form>
        </li>
  <% } %>
  </ul>
  <p/>
 </body>
</html>

