January 12, 2009
Version 1.0

This extension adds information card support
to a Shibboleth 2.1 Identity Provider.

----------------------------------------------------------------


Build and install:

(( see the wiki page - https://spaces.internet2.edu/x/fRw ))

-----------------------------------------------------------

Roughly:

[ need step 0 only if you want to interoperate with version 1 of microsoft cardspace (only version) ]

0) Get the modified xml-security-4.1

   run your java with "-Dorg.apache.xml.security.ignoreLineBreaks=true"


1) compile and install  (mvn install)

   ant 

3) copy jar to idp

   cp target/infocard-1.1.jar $idpsrc/lib


3) copy webapp files to idp src

   cp resources/webapp/cardtype.jsp $idpsrc/src/main/webapp
   cp resources/webapp/infocard.css $idpsrc/src/main/webapp
   cp resources/webapp/images/UWlogo.gif $idpsrc/src/main/webapp/images




Configure:

1) handler.xml

   Add a namespace and a schema location.

      <ProfileHandlerGroup xmlns="urn:mace:shibboleth:2.0:idp:profile-handler"
           xmlns:uw="urn:mace:shibboleth:2.0:idp:uw"
           xmlns:icard="urn:mace:shibboleth:2.0:idp:infocard"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:schemaLocation="urn:mace:shibboleth:2.0:idp:profile-handler classpath:/schema/shibboleth-2.0-idp-profile-handler.xsd
           urn:mace:shibboleth:2.0:idp:infocard classpath:/edu/internet2/middleware/shibboleth/infocard/schema/infocard.xsd ">

      
   Add profile handler path definitions (e.g.)

<!-- Infocard handlers -->

    <ProfileHandler xsi:type="icard:InfocardStatus">
        <RequestPath>/infocard/status</RequestPath>
    </ProfileHandler>
    <ProfileHandler xsi:type="icard:InfocardCard" relyingParty="urn:mace:shibboleth:2.0:infocard">
      <RequestPath>/infocard/card</RequestPath>
    </ProfileHandler>
    <ProfileHandler xsi:type="icard:InfocardMex" relyingParty="urn:mace:shibboleth:2.0:infocard">
        <RequestPath>/infocard/mex</RequestPath>
    </ProfileHandler>
    <ProfileHandler xsi:type="icard:InfocardMex" relyingParty="urn:mace:shibboleth:2.0:infocard">
        <RequestPath>/infocard/mex/pw</RequestPath>
    </ProfileHandler>
    <ProfileHandler xsi:type="icard:InfocardMex" relyingParty="urn:mace:shibboleth:2.0:infocard">
        <RequestPath>/infocard/mex/pc</RequestPath>
    </ProfileHandler>
    <ProfileHandler xsi:type="icard:InfocardSTS" relyingParty="urn:mace:shibboleth:2.0:infocard">
        <RequestPath>/infocard/sts</RequestPath>
    </ProfileHandler>



2) relying-party.xml

   The infocard handlers need signing credentials.

   Add namespace and schema as with handler.xml.  This time it is
   
       xmlns:icard="urn:mace:shibboleth:2.0:idp:infocard-rp"

       and 

       urn:mace:shibboleth:2.0:idp:infocard-rp classpath:/edu/internet2/middleware/shibboleth/infocard/schema/infocard-rp.xsd



   Add a relying party section for infocard (e.g.)


--- relying-party.xml -----------------------------------------------------

    <RelyingParty id="urn:mace:shibboleth:2.0:infocard"
                  provider="idp-test.u.washington.edu"
                  defaultAuthenticationMethod="urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified"
                  defaultSigningCredentialRef="idp-test-credential">
        <ProfileConfiguration xsi:type="icard:InfocardCardProfile"
                 cardName="UW Infocard" cardId="UW2:{0}" cardVersion="4"
                 imageGenerator="/data/local/bin/mkinfocard"
                 mexAddress="https://idp-test.u.washington.edu:7443/idp/profile/infocard/mex"
                 stsAddress="https://idp-test.u.washington.edu:7443/idp/profile/infocard/sts"
                 privacyNotice="https://idp-test.u.washington.edu:7443/shibcard/privacynotice">

           <!-- Claims on the card.  uri is "namespace/attribute" -->

           <!-- edu person -->
           <icard:SupportedClaim key="affiliation" uri="urn:mace:shibboleth:1.0:attributeNamespace:uri/urn:mace:dir:attribute-def:eduPersonAffiliation" displayName="Affiliation"/>
           <icard:SupportedClaim key="affiliation" uri="urn:mace:shibboleth:1.0:attributeNamespace:uri/urn:mace:dir:attribute-def:eduPersonScopedAffiliation" displayName="Affiliation"/>
           <icard:SupportedClaim key="uwnetid" uri="urn:mace:shibboleth:1.0:attributeNamespace:uri/urn:mace:dir:attribute-def:eduPersonPrincipalName" displayName="UW NetID"/>
           <icard:SupportedClaim key="entitlement" uri="urn:mace:shibboleth:1.0:attributeNamespace:uri/urn:mace:dir:attribute-def:eduPersonEntitlement" displayName="Entitlement"/>
           <icard:SupportedClaim key="givenname" uri="urn:mace:shibboleth:1.0:attributeNamespace:uri/urn:mace:dir:attribute-def:givenName" displayName="Given name"/>
           <icard:SupportedClaim key="surname" uri="urn:mace:shibboleth:1.0:attributeNamespace:uri/urn:mace:dir:attribute-def:surname" displayName="Surname"/>
           <icard:SupportedClaim key="tgtid" uri="urn:mace:shibboleth:1.0:attributeNamespace:uri/urn:mace:dir:attribute-def:eduPersonTargetedID" displayName="Targeted ID"/>
           <icard:SupportedClaim key="email" uri="urn:mace:shibboleth:1.0:attributeNamespace:uri/urn:mace:dir:attribute-def:mail" displayName="Email"/>
           <!-- edu person saml2 -->
           <icard:SupportedClaim key="affiliation" uri="urn:oasis:names:tc:SAML:2.0:attrname-format:uri/urn:oid:1.3.6.1.4.1.5923.1.1.1.1" displayName="Affiliation"/>
           <icard:SupportedClaim key="affiliation" uri="urn:oasis:names:tc:SAML:2.0:attrname-format:uri/urn:oid:1.3.6.1.4.1.5923.1.1.1.9" displayName="Affiliation"/>
           <icard:SupportedClaim key="uwnetid" uri="urn:oasis:names:tc:SAML:2.0:attrname-format:uri/urn:oid:1.3.6.1.4.1.5923.1.1.1.6" displayName="UW NetID"/>
           <icard:SupportedClaim key="entitlement" uri="urn:oasis:names:tc:SAML:2.0:attrname-format:uri/urn:oid:1.3.6.1.4.1.5923.1.1.1.7" displayName="Entitlement"/>
           <!-- MS attrs -->
           <icard:SupportedClaim key="givenname" uri="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname" displayName="Given name"/>
           <icard:SupportedClaim key="surname" uri="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname" displayName="Surname"/>
           <icard:SupportedClaim key="email" uri="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress" displayName="Email"/>
           <icard:SupportedClaim key="tgtid" uri="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/privatepersonalidentifier" displayName="Private ID"/>

        </ProfileConfiguration>
        <ProfileConfiguration xsi:type="icard:InfocardMexProfile"/>
        <ProfileConfiguration xsi:type="icard:InfocardSTS1Profile"/>
        <ProfileConfiguration xsi:type="icard:InfocardSTS2Profile"/>
    </RelyingParty>


-----------------------------------------------------------------------------



ToDo: add doc for personalcard db



Issues:

0) Current cardspace IS needs old, hacked xml-security-4.1

1) The card responder will generate an image for the card
   background.  That's done with an exec.  Might be
   a better way to do this.



  


