/*
 * Copyright [2007] [University Corporation for Advanced Internet Development, Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package edu.internet2.middleware.shibboleth.infocard.profile;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import org.opensaml.common.SAMLObject;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml1.core.Request;
import org.opensaml.saml1.core.Response;
import org.opensaml.saml1.core.Status;
import org.opensaml.saml1.core.StatusCode;
import org.opensaml.saml1.core.StatusCode;
import org.opensaml.saml1.core.StatusMessage;
import org.opensaml.saml2.metadata.Endpoint;
import org.opensaml.saml2.metadata.Endpoint;
import org.opensaml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml2.metadata.SPSSODescriptor;
import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.ws.transport.http.HTTPInTransport;
import org.opensaml.ws.transport.http.HTTPOutTransport;
import org.opensaml.ws.transport.http.HTTPTransportUtils;
import org.opensaml.ws.transport.http.HttpServletRequestAdapter;
import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.parse.ParserPool;
import org.opensaml.xml.parse.XMLParserException;
import org.opensaml.xml.security.x509.X509Credential;
import org.opensaml.xml.util.Base64;
import org.opensaml.xml.util.XMLHelper;

import edu.internet2.middleware.shibboleth.common.profile.ProfileException;
import edu.internet2.middleware.shibboleth.common.profile.provider.BaseSAMLProfileRequestContext;
import edu.internet2.middleware.shibboleth.common.profile.provider.BaseSAMLProfileRequestContext;
import edu.internet2.middleware.shibboleth.common.relyingparty.RelyingPartyConfiguration;
import edu.internet2.middleware.shibboleth.idp.profile.AbstractSAMLProfileHandler;
import edu.internet2.middleware.shibboleth.infocard.config.relyingparty.InfocardMexConfiguration;
import edu.internet2.middleware.shibboleth.infocard.config.relyingparty.InfocardCardConfiguration;


/** Infocard handler for mex requests. */
public class InfocardMexHandler extends AbstractSAMLProfileHandler {

    public final static String XMLNS_WSA = "http://www.w3.org/2005/08/addressing";

    // should be config arg?
    private String infocardRelyingParty = "urn:mace:shibboleth:2.0:infocard:mex-dont-use";
    private String relyingParty = infocardRelyingParty;

    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(InfocardMexHandler.class);

    /** Builder for Status objects. */
    private SAMLObjectBuilder<Status> statusBuilder;

    /** Builder for StatusCode objects. */
    private SAMLObjectBuilder<StatusCode> statusCodeBuilder;

    /** Builder for StatusMessage objects. */
    private SAMLObjectBuilder<StatusMessage> statusMessageBuilder;

    // should be config?
    private String mexTemplateBase = "/template";
    private String mexTemplate;

    String messageID = null;
    String stsAddress = null;

    /** Canned bad-method error **/
    private static String mnaErrorResponse =
"<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\"" +
" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">" +
" <soap:Body>" +
" <soap:Fault>" +
" <soap:Code>" +
" <soap:Value>env:Sender</env:Value>" +
" <soap:Subcode>" +
" <soap:Value>wsa:ActionNotSupported</env:Value>" +
" </soap:Subcode>" +
" </soap:Code>" +
" <soap:Reason>" +
" <soap:Text xml:lang=\"en\">Action not supported</env:Text>" +
" </soap:Reason>" +
" </soap:Fault>" +
" </soap:Body>" +
"</soap:Envelope>";


    /**
     * Constructor.
     * 
     */
    public InfocardMexHandler(String rp) {
        log.debug("Infocard Mexhandler constructor:");

        relyingParty = rp;
        log.debug("..  set relying party: " + relyingParty);

        statusBuilder = (SAMLObjectBuilder<Status>) getBuilderFactory().getBuilder(Status.DEFAULT_ELEMENT_NAME);
        statusCodeBuilder = (SAMLObjectBuilder<StatusCode>) getBuilderFactory().getBuilder(
                StatusCode.DEFAULT_ELEMENT_NAME);
        statusMessageBuilder = (SAMLObjectBuilder<StatusMessage>) getBuilderFactory().getBuilder(
                StatusMessage.DEFAULT_ELEMENT_NAME);

    }

    /** {@inheritDoc} */
    public String getProfileId() {
        return "urn:mace:shibboleth:2.0:idp:profiles:infocard:mex";
    }

    /** {@inheritDoc} */
    public void processRequest(HTTPInTransport inTransport, HTTPOutTransport outTransport) throws ProfileException {
        log.debug("Infocard Mexhandler processing incomming request");

        HttpServletRequest httpRequest = ((HttpServletRequestAdapter) inTransport).getWrappedRequest();
        if (httpRequest.getMethod().equals("POST") || httpRequest.getMethod().equals("GET")) { 
        
           InfocardMexRequestContext requestContext = decodeRequest(inTransport, outTransport);

           // HttpServletRequest httpRequest = ((HttpServletRequestAdapter) inTransport).getWrappedRequest();

           completeMexRequest(requestContext, inTransport, outTransport);
        } else {
           log.debug(".. refusing " + httpRequest.getMethod() + " request.");
            
           sendCannedError(inTransport, outTransport, mnaErrorResponse);
        }
    }


    /**
     * Decodes an incoming request and populates a created request context with the resultant information.
     * 
     * @param inTransport inbound message transport
     * @param outTransport outbound message transport
     * 
     * @return the created request context
     * 
     * @throws ProfileException throw if there is a problem decoding the request
     */

    /* At some time the request might have accompaning info - a certificate perhaps.
       For now there are no parameters */
    protected InfocardMexRequestContext decodeRequest(HTTPInTransport inTransport, HTTPOutTransport outTransport)
            throws ProfileException {
 
        HttpServletRequest httpRequest = ((HttpServletRequestAdapter) inTransport).getWrappedRequest();

        MetadataProvider metadataProvider = getMetadataProvider();

        InfocardMexRequestContext requestContext = new InfocardMexRequestContext();
        requestContext.setMetadataProvider(metadataProvider);
        requestContext.setSecurityPolicyResolver(getSecurityPolicyResolver());

        requestContext.setCommunicationProfileId(InfocardMexConfiguration.PROFILE_ID);
        requestContext.setInboundMessageTransport(inTransport);
        requestContext.setInboundSAMLProtocol(SAMLConstants.SAML11P_NS);
        requestContext.setPeerEntityRole(SPSSODescriptor.DEFAULT_ELEMENT_NAME);

        requestContext.setOutboundMessageTransport(outTransport);
        // requestContext.setOutboundSAMLProtocol(SAMLConstants.SAML11P_NS);

          // use path info to see which wsdl to send
          if (httpRequest.getPathInfo().endsWith("/pw")) mexTemplate = mexTemplateBase + "/mex_pw.xml";
          else if (httpRequest.getPathInfo().endsWith("/pc")) mexTemplate = mexTemplateBase + "/mex_pc.xml";
          else mexTemplate = mexTemplateBase + "/sts_mex.xml";  // historical
          log.debug(" .. pathinfo: " + httpRequest.getPathInfo() + ", template: " + mexTemplate);

        Element envelope = null;
        ParserPool pp = new BasicParserPool();

        try {
            Document messageDoc = pp.parse(inTransport.getIncomingStream());
            envelope = messageDoc.getDocumentElement();

            log.debug("Resultant DOM message was:\n{}", XMLHelper.nodeToString(envelope));

        } catch (XMLParserException e) { 
            log.error("Encountered error parsing message into its DOM representation", e); 
            throw new ProfileException("Encountered error parsing message into its DOM representation", e);
        }


        // get the message id

        // log.debug("envelop is:" + envelope.getTagName());
        Element child = XMLHelper.getFirstChildElement(envelope);
        // log.debug("1st child is:" + child.getTagName());
        Element mid = getFirstChildElement(child, XMLNS_WSA, "MessageID");
        if (mid!=null) {
               messageID = mid.getTextContent();
               log.debug("MessageID:" + messageID);
        }

        // get cert from relying party

        try {
            EntityDescriptor relyingPartyMetadata = metadataProvider.getEntityDescriptor(relyingParty);
            if (relyingPartyMetadata != null) {
                requestContext.setPeerEntityMetadata(relyingPartyMetadata);
            }
        } catch (MetadataProviderException e) {
            log.error("Unable to locate metadata for relying party");
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER, null,
                    "Error locating relying party metadata"));
            throw new ProfileException("Error locating relying party metadata");
        }
            log.debug(".. mexRequestContext, 5");

        RelyingPartyConfiguration rpConfig = getRelyingPartyConfiguration(relyingParty);
        if (rpConfig == null) {
            log.error("Unable to retrieve relying party configuration data for entity with ID {}", relyingParty);
            throw new ProfileException("Unable to retrieve relying party configuration data for entity with ID "
                    + relyingParty);
        }
        requestContext.setRelyingPartyConfiguration(rpConfig);
    
        InfocardMexConfiguration profileConfig = (InfocardMexConfiguration) rpConfig
                .getProfileConfiguration(InfocardMexConfiguration.PROFILE_ID);
        requestContext.setProfileConfiguration(profileConfig);

        InfocardCardConfiguration cConfig = (InfocardCardConfiguration) rpConfig
                .getProfileConfiguration(InfocardCardConfiguration.PROFILE_ID);
        stsAddress = cConfig.getStsAddress();

        if (profileConfig.getSigningCredential() != null) {
            requestContext.setOutboundSAMLMessageSigningCredential(profileConfig.getSigningCredential());
        } else if (rpConfig.getDefaultSigningCredential() != null) {
            requestContext.setOutboundSAMLMessageSigningCredential(rpConfig.getDefaultSigningCredential());
        }
        
        return requestContext;
    }

    /**
     * Creates a response to the Infocard Mexhandler and sends a card to the user.
     * 
     * @param inTransport inbound message transport
     * @param outTransport outbound message transport
     * 
     * @throws ProfileException thrown if the response can not be created 
     */
    protected void completeMexRequest(InfocardMexRequestContext requestContext, HTTPInTransport inTransport, HTTPOutTransport outTransport)
            throws ProfileException {
        HttpServletRequest httpRequest = ((HttpServletRequestAdapter) inTransport).getWrappedRequest();

            log.debug(".. completeMexRequest, return from buildMexResponse");

        X509Credential creds = (X509Credential) requestContext.getOuboundSAMLMessageSigningCredential();
        X509Certificate xcert = creds.getEntityCertificate();
         
        String cert = null;
        try {
            cert = Base64.encodeBytes(xcert.getEncoded());
        } catch (Exception e) {
            log.error("mex cert error: " + e);
        }

            StringBuffer mexBuff = new StringBuffer();

            int lc = 0;
            try {
               InputStream in = InfocardMexHandler.class.getResourceAsStream(mexTemplate);
               BufferedReader ins = new BufferedReader(new InputStreamReader(in));

               while (true) {
                   String line = ins.readLine();
                   if (line==null) break;
                   mexBuff.append(line);
                   lc++;
               }

               in.close();
               ins.close();

            } catch (IOException e) {
                log.error ("mexTemplate read " + e);
                // what to do?
            }

            // make the substitutions
            MessageFormat tplResp = new MessageFormat(mexBuff.toString());
            String[] args = {messageID, cert, stsAddress};
            String mexResp = tplResp.format(args);

       try {
            HTTPTransportUtils.setContentType(outTransport, "application/soap+xml; charset=UTF-8");
            OutputStreamWriter writer = new OutputStreamWriter(outTransport.getOutgoingStream());
            writer.write(mexResp);
            writer.flush();
            writer.close();
       } catch (Exception e) {
            log.error("Mex write response: " +  e);
       }
       log.info("Mex reply completed.");

    }



    /** Represents the internal state of a Infocard Mexhandler Request while it's being processed by the IdP. */
    public interface NameIdentifier extends SAMLObject {}
    public class InfocardMexRequestContext
            extends
            BaseSAMLProfileRequestContext<Request, Response, NameIdentifier, InfocardMexConfiguration> {

        /** SP-provide assertion consumer service URL. */
        // private String spAssertionConsumerService;

    /** The request failure status. */
    private Status failureStatus;


    /**
     * Gets the status reflecting a request failure.
     *
     * @return status reflecting a request failure
     */
    public Status getFailureStatus() {
        return failureStatus;
    }

    /**
     * Sets the status reflecting a request failure.
     *
     * @param status status reflecting a request failure
     */
    public void setFailureStatus(Status status) {
        failureStatus = status;
    }


    }


    /**
     * Build a status message, with an optional second-level failure message.
     *
     * @param topLevelCode top-level status code
     * @param secondLevelCode second-level status code
     * @param failureMessage An optional second-level failure message
     *
     * @return a Status object.
     */
    protected Status buildStatus(QName topLevelCode, QName secondLevelCode, String failureMessage) {
        Status status = statusBuilder.buildObject();

        StatusCode statusCode = statusCodeBuilder.buildObject();
        statusCode.setValue(topLevelCode);
        status.setStatusCode(statusCode);

        if (secondLevelCode != null) {
            StatusCode secondLevelStatusCode = statusCodeBuilder.buildObject();
            secondLevelStatusCode.setValue(secondLevelCode);
            statusCode.setStatusCode(secondLevelStatusCode);
        }

        if (failureMessage != null) {
            StatusMessage msg = statusMessageBuilder.buildObject();
            msg.setMessage(failureMessage);
            status.setStatusMessage(msg);
        }

        return status;
    }

    public static Element getFirstChildElement(Node n, String ns, String localName) {
        Element e = XMLHelper.getFirstChildElement(n);
        while (e != null && !XMLHelper.isElementNamed(e, ns, localName))
            e = XMLHelper.getNextSiblingElement(e);
        return e;
    }

    /* We don't have a relying party endpoint */
    protected Endpoint selectEndpoint(BaseSAMLProfileRequestContext requestContext) {
        return null;
    }
    protected void populateUserInformation(BaseSAMLProfileRequestContext requestContext) {
    }
    protected void populateSAMLMessageInformation(BaseSAMLProfileRequestContext requestContext) throws ProfileException {
    }

    /**
     * Sends error response.
     *
     * @param inTransport inbound message transport
     * @param outTransport outbound message transport
     *
     */
    protected void sendCannedError(HTTPInTransport inTransport, HTTPOutTransport outTransport, String errmsg) {

       try {
            // HTTPTransportUtils.setContentType(outTransport, "application/soap+xml; charset=UTF-8");
            HTTPTransportUtils.addNoCacheHeaders(outTransport);
            HTTPTransportUtils.setUTF8Encoding(outTransport);
            HTTPTransportUtils.setContentType(outTransport, "application/soap+xml");
            Writer out = new OutputStreamWriter(outTransport.getOutgoingStream(), "UTF-8");
            out.write(errmsg);
            out.flush();
       } catch (Exception e) {
            log.error(".. mex error write error: " +  e);
       }

       log.info("Mex reply completed.");

    }






}
