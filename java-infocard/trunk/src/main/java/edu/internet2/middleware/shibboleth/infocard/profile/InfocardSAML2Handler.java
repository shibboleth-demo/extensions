/*
 * Copyright [2007] [University Corporation for Advanced Internet Development, Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package edu.internet2.middleware.shibboleth.infocard.profile;


import java.io.FileInputStream;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.joda.time.DateTime;

import org.opensaml.Configuration;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.AttributeQuery;
import org.opensaml.saml2.core.AttributeQuery;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.Statement;
import org.opensaml.saml2.core.StatusCode;
import org.opensaml.saml2.metadata.Endpoint;
import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.ws.transport.http.HTTPInTransport;
import org.opensaml.ws.transport.http.HTTPOutTransport;
import org.opensaml.xml.XMLObjectBuilder;
import org.opensaml.xml.io.Marshaller;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.security.SecurityHelper;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.SignatureException;
import org.opensaml.xml.signature.Signer;

import edu.internet2.middleware.shibboleth.common.attribute.AttributeRequestException;
import edu.internet2.middleware.shibboleth.common.attribute.BaseAttribute;
import edu.internet2.middleware.shibboleth.common.attribute.encoding.AttributeEncoder;
import edu.internet2.middleware.shibboleth.common.attribute.encoding.SAML2AttributeEncoder;
import edu.internet2.middleware.shibboleth.common.attribute.provider.SAML2AttributeAuthority;
import edu.internet2.middleware.shibboleth.common.attribute.provider.ShibbolethSAML2AttributeAuthority;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.provider.attributeDefinition.AttributeDefinition;
import edu.internet2.middleware.shibboleth.common.profile.ProfileException;
import edu.internet2.middleware.shibboleth.common.profile.provider.BaseSAMLProfileRequestContext;
import edu.internet2.middleware.shibboleth.common.profile.provider.BaseSAMLProfileRequestContext;
import edu.internet2.middleware.shibboleth.common.relyingparty.RelyingPartyConfiguration;
import edu.internet2.middleware.shibboleth.common.relyingparty.provider.saml2.AbstractSAML2ProfileConfiguration;
import edu.internet2.middleware.shibboleth.idp.profile.saml2.AbstractSAML2ProfileHandler;
import edu.internet2.middleware.shibboleth.idp.profile.saml2.BaseSAML2ProfileRequestContext;
import edu.internet2.middleware.shibboleth.infocard.config.relyingparty.InfocardSTS2Configuration;

/** Infocard handler for STS SAML2 requests. */
public class InfocardSAML2Handler extends AbstractSAML2ProfileHandler {

        public final static String XMLNS_SOAP11 = "http://schemas.xmlsoap.org/soap/envelope/";
        public final static String XMLNS_SOAP12 = "http://www.w3.org/2003/05/soap-envelope";
        public final static String XMLNS_IC = "http://schemas.xmlsoap.org/ws/2005/05/identity";
        public final static String XMLNS_WSU = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
        public final static String XMLNS_WSSE = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
        public final static String XMLNS_WSA = "http://www.w3.org/2005/08/addressing";
        public final static String XMLNS_WST = "http://schemas.xmlsoap.org/ws/2005/02/trust";
        public final static String XMLNS_WSP = "http://schemas.xmlsoap.org/ws/2004/09/policy";
        public final static String XMLNS_WSID = "http://schemas.xmlsoap.org/ws/2006/02/addressingidentity";
        public final static String SAML_ASSERTIONID = "http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.0#SAMLAssertionID";
        public final static String XMLNS_DS = "http://www.w3.org/2000/09/xmldsig#";

        public final static String subjectConfMethod = "urn:oasis:names:tc:SAML:2.0:cm:bearer";

    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(InfocardSAML2Handler.class);

    private XMLObjectBuilder<Signature> signatureBuilder;

    private HashMap<String, String> attributeMap;

    /**
     * Constructor.
     * 
     */
    public InfocardSAML2Handler() {
        super();
        signatureBuilder = (XMLObjectBuilder<Signature>) getBuilderFactory().getBuilder(Signature.DEFAULT_ELEMENT_NAME);

    }

    /** {@inheritDoc} */
    public String getProfileId() {
        return "urn:mace:shibboleth:2.0:idp:profiles:infocard:sts";
    }

    protected InfocardSTSRequest request;
    public void setRequest(InfocardSTSRequest req) {
          request = req;
    }

        
    /** {@inheritDoc} */
    public void processRequest(HTTPInTransport inTransport, HTTPOutTransport outTransport) throws ProfileException {
        log.debug("Infocard SAML2 STShandler processing incomming request");
        InfocardSTS2RequestContext requestContext = completeDecodeRequest(inTransport, outTransport);
        generateAssertion(requestContext);
    }


    protected List<String> getNameFormats(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext)
            throws ProfileException {
        ArrayList<String> nameFormats = new ArrayList<String>();
        nameFormats.add("urn:mace:shibboleth:2.0:nameIdentifier");
        return nameFormats;
    }

    /**
     * Decodes an incoming request and populates a created request context with the resultant information.
     * 
     * @param inTransport inbound message transport
     * @param outTransport outbound message transport
     * 
     * @return the created request context
     * 
     * @throws ProfileException throw if there is a problem decoding the request
     */

    protected InfocardSTS2RequestContext completeDecodeRequest(HTTPInTransport inTransport, HTTPOutTransport outTransport)
            throws ProfileException {

        log.debug(".. complete decode saml2 STS request");
 
        // MetadataProvider metadataProvider = getMetadataProvider();

        InfocardSTS2RequestContext requestContext = new InfocardSTS2RequestContext();
        requestContext.setPrincipalName(request.principalName);
        requestContext.setSecurityPolicyResolver(getSecurityPolicyResolver());

        requestContext.setCommunicationProfileId(InfocardSTS2Configuration.PROFILE_ID);
        requestContext.setInboundMessageTransport(inTransport);
        requestContext.setInboundSAMLProtocol(SAMLConstants.SAML20P_NS);

        requestContext.setOutboundMessageTransport(outTransport);
        requestContext.setOutboundSAMLProtocol(SAMLConstants.SAML20P_NS);

        String relyingPartyID = request.relyingPartyID;

        requestContext.setInboundMessageIssuer(relyingPartyID);

        RelyingPartyConfiguration rpConfig = getRelyingPartyConfiguration(relyingPartyID);
        if (rpConfig == null) {
            log.error("Unable to retrieve relying party configuration data for entity with ID {}", relyingPartyID);
            throw new ProfileException("Unable to retrieve relying party configuration data for entity with ID "
                    + relyingPartyID);
        }
        requestContext.setRelyingPartyConfiguration(rpConfig);
        requestContext.setLocalEntityId(rpConfig.getProviderId());
    
        InfocardSTS2Configuration profileConfig = (InfocardSTS2Configuration) rpConfig
                .getProfileConfiguration(InfocardSTS2Configuration.PROFILE_ID);
        requestContext.setProfileConfiguration(profileConfig);
      
        // set the real relying party as the audience
        Collection<String> aud = profileConfig.getAssertionAudiences();
        aud.clear();
        aud.add(request.realRelyingParty);

        if (profileConfig.getSigningCredential() != null) {
            requestContext.setOutboundSAMLMessageSigningCredential(profileConfig.getSigningCredential());
        } else if (rpConfig.getDefaultSigningCredential() != null) {
            requestContext.setOutboundSAMLMessageSigningCredential(rpConfig.getDefaultSigningCredential());
        }
        
        // add requested attributes - convert from requested uri to attribute id

        HashSet<String> attrs = new HashSet<String>();
        attrs.add("transientId");

        attributeMap = new HashMap<String, String>();   // <id, uri>

        // Look through SAML2 encoders for requested attributes

        SAML2AttributeAuthority attributeAuthority = profileConfig.getAttributeAuthority();
        Map<String, AttributeDefinition> definitions =
                     ((ShibbolethSAML2AttributeAuthority)attributeAuthority).getAttributeResolver().getAttributeDefinitions();
        for (AttributeDefinition definition : definitions.values()) {
           List<AttributeEncoder> encoders = definition.getAttributeEncoders();
           for (AttributeEncoder encoder : encoders) {
              if (encoder instanceof SAML2AttributeEncoder) {
                 SAML2AttributeEncoder enc = (SAML2AttributeEncoder)encoder;
                 String attrname = enc.getAttributeName();
                 String attrns = enc.getNameFormat();
                 int nslen = attrns.length();
                 // log.debug(".... looking at " + attrname + ", nameFormat=" + attrns);
                 for (String ra : request.requestedAttributes) {
                   if (ra.startsWith(attrns)) {
                      // log.debug(".... nameFormat match");
                      if (ra.substring(nslen+1).equals(attrname)) {
                         String id = definition.getId();
                         log.debug(".... found attribute " + id + " = " + ra);
                         attrs.add(id);
                         attributeMap.put(id, ra);
                      }
                   }
                 }
              }
           }
        }
        
        requestContext.setRequestedAttributes(attrs);

        return requestContext;
    }


    protected void generateAssertion(InfocardSTS2RequestContext requestContext) throws ProfileException {

         // resolve the attributes

         InfocardSTS2Configuration profileConfig = requestContext.getProfileConfiguration();
         SAML2AttributeAuthority attributeAuthority = profileConfig.getAttributeAuthority();
         if (attributeAuthority==null) {
            log.error(".. no attribute authority");
            return;
         }
         
         try {
            log.debug("Resolving attributes for principal {} of SAML request from relying party {}",
                      requestContext.getPrincipalName(), requestContext.getInboundMessageIssuer());
            Map<String, BaseAttribute> principalAttributes =
                        ((ShibbolethSAML2AttributeAuthority)attributeAuthority).getAttributeResolver().resolveAttributes(requestContext);
            requestContext.setAttributes(principalAttributes);
            // log.debug(".. have " + principalAttributes.size() + "attributes");

            // store the string values for display

            HashMap<String, String> displayAttributes = new HashMap<String, String>();   // <uri, display value>
            for (BaseAttribute attr : principalAttributes.values())  {
                for (Object o : attr.getValues()) {
                    if (o!=null) {
                        String uri = attributeMap.get(attr.toString());
                        if (uri != null) {
                           // log.debug("... found " + attr.toString() + "in the attribute map = " + uri);
                           String ov = displayAttributes.get(uri);
                           if (ov==null) ov = "";
                           else ov = ov.concat(";");
                           displayAttributes.put(uri, ov.concat(o.toString()));
                        }
                    }
                }
            }
            request.displayAttributes = displayAttributes;
        
        } catch (AttributeRequestException e) {
            log.error("Error resolving attributes for SAML request from relying party "
                    + requestContext.getInboundMessageIssuer(), e);
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, "Error resolving attributes"));
            throw new ProfileException("Error resolving attributes for SAML request from relying party "
                    + requestContext.getInboundMessageIssuer(), e);
        }

        requestContext.setReleasedAttributes(requestContext.getAttributes().keySet());

        log.debug(".. building attribute statement");

        ArrayList<Statement> statements = new ArrayList<Statement>();

        AttributeStatement attributeStatement = buildAttributeStatement(requestContext);
        if (attributeStatement != null) {
            statements.add(attributeStatement);
        }

        DateTime issueInstant = new DateTime();
        Assertion assertion = buildAssertion(requestContext, issueInstant);
        if (statements != null && !statements.isEmpty()) {
            assertion.getStatements().addAll(statements);
        }

        log.debug(".. signing assertion");
        signAssertion(requestContext, assertion);
        request.assertion = assertion;
    }



    /** Represents the internal state of a Infocard STS request while it's being processed by the IdP. */

    protected class InfocardSTS2RequestContext extends
            BaseSAML2ProfileRequestContext<AttributeQuery, Response, InfocardSTS2Configuration>  {

        /** SP-provide assertion consumer service URL. */
        private String spAssertionConsumerService;
    }


    protected void signAssertion(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext, Assertion assertion)
            throws ProfileException {
        log.debug("Determining if SAML assertion to relying party {} should be signed", requestContext
                .getInboundMessageIssuer());

        AbstractSAML2ProfileConfiguration profileConfig = requestContext.getProfileConfiguration();

        log.debug("Determining signing credntial for assertion to relying party {}", requestContext
                .getInboundMessageIssuer());
        Credential signatureCredential = profileConfig.getSigningCredential();
        if (signatureCredential == null) {
            signatureCredential = requestContext.getRelyingPartyConfiguration().getDefaultSigningCredential();
            log.debug("no saml2 profile signing cred, using def cred");
        } else log.debug("using saml2 profile signing cred");

        if (signatureCredential == null) {
            throw new ProfileException("No signing credential is specified for relying party configuration "
                    + requestContext.getRelyingPartyConfiguration().getProviderId()
                    + " or it's SAML2 attribute query profile configuration");
        }

        log.debug("Signing assertion to relying party {}", requestContext.getInboundMessageIssuer());
        Signature signature = signatureBuilder.buildObject(Signature.DEFAULT_ELEMENT_NAME);

        signature.setSigningCredential(signatureCredential);
        try {
            SecurityHelper.prepareSignatureParams(signature, signatureCredential, null, null);
        } catch (SecurityException e) {
            throw new ProfileException("Error preparing signature for signing", e);
        }

        assertion.setSignature(signature);

        Marshaller assertionMarshaller = Configuration.getMarshallerFactory().getMarshaller(assertion);
        try {
            assertionMarshaller.marshall(assertion);
            Signer.signObject(signature);
        } catch (MarshallingException e) {
            log.error("Unable to marshall assertion for signing", e);
            throw new ProfileException("Unable to marshall assertion for signing", e);
        } catch (SignatureException e) {
            log.error("Unable to sign assertion ", e);
            throw new ProfileException("Unable to sign assertion", e);
        }
    }

    /* We don't have a relying party endpoint */
    protected Endpoint selectEndpoint(BaseSAMLProfileRequestContext requestContext) {
        return null;
    }
    protected void populateSAMLMessageInformation(BaseSAMLProfileRequestContext requestContext) throws ProfileException {
    }



}
