/*
 * Copyright [2007] [University Corporation for Advanced Internet Development, Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.infocard.config.relyingparty;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;

import java.beans.PropertyVetoException;

import javax.xml.namespace.QName;
import javax.sql.DataSource;

import org.opensaml.xml.util.DatatypeHelper;
import org.opensaml.xml.util.XMLHelper;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.beans.factory.xml.AbstractSingleBeanDefinitionParser;
import org.w3c.dom.Element;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.mchange.v2.c3p0.ComboPooledDataSource;



/**
 * Spring configuration parser for Infocard card relyingparty profile.
 */
public class InfocardCardProfileConfigurationBeanDefinitionParser extends AbstractSingleBeanDefinitionParser
         {

    /** Schema type name. */
    public static final QName TYPE_NAME = new QName(InfocardRelyingPartyNamespaceHandler.NAMESPACE, "InfocardCardProfile");

    /** {@inheritDoc} */
    protected Class getBeanClass(Element element) {
        return InfocardCardConfiguration.class;
    }

    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(InfocardCardProfileConfigurationBeanDefinitionParser.class);


    /** {@inheritDoc} */
    protected void doParse(Element element, ParserContext parserContext, BeanDefinitionBuilder builder) {
log.debug("icard bean parser: doParse");
        super.doParse(element, parserContext, builder);
        builder.addConstructorArg(DatatypeHelper.safeTrimOrNullString(element.getAttributeNS(null,
                "cardName")));
        builder.addConstructorArg(DatatypeHelper.safeTrimOrNullString(element.getAttributeNS(null,
                "cardId")));
        builder.addConstructorArg(DatatypeHelper.safeTrimOrNullString(element.getAttributeNS(null,
                "cardVersion")));
        builder.addConstructorArg(DatatypeHelper.safeTrimOrNullString(element.getAttributeNS(null,
                "imageGenerator")));
        builder.addConstructorArg(DatatypeHelper.safeTrimOrNullString(element.getAttributeNS(null,
                "mexAddress")));
        builder.addConstructorArg(DatatypeHelper.safeTrimOrNullString(element.getAttributeNS(null,
                "stsAddress")));
        builder.addConstructorArg(DatatypeHelper.safeTrimOrNullString(element.getAttributeNS(null,
                "privacyNotice")));
        builder.addPropertyValue("supportedClaims", getSupportedClaims(element));
        builder.addPropertyValue("cardDataSources", getCardDataSources(element));
log.debug("icard bean parser: doParse exit");

    }

    /** {@inheritDoc} */
    protected boolean shouldGenerateId() {
        return true;
    }

    /**
     * Gets the list of claims the card issuer includes.
     *
     * @param config profile handler configuration element
     *
     * @return list of claims
     */
    protected Map<String, InfocardClaim> getSupportedClaims(Element config) {
        Map<String, InfocardClaim> claims = new HashMap<String, InfocardClaim>();
        List<Element> claimElems = XMLHelper.getChildElementsByTagName(config, "SupportedClaim");
        if (claimElems != null) {
            for (Element claimElem : claimElems) {
                InfocardClaim claim = new InfocardClaim();
                claim.key = claimElem.getAttribute("key");
                claim.uri = claimElem.getAttribute("uri");
                claim.displayName = claimElem.getAttribute("displayName");
                if (claim.displayName==null) claim.displayName = claim.uri;
                claims.put(claim.uri, claim);
            }
        }
        return claims;
    }

    /**
     * Gets the data sources for personal card authn.
     *
     * @param config profile handler configuration element
     *
     * @return datasources
     */
    protected Vector<DataSource> getCardDataSources(Element config) {
        Vector<DataSource> sources = new Vector<DataSource>(1);
        List<Element> eles = XMLHelper.getChildElementsByTagName(config, "JDBCConnection");
        if (eles != null) {
            for (Element e : eles) {
                DataSource src = getDataSource(e);
                sources.add(src);
            }
        }
        return sources;
    }

    /**
     * Builds a JDBC {@link DataSource} from an CardDataSource configuration element.
     * 
     * @param pluginId ID of this data connector
     * @param amc the application managed configuration element
     * 
     * @return the built data source
     */
    protected DataSource getDataSource(Element cds) {
        ComboPooledDataSource datasource = new ComboPooledDataSource();

        String driverClass = DatatypeHelper.safeTrim(cds.getAttributeNS(null, "jdbcDriver"));
        ClassLoader classLoader = this.getClass().getClassLoader();
        try {
            classLoader.loadClass(driverClass);
        } catch (ClassNotFoundException e) {
            log.error("Unable to create relational database connector, JDBC driver can not be found on the classpath");
            throw new BeanCreationException(
                    "Unable to create relational database connector, JDBC driver can not be found on the classpath");
        }

        try {
            datasource.setDriverClass(driverClass);
            datasource.setJdbcUrl(DatatypeHelper.safeTrim(cds.getAttributeNS(null, "jdbcURL")));
            datasource.setUser(DatatypeHelper.safeTrim(cds.getAttributeNS(null, "jdbcUserName")));
            datasource.setPassword(DatatypeHelper.safeTrim(cds.getAttributeNS(null, "jdbcPassword")));

            if (cds.hasAttributeNS(null, "id")) {
                datasource.setDescription(DatatypeHelper.safeTrim(cds.getAttributeNS(null, "id")));
                log.debug("loading datasource: " + DatatypeHelper.safeTrim(cds.getAttributeNS(null, "id")));
                log.debug("loading datasource: " + datasource.getDescription());
            } else {
                datasource.setDescription("");
            }

            if (cds.hasAttributeNS(null, "poolAcquireIncrement")) {
                datasource.setAcquireIncrement(Integer.parseInt(DatatypeHelper.safeTrim(cds.getAttributeNS(null,
                        "poolAcquireIncrement"))));
            } else {
                datasource.setAcquireIncrement(3);
            }

            if (cds.hasAttributeNS(null, "poolAcquireRetryAttempts")) {
                datasource.setAcquireRetryAttempts(Integer.parseInt(DatatypeHelper.safeTrim(cds.getAttributeNS(null,
                        "poolAcquireRetryAttempts"))));
            } else {
                datasource.setAcquireRetryAttempts(36);
            }

            if (cds.hasAttributeNS(null, "poolAcquireRetryDelay")) {
                datasource.setAcquireRetryDelay(Integer.parseInt(DatatypeHelper.safeTrim(cds.getAttributeNS(null,
                        "poolAcquireRetryDelay"))));
            } else {
                datasource.setAcquireRetryDelay(5000);
            }

            if (cds.hasAttributeNS(null, "poolBreakAfterAcquireFailure")) {
                datasource.setBreakAfterAcquireFailure(XMLHelper.getAttributeValueAsBoolean(cds.getAttributeNodeNS(
                        null, "poolBreakAfterAcquireFailure")));
            } else {
                datasource.setBreakAfterAcquireFailure(true);
            }

            if (cds.hasAttributeNS(null, "poolMinSize")) {
                datasource.setMinPoolSize(Integer.parseInt(DatatypeHelper.safeTrim(cds.getAttributeNS(null,
                        "poolMinSize"))));
            } else {
                datasource.setMinPoolSize(2);
            }

            if (cds.hasAttributeNS(null, "poolMaxSize")) {
                datasource.setMaxPoolSize(Integer.parseInt(DatatypeHelper.safeTrim(cds.getAttributeNS(null,
                        "poolMaxSize"))));
            } else {
                datasource.setMaxPoolSize(50);
            }

            if (cds.hasAttributeNS(null, "poolMaxIdleTime")) {
                datasource.setMaxIdleTime(Integer.parseInt(DatatypeHelper.safeTrim(cds.getAttributeNS(null,
                        "poolMaxIdleTime"))));
            } else {
                datasource.setMaxIdleTime(600);
            }

            if (cds.hasAttributeNS(null, "poolIdleTestPeriod")) {
                datasource.setIdleConnectionTestPeriod(Integer.parseInt(DatatypeHelper.safeTrim(cds.getAttributeNS(
                        null, "poolIdleTestPeriod"))));
            } else {
                datasource.setIdleConnectionTestPeriod(180);
            }

            log.debug("Created card data source: " + datasource.getDescription());
            return datasource;

        } catch (PropertyVetoException e) {
            log.error("Unable to create card data source " + datasource.getDescription() + " with JDBC driver class {}", driverClass);
            return null;
        }
    }


     
}
