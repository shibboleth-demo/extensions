/*
 * Copyright [2007] [University Corporation for Advanced Internet Development, Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package edu.internet2.middleware.shibboleth.infocard.profile;


import java.security.Key;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;
import java.util.Vector;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/** Infocard handler for card requests.  Collect info and issue card. */

public class PersonalCardDB {

    private Vector<DataSource> dataSources;

    private final Logger log = LoggerFactory.getLogger(PersonalCardDB.class);

    public PersonalCardDB(Vector<DataSource> ds) {
        log.debug(". new pcard db: " + ds.toString());
        dataSources = ds;
    }

    public boolean updateUser(String userId, String ppid, String key) {
      log.debug("Saving personal card for " + userId + ", ppid = " + ppid);
      Statement updateStatement;
      ResultSet results;
      int status;
      boolean ok = true;

      // check if already there
      String ckid = getUser(ppid, key);
      if (ckid!=null && ckid.equals(userId)) return (true);

      for (DataSource source : dataSources ) {
         try {
            Connection connection = getConnection(source);
            String query = "insert into personalcard values ('" + userId + "','" + ppid + "','" + key + "')";
            updateStatement = connection.createStatement();
            status = updateStatement.executeUpdate(query);
            log.debug(sourceId(source) + " update status: " + status);
            updateStatement.close();
         } catch (SQLException e) { 
            log.error(sourceId(source) + " db update error: " + e);
            ok = false;
         }
      }
      return (ok);
    }

   
    public String getUser(String ppid, String key) {
       log.debug("Retrieving id for ppid = " + ppid);
       String id;
       String dbkey;
       Statement selectStatement;
       ResultSet results;
       int updateCount;

       for (DataSource source : dataSources ) {
          try {
             Connection connection = getConnection(source);
             String query = "select * from personalcard where ppid='" + ppid + "'";
             selectStatement = connection.createStatement();
             results = selectStatement.executeQuery(query);
             if (results.next()) {
               id = results.getString(1);
               dbkey = results.getString(3);
               log.debug(sourceId(source) + " id=" + id + ", key=" + dbkey);
             } else {
               id = null;
               log.debug(sourceId(source) + " not found");
             }
             selectStatement.close();
             return (id);
          } catch (SQLException e) { 
             log.error(sourceId(source) + " select error: " + e);
          }
       }
       return (null);
    }

    private Connection getConnection(DataSource ds) {
      Connection connection = null;
      try {
         connection = ds.getConnection();
         if (connection == null) { 
            log.error("RDBMS card data - Unable to create connections");
            return (null);
         }
      } catch (SQLException e) { 
         if (e.getSQLState() != null) {
            log.error("RDBMS card data - Invalid connector configuration; SQL state: {}, SQL Code: {}",
                        new Object[] { e.getSQLState(), e.getErrorCode() }, e);
         } else {
            log.error("RDBMS card data - Invalid connector configuration", e);
         }
         return (null);
      }
      return (connection);
    }

    private String sourceId(DataSource ds) {
       if (ds instanceof ComboPooledDataSource) {
          return ((ComboPooledDataSource) ds).getDescription();
       }
       return (new String(""));
    }
}

