/*
 * Copyright [2007] [University Corporation for Advanced Internet Development, Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.infocard.config.relyingparty;

import javax.xml.namespace.QName;

import org.opensaml.xml.util.DatatypeHelper;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.beans.factory.xml.AbstractSingleBeanDefinitionParser;
import org.w3c.dom.Element;

/**
 * Spring configuration parser for Shibboleth Infocard saml1 profile configurations.
 *
 */
public class InfocardSTS1ProfileConfigurationBeanDefinitionParser extends AbstractSingleBeanDefinitionParser
         {

    /** Schema type name. */
    public static final QName TYPE_NAME = new QName(InfocardRelyingPartyNamespaceHandler.NAMESPACE, "InfocardSTS1Profile");

    /** {@inheritDoc} */
    protected Class getBeanClass(Element element) {
        return InfocardSTS1Configuration.class;
    }

    /** {@inheritDoc} */
    protected void doParse(Element element, ParserContext parserContext, BeanDefinitionBuilder builder) {
        super.doParse(element, parserContext, builder);
        builder.addPropertyReference("attributeAuthority", DatatypeHelper.safeTrimOrNullString(element.getAttributeNS(
            null, "attributeAuthority")));
        String secCredRef = DatatypeHelper.safeTrimOrNullString(element.getAttributeNS(null, "signingCredentialRef"));
        if (secCredRef != null) {
            builder.addDependsOn(secCredRef);
            builder.addPropertyReference("signingCredential", secCredRef);
        }
    }

    /** {@inheritDoc} */
    protected boolean shouldGenerateId() {
        return true;
    }



}
