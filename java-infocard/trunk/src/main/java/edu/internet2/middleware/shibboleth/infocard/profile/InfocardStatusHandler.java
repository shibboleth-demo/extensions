/*
 * Copyright [2007] [University Corporation for Advanced Internet Development, Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.infocard.profile;

import java.io.IOException;
import java.io.OutputStreamWriter;

import org.opensaml.ws.transport.InTransport;
import org.opensaml.ws.transport.OutTransport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.common.profile.provider.AbstractRequestURIMappedProfileHandler;

/**
 * An example and test profile handler extension that returns the string "Infocard is up." to any request. 
 */

public class InfocardStatusHandler extends AbstractRequestURIMappedProfileHandler {

    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(InfocardStatusHandler.class);

    /** {@inheritDoc} */
    public String getProfileId() {
        return "urn:mace:shibboleth:2.0:idp:profiles:infocard:status";
    }

    /** {@inheritDoc} */
    public void processRequest(InTransport in, OutTransport out) {
        try {
            OutputStreamWriter writer = new OutputStreamWriter(out.getOutgoingStream());
            writer.write("Infocard is up.");
            writer.flush();
        } catch (IOException e) {
            log.error("Unable to write up response", e);
        }
    }
}
