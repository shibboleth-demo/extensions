/*
 * Copyright [2007] [University Corporation for Advanced Internet Development, Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package edu.internet2.middleware.shibboleth.infocard.profile;


import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.Reader;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import java.net.URLEncoder;
import java.io.UnsupportedEncodingException;

import java.security.Key;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


import org.apache.xml.security.algorithms.MessageDigestAlgorithm;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.keys.KeyInfo;
import org.apache.xml.security.keys.content.X509Data;
import org.apache.xml.security.signature.ObjectContainer;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.signature.XMLSignatureException;
import org.apache.xml.security.transforms.Transforms;

import org.opensaml.common.SAMLObject;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml1.core.AuthenticationStatement;
import org.opensaml.saml1.core.Request;
import org.opensaml.saml1.core.Response;
import org.opensaml.saml1.core.Status;
import org.opensaml.saml1.core.StatusCode;
import org.opensaml.saml1.core.StatusMessage;
import org.opensaml.saml2.metadata.Endpoint;
import org.opensaml.ws.transport.http.HTTPInTransport;
import org.opensaml.ws.transport.http.HTTPOutTransport;
import org.opensaml.ws.transport.http.HTTPTransportUtils;
import org.opensaml.ws.transport.http.HttpServletRequestAdapter;
import org.opensaml.ws.transport.http.HttpServletResponseAdapter;
import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.parse.ParserPool;
import org.opensaml.xml.parse.XMLParserException;
import org.opensaml.xml.security.x509.X509Credential;
import org.opensaml.xml.util.Base64;
import org.opensaml.xml.util.DatatypeHelper;
import org.opensaml.xml.util.XMLConstants;
import org.opensaml.xml.util.XMLHelper;


import edu.internet2.middleware.shibboleth.common.ShibbolethConstants;
import edu.internet2.middleware.shibboleth.common.profile.ProfileException;
import edu.internet2.middleware.shibboleth.common.profile.provider.BaseSAMLProfileRequestContext;
import edu.internet2.middleware.shibboleth.common.profile.provider.BaseSAMLProfileRequestContext;
import edu.internet2.middleware.shibboleth.common.relyingparty.ProfileConfiguration;
import edu.internet2.middleware.shibboleth.common.relyingparty.RelyingPartyConfiguration;
import edu.internet2.middleware.shibboleth.common.util.HttpHelper;
import edu.internet2.middleware.shibboleth.idp.authn.LoginContext;
import edu.internet2.middleware.shibboleth.idp.util.HttpServletHelper;
import edu.internet2.middleware.shibboleth.idp.profile.AbstractSAMLProfileHandler;
import edu.internet2.middleware.shibboleth.infocard.config.relyingparty.InfocardCardConfiguration;
import edu.internet2.middleware.shibboleth.infocard.config.relyingparty.InfocardClaim;

import edu.internet2.middleware.shibboleth.idp.session.Session;

/** Infocard handler for card requests.  Collect info and issue card. */

public class InfocardCardHandler extends AbstractSAMLProfileHandler {

        public final static String XMLNS_SOAP = "http://www.w3.org/2003/05/soap-envelope";
        public final static String XMLNS_IC = "http://schemas.xmlsoap.org/ws/2005/05/identity";
        public final static String XMLNS_WSU = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
        public final static String XMLNS_WSSE = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
        public final static String XMLNS_WSA = "http://www.w3.org/2005/08/addressing";
        public final static String XMLNS_WST = "http://schemas.xmlsoap.org/ws/2005/02/trust";
        public final static String XMLNS_WSP = "http://schemas.xmlsoap.org/ws/2004/09/policy";
        public final static String XMLNS_WSX = "http://schemas.xmlsoap.org/ws/2004/09/mex";
        public final static String XMLNS_WSID = "http://schemas.xmlsoap.org/ws/2006/02/addressingidentity";
        public final static String XMLNS_DS = "http://www.w3.org/2000/09/xmldsig#";

    private String infocardRelyingParty = "urn:mace:shibboleth:2.0:infocard:default-dont-use";
    protected String relyingParty = infocardRelyingParty;  // actually comes from config  

    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(InfocardCardHandler.class);

    /** Builder of AuthenticationStatement objects. */
    private SAMLObjectBuilder<AuthenticationStatement> authnStatementBuilder;

    /** URL of the authentication manager servlet. */
    private String authenticationManagerPath;

    /** Builder for Status objects. */
    private SAMLObjectBuilder<Status> statusBuilder;

    /** Builder for StatusCode objects. */
    private SAMLObjectBuilder<StatusCode> statusCodeBuilder;

    /** Builder for StatusMessage objects. */
    private SAMLObjectBuilder<StatusMessage> statusMessageBuilder;

    /* type of card wanted */
    private String cardType;

    /* logged in user */
    private String userId;

    /* ppid if personal card */
    private String cardPPID;

    /* b64 hash of card modulus */
    private String cardKey;

    /* name on the card */
    private String cardName;

    /**
     * Constructor.
     * 
     * @param authnManagerPath path to the authentication manager servlet
     * 
     * @throws IllegalArgumentException thrown if either the authentication manager path or encoding binding URI are
     *             null or empty
     */
    public InfocardCardHandler(String authnManagerPath, String rp) {
        if (DatatypeHelper.isEmpty(authnManagerPath)) {
            throw new IllegalArgumentException("Authentication manager path may not be null");
        }
        log.debug("Infocard Cardhandler constructor: authn={}", authnManagerPath);
        authenticationManagerPath = authnManagerPath;
        relyingParty = rp;
        
        authnStatementBuilder = (SAMLObjectBuilder<AuthenticationStatement>) getBuilderFactory().getBuilder(
                AuthenticationStatement.DEFAULT_ELEMENT_NAME);

        statusBuilder = (SAMLObjectBuilder<Status>) getBuilderFactory().getBuilder(Status.DEFAULT_ELEMENT_NAME);
        statusCodeBuilder = (SAMLObjectBuilder<StatusCode>) getBuilderFactory().getBuilder(
                StatusCode.DEFAULT_ELEMENT_NAME);
        statusMessageBuilder = (SAMLObjectBuilder<StatusMessage>) getBuilderFactory().getBuilder(
                StatusMessage.DEFAULT_ELEMENT_NAME);

    }

    /** {@inheritDoc} */
    public String getProfileId() {
        return "urn:mace:shibboleth:2.0:idp:profiles:infocard:card";
    }

    /** {@inheritDoc} */
    public void processRequest(HTTPInTransport inTransport, HTTPOutTransport outTransport) throws ProfileException {
        log.debug("* * * * Infocard Cardhandler processing incoming request");

        HttpServletRequest httpRequest = ((HttpServletRequestAdapter) inTransport).getWrappedRequest();
        BaseSAMLProfileRequestContext requestContext = buildRequestContext(inTransport, outTransport);

        userId = null;
        cardType = null;
        cardName = null;
        cardKey = null;
        cardPPID = null;

        // First step: get authn
        Session us = getUserSession(inTransport);
        if (us != null) {
           userId = getUserSession(inTransport).getPrincipalName();
           if (userId == null) log.debug("Incoming request userId = null");
           else log.debug("Incoming request userId = " + userId);
        }

        if (userId==null) {
            log.debug("Request does not contain a user name, redirecting to login");
            performAuthentication(inTransport, outTransport);

        // Second step: get card type
        } else {
            log.debug("Incoming request contains a username, looking for card choice");

            cardType = DatatypeHelper.safeTrimOrNullString(httpRequest.getParameter("cardtype"));
 
            if (cardType==null) {
               log.debug("Request does not contain a cardtype, redirecting to chooser jsp");
               performFormRedirect(inTransport, outTransport, "/cardtype.jsp");  // should be param
            } else {
               log.debug("have cardtype: " + cardType);
               cardName = DatatypeHelper.safeTrimOrNullString(httpRequest.getParameter("cardname"));
               if (cardName==null) cardName = "UW Infocard";

               if (cardType.equals("password")) {
                  if (httpRequest.getRequestURI().endsWith(".crd")) {
                     log.debug("have .crd for pw card");
                     completeCardRequest(inTransport, outTransport);
                     return;
                  }
               } else if (cardType.equals("personal")) {
                  if (httpRequest.getRequestURI().endsWith(".crd")) {
                     log.debug("have .crd for pc card");
                     cardPPID = DatatypeHelper.safeTrimOrNullString(httpRequest.getParameter("ppid"));
                     cardKey = DatatypeHelper.safeTrimOrNullString(httpRequest.getParameter("key"));
                     if (cardPPID!=null && cardPPID.length()>0 && cardKey!=null && cardKey.length()>0) {
                        completeCardRequest(inTransport, outTransport);
                        return;
                     } else {
                        log.debug(".crd without ppid info");
                        performErrorRedirect(inTransport, outTransport, "/carderror.jsp",  "invalid authentication");  
                        return;
                     }
                  }

                  // otherwise this should have been a pc login
                  String xmlToken = DatatypeHelper.safeTrimOrNullString(httpRequest.getParameter("xmlToken"));
                  if (xmlToken!=null) {
                     log.debug(".. got xmlToken");
                     // doc'ify the token and decrypt
                     ParserPool pp = new BasicParserPool();
                     Element assertionElement = null;
                     try {
                        Reader sr = new StringReader(xmlToken);
                        Document messageDoc = pp.parse(sr);
                        Element ele = messageDoc.getDocumentElement();
                        // log.debug(".. resultant DOM message was:\n{}", XMLHelper.nodeToString(ele));
                       
                        // decrypt
                        ElementDecrypter dec = new ElementDecrypter(requestContext.getOuboundSAMLMessageSigningCredential());
                        dec.decrypt(messageDoc, ele);
                        assertionElement = messageDoc.getDocumentElement();
                        log.debug(".. decrypted assertion:\n{}", XMLHelper.nodeToString(assertionElement));
                        AssertionAnalyzer alz = new AssertionAnalyzer();
                        alz.analyze(assertionElement);
                        cardPPID = alz.getPPID();
                        cardKey = alz.getCardKey();
                     } catch (Exception e) {
                        log.warn(". exception " + e);
                        performErrorRedirect(inTransport, outTransport, "/carderror.jsp",  e.toString());  
                        return;
                     }

                     if (cardPPID!=null && cardKey!=null) {
                        log.debug(".. got ppid '" + cardPPID + "' and key '" + cardKey + "'");
                        InfocardCardConfiguration profileConfig = (InfocardCardConfiguration) requestContext.getProfileConfiguration();
                        PersonalCardDB db = new PersonalCardDB(profileConfig.getCardDataSources());
                        db.updateUser(userId, cardPPID, cardKey);
                     } else {
                        log.warn(".. failed to get ppid from token! ");
                        performErrorRedirect(inTransport, outTransport, "/carderror.jsp",  "invalid card login");  
                        return;
                     }
                  }
               }

               // redirect browser so the url that ends with .crd (from handler.xml endpoints)
               String context = DatatypeHelper.safeTrimOrNullString(httpRequest.getContextPath());
               String url =  context + httpRequest.getServletPath();
               for (String requestPath : getRequestPaths()) {
                   if (requestPath.endsWith(".crd")) url = url + requestPath;
               }
             try {
               url = url + "?cardtype=" + URLEncoder.encode(cardType, "UTF-8") + "&cardname=" + URLEncoder.encode(cardName, "UTF-8");
               if (cardPPID!=null && cardKey!=null) url = url + "&ppid=" + URLEncoder.encode(cardPPID, "UTF-8") +
                                                                "&key=" + URLEncoder.encode(cardKey, "UTF-8");
             } catch (UnsupportedEncodingException e) {
               log.error("utf-8 encoding not supported??");
             }
            
               log.debug("card fetch url: " + url);

               try {
                  HTTPTransportUtils.setContentType(outTransport, "text/html; charset=UTF-8");
                  OutputStreamWriter writer = new OutputStreamWriter(outTransport.getOutgoingStream());
                  writer.write("<html><head><meta http-equiv=\"refresh\" content=\"2;url=" + url + "\"></head>");
                  writer.write("<body>Your card should load automatically.  If it doesn't, <a href=\"" + url + "\">get the card</a>.</body></html>");
                  writer.flush();
                  log.debug("infocard card redirect complete");
               } catch (Exception e) {
                  log.debug("infocard card redirect error: " + e);
                        performErrorRedirect(inTransport, outTransport, "/carderror.jsp",  e.toString());  
                        return;
               }

           }

        }
    }

    /**
     * Redirect to card form
     * 
     * @param inTransport inbound message transport
     * @param outTransport outbound message transport
     * 
     * @throws ProfileException thrown if there is a problem 
     */
    protected void performFormRedirect(HTTPInTransport inTransport, HTTPOutTransport outTransport, String jsp)
            throws ProfileException {

        log.debug("Infocard Cardhandler performFormRedirect");
        HttpServletRequest httpRequest = ((HttpServletRequestAdapter) inTransport).getWrappedRequest();
        HttpServletResponse httpResponse = ((HttpServletResponseAdapter) outTransport).getWrappedResponse();
        String context = DatatypeHelper.safeTrimOrNullString(httpRequest.getContextPath());
        if (httpRequest == null) context = "/";

        httpRequest.setAttribute("passwordAction", DatatypeHelper.safeTrimOrNullString(httpRequest.getRequestURI()));
        httpRequest.setAttribute("personalAction", DatatypeHelper.safeTrimOrNullString(httpRequest.getRequestURI()));
        log.debug("url: " + DatatypeHelper.safeTrimOrNullString(httpRequest.getRequestURI()));

        if (userId!=null) httpRequest.setAttribute("userid", userId);

        try {
            RequestDispatcher dispatcher = httpRequest.getRequestDispatcher(jsp);
            dispatcher.forward(httpRequest, httpResponse);
            return;
        } catch (IOException ex) {
            log.error("Error forwarding Infocard Cardhandler request to jsp", ex);
            throw new ProfileException("Error forwarding Infocard Cardhandler request to jsp", ex);
        } catch (ServletException ex) {
            log.error("Error forwarding Infocard Cardhandler request to jsp", ex);
            throw new ProfileException("Error forwarding Infocard Cardhandler request to jsp", ex);
        }
    }

    /**
     * Creates a {@link LoginContext} an sends the request to the AuthenticationManager to authenticate the user.
     * 
     * @param inTransport inbound message transport
     * @param outTransport outbound message transport
     * 
     * @throws ProfileException thrown if there is a problem creating the login context and transferring control to the
     *             authentication manager
     */
    protected void performAuthentication(HTTPInTransport inTransport, HTTPOutTransport outTransport)
            throws ProfileException {

        log.debug("Infocard Cardhandler performAuthentication");
        HttpServletRequest httpRequest = ((HttpServletRequestAdapter) inTransport).getWrappedRequest();
        HttpServletResponse httpResponse = ((HttpServletResponseAdapter) outTransport).getWrappedResponse();

        LoginContext loginContext = new LoginContext();
        loginContext.setRelyingParty(relyingParty);
        loginContext.setAuthenticationEngineURL(authenticationManagerPath);
        loginContext.setProfileHandlerURL(HttpHelper.getRequestUriWithoutContext(httpRequest));

        RelyingPartyConfiguration rpConfig = getRelyingPartyConfiguration(relyingParty);
        ProfileConfiguration ssoConfig = rpConfig.getProfileConfiguration(InfocardCardConfiguration.PROFILE_ID);
        if (ssoConfig == null) {
            log.error("Infocard Cardhandler profile is not configured for relying party " + relyingParty);
            throw new ProfileException("Infocard Cardhandler profile is not configured for relying party "
                    + relyingParty);
        }
        loginContext.getRequestedAuthenticationMethods().add(rpConfig.getDefaultAuthenticationMethod());

        // httpRequest.setAttribute(LoginContext.LOGIN_CONTEXT_KEY, loginContext);
        HttpServletHelper.bindLoginContext(loginContext, httpRequest);

        try {
            RequestDispatcher dispatcher = httpRequest.getRequestDispatcher(authenticationManagerPath);
            dispatcher.forward(httpRequest, httpResponse);
            return;
        } catch (IOException ex) {
            log.error("Error forwarding Infocard Cardhandler request to AuthenticationManager", ex);
            throw new ProfileException("Error forwarding Infocard Cardhandler request to AuthenticationManager", ex);
        } catch (ServletException ex) {
            log.error("Error forwarding Infocard Cardhandler request to AuthenticationManager", ex);
            throw new ProfileException("Error forwarding Infocard Cardhandler request to AuthenticationManager", ex);
        }
    }

    /**
     * Redirect to error page
     * 
     * @param inTransport inbound message transport
     * @param outTransport outbound message transport
     * @param jsp error page
     * @param msg error message
     * 
     * @throws ProfileException thrown if there is a problem 
     */
    protected void performErrorRedirect(HTTPInTransport inTransport, HTTPOutTransport outTransport, String jsp, String error)
            throws ProfileException {

        log.debug("Infocard Cardhandler performErrorRedirect: " + error);
        HttpServletRequest httpRequest = ((HttpServletRequestAdapter) inTransport).getWrappedRequest();
        HttpServletResponse httpResponse = ((HttpServletResponseAdapter) outTransport).getWrappedResponse();
        String context = DatatypeHelper.safeTrimOrNullString(httpRequest.getContextPath());
        if (httpRequest == null) context = "/";

        httpRequest.setAttribute("error", error);

        try {
            RequestDispatcher dispatcher = httpRequest.getRequestDispatcher(jsp);
            dispatcher.forward(httpRequest, httpResponse);
            return;
        } catch (Exception ex) {
            log.error("Error forwarding error", ex);
            throw new ProfileException("Error forwarding error", ex);
        }
    }

    /**
     * Creates a response to the Infocard Cardhandler and sends a card to the user.
     * 
     * @param inTransport inbound message transport
     * @param outTransport outbound message transport
     * 
     * @throws ProfileException thrown if the response can not be created 
     */
    protected void completeCardRequest(HTTPInTransport inTransport, HTTPOutTransport outTransport)
            throws ProfileException {

        HttpServletRequest httpRequest = ((HttpServletRequestAdapter) inTransport).getWrappedRequest();
        BaseSAMLProfileRequestContext requestContext = buildRequestContext(inTransport, outTransport);
        InfocardCardConfiguration profileConfig = (InfocardCardConfiguration) requestContext.getProfileConfiguration();

        try {
            Element icardCard = buildCardResponse(inTransport, requestContext);

            // sign card
            Canonicalizer c = Canonicalizer.getInstance(Canonicalizer.ALGO_ID_C14N_OMIT_COMMENTS);
            X509Credential creds = (X509Credential) requestContext.getOuboundSAMLMessageSigningCredential();
            Element signedCard = signResponse(icardCard, creds.getPrivateKey(), creds.getEntityCertificateChain());

            // send card 
            String stringCard = new String(c.canonicalizeSubtree(signedCard), "UTF-8");
            HTTPTransportUtils.setContentType(outTransport, "application/soap+xml; charset=UTF-8");
            OutputStreamWriter writer = new OutputStreamWriter(outTransport.getOutgoingStream());
            writer.write(stringCard);
            writer.flush();
            log.debug("infocard card completed");

            // writeAuditLogEntry(requestContext);

        } catch (Exception e) {
            log.debug("completeCardRequest, exception: ", e);
        }

    }

    /**
     * Creates an authentication request context from the current environmental information.
     * 
     * @param loginContext current login context
     * @param in inbound transport
     * @param out outbount transport
     * 
     * @return created authentication request context
     * 
     * @throws ProfileException thrown if there is a problem creating the context
     */

    protected BaseSAMLProfileRequestContext buildRequestContext(
            HTTPInTransport in, HTTPOutTransport out) throws ProfileException {
        BaseSAMLProfileRequestContext requestContext = new BaseSAMLProfileRequestContext();

        requestContext.setUserSession(getUserSession(in));
        requestContext.setInboundMessageTransport(in);
        requestContext.setOutboundMessageTransport(out);

        String relyingPartyId = relyingParty;

        RelyingPartyConfiguration rpConfig = getRelyingPartyConfiguration(relyingPartyId);
        if (rpConfig == null) {
            log.error("Unable to retrieve relying party configuration data for entity with ID {}", relyingPartyId);
            throw new ProfileException("Unable to retrieve relying party configuration data for entity with ID "
                    + relyingPartyId);
        }
        requestContext.setRelyingPartyConfiguration(rpConfig);

        InfocardCardConfiguration profileConfig = (InfocardCardConfiguration) rpConfig
                .getProfileConfiguration(InfocardCardConfiguration.PROFILE_ID);
        requestContext.setProfileConfiguration(profileConfig);

        if (profileConfig.getSigningCredential() != null) {
            requestContext.setOutboundSAMLMessageSigningCredential(profileConfig.getSigningCredential());
            log.debug("signing cred from profile config");
        } else if (rpConfig.getDefaultSigningCredential() != null) {
            requestContext.setOutboundSAMLMessageSigningCredential(rpConfig.getDefaultSigningCredential());
            log.debug("signing cred from default config");
        }

        String assertingPartyId = rpConfig.getProviderId();
        requestContext.setLocalEntityId(assertingPartyId);
        requestContext.setOutboundMessageIssuer(assertingPartyId);
        return requestContext;
    }



    /**
     * Builds the card statement for the authenticated principal.
     * 
     * @param requestContext current request context
     * 
     * @return the created card
     * 
     * @throws ProfileException thrown if the authentication statement can not be created
     */
    protected Element buildCardResponse(HTTPInTransport inTransport, BaseSAMLProfileRequestContext requestContext)
            throws ProfileException {

        HttpServletRequest httpRequest = ((HttpServletRequestAdapter) inTransport).getWrappedRequest();
        InfocardCardConfiguration profileConfig = (InfocardCardConfiguration) requestContext.getProfileConfiguration();
        log.debug(".. buildCardResponse, user=" + userId + ", Cardname=" + cardName);

        try {
            BasicParserPool parserPool = new BasicParserPool();
            Document doc = parserPool.newDocument();

            String typeExtension = "pw";
            if (cardPPID!=null) typeExtension = "pc";

            // Build the card body.

            Element ic = doc.createElementNS(XMLNS_IC, "ic:InformationCard");
            ic.setAttributeNS(XMLConstants.XMLNS_NS,"xmlns:ic", XMLNS_IC);
            ic.setAttributeNS(XMLConstants.XMLNS_NS,"xmlns:wsa", XMLNS_WSA);
            ic.setAttributeNS(XMLConstants.XMLNS_NS,"xmlns:wst", XMLNS_WST);
            ic.setAttributeNS(XMLConstants.XMLNS_NS,"xmlns:wsx", XMLNS_WSX);
            ic.setAttribute("xml:lang", "en-us");

            if (doc.getDocumentElement()==null)
                doc.appendChild(ic);
            else
                doc.replaceChild(ic, doc.getDocumentElement());

            // add the card reference.  id can include username
            Element icr = doc.createElementNS(XMLNS_IC, "ic:InformationCardReference");

            Element cardid = doc.createElementNS(XMLNS_IC, "ic:CardId");
            MessageFormat idfmt = new MessageFormat(profileConfig.getCardId());
            String[] args = {userId};
            cardid.appendChild(doc.createTextNode(idfmt.format(args) + ":" + typeExtension));
            icr.appendChild(cardid);

            Element cardver = doc.createElementNS(XMLNS_IC, "ic:CardVersion");
            cardver.appendChild(doc.createTextNode(profileConfig.getCardVersion()));
            icr.appendChild(cardver);
            ic.appendChild(icr);

            // add the card name
            Element cardname = doc.createElementNS(XMLNS_IC, "ic:CardName");
            cardname.appendChild(doc.createTextNode(cardName));
            ic.appendChild(cardname);

            // add the card image

            Element cardImg = genCardImage(doc, profileConfig.getCardImageGenerator(), userId);
            if (cardImg!=null) ic.appendChild(cardImg);

            // add the issuer
            Element issuer = doc.createElementNS(XMLNS_IC, "ic:Issuer");
            issuer.appendChild(doc.createTextNode(relyingParty));
            ic.appendChild(issuer);

            // add the time
            Element issuet = doc.createElementNS(XMLNS_IC, "ic:TimeIssued");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Calendar cal = Calendar.getInstance(); // today
            issuet.appendChild(doc.createTextNode(sdf.format(cal.getTime())));
            ic.appendChild(issuet);


            // add the token service list
            Element tsl = doc.createElementNS(XMLNS_IC, "ic:TokenServiceList");
            Element ts = doc.createElementNS(XMLNS_IC, "ic:TokenService");
            Element epr = doc.createElementNS(XMLNS_WSA, "wsa:EndpointReference");
            Element addr = doc.createElementNS(XMLNS_WSA, "wsa:Address");
            addr.appendChild(doc.createTextNode(profileConfig.getStsAddress()));
            epr.appendChild(addr);

            Element meta = doc.createElementNS(XMLNS_WSA, "wsa:Metadata");
            Element metax = doc.createElementNS(XMLNS_WSX, "wsx:Metadata");
            Element metasec = doc.createElementNS(XMLNS_WSX, "wsx:MetadataSection");
            Element metaref = doc.createElementNS(XMLNS_WSX, "wsx:MetadataReference");
            Element metaaddr = doc.createElementNS(XMLNS_WSA, "wsa:Address");
            metaaddr.appendChild(doc.createTextNode(profileConfig.getMexAddress() + "/" + typeExtension));
            metaref.appendChild(metaaddr);
            metasec.appendChild(metaref);
            metax.appendChild(metasec);
            meta.appendChild(metax);
            epr.appendChild(meta);

            ts.appendChild(epr);

            // add the credential info
            Element uc = doc.createElementNS(XMLNS_IC, "ic:UserCredential");
            if (cardPPID!=null) {
               log.debug(".. building a PC card: " + cardPPID);
               Element ucs = doc.createElementNS(XMLNS_IC, "ic:SelfIssuedCredential");
               Element ucp = doc.createElementNS(XMLNS_IC, "ic:PrivatePersonalIdentifier");
               ucp.appendChild(doc.createTextNode(cardPPID));
               ucs.appendChild(ucp);
               uc.appendChild(ucs);
            } else {
               log.debug(".. building a PW card");
               Element uch = doc.createElementNS(XMLNS_IC, "ic:DisplayCredentialHint");
               uch.appendChild(doc.createTextNode("Enter your UW NetID and password"));
               uc.appendChild(uch);
               Element upc = doc.createElementNS(XMLNS_IC, "ic:UsernamePasswordCredential");
               Element un = doc.createElementNS(XMLNS_IC, "ic:Username");
               un.appendChild(doc.createTextNode(userId));
               upc.appendChild(un);
               uc.appendChild(upc);
            }
            ts.appendChild(uc);
            tsl.appendChild(ts);
            ic.appendChild(tsl);


            // add the supported token types

            Element sttl = doc.createElementNS(XMLNS_IC, "ic:SupportedTokenTypeList");
            Element tt = doc.createElementNS(XMLNS_WST, "wst:TokenType");
            tt.appendChild(doc.createTextNode(SAMLConstants.SAML1_NS));
            sttl.appendChild(tt);
            tt = doc.createElementNS(XMLNS_WST, "wst:TokenType");
            tt.appendChild(doc.createTextNode(SAMLConstants.SAML20_NS));
            sttl.appendChild(tt);
            ic.appendChild(sttl);

            // add the supported claims

            Element sctl = doc.createElementNS(XMLNS_IC, "ic:SupportedClaimTypeList");
            Map <String, InfocardClaim> claims = profileConfig.getSupportedClaims();

            for (InfocardClaim claim : claims.values()) {
               log.debug(".. adding: " + claim.uri);
               sctl.appendChild(supportedClaim(doc, claim.uri, claim.displayName));
            }
            ic.appendChild(sctl);

            // require applies-to
            Element applies = doc.createElementNS(XMLNS_IC, "ic:RequireAppliesTo");
            applies.setAttribute("Optional", "false");
            ic.appendChild(applies);

            // add the privacy notice

            Element pn = doc.createElementNS(XMLNS_IC, "ic:PrivacyNotice");
            pn.appendChild(doc.createTextNode(profileConfig.getPrivacyNotice()));
            ic.appendChild(pn);

          return (ic);

          } catch (Exception e) {
              throw new ProfileException("card build error:" +  e);
          }
     }

     Element supportedClaim(Document doc, String uri, String disp) {
          Element sct = doc.createElementNS(XMLNS_IC, "ic:SupportedClaimType");
          sct.setAttribute("Uri", uri);
          Element dt = doc.createElementNS(XMLNS_IC, "ic:DisplayTag");
          dt.appendChild(doc.createTextNode(disp));
          sct.appendChild(dt);
          return (sct);
     }


   /* Sign the card */

   Element signResponse(Element ic,  Key k, Collection certs) {
  
     try {
        Canonicalizer c = Canonicalizer.getInstance(Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);

       XMLSignature sig = new XMLSignature(ic.getOwnerDocument(), "", 
                   XMLSignature.ALGO_ID_SIGNATURE_RSA_SHA1,
                   Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);


         Transforms transforms = new Transforms(sig.getDocument());
         transforms.addTransform(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);

         sig.addDocument("#_Object_InformationCard", transforms, MessageDigestAlgorithm.ALGO_ID_DIGEST_SHA1);

            // Add any X.509 certificates provided.
            X509Data x509 = new X509Data(ic.getOwnerDocument());
            if (certs!=null)
            {
                int count = 0;
                Iterator i=certs.iterator();
                while (i.hasNext())
                {
                    Object cert=i.next();
                    // if (cert instanceof X509Certificate) {
                        x509.addCertificate((X509Certificate)cert);
                    // }
                    count++;
                }
            }
            if (x509.lengthCertificate()>0)
            {
                KeyInfo keyinfo = new KeyInfo(ic.getOwnerDocument());
                keyinfo.add(x509);
                sig.getElement().appendChild(keyinfo.getElement());
            }

         ObjectContainer obj = new ObjectContainer(ic.getOwnerDocument());
         obj.appendChild(ic);
         obj.setId("_Object_InformationCard");
         sig.appendObject(obj);

         try {
             sig.sign(k);
         } catch (XMLSignatureException ex) {
            log.error(".. sign sig exception: " + ex);
         }
         //  log.debug(".. sign ok");
         return (sig.getElement());

    } catch (Exception ex) {
       log.error(".. sign: signature exception: " + ex);
       return (null);
    }

   }

   // Make the card's background image
   // Any way to do this in java?
   protected Element genCardImage(Document doc, String gen, String username) {

       String genImage =  gen + " " + username;
       ByteArrayOutputStream out = new ByteArrayOutputStream();

       log.debug(".. genimage, prog = " + genImage);

       try {
          Runtime rt = Runtime.getRuntime();
          Process p = rt.exec( genImage );
          BufferedInputStream imageStream = new BufferedInputStream(p.getInputStream());
          byte[] buf = new byte[1024];
          int nb;
          while ((nb=imageStream.read(buf)) >=0 ) {
             out.write(buf, 0, nb);
          }
          p.waitFor();
          imageStream.close();
       } catch(Exception e){
          System.err.println(e.getMessage());
          e.printStackTrace(System.err);
          return (null);
       }
       log.debug(".. genimage, done" );
       byte[] img = out.toByteArray();
       Element cardimg = doc.createElementNS(XMLNS_IC, "ic:CardImage");
       cardimg.setAttribute("MimeType", "image/png");
       String enc = Base64.encodeBytes(img);
       cardimg.appendChild(doc.createTextNode(enc));

     return(cardimg);
   }


    /**
     * Build a status message, with an optional second-level failure message.
     *
     * @param topLevelCode top-level status code
     * @param secondLevelCode second-level status code
     * @param failureMessage An optional second-level failure message
     *
     * @return a Status object.
     */
    protected Status buildStatus(QName topLevelCode, QName secondLevelCode, String failureMessage) {
        Status status = statusBuilder.buildObject();

        StatusCode statusCode = statusCodeBuilder.buildObject();
        statusCode.setValue(topLevelCode);
        status.setStatusCode(statusCode);

        if (secondLevelCode != null) {
            StatusCode secondLevelStatusCode = statusCodeBuilder.buildObject();
            secondLevelStatusCode.setValue(secondLevelCode);
            statusCode.setStatusCode(secondLevelStatusCode);
        }

        if (failureMessage != null) {
            StatusMessage msg = statusMessageBuilder.buildObject();
            msg.setMessage(failureMessage);
            status.setStatusMessage(msg);
        }

        return status;
    }

    /* We don't have a relying party endpoint */
    protected Endpoint selectEndpoint(BaseSAMLProfileRequestContext requestContext) {
        return null;
    }
    protected void populateUserInformation(BaseSAMLProfileRequestContext requestContext) {
    }
    protected void populateSAMLMessageInformation(BaseSAMLProfileRequestContext requestContext) throws ProfileException {
    }

}
