/*
 * Copyright [2005] [University Corporation for Advanced Internet Development, Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.provider;

import java.io.IOException;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.opensaml.SAMLAssertion;
import org.opensaml.SAMLAttribute;
import org.opensaml.SAMLAttributeStatement;
import org.opensaml.SAMLAuthenticationStatement;
import org.opensaml.SAMLException;
import org.opensaml.SAMLNameIdentifier;
import org.opensaml.SAMLRequest;
import org.opensaml.SAMLResponse;
import org.opensaml.SAMLStatement;
import org.opensaml.SAMLSubject;
import org.opensaml.artifact.Artifact;
import org.w3c.dom.Element;

import edu.internet2.middleware.shibboleth.aa.AAException;
import edu.internet2.middleware.shibboleth.common.LocalPrincipal;
import edu.internet2.middleware.shibboleth.common.NameIdentifierMappingException;
import edu.internet2.middleware.shibboleth.common.RelyingParty;
import edu.internet2.middleware.shibboleth.common.ShibbolethConfigurationException;
import edu.internet2.middleware.shibboleth.idp.IdPProtocolHandler;
import edu.internet2.middleware.shibboleth.idp.IdPProtocolSupport;
import edu.internet2.middleware.shibboleth.idp.InvalidClientDataException;
import edu.internet2.middleware.shibboleth.metadata.Endpoint;
import edu.internet2.middleware.shibboleth.metadata.EntityDescriptor;
import edu.internet2.middleware.shibboleth.metadata.SPSSODescriptor;

/**
 * <code>ProtocolHandler</code> implementation that responds to SSO flows as specified in "E-Authentication Interface
 * Specifications for the SAML Artifact Profile ".
 * 
 * @author Walter Hoehn
 */
public class E_AuthSSOHandler extends SSOHandler implements IdPProtocolHandler {

	private static Logger log = Logger.getLogger(E_AuthSSOHandler.class.getName());
	private final static String E_AUTH_NAMEID = "urn:oasis:names:tc:SAML:1.0:assertion#X509SubjectName";
	private final static String E_AUTH_ATTR_NAMESPACE = "http://eauthentication.gsa.gov/federated/attribute";
	private String eAuthPortal = "http://eauth.firstgov.gov/service/select";
	private String eAuthError = "http://eauth.firstgov.gov/service/error";
	private String csid;
	private int defaultAssuranceLevel = 1;

	/**
	 * Required DOM-based constructor.
	 */
	public E_AuthSSOHandler(Element config) throws ShibbolethConfigurationException {

		super(config);
		csid = config.getAttribute("csid");
		if (csid == null || csid.equals("")) {
			log.error("(csid) attribute is required for the " + getHandlerName() + "protocol handler.");
			throw new ShibbolethConfigurationException("Unable to initialize protocol handler.");
		}

		String portal = config.getAttribute("eAuthPortal");
		if (portal != null && !portal.equals("")) {
			eAuthPortal = portal;
		}

		String error = config.getAttribute("eAuthError");
		if (error != null && !error.equals("")) {
			eAuthError = portal;
		}

		String rawAssurance = config.getAttribute("defaultAssuranceLevel");
		if (rawAssurance != null && !rawAssurance.equals("")) {
			try {
				defaultAssuranceLevel = Integer.parseInt(rawAssurance);
				if (defaultAssuranceLevel < 1 || defaultAssuranceLevel > 5) { throw new NumberFormatException(); }
			} catch (NumberFormatException e) {
				log.error("E-Authentication (defaultAssuranceLevel) attribute must be an integer between 1 & 5.");
				throw new ShibbolethConfigurationException("Unable to initialize protocol handler.");
			}
		}
	}

	/*
	 * @see edu.internet2.middleware.shibboleth.idp.IdPProtocolHandler#getHandlerName()
	 */
	public String getHandlerName() {

		return "E-Authentication SSO";
	}

	/*
	 * @see edu.internet2.middleware.shibboleth.idp.IdPProtocolHandler#processRequest(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse, org.opensaml.SAMLRequest,
	 *      edu.internet2.middleware.shibboleth.idp.IdPProtocolSupport)
	 */
	public SAMLResponse processRequest(HttpServletRequest request, HttpServletResponse response,
			SAMLRequest samlRequest, IdPProtocolSupport support) throws SAMLException, IOException, ServletException {

		// Sanity check
		if (samlRequest != null) {
			log.error("Protocol Handler received a SAML Request, but is unable to handle it.");
			throw new SAMLException(SAMLException.RESPONDER, "General error processing request.");
		}

		// If no aaid is specified, redirect to the eAuth portal
		if (request.getParameter("aaid") == null || request.getParameter("aaid").equals("")) {
			log.debug("Received an E-Authentication request with no (aaid) parameter.  "
					+ "Redirecting to the E-Authentication portal.");
			response.sendRedirect(eAuthPortal + "?csid=" + csid);
			return null;
		}

		// FUTURE at some point this needs to be integrated with SAML2 session reset
		// If session reset was requested, delete the session and re-direct back
		// Note, this only works with servlet form-auth
		String reAuth = request.getParameter("sessionreset");
		if (reAuth != null && reAuth.equals("1")) {
			log.debug("E-Authebtication session reset requested.");
			Cookie session = new Cookie("JSESSIONID", null);
			session.setMaxAge(0);
			response.addCookie(session);
			if (support.getIdPConfig().getAuthHeaderName().equalsIgnoreCase("COOKIE")) {
				Cookie ssocookie = new Cookie(support.getIdPConfig().getCookieName(), null);
				ssocookie.setMaxAge(0);
				response.addCookie(ssocookie);
			}

			response.sendRedirect(request.getRequestURI()
					+ (request.getQueryString() != null ? "?"
							+ request.getQueryString().replaceAll("(^sessionreset=1&?|&?sessionreset=1)", "") : ""));
			return null;
		}
		// Sanity check
		try {
			validateEngineData(request);
		} catch (InvalidClientDataException e) {
			throw new SAMLException(SAMLException.RESPONDER, e.getMessage());
		}

		// Get the authN info (moved up to SSOHandler to support SSO code from OSU)
		StringBuffer username = new StringBuffer();
		StringBuffer authenticationMethod = new StringBuffer();
		if (!getRemoteUser(username, authenticationMethod, request, response, support)) {
			// Had to respond to client, so bail.
			return null;
		}

		if (username.length() == 0) {
			throw new SAMLException(
				SAMLException.RESPONDER,
				"Unauthenticated principal. This protocol handler requires that authentication information be "
						+ "provided from the servlet container."); }
		LocalPrincipal principal = new LocalPrincipal(username.toString());

		// Select the appropriate Relying Party configuration for the request
		String remoteProviderId = request.getParameter("aaid");
		log.debug("Remote provider has identified itself as: (" + remoteProviderId + ").");
		RelyingParty relyingParty = support.getServiceProviderMapper().getRelyingParty(remoteProviderId);

		if (relyingParty == null || relyingParty.isLegacyProvider()) {
			log.error("Unable to identify appropriate relying party configuration.");
			eAuthError(response, 30, remoteProviderId, csid);
			return null;
		}

		// Lookup the provider in the metadata
		EntityDescriptor entity = support.lookup(relyingParty.getProviderId());
		if (entity == null) {
			log.error("No metadata found for EAuth provider.");
			eAuthError(response, 30, remoteProviderId, csid);
			return null;
		}
		SPSSODescriptor role = entity.getSPSSODescriptor("urn:oasis:names:tc:SAML:1.1:protocol");
		if (role == null) {
			log.error("Inappropriate metadata for EAuth provider.");
			eAuthError(response, 30, remoteProviderId, csid);
			return null;
		}

		// The EAuth profile requires metadata, since the assertion consumer is not supplied as a request parameter
		// Pull the consumer URL from the metadata
		Iterator endpoints = role.getAssertionConsumerServiceManager().getEndpoints();
		if (endpoints == null || !endpoints.hasNext()) {
			log.error("Inappropriate metadata for provider: no roles specified.");
			eAuthError(response, 30, remoteProviderId, csid);
			return null;
		}
		String consumerURL = ((Endpoint) endpoints.next()).getLocation();
		log.debug("Assertion Consumer URL provider: " + consumerURL);

		// Create SAML Name Identifier & Subject
		SAMLNameIdentifier nameId;
		try {
			nameId = getNameIdentifier(support.getNameMapper(), principal, relyingParty, entity);
			if (!nameId.getFormat().equals(E_AUTH_NAMEID)) {
				log.error("SAML Name Identifier format is inappropriate for use with E-Authentication provider.  Was ("
						+ nameId.getFormat() + ").  Expected (" + E_AUTH_NAMEID + ").");
				eAuthError(response, 60, remoteProviderId, csid);
				return null;
			}
		} catch (NameIdentifierMappingException e) {
			log.error("Error converting principal to SAML Name Identifier: " + e);
			eAuthError(response, 60, remoteProviderId, csid);
			return null;
		}

		String[] confirmationMethods = {SAMLSubject.CONF_ARTIFACT};
		SAMLSubject authNSubject = new SAMLSubject(nameId, Arrays.asList(confirmationMethods), null, null);

		// Determine AuthN method
		if (authenticationMethod.length() == 0) {
			authenticationMethod.append(relyingParty.getDefaultAuthMethod().toString());
			log.debug("User was authenticated via the default method for this relying party (" + authenticationMethod
					+ ").");
		} else {
			log.debug("User was authenticated via the method (" + authenticationMethod + ").");
		}

		String issuer = relyingParty.getIdentityProvider().getProviderId();

		log.info("Resolving attributes.");
		List attributes = null;
		try {
			attributes = Arrays.asList(support.getReleaseAttributes(principal, relyingParty, relyingParty
					.getProviderId(), null));
		} catch (AAException e1) {
			log.error("Error resolving attributes: " + e1);
			eAuthError(response, 90, remoteProviderId, csid);
			return null;
		}
		log.info("Found " + attributes.size() + " attribute(s) for " + principal.getName());

		// Bail if we didn't get any attributes
		if (attributes == null || attributes.size() < 1) {
			log.error("Attribute resolver did not return any attributes. "
					+ " The E-Authentication profile's minimum attribute requirements were not met.");
			eAuthError(response, 60, remoteProviderId, csid);
			return null;

			// OK, we got attributes back, package them as required for eAuth and combine them with the authN data in an
			// assertion
		} else {
			try {
				attributes = repackageForEauth(attributes);
			} catch (SAMLException e) {
				eAuthError(response, 90, remoteProviderId, csid);
				return null;
			}

			// Put all attributes into an assertion
			try {
				SAMLStatement attrStatement = new SAMLAttributeStatement((SAMLSubject) authNSubject.clone(), attributes);
				SAMLStatement[] statements = {
						new SAMLAuthenticationStatement(authNSubject, authenticationMethod.toString(), getAuthNTime(request),
								request.getRemoteAddr(), null, null), attrStatement};
				SAMLAssertion assertion = new SAMLAssertion(issuer, new Date(System.currentTimeMillis()), new Date(
						System.currentTimeMillis() + 300000), null, null, Arrays.asList(statements));
				if (log.isDebugEnabled()) {
					log.debug("Dumping generated SAML Assertion:" + System.getProperty("line.separator")
							+ assertion.toString());
				}

				// Redirect to agency application
				try {
					respondWithArtifact(response, support, consumerURL, principal, assertion, nameId, role,
							relyingParty);
					return null;
				} catch (SAMLException e) {
					eAuthError(response, 90, remoteProviderId, csid);
					return null;
				}

			} catch (CloneNotSupportedException e) {
				log.error("An error was encountered while generating assertion: " + e);
				eAuthError(response, 90, remoteProviderId, csid);
				return null;
			}
		}
	}

	private void respondWithArtifact(HttpServletResponse response, IdPProtocolSupport support, String acceptanceURL,
			Principal principal, SAMLAssertion assertion, SAMLNameIdentifier nameId, SPSSODescriptor descriptor,
			RelyingParty relyingParty) throws SAMLException, IOException {

		// Create artifacts for each assertion
		ArrayList artifacts = new ArrayList();

		artifacts.add(support.getArtifactMapper().generateArtifact(assertion, relyingParty));

		String target = relyingParty.getDefaultTarget();
		if (target == null || target.equals("")) {
			log.error("No default target found.  Relying Party elements corresponding to "
					+ "E-Authentication providers must have a (defaultTarget) attribute specified.");
			throw new SAMLException(SAMLException.RESPONDER, "General error processing request.");
		}

		// Assemble the query string
		StringBuffer destination = new StringBuffer(acceptanceURL);
		destination.append("?TARGET=");
		destination.append(URLEncoder.encode(target, "UTF-8"));
		Iterator iterator = artifacts.iterator();
		StringBuffer artifactBuffer = new StringBuffer(); // Buffer for the transaction log

		// Construct the artifact query parameter
		while (iterator.hasNext()) {
			Artifact artifact = (Artifact) iterator.next();
			artifactBuffer.append("(" + artifact.encode() + ")");
			destination.append("&SAMLart=");
			destination.append(URLEncoder.encode(artifact.encode(), "UTF-8"));
		}

		log.debug("Redirecting to (" + destination.toString() + ").");
		response.sendRedirect(destination.toString()); // Redirect to the artifact receiver
		support.getTransactionLog().info(
				"Assertion artifact(s) (" + artifactBuffer.toString() + ") issued to E-Authentication provider ("
						+ relyingParty.getProviderId() + ") on behalf of principal (" + principal.getName()
						+ "). Name Identifier: (" + nameId.getName() + "). Name Identifier Format: ("
						+ nameId.getFormat() + ").");

	}

	private List repackageForEauth(List attributes) throws SAMLException {

		ArrayList writeable = new ArrayList(attributes);
		// Bail if we didn't get a commonName, because it is required by the profile
		SAMLAttribute commonName = getAttribute("commonName", writeable);
		if (commonName == null) {
			log.error("The attribute resolver did not return a (commonName) attribute, "
					+ " which is required for the E-Authentication profile.");
			throw new SAMLException(SAMLException.RESPONDER, "General error processing request.");
		} else if (!E_AUTH_ATTR_NAMESPACE.equals(commonName.getNamespace())) {
			log.warn("The (commonName) attribute seems to have an incorrect namespace set.  It should be ("
					+ E_AUTH_ATTR_NAMESPACE + "), but it is currently set to " + commonName.getNamespace() + ").");
		}
		writeable.add(new SAMLAttribute("csid", E_AUTH_ATTR_NAMESPACE, null, 0, Arrays.asList(new String[]{csid})));

		// Pull assurance level from the resolver, if it is available
		// If it isn't, use the handler default
		SAMLAttribute assuranceLevel = getAttribute("assuranceLevel", writeable);
		if (assuranceLevel == null) {
			writeable.add(new SAMLAttribute("assuranceLevel", "http://eauthentication.gsa.gov/federated/attribute",
					null, 0, Arrays.asList(new String[]{Integer.toString(defaultAssuranceLevel)})));
		} else {
			log.debug("Using user-specifc assuranceLevel override.");
		}

		return writeable;
	}

	private SAMLAttribute getAttribute(String name, List attributes) {

		Iterator iterator = attributes.iterator();
		while (iterator.hasNext()) {
			SAMLAttribute attribute = (SAMLAttribute) iterator.next();
			if (attribute.getName().equals(name)) { return attribute; }
		}
		return null;
	}

	private void eAuthError(HttpServletResponse response, int code, String aaid, String csid) throws IOException {

		log.info("Redirecting to E-Authentication error page.");
		response.sendRedirect(eAuthError + "?aaid=" + aaid + "&csid=" + csid + "&errcode=" + code);
	}
}