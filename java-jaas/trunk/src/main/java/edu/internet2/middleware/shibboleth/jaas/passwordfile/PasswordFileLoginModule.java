/*
 * Copyright 2008 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.jaas.passwordfile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.security.Principal;
import java.util.Map;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.jaas.BasicPrincipal;

/**
 * A JAAS login module that validates a username/password against a file.
 * 
 * This login module requires an option called "passwordFile" which is the location of the password file that will be
 * used. The provided login handler must also be an instance of {@link javax.security.auth.callback.NameCallback} and
 * {@link javax.security.auth.callback.PasswordCallback}.
 * 
 * The password file is formatted such that each line contains a user name then the plaintext password, separated by a
 * space.
 * 
 * The password is not stored within the Subject's credential sets.
 */
public class PasswordFileLoginModule implements LoginModule {

    /** Name of the JAAS login module option that gives the password file location. */
    public static final String PASSWORD_FILE_OPTION = "passwordFile";

    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(PasswordFileLoginModule.class);

    /** Subject to populate with information. */
    private Subject subject;

    /** Callback handler used to collect authentication material. */
    private CallbackHandler callbackHandler;

    /** Password file location. */
    private String passwordFilePath;

    /** Principal created for the user. */
    private Principal principal;

    /** Constructor. */
    public PasswordFileLoginModule() {

    }

    /** {@inheritDoc} */
    public void initialize(Subject newSubject, CallbackHandler newCallbackHandler, Map<String, ?> sharedState,
            Map<String, ?> options) {
        this.subject = newSubject;
        this.callbackHandler = newCallbackHandler;

        passwordFilePath = (String) options.get(PASSWORD_FILE_OPTION);
    }

    /** {@inheritDoc} */
    public boolean login() throws LoginException {
        log.debug("Beginning password based login process.");
        NameCallback nameCb = new NameCallback("Enter user name: ");
        PasswordCallback passCb = new PasswordCallback("Enter password: ", false);

        try {
            if (callbackHandler != null) {
                callbackHandler.handle(new Callback[] { nameCb, passCb });

                if (authenticate(nameCb.getName(), new String(passCb.getPassword()))) {
                    principal = new BasicPrincipal(nameCb.getName());
                    return true;
                }
            }
        } catch (IOException e) {
            log.error("Unable to process callbacks", e);
            throw new LoginException(e.toString());
        } catch (UnsupportedCallbackException e) {
            log.error("Unsupported callbacks", e);
            throw new LoginException(e.toString());
        }

        return false;
    }

    /** {@inheritDoc} */
    public boolean commit() throws LoginException {
        log.debug("Committing login transaction");
        subject.getPrincipals().add(principal);
        return true;
    }

    /** {@inheritDoc} */
    public boolean logout() throws LoginException {
        subject.getPrincipals().remove(principal);
        return false;
    }

    /** {@inheritDoc} */
    public boolean abort() throws LoginException {
        subject.getPrincipals().remove(principal);
        return true;
    }

    /**
     * Authenticates the user against the password file.
     * 
     * @param user user to authentication
     * @param password user's password
     * 
     * @return true if the user is authenticated
     * 
     * @throws LoginException thrown if the user is not authenticated
     */
    protected boolean authenticate(String user, String password) throws LoginException {
        log.debug("Attempting to authenticate {} against htpasswd file {}", user, passwordFilePath);
        BufferedReader passwordReader = null;
        try {
            passwordReader = new BufferedReader(new FileReader(passwordFilePath));

            String passwordEntry = passwordReader.readLine();
            String[] entryParts;
            while (passwordEntry != null) {
                entryParts = passwordEntry.split(" ");
                if (entryParts[0].equals(user)) {
                    log.debug("Located user {} in password file, attempting to validate password", user);
                    return entryParts[1].equals(password);
                }

                passwordEntry = passwordReader.readLine();
            }

            log.debug("Unable to authenticate user {}", user);
            throw new LoginException("Unable to authenticate user");
        } catch (IOException e) {
            log.error("Unable to read password file", e);
            throw new LoginException(e.getMessage());
        } finally {
            try {
                passwordReader.close();
            } catch (IOException e) {
                // do nothing
            }
        }
    }
}