/*
 * Copyright 2008 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.jaas.passwordfile;

import edu.internet2.middleware.shibboleth.jaas.BasicPrincipal;

/**
 * An implementation of {@link java.security.Principal} meant to represents a role that indicates the user was
 * authenticated by means of a password.
 */
public class PasswordRole extends BasicPrincipal {

    /**
     * Constructor.
     * 
     * @param newName name of the role
     */
    public PasswordRole(String newName) {
        super(newName);
    }

    /** {@inheritDoc} */
    public String toString() {
        StringBuffer sb = new StringBuffer("PasswordRole[").append(getName()).append("]");
        return sb.toString();
    }
}