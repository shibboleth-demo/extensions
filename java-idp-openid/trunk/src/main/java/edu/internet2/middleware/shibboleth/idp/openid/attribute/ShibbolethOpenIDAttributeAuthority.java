/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.attribute;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.opensaml.saml2.core.AttributeQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import edu.internet2.middleware.openid.extensions.ax.FetchRequest;
import edu.internet2.middleware.openid.extensions.ax.FetchResponse;
import edu.internet2.middleware.openid.extensions.sreg.SimpleRegistrationRequest;
import edu.internet2.middleware.openid.extensions.sreg.SimpleRegistrationResponse;
import edu.internet2.middleware.shibboleth.common.attribute.AttributeRequestException;
import edu.internet2.middleware.shibboleth.common.attribute.BaseAttribute;
import edu.internet2.middleware.shibboleth.common.attribute.filtering.provider.ShibbolethAttributeFilteringEngine;
import edu.internet2.middleware.shibboleth.common.attribute.resolver.provider.ShibbolethAttributeResolver;
import edu.internet2.middleware.shibboleth.common.config.BaseService;
import edu.internet2.middleware.shibboleth.common.service.ServiceException;
import edu.internet2.middleware.shibboleth.idp.openid.profile.provider.OpenIDAuthenticationRequestContext;

/**
 * OpenID Attribute Authority.
 */
public class ShibbolethOpenIDAttributeAuthority extends BaseService implements
        OpenIDAttributeAuthority {

    /** Class logger. */
    private final Logger log = LoggerFactory
            .getLogger(ShibbolethOpenIDAttributeAuthority.class);

    /** Attribute resolver. */
    private ShibbolethAttributeResolver attributeResolver;

    /** To determine releasable attributes. */
    private ShibbolethAttributeFilteringEngine filteringEngine;

    /**
     * Constructor.
     * 
     * @param resolver attribute resolver
     */
    public ShibbolethOpenIDAttributeAuthority(
            ShibbolethAttributeResolver resolver) {
        attributeResolver = resolver;
    }

    /**
     * Gets the attribute resolver.
     * 
     * @return Returns the attributeResolver.
     */
    public ShibbolethAttributeResolver getAttributeResolver() {
        return attributeResolver;
    }

    /**
     * Gets the filtering engine.
     * 
     * @return Returns the filteringEngine.
     */
    public ShibbolethAttributeFilteringEngine getFilteringEngine() {
        return filteringEngine;
    }

    /**
     * Sets the attribute filtering engine.
     * 
     * @param engine attribute filtering engine
     */
    public void setFilteringEngine(ShibbolethAttributeFilteringEngine engine) {
        filteringEngine = engine;
    }

    /** {@inheritDoc} */
    public Map<String, BaseAttribute> getAttributes(
            OpenIDAuthenticationRequestContext requestContext)
            throws AttributeRequestException {
        HashSet<String> requestedAttributes = new HashSet<String>();

        // TODO get SReg attributes from the message

        // TODO get AX attributes from the message

        requestContext.setRequestedAttributes(requestedAttributes);

        // Resolve attributes
        // Map<String, BaseAttribute> attributes = attributeResolver.resolveAttributes(requestContext);
        return null;
    }

    /** {@inheritDoc} */
    protected void onNewContextCreated(ApplicationContext newServiceContext)
            throws ServiceException {
    }

    /** {@inheritDoc} */
    public FetchResponse buildAXResponse(FetchRequest request,
            Collection<BaseAttribute> attributes) {
        // TODO Auto-generated method stub
        return null;
    }

    /** {@inheritDoc} */
    public SimpleRegistrationResponse buildSRegResponse(
            SimpleRegistrationRequest request,
            Collection<BaseAttribute> attributes) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Gets the attribute IDs for those attributes requested in the AX query.
     * 
     * @param request incoming AX request
     * 
     * @return attribute IDs for those attributes requested in the attribute query
     */
    protected Set<String> getAttributeIds(FetchRequest request) {
        Set<String> queryAttributeIds = new HashSet<String>();

        return queryAttributeIds;
    }

    /** {@inheritDoc} */
    public String getAXAttributeByAttributeID(String id) {
        return null;
    }

    /** {@inheritDoc} */
    public String getAttributeIDByAXAttribute(String attribute) {
        return null;
    }

    /** {@inheritDoc} */
    public String getAttributeIDBySRegAttribute(String attribute) {
        return null;
    }

    /** {@inheritDoc} */
    public String getSRegAttributeByAttributeID(String id) {
        return null;
    }

}