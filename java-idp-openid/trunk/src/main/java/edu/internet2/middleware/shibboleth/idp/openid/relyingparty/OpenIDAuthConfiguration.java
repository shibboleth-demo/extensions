/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.relyingparty;

import org.opensaml.ws.security.SecurityPolicy;

import edu.internet2.middleware.shibboleth.common.relyingparty.ProfileConfiguration;
import edu.internet2.middleware.shibboleth.idp.openid.attribute.OpenIDAttributeAuthority;

/**
 * OpenID Authentication configuration settings.
 */
public class OpenIDAuthConfiguration implements ProfileConfiguration {

    /** ID for this profile configuration. */
    public static final String PROFILE_ID = "urn:mace:shibboleth:2.0:profiles:openid";

    /** Security policy for this profile. */
    private SecurityPolicy profileSecurityPolicy;

    /** Attribute authority to use. */
    private OpenIDAttributeAuthority attributeAuthority;

    /** Whether to use directed identifiers. */
    private boolean directedIdentfiers;

    /** Salt used to calculating directed identifiers. */
    private byte[] directedIdentifierSalt;

    /** {@inheritDoc} */
    public String getProfileId() {
        return PROFILE_ID;
    }

    /** {@inheritDoc} */
    public SecurityPolicy getSecurityPolicy() {
        return profileSecurityPolicy;
    }

    /**
     * Set the security policy of the profile.
     * 
     * @param policy the security policy
     */
    public void setSecurityPolicy(SecurityPolicy policy) {
        profileSecurityPolicy = policy;
    }

    /**
     * Gets the Attribute authority to use.
     * 
     * @return Attribute authority to use
     */
    public OpenIDAttributeAuthority getAttributeAuthority() {
        return attributeAuthority;
    }

    /**
     * Sets the Attribute authority to use.
     * 
     * @param authority Attribute authority to use
     */
    public void setAttributeAuthority(OpenIDAttributeAuthority authority) {
        attributeAuthority = authority;
    }

    /**
     * Get whether to use directed identifiers.
     * 
     * @return whether to use directed identifiers
     */
    public boolean getDirectedIdentifiers() {
        return directedIdentfiers;
    }

    /**
     * Set whether to use directed identifiers.
     * 
     * @param directed whether to use directed identifiers
     */
    public void setDirectedIdentifiers(boolean directed) {
        directedIdentfiers = directed;
    }

    /**
     * Get the salt used for calculating directed identifiers.
     * 
     * @return salt used for calculating directed identifiers
     */
    public byte[] getDirectedIdentifierSalt() {
        return directedIdentifierSalt;
    }

    /**
     * Set the salt used for calculating directed identifiers.
     * 
     * @param salt salt used for calculating directed identifiers
     */
    public void setDirectedIdentifierSalt(byte[] salt) {
        directedIdentifierSalt = salt;
    }

}