/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.config.relyingparty;

import org.opensaml.ws.security.SecurityPolicy;
import org.springframework.beans.factory.config.AbstractFactoryBean;

import edu.internet2.middleware.shibboleth.idp.openid.attribute.OpenIDAttributeAuthority;
import edu.internet2.middleware.shibboleth.idp.openid.relyingparty.OpenIDAuthConfiguration;

/**
 * Spring factory bean for OpenID Auth profile configurations.
 */
public class OpenIDAuthProfileConfigurationFactoryBean extends AbstractFactoryBean {

    /** Security policy for this profile. */
    private SecurityPolicy profileSecurityPolicy;

    /** Attribute authority for the profile configuration. */
    private OpenIDAttributeAuthority attributeAuthority;

    /** Use directed identifiers for this profile. */
    private boolean directedIdentifiers;

    /** Salt used to calculating directed identifiers. */
    private byte[] directedIdentifierSalt;

    /** {@inheritDoc} */
    protected Object createInstance() throws Exception {
        OpenIDAuthConfiguration configuration = new OpenIDAuthConfiguration();
        configuration.setSecurityPolicy(getProfileSecurityPolicy());
        configuration.setAttributeAuthority(attributeAuthority);
        configuration.setDirectedIdentifiers(directedIdentifiers);
        configuration.setDirectedIdentifierSalt(directedIdentifierSalt);
        return configuration;
    }

    /** {@inheritDoc} */
    public Class getObjectType() {
        return OpenIDAuthConfiguration.class;
    }

    /**
     * Gets the security policy for this profile.
     * 
     * @return security policy for this profile
     */
    public SecurityPolicy getProfileSecurityPolicy() {
        return profileSecurityPolicy;
    }

    /**
     * Sets the security policy for this profile.
     * 
     * @param policy security policy for this profile
     */
    public void setProfileSecurityPolicy(SecurityPolicy policy) {
        profileSecurityPolicy = policy;
    }

    /**
     * Gets the attribute authority for the profile configuration.
     * 
     * @return attribute authority for the profile configuration
     */
    public OpenIDAttributeAuthority getAttributeAuthority() {
        return attributeAuthority;
    }

    /**
     * Sets the attribute authority for the profile configuration.
     * 
     * @param authority attribute authority for the profile configuration
     */
    public void setAttributeAuthority(OpenIDAttributeAuthority authority) {
        attributeAuthority = authority;
    }

    /**
     * Gets whether directed identifiers are used for the profile configuration.
     * 
     * @return whether directed identifiers are used
     */
    public boolean getDirectedIdentifiers() {
        return directedIdentifiers;
    }

    /**
     * Sets whether directed identifiers are used for the profile configuration.
     * 
     * @param directed whether directed identifiers are used
     */
    public void setDirectedIdentifiers(boolean directed) {
        directedIdentifiers = directed;
    }

    /**
     * Get the salt used for calculating directed identifiers.
     * 
     * @return salt used for calculating directed identifiers
     */
    public byte[] getDirectedIdentifierSalt() {
        return directedIdentifierSalt;
    }

    /**
     * Set the salt used for calculating directed identifiers.
     * 
     * @param salt salt used for calculating directed identifiers
     */
    public void setDirectedIdentifierSalt(byte[] salt) {
        directedIdentifierSalt = salt;
    }

}