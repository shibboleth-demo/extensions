/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.openid.Configuration;
import edu.internet2.middleware.openid.DefaultBootstrap;
import edu.internet2.middleware.openid.common.OpenIDConstants;
import edu.internet2.middleware.openid.common.ParameterMap;
import edu.internet2.middleware.openid.common.OpenIDConstants.Parameter;
import edu.internet2.middleware.openid.common.impl.RandomIdentifierGenerator;
import edu.internet2.middleware.openid.message.AssociationRequest;
import edu.internet2.middleware.openid.message.AuthenticationRequest;
import edu.internet2.middleware.openid.message.ErrorResponse;
import edu.internet2.middleware.openid.message.MalformedMessageException;
import edu.internet2.middleware.openid.message.Message;
import edu.internet2.middleware.openid.message.VerifyRequest;
import edu.internet2.middleware.openid.message.encoding.EncodingException;
import edu.internet2.middleware.openid.message.encoding.impl.ServletRequestParameterMapDecoder;
import edu.internet2.middleware.openid.message.io.MessageUnmarshaller;
import edu.internet2.middleware.openid.message.io.UnmarshallingException;
import edu.internet2.middleware.openid.security.AssociationManager;
import edu.internet2.middleware.openid.security.impl.BasicAssociationBuilder;
import edu.internet2.middleware.openid.security.impl.IdentifierNonceGenerator;
import edu.internet2.middleware.openid.security.impl.InMemoryAssociationStore;
import edu.internet2.middleware.openid.security.impl.InMemoryNonceValidator;
import edu.internet2.middleware.openid.util.DatatypeHelper;
import edu.internet2.middleware.shibboleth.common.relyingparty.RelyingPartyConfigurationManager;
import edu.internet2.middleware.shibboleth.idp.openid.OpenIDProviderHelper;
import edu.internet2.middleware.shibboleth.idp.openid.authn.OpenIDLoginContext;
import edu.internet2.middleware.shibboleth.idp.openid.profile.OpenIDRequestContext;
import edu.internet2.middleware.shibboleth.idp.openid.profile.provider.AssociationRequestHandler;
import edu.internet2.middleware.shibboleth.idp.openid.profile.provider.AuthenticationRequestHandler;
import edu.internet2.middleware.shibboleth.idp.openid.profile.provider.BasicOpenIDRequestContext;
import edu.internet2.middleware.shibboleth.idp.openid.profile.provider.DiscoveryHandler;
import edu.internet2.middleware.shibboleth.idp.openid.profile.provider.VerifyRequestHandler;
import edu.internet2.middleware.shibboleth.idp.profile.IdPProfileHandlerManager;
import edu.internet2.middleware.shibboleth.idp.util.HttpServletHelper;

/**
 * OpenID Provider Servlet.
 */
public class OpenIDProvider extends HttpServlet {

    /** {@link ServletContext} parameter name bearing the ID of the {@link VelocityEngine} service: {@value} . */
    public static final String VELOCITY_ENGINE_SID_CTX_PARAM = "VelocityEngineId";

    /** Default ID by which the {@link VelocityEngine} is known within the Servlet context: {@value} . */
    public static final String DEFAULT_VELOCITY_ENGINE_SID = "shibboleth.VelocityEngine";

    /** Serial Version ID. */
    private static final long serialVersionUID = 1592602555851413374L;

    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(OpenIDProvider.class);

    /** Relying party configuration manager used by the IdP. */
    private RelyingPartyConfigurationManager rpConfigManager;

    /** Profile handler manager. */
    private IdPProfileHandlerManager profileHandlerManager;

    /** Association Manager. */
    private AssociationManager associationManager;

    /** Nonce Manager. */
    private IdentifierNonceGenerator nonceGenerator;

    /** Association handler. */
    private AssociationRequestHandler associationHandler;

    /** Authentication handler. */
    private AuthenticationRequestHandler authenticationHandler;

    /** Verification handler. */
    private VerifyRequestHandler verificationHandler;

    /** Discovery handler. */
    private DiscoveryHandler discoveryHandler;

    /** {@inheritDoc} */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        DefaultBootstrap.bootstrap();

        rpConfigManager = HttpServletHelper.getRelyingPartyConfirmationManager(config.getServletContext());
        profileHandlerManager = HttpServletHelper.getProfileHandlerManager(config.getServletContext());

        associationManager = new AssociationManager();
        associationManager.setBuilder(new BasicAssociationBuilder());
        associationManager.setStore(new InMemoryAssociationStore());
        associationManager.setDefaultLifetime(60 * 60 * 24 * 365);

        nonceGenerator = new IdentifierNonceGenerator();
        nonceGenerator.setIdentifierGenerator(new RandomIdentifierGenerator());

        associationHandler = new AssociationRequestHandler();
        associationHandler.setAssociationManager(associationManager);

        authenticationHandler = new AuthenticationRequestHandler();
        authenticationHandler.setAssociationManager(associationManager);
        authenticationHandler.setNonceManager(nonceGenerator);
        authenticationHandler.setRelyingPartConfigurationManager(rpConfigManager);
        authenticationHandler.setProfileHandlerManager(profileHandlerManager);

        verificationHandler = new VerifyRequestHandler();
        verificationHandler.setAssociationManager(associationManager);
        verificationHandler.setNonceValidator(new InMemoryNonceValidator());
        verificationHandler.getNonceValidator().setLifetime(3600);

        String velocityEngineId = HttpServletHelper.getContextParam(config.getServletContext(),
                VELOCITY_ENGINE_SID_CTX_PARAM, DEFAULT_VELOCITY_ENGINE_SID);
        VelocityEngine velocityEngine = (VelocityEngine) config.getServletContext().getAttribute(velocityEngineId);

        discoveryHandler = new DiscoveryHandler();
        discoveryHandler.setProfileHandlerManager(profileHandlerManager);
        discoveryHandler.setVelocityEngine(velocityEngine);
    }

    /** {@inheritDoc} */
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        log.info("Processing HTTP {} request to OpenID Endpoint", request.getMethod());

        if (request.getPathInfo() == null) {
            sendBlankPage(response);
        }

        if (request.getPathInfo().startsWith("/user/")) {
            discoveryHandler.processUserRequest(request, response);
        } else if (request.getPathInfo().equals("/provider/xrds")) {
            discoveryHandler.processProviderRequest(request, response);
        } else if (request.getPathInfo().equals("/provider")) {
            doOpenIDRequest(request, response);
        } else {
            sendBlankPage(response);
        }
    }

    /**
     * Process OpenID request.
     * 
     * @param httpRequest HTTP request
     * @param httpResponse HTTP response
     * @throws ServletException if unable to process the request
     * @throws IOException if unable to process the request
     */
    protected void doOpenIDRequest(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
            throws ServletException, IOException {
        log.info("Processing HTTP {} request to OpenID Provider", httpRequest.getMethod());
        OpenIDRequestContext context;

        try {
            context = buildRequestContext(httpRequest, httpResponse);
        } catch (MalformedMessageException e) {
            sendErrorResponse(e.getMessage(), httpRequest, httpResponse);
            return;
        }

        if ("GET".equals(httpRequest.getMethod())) {
            if (context.getOpenIDRequest() instanceof AssociationRequest) {
                log.error("Association Requests must use POST");
                throw new ServletException("Association Requests must use POST");
            } else if (context.getOpenIDRequest() instanceof VerifyRequest) {
                log.error("Verification Requests must use POST");
                throw new ServletException("Verification Requests must use POST");
            }
        }

        processRequest(context);
    }

    /**
     * Build the OpenID request context from the HTTP servlet request and response.
     * 
     * @param httpRequest HTTP servlet request
     * @param httpResponse HTTP servlet response
     * @return request context
     * @throws ServletException if unable to build OpenID request
     * @throws MalformedMessageException if OpenID request is malformed
     */
    private OpenIDRequestContext buildRequestContext(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
            throws ServletException, MalformedMessageException {
        OpenIDRequestContext context = new BasicOpenIDRequestContext();
        context.setSecure(httpRequest.isSecure());

        context.setServletRequest(httpRequest);
        context.setServletResponse(httpResponse);

        Message request = buildOpenIDRequest(context);
        if (request == null) {
            OpenIDLoginContext loginContext = (OpenIDLoginContext) HttpServletHelper.getLoginContext(httpRequest);
            if (loginContext != null) {
                request = loginContext.getAuthenticationRequest();
            }
        }
        context.setOpenIDRequest(request);

        return context;
    }

    /**
     * Build the OpenID request from the request context.
     * 
     * @param context request context
     * @return OpenID request message
     * @throws MalformedMessageException if OpenID request is malformed
     */
    private Message buildOpenIDRequest(OpenIDRequestContext context) throws MalformedMessageException {
        ParameterMap parameters;

        try {
            ServletRequestParameterMapDecoder codec = ServletRequestParameterMapDecoder.getInstance();
            parameters = codec.decode(context.getServletRequest().getParameterMap());
        } catch (EncodingException e) {
            throw new MalformedMessageException(e);
        }

        if (parameters.isEmpty()) {
            log.debug("Empty OpenID Request");
            return null;
        }

        if (log.isDebugEnabled()) {
            log.debug("OpenID Request:");
            for (QName key : parameters.keySet()) {
                log.debug("  {} = {}", key, parameters.get(key));
            }
        }

        try {
            MessageUnmarshaller unmarshaller = Configuration.getMessageUnmarshallers().getUnmarshaller(parameters);
            if (unmarshaller == null) {
                throw new UnmarshallingException("Unrecognized mode parameter in request: "
                        + parameters.get(Parameter.mode.QNAME));
            }

            return unmarshaller.unmarshall(parameters);
        } catch (UnmarshallingException e) {
            throw new MalformedMessageException(e);
        }
    }

    /**
     * Process the OpenID Request.
     * 
     * @param context request context
     * @throws ServletException if unable to process request
     * @throws IOException if unable to write response
     */
    private void processRequest(OpenIDRequestContext context) throws ServletException, IOException {

        if (context.getOpenIDRequest() instanceof AssociationRequest) {
            associationHandler.processRequest(context);
        } else if (context.getOpenIDRequest() instanceof AuthenticationRequest) {
            authenticationHandler.processRequest(context);
        } else if (context.getOpenIDRequest() instanceof VerifyRequest) {
            verificationHandler.processRequest(context);
        } else if (context.getOpenIDResponse() == null) {
            sendBlankPage(context.getServletResponse());
        }

    }

    /**
     * Send error message.
     * 
     * @param response HTTP response
     * @throws IOException if unable to send error message
     */
    private void sendBlankPage(HttpServletResponse response) throws IOException {
        response.setContentType("text/plain");
        PrintWriter output = response.getWriter();
        output.write("OpenID provider endpoint... nothing to see here.");
    }

    /**
     * Send error message.
     * 
     * @param message error message
     * @param httpRequest HTTP request
     * @param httpResponse HTTP response
     * 
     * @throws ServletException if unable to send error message
     */
    private void sendErrorResponse(String message, HttpServletRequest httpRequest, HttpServletResponse httpResponse)
            throws ServletException {
        QName qname = new QName(OpenIDConstants.OPENID_20_NS, ErrorResponse.MODE);
        ErrorResponse response = (ErrorResponse) Configuration.getMessageBuilders().getBuilder(qname).buildObject();
        response.setError(message);

        String returnTo = null;
        try {
            ServletRequestParameterMapDecoder codec = ServletRequestParameterMapDecoder.getInstance();
            ParameterMap parameters = codec.decode(httpRequest.getParameterMap());
            returnTo = parameters.get(Parameter.return_to.QNAME);
        } catch (EncodingException e) {
            // ignore
        }

        try {
            if (!DatatypeHelper.isEmpty(returnTo)) {
                URL url = new URL(returnTo);
                OpenIDProviderHelper.sendIndirectResponse(response, httpResponse, url);
            } else {
                httpResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                OpenIDProviderHelper.writeDirectResponse(response, httpResponse.getWriter());
            }
        } catch (IOException e) {
            throw new ServletException("Unable to write response");
        }
    }

}