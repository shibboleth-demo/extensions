/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.attribute;

import java.util.Collection;

import edu.internet2.middleware.openid.extensions.ax.FetchRequest;
import edu.internet2.middleware.openid.extensions.ax.FetchResponse;
import edu.internet2.middleware.openid.extensions.sreg.SimpleRegistrationRequest;
import edu.internet2.middleware.openid.extensions.sreg.SimpleRegistrationResponse;
import edu.internet2.middleware.shibboleth.common.attribute.AttributeAuthority;
import edu.internet2.middleware.shibboleth.common.attribute.BaseAttribute;
import edu.internet2.middleware.shibboleth.common.attribute.encoding.AttributeEncodingException;
import edu.internet2.middleware.shibboleth.idp.openid.profile.provider.OpenIDAuthenticationRequestContext;

/**
 * An attribute authority that can take an OpenID attribute request and produce a resultant response.
 */
public interface OpenIDAttributeAuthority extends AttributeAuthority<OpenIDAuthenticationRequestContext> {

    /**
     * Translates AX attribute naming information into the internal attribute ID used by the resolver and filtering
     * engine.
     * 
     * @param attribute the AX attribute to translate
     * 
     * @return the attribute ID used by the resolver and filtering engine
     */
    public String getAttributeIDByAXAttribute(String attribute);

    /**
     * Translates the internal attribute ID, used by the resolver and filtering engine, into its representative AX
     * attribute name.
     * 
     * @param id internal attribute ID
     * 
     * @return AX attribute name
     */
    public String getAXAttributeByAttributeID(String id);

    /**
     * Creates an Attribute Exchange response from a collection of {@link BaseAttribute}.
     * 
     * @param request the AX request the response is to
     * @param attributes the attributes to create the response form
     * 
     * @return the generated AX response
     * 
     * @throws AttributeEncodingException thrown if an {@link BaseAttribute} can not be encoded
     */
    public FetchResponse buildAXResponse(FetchRequest request, Collection<BaseAttribute> attributes)
            throws AttributeEncodingException;

    /**
     * Translates Simple Registration attribute naming information into the internal attribute ID used by the resolver
     * and filtering engine.
     * 
     * @param attribute the Simple Registration attribute to translate
     * 
     * @return the attribute ID used by the resolver and filtering engine
     */
    public String getAttributeIDBySRegAttribute(String attribute);

    /**
     * Translates the internal attribute ID, used by the resolver and filtering engine, into its representative Simple
     * Registration attribute name.
     * 
     * @param id internal attribute ID
     * 
     * @return Simple Registration attribute name
     */
    public String getSRegAttributeByAttributeID(String id);

    /**
     * Creates an Simple Registration response from a collection of {@link BaseAttribute}.
     * 
     * @param request the SReg request the response is to
     * @param attributes the attributes to create the response form
     * 
     * @return the generated SReg response
     * 
     * @throws AttributeEncodingException thrown if an {@link BaseAttribute} can not be encoded
     */
    public SimpleRegistrationResponse buildSRegResponse(SimpleRegistrationRequest request,
            Collection<BaseAttribute> attributes) throws AttributeEncodingException;

}