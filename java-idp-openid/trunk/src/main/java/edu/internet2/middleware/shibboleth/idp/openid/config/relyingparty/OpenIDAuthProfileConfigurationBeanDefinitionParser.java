/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.config.relyingparty;

import javax.xml.namespace.QName;

import org.opensaml.xml.util.DatatypeHelper;
import org.opensaml.xml.util.XMLHelper;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.AbstractSingleBeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

/**
 * Spring configuration parser for OpenID Auth profile configurations.
 */
public class OpenIDAuthProfileConfigurationBeanDefinitionParser extends AbstractSingleBeanDefinitionParser {

    /** Schema type name. */
    public static final QName TYPE_NAME = new QName(OpenIDRelyingPartyNamespaceHandler.NAMESPACE, "OpenIDAuthProfile");

    /** {@inheritDoc} */
    protected Class getBeanClass(Element element) {
        return OpenIDAuthProfileConfigurationFactoryBean.class;
    }

    /** {@inheritDoc} */
    protected void doParse(Element element, ParserContext parserContext, BeanDefinitionBuilder builder) {
        builder.setLazyInit(true);

        String secPolRef = DatatypeHelper.safeTrimOrNullString(element.getAttributeNS(null, "securityPolicyRef"));
        if (secPolRef != null) {
            builder.addDependsOn(secPolRef);
            builder.addPropertyReference("profileSecurityPolicy", secPolRef);
        }

        // builder.addPropertyReference("attributeAuthority", DatatypeHelper.safeTrimOrNullString(element.getAttributeNS(
        //        null, "attributeAuthority")));

        boolean directed = false;
        if (element.hasAttributeNS(null, "directedIdentifiers")) {
            directed = XMLHelper.getAttributeValueAsBoolean(element.getAttributeNodeNS(null, "directedIdentifiers"));
        }
        builder.addPropertyValue("directedIdentifiers", directed);

        String salt = DatatypeHelper.safeTrimOrNullString(element.getAttributeNS(null, "directedIdentifierSalt"));
        if (salt != null) {
            builder.addPropertyValue("directedIdentifierSalt", salt.getBytes());
        }
    }

    /** {@inheritDoc} */
    protected boolean shouldGenerateId() {
        return true;
    }

}