/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.profile.provider;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.internet2.middleware.openid.message.Message;
import edu.internet2.middleware.shibboleth.idp.openid.profile.OpenIDRequestContext;

/**
 * Basic {@link OpenIDRequestContext} implementation.
 */
public class BasicOpenIDRequestContext implements OpenIDRequestContext {

    /** HTTP Servlet Request. */
    private HttpServletRequest servletRequest;

    /** HTTP Servlet Response. */
    private HttpServletResponse servletResponse;

    /** OpenID Request. */
    private Message openidRequest;

    /** OpenID Response. */
    private Message openidResponse;

    /** If transport layer security is being used. */
    private boolean transportSecure;

    /** {@inheritDoc} */
    public HttpServletRequest getServletRequest() {
        return servletRequest;
    }

    /** {@inheritDoc} */
    public void setServletRequest(HttpServletRequest request) {
        servletRequest = request;
    }

    /** {@inheritDoc} */
    public HttpServletResponse getServletResponse() {
        return servletResponse;
    }

    /** {@inheritDoc} */
    public void setServletResponse(HttpServletResponse response) {
        servletResponse = response;
    }

    /** {@inheritDoc} */
    public Message getOpenIDRequest() {
        return openidRequest;
    }

    /** {@inheritDoc} */
    public void setOpenIDRequest(Message request) {
        openidRequest = request;
    }

    /** {@inheritDoc} */
    public Message getOpenIDResponse() {
        return openidResponse;
    }

    /** {@inheritDoc} */
    public void setOpenIDResponse(Message response) {
        openidResponse = response;
    }

    /** {@inheritDoc} */
    public boolean isSecure() {
        return transportSecure;
    }

    /** {@inheritDoc} */
    public void setSecure(boolean secure) {
        transportSecure = secure;
    }

}