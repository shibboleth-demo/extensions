/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.profile.provider;

import java.io.IOException;
import java.security.KeyPair;
import java.util.Arrays;
import java.util.List;

import javax.crypto.SecretKey;
import javax.crypto.interfaces.DHPublicKey;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.openid.Configuration;
import edu.internet2.middleware.openid.common.OpenIDConstants;
import edu.internet2.middleware.openid.common.OpenIDConstants.AssociationType;
import edu.internet2.middleware.openid.common.OpenIDConstants.SessionType;
import edu.internet2.middleware.openid.message.AssociationError;
import edu.internet2.middleware.openid.message.AssociationRequest;
import edu.internet2.middleware.openid.message.AssociationResponse;
import edu.internet2.middleware.openid.message.Message;
import edu.internet2.middleware.openid.security.Association;
import edu.internet2.middleware.openid.security.AssociationException;
import edu.internet2.middleware.openid.security.AssociationUtils;
import edu.internet2.middleware.openid.util.MessageUtils;
import edu.internet2.middleware.shibboleth.idp.openid.profile.OpenIDRequestContext;

/**
 * Request handler for {@link AssociationRequest} messages.
 */
public class AssociationRequestHandler extends BaseRequestHandler<AssociationRequest> {

    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(AssociationRequestHandler.class);

    /** Supported association types, in order of preference. */
    private List<AssociationType> supportedAssociationTypes;

    /** Supported session types, in order of preference. */
    private List<SessionType> supportedSessionTypes;

    /** Constructor. */
    public AssociationRequestHandler() {
        supportedAssociationTypes = Arrays.asList(new AssociationType[] { AssociationType.HMAC_SHA256,
                AssociationType.HMAC_SHA1, });
        supportedSessionTypes = Arrays.asList(new SessionType[] { SessionType.DH_SHA256, SessionType.DH_SHA1,
                SessionType.no_encryption, });
    }

    /**
     * Get supported association types, in order of preference.
     * 
     * @return supported association types
     */
    public List<AssociationType> getSupportedAssociationTypes() {
        return supportedAssociationTypes;
    }

    /**
     * Get supported session types, in order of preference.
     * 
     * @return supported session types
     */
    public List<SessionType> getSupportedSessionTypes() {
        return supportedSessionTypes;
    }

    /** {@inheritDoc} */
    public void processRequest(OpenIDRequestContext context) throws ServletException, IOException {
        log.info("handling association request");

        AssociationRequest request = (AssociationRequest) context.getOpenIDRequest();

        Message response;

        try {
            if (!context.isSecure() && SessionType.no_encryption.equals(request.getSessionType())) {
                throw new AssociationException("Session type no_encryption may only be used in "
                        + "conjunction with transport layer security");
            }
            response = buildAssociationResponse(request);
        } catch (AssociationException e) {
            AssociationError error = (AssociationError) MessageUtils
                    .buildMessage(OpenIDConstants.ASSOCIATION_ERROR_MODE);

            error.setAssociationType(supportedAssociationTypes.get(0));
            error.setSessionType(supportedSessionTypes.get(0));
            error.setError(e.getMessage());

            response = error;
            context.getServletResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

        context.setOpenIDResponse(response);
        sendDirectResponse(context);
    }

    /**
     * Process association request and build appropriate response.
     * 
     * @param request association request
     * @return association response
     * @throws AssociationException if association request is invalid
     */
    public AssociationResponse buildAssociationResponse(AssociationRequest request) throws AssociationException {

        // TODO make use of UnsupportAssociationException below, so that an appropriate supported association and
        // session type can be included in the error response

        if (!supportedAssociationTypes.contains(request.getAssociationType())) {
            throw new AssociationException("Unsupported association type: " + request.getAssociationType().toString());
        }

        if (!supportedSessionTypes.contains(request.getSessionType())) {
            throw new AssociationException("Unsupported session type: " + request.getSessionType().toString());
        }

        Association association = getAssociationManager().buildAssociation(request.getAssociationType());

        AssociationResponse response = (AssociationResponse) Configuration.getMessageBuilders().getBuilder(
                new QName(OpenIDConstants.OPENID_20_NS, OpenIDConstants.ASSOCIATION_RESPONSE_MODE)).buildObject();

        response.setAssociationHandle(association.getHandle());
        response.setAssociationType(association.getAssociationType());
        response.setSessionType(request.getSessionType());
        response.setLifetime(getAssociationManager().getDefaultLifetime());

        if (request.getSessionType() == SessionType.DH_SHA1 || request.getSessionType() == SessionType.DH_SHA256) {
            KeyPair keyPair = AssociationUtils.generateKeyPair();
            response.setDHServerPublic((DHPublicKey) keyPair.getPublic());

            SecretKey sharedSecret = AssociationUtils.generateSharedSecret(keyPair.getPrivate(), request
                    .getDHConsumerPublic(), request.getSessionType().getAlgorithm());
            SecretKey encryptedMacKey = AssociationUtils.encryptMacKey(association.getMacKey(), sharedSecret);
            response.setMacKey(encryptedMacKey);
        } else {
            response.setMacKey(association.getMacKey());
        }

        return response;
    }

    /** {@inheritDoc} */
    public Class getRequestType() {
        return AssociationRequest.class;
    }

    /**
     * Exception thrown when an association request includes an unsupported association or session type.
     */
    private class UnsupportedAssociationException extends AssociationException {

        /** Serial Version UID. */
        private static final long serialVersionUID = 3461769373980254885L;

        /** Supported association type. */
        private AssociationType associationType;

        /** Supported session type. */
        private SessionType sessionType;

        /** Error message. */
        private String message;

        /**
         * Constructor.
         * 
         * @param request Association request that caused this exception to be thrown
         */
        public UnsupportedAssociationException(AssociationRequest request) {
            if (supportedAssociationTypes.contains(request.getAssociationType())) {
                associationType = request.getAssociationType();
            } else if (!supportedAssociationTypes.isEmpty()) {
                associationType = supportedAssociationTypes.get(0);
            }

            if (!supportedSessionTypes.contains(request.getSessionType())) {

            }

        }

        /**
         * A supported association type the relying party can use.
         * 
         * @return supported association type
         */
        public AssociationType getAssociationType() {
            return associationType;
        }

        /**
         * As supported session type the relying party can use.
         * 
         * @return supported session type
         */
        public SessionType getSessionType() {
            return sessionType;
        }

        /** {@inheritDoc} */
        public String getMessage() {
            if (message != null) {
                return message;
            } else {
                return super.getMessage();
            }
        }
    }

}