/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.authn;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.openid.extensions.MessageExtension;
import edu.internet2.middleware.openid.extensions.pape.PolicyRequest;
import edu.internet2.middleware.openid.message.AuthenticationRequest;
import edu.internet2.middleware.shibboleth.idp.authn.LoginContext;

/**
 * OpenID Login Context that carries the original OpenID authentication request.
 */
public class OpenIDLoginContext extends LoginContext {

    /** Serial Version UID. */
    private static final long serialVersionUID = 4704616158179727489L;

    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(OpenIDLoginContext.class);

    /** Authentication request. */
    private AuthenticationRequest authenticationRequest;

    /**
     * Constructor.
     * 
     * @param request OpenID authentication request
     */
    public OpenIDLoginContext(AuthenticationRequest request) {
        authenticationRequest = request;

        setRelyingParty(authenticationRequest.getRealm());
        setPassiveAuthRequired(authenticationRequest.isImmediate());
        getRequestedAuthenticationMethods().addAll(extractRequestedAuthenticationMethods(authenticationRequest));
    }

    /**
     * Get the authentication request.
     * 
     * @return the authentication request
     */
    public AuthenticationRequest getAuthenticationRequest() {
        return authenticationRequest;
    }

    /**
     * Extract any PAPE authentication methods from the authentication request.
     * 
     * @param request authentication request
     * @return list of requested authentication methods
     */
    protected List<String> extractRequestedAuthenticationMethods(AuthenticationRequest request) {
        for (MessageExtension extension : request.getExtensions().values()) {
            if (extension instanceof PolicyRequest) {
                return ((PolicyRequest) extension).getAuthenticationPolicies();
            }
        }

        return Collections.EMPTY_LIST;
    }

}