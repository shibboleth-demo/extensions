/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.profile;

import java.io.IOException;

import javax.servlet.ServletException;

import edu.internet2.middleware.openid.message.Message;

/**
 * Handler of specific OpenID request messages.
 * 
 * @param <RequestType> type of request this handler can handle
 */
public interface RequestHandler<RequestType extends Message> {

    /**
     * Get the type of requests this handler accepts.
     * 
     * @return the request type class
     */
    public Class<RequestType> getRequestType();

    /**
     * Process the request.
     * 
     * @param context OpenID request context
     * @throws ServletException if unable to process the request
     * @throws IOException if unable to send the response
     */
    public void processRequest(OpenIDRequestContext context) throws ServletException, IOException;

}