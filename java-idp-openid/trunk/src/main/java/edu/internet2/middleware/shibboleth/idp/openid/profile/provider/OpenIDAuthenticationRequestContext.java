/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.profile.provider;

import java.util.Collection;
import java.util.Map;

import edu.internet2.middleware.shibboleth.common.attribute.AttributeRequestContext;
import edu.internet2.middleware.shibboleth.common.attribute.BaseAttribute;
import edu.internet2.middleware.shibboleth.idp.openid.relyingparty.OpenIDAuthConfiguration;

/**
 * Contextual object used to accumulate information as profile requests are being processed.
 */
public class OpenIDAuthenticationRequestContext extends BasicOpenIDRequestContext implements AttributeRequestContext {

    /** Attributes retrieved for the principal. */
    private Map<String, BaseAttribute> principalAttributes;

    /** Authentication method used to authenticate the principal. */
    private String principalAuthenticationMethod;

    /** Principal name of the subject of the request. */
    private String principalName;

    /** Relying Party ID. */
    private String relyingParty;

    /** IDs of attribute requested by relaying party. */
    private Collection<String> requestedAttributeIds;

    /** Configuration for the profile. */
    private OpenIDAuthConfiguration profileConfiguration;

    /** {@inheritDoc} */
    public Map<String, BaseAttribute> getAttributes() {
        return principalAttributes;
    }

    /** {@inheritDoc} */
    public String getPrincipalAuthenticationMethod() {
        return principalAuthenticationMethod;
    }

    /** {@inheritDoc} */
    public String getPrincipalName() {
        return principalName;
    }

    /**
     * Gets the configuration for the profile for the relying party.
     * 
     * @return configuration for the profile for the relying party
     */
    public OpenIDAuthConfiguration getProfileConfiguration() {
        return profileConfiguration;
    }

    /**
     * Get relying party.
     * 
     * @return relying party
     */
    public String getRelyingParty() {
        return relyingParty;
    }

    /** {@inheritDoc} */
    public Collection<String> getRequestedAttributesIds() {
        return requestedAttributeIds;
    }

    /** {@inheritDoc} */
    public void setAttributes(Map<String, BaseAttribute> attributes) {
        principalAttributes = attributes;
    }

    /** {@inheritDoc} */
    public void setPrincipalAuthenticationMethod(String method) {
        principalAuthenticationMethod = method;
    }

    /** {@inheritDoc} */
    public void setPrincipalName(String name) {
        principalName = name;
    }

    /**
     * Set profile configuration.
     * 
     * @param configuration profile configuration
     */
    public void setProfileConfiguration(OpenIDAuthConfiguration configuration) {
        profileConfiguration = configuration;
    }

    /**
     * Set relying party.
     * 
     * @param newRelyingParty relying party
     */
    public void setRelyingParty(String newRelyingParty) {
        relyingParty = newRelyingParty;
    }

    /** {@inheritDoc} */
    public void setRequestedAttributes(Collection<String> ids) {
        requestedAttributeIds = ids;
    }

}