/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.profile.provider;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;

import org.bouncycastle.util.encoders.UrlBase64;
import org.joda.time.DateTime;
import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;

import edu.internet2.middleware.openid.Configuration;
import edu.internet2.middleware.openid.common.OpenIDConstants;
import edu.internet2.middleware.openid.common.OpenIDConstants.AssociationType;
import edu.internet2.middleware.openid.extensions.MessageExtension;
import edu.internet2.middleware.openid.extensions.MessageExtensionBuilder;
import edu.internet2.middleware.openid.extensions.pape.PolicyRequest;
import edu.internet2.middleware.openid.extensions.pape.PolicyResponse;
import edu.internet2.middleware.openid.message.AuthenticationRequest;
import edu.internet2.middleware.openid.message.NegativeAssertion;
import edu.internet2.middleware.openid.message.PositiveAssertion;
import edu.internet2.middleware.openid.security.Association;
import edu.internet2.middleware.openid.security.AssociationManager;
import edu.internet2.middleware.openid.security.NonceGenerator;
import edu.internet2.middleware.openid.security.SecurityException;
import edu.internet2.middleware.openid.security.SecurityUtils;
import edu.internet2.middleware.shibboleth.common.relyingparty.ProfileConfiguration;
import edu.internet2.middleware.shibboleth.common.relyingparty.RelyingPartyConfiguration;
import edu.internet2.middleware.shibboleth.common.relyingparty.RelyingPartyConfigurationManager;
import edu.internet2.middleware.shibboleth.common.relyingparty.provider.SAMLMDRelyingPartyConfigurationManager;
import edu.internet2.middleware.shibboleth.common.util.HttpHelper;
import edu.internet2.middleware.shibboleth.idp.authn.LoginContext;
import edu.internet2.middleware.shibboleth.idp.authn.LoginHandler;
import edu.internet2.middleware.shibboleth.idp.openid.authn.OpenIDLoginContext;
import edu.internet2.middleware.shibboleth.idp.openid.profile.OpenIDRequestContext;
import edu.internet2.middleware.shibboleth.idp.openid.relyingparty.OpenIDAuthConfiguration;
import edu.internet2.middleware.shibboleth.idp.profile.IdPProfileHandlerManager;
import edu.internet2.middleware.shibboleth.idp.util.HttpServletHelper;

/**
 * Association Manager.
 */
public class AuthenticationRequestHandler extends BaseRequestHandler<AuthenticationRequest> {

    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(AuthenticationRequestHandler.class);

    /** Nonce Manager. */
    private NonceGenerator nonceGenerator;

    /** Relying party configuration manager used by the IdP. */
    private RelyingPartyConfigurationManager rpConfigManager;

    /** Profile handler manager. */
    private IdPProfileHandlerManager profileHandlerManager;

    /** Constructor. */
    public AuthenticationRequestHandler() {
    }

    /**
     * Get the nonce Manager.
     * 
     * @return the nonce manager
     */
    public NonceGenerator getNonceManager() {
        return nonceGenerator;
    }

    /**
     * Set the nonce manager.
     * 
     * @param newGenerator the nonce manager
     */
    public void setNonceManager(NonceGenerator newGenerator) {
        nonceGenerator = newGenerator;
    }

    /**
     * Get the relying party configuration manager.
     * 
     * @return the relying party configuration manager
     */
    public RelyingPartyConfigurationManager getRelyingPartyConfigurationManager() {
        return rpConfigManager;
    }

    /**
     * Set the relying party configuration manager.
     * 
     * @param manager the relying party configuration manager
     */
    public void setRelyingPartConfigurationManager(RelyingPartyConfigurationManager manager) {
        rpConfigManager = manager;
    }

    /**
     * Get the profile handler manager.
     * 
     * @return the profile handler manager
     */
    public IdPProfileHandlerManager getProfileHandlerManager() {
        return profileHandlerManager;
    }

    /**
     * Set the profile handler manager.
     * 
     * @param manager the profile handler manager
     */
    public void setProfileHandlerManager(IdPProfileHandlerManager manager) {
        profileHandlerManager = manager;
    }

    /**
     * {@inheritDoc}
     * 
     * @throws IOException
     * @throws ServletException
     */
    public void processRequest(OpenIDRequestContext context) throws ServletException, IOException {
        log.info("handling authentication request");
        OpenIDAuthenticationRequestContext requestContext = buildRequestContext(context);

        LoginContext loginContext = HttpServletHelper.getLoginContext(context.getServletRequest());
        if (loginContext == null) {
            log.debug("Incoming request does not contain a login context, processing as first leg of request");
            performAuthentication(requestContext);
        } else {
            log.debug("Incoming request contains a login context, processing as second leg of request");
            completeAuthenticationRequest(requestContext);
        }

    }

    /**
     * Creates a {@link LoginContext} and sends the request off to the AuthenticationManager to begin the process of
     * authenticating the user.
     * 
     * @param context request context
     * 
     * @throws ServletException thrown if there is a problem creating the login context and transferring control to the
     *             authentication manager
     */
    public void performAuthentication(OpenIDAuthenticationRequestContext context) throws ServletException {
        AuthenticationRequest request = (AuthenticationRequest) context.getOpenIDRequest();

        log.debug("Creating login context and transferring control to authentication engine");
        OpenIDLoginContext loginContext = new OpenIDLoginContext(request);
        loginContext.setAuthenticationEngineURL("/AuthnEngine");
        loginContext.setProfileHandlerURL(HttpHelper.getRequestUriWithoutContext(context.getServletRequest()));

        RelyingPartyConfiguration rpConfig = getRelyingPartyConfiguration(loginContext.getRelyingPartyId());
        loginContext.setDefaultAuthenticationMethod(rpConfig.getDefaultAuthenticationMethod());
        ProfileConfiguration profileConfig = rpConfig.getProfileConfiguration(OpenIDAuthConfiguration.PROFILE_ID);
        if (profileConfig == null) {
            String msg = MessageFormatter.format("OpenID Auth profile is not configured for relying party '{}'",
                    loginContext.getRelyingPartyId());
            log.warn(msg);
            throw new ServletException(msg);
        }

        HttpServletHelper.bindLoginContext(loginContext, context.getServletRequest());

        try {
            RequestDispatcher dispatcher = context.getServletRequest().getRequestDispatcher(
                    loginContext.getAuthenticationEngineURL());
            dispatcher.forward(context.getServletRequest(), context.getServletResponse());
            return;
        } catch (IOException e) {
            String msg = "Error forwarding Shibboleth SSO request to AuthenticationManager";
            log.error(msg, e);
            throw new ServletException(msg, e);
        } catch (ServletException e) {
            String msg = "Error forwarding Shibboleth SSO request to AuthenticationManager";
            log.error(msg, e);
            throw new ServletException(msg, e);
        }
    }

    /**
     * Creates a response to the OpenID authentication request and sends the user, with response in tow, back to the
     * OpenID consumer after they've been authenticated.
     * 
     * @param requestContext request context
     * 
     * @throws ServletException if the response can not be created
     * @throws IOException if unable to write the servlet response
     */
    public void completeAuthenticationRequest(OpenIDAuthenticationRequestContext requestContext)
            throws ServletException, IOException {

        OpenIDLoginContext loginContext = (OpenIDLoginContext) HttpServletHelper.getLoginContext(requestContext
                .getServletRequest());
        AuthenticationRequest request = loginContext.getAuthenticationRequest();

        if (!loginContext.isPrincipalAuthenticated()) {
            log.error("Principal is not authenticated.");
            sendNegativeAssertion(requestContext);
            return;
        }

        URL authenticatedURL = getAuthenticatedURL(requestContext, loginContext);
        if (!OpenIDConstants.IDENTIFIER_SELECT_IDENTITY.equals(request.getIdentity())
                && !authenticatedURL.toString().equals(request.getIdentity())) {
            log.error("Principal '{}' is not authorized to use the OpenID '{}'", loginContext.getPrincipalName(),
                    request.getIdentity());
            log.info("Princpal URL is '{}'", authenticatedURL);
            sendNegativeAssertion(requestContext);
            return;
        }

        populateRequestContext(requestContext, loginContext);

        PositiveAssertion response = (PositiveAssertion) Configuration.getMessageBuilders().getBuilder(
                new QName(OpenIDConstants.OPENID_20_NS, PositiveAssertion.MODE)).buildObject();

        AssociationManager associationManager = getAssociationManager();
        Association association = associationManager.getAssociation(request.getAssociationHandle());

        if (!associationManager.isValid(association)) {
            response.setInvalidateHandle(request.getAssociationHandle());
            association = associationManager.buildAssociation(AssociationType.HMAC_SHA256);
        }

        response.setAssociationHandle(association.getHandle());
        if (OpenIDConstants.IDENTIFIER_SELECT_IDENTITY.equals(request.getClaimedId())) {
            response.setClaimedId(authenticatedURL.toString());
        } else {
            response.setClaimedId(request.getClaimedId());
        }
        response.setIdentity(authenticatedURL.toString());
        response.setResponseNonce(nonceGenerator.generateNonce());
        response.setReturnTo(request.getReturnTo());
        response.setEndpoint(requestContext.getServletRequest().getRequestURL().toString());

        for (MessageExtension extension : request.getExtensions().values()) {
            log.info("handling authentication request extension: {}", extension.getNamespace());
            if (extension instanceof PolicyRequest) {
                PolicyResponse papeResponse = handlePAPE((PolicyRequest) extension, loginContext);
                if (papeResponse != null) {
                    response.getExtensions().put(papeResponse.getNamespace(), papeResponse);
                }
            }
        }

        try {
            SecurityUtils.signMessage(response, association);
        } catch (SecurityException e) {
            log.error(e.getMessage());
        }

        requestContext.setOpenIDResponse(response);
        sendIndirectResponse(requestContext);
    }

    /**
     * Get the OpenID URL for the authenticated user.
     * 
     * @param requestContext OpenID request context
     * @param loginContext login context of the authenticated user
     * @return OpenID URL for the authenticated user
     * @throws ServletException if unable to calculated directed identifier
     */
    private URL getAuthenticatedURL(OpenIDRequestContext requestContext, OpenIDLoginContext loginContext)
            throws ServletException {

        RelyingPartyConfiguration rpConfig = getRelyingPartyConfiguration(loginContext.getRelyingPartyId());
        OpenIDAuthConfiguration profileConfig = (OpenIDAuthConfiguration) rpConfig
                .getProfileConfiguration(OpenIDAuthConfiguration.PROFILE_ID);

        if (profileConfig == null) {
            return null;
        }

        String userIdentifier;
        if (profileConfig.getDirectedIdentifiers()) {
            userIdentifier = calculateDirectedIdentifier(loginContext, rpConfig);
        } else {
            userIdentifier = loginContext.getPrincipalName();
        }

        HttpServletRequest request = requestContext.getServletRequest();
        try {
            URL requestURL = new URL(request.getRequestURL().toString());
            return new URL(requestURL, request.getContextPath() + request.getServletPath() + "/user/" + userIdentifier);
        } catch (MalformedURLException e) {
            return null;
        }

    }

    /**
     * Calculate directed identifier for login context.
     * 
     * @param loginContext login context to create directed identifier for
     * @param rpConfig relying party configuration the directed identifier is for
     * @return calculated directed identifier
     * @throws ServletException if unable to calculate hash
     */
    private String calculateDirectedIdentifier(OpenIDLoginContext loginContext, RelyingPartyConfiguration rpConfig)
            throws ServletException {
        OpenIDAuthConfiguration profileConfig = (OpenIDAuthConfiguration) rpConfig
                .getProfileConfiguration(OpenIDAuthConfiguration.PROFILE_ID);
        try {
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(rpConfig.getProviderId().getBytes());
            md.update((byte) '!');
            md.update(loginContext.getRelyingPartyId().getBytes());
            md.update((byte) '!');
            md.update(loginContext.getPrincipalName().getBytes());

            byte[] digest = md.digest(profileConfig.getDirectedIdentifierSalt());
            return "_" + new String(UrlBase64.encode(digest));
        } catch (NoSuchAlgorithmException e) {
            log.error("JVM error, SHA-1 hash is not supported.");
            throw new ServletException("JVM error, SHA-1 hash is not supported.");
        }
    }

    /**
     * Send a negative assertion.
     * 
     * @param requestContext OpenID request context
     * @throws ServletException if unable to send the response
     * @throws IOException if unable to send the response
     */
    private void sendNegativeAssertion(OpenIDAuthenticationRequestContext requestContext) throws ServletException,
            IOException {
        NegativeAssertion response = (NegativeAssertion) Configuration.getMessageBuilders().getBuilder(
                new QName(OpenIDConstants.OPENID_20_NS, NegativeAssertion.MODE_INTERACTIVE)).buildObject();
        response.setMode(NegativeAssertion.MODE_INTERACTIVE);

        requestContext.setOpenIDResponse(response);
        sendIndirectResponse(requestContext);
    }

    /**
     * Process the PAPE request.
     * 
     * @param request PAPE request
     * @param loginContext login context
     * @return PAPE response
     */
    public PolicyResponse handlePAPE(PolicyRequest request, LoginContext loginContext) {
        MessageExtensionBuilder<PolicyResponse> builder = Configuration.getExtensionBuilders().getBuilder(
                PolicyResponse.class);
        PolicyResponse response = (PolicyResponse) builder.buildObject();

        if (loginContext != null) {
            DateTime authInstant = loginContext.getAuthenticationInstant();
            if (authInstant != null) {
                response.setAuthenticationTime(authInstant.toDate());
            }

            LoginHandler handler = profileHandlerManager.getLoginHandlers().get(loginContext.getAuthenticationMethod());
            for (String method : handler.getSupportedAuthenticationMethods()) {
                if (loginContext.getRequestedAuthenticationMethods().contains(method)) {
                    response.getAuthenticationPolicies().add(method);
                }
            }
        }

        return response;
    }

    /** {@inheritDoc} */
    public Class getRequestType() {
        return AuthenticationRequest.class;
    }

    /**
     * Build a new OpenID authentication request context for the OpenID request context.
     * 
     * @param context request context
     * @return authentication request context
     */
    protected OpenIDAuthenticationRequestContext buildRequestContext(OpenIDRequestContext context) {
        OpenIDAuthenticationRequestContext requestContext = new OpenIDAuthenticationRequestContext();
        requestContext.setServletRequest(context.getServletRequest());
        requestContext.setServletResponse(context.getServletResponse());
        requestContext.setOpenIDRequest(context.getOpenIDRequest());

        return requestContext;
    }

    /**
     * Populate an OpenID authentication request context using the login context.
     * 
     * @param requestContext authentication request context to populate
     * @param loginContext login context for the current user
     */
    protected void populateRequestContext(OpenIDAuthenticationRequestContext requestContext,
            OpenIDLoginContext loginContext) {
        RelyingPartyConfiguration rpConfig = getRelyingPartyConfiguration(loginContext.getRelyingPartyId());
        OpenIDAuthConfiguration profileConfig = (OpenIDAuthConfiguration) rpConfig
                .getProfileConfiguration(OpenIDAuthConfiguration.PROFILE_ID);

        requestContext.setPrincipalName(loginContext.getPrincipalName());
        requestContext.setRelyingParty(loginContext.getRelyingPartyId());
        requestContext.setProfileConfiguration(profileConfig);
    }

    /**
     * Gets the relying party configuration for the given entity.
     * 
     * @param relyingPartyId ID of the relying party
     * @return the relying party configuration or null
     */
    public RelyingPartyConfiguration getRelyingPartyConfiguration(String relyingPartyId) {
        try {
            MetadataProvider metadataProvider = getMetadataProvider();
            if (metadataProvider == null || metadataProvider.getEntityDescriptor(relyingPartyId) == null) {
                log.warn("No metadata for relying party {}, treating party as anonymous", relyingPartyId);
                return rpConfigManager.getAnonymousRelyingConfiguration();
            }
        } catch (MetadataProviderException e) {
            log.error("Unable to look up relying party metadata", e);
            return null;
        }

        return rpConfigManager.getRelyingPartyConfiguration(relyingPartyId);
    }

    /**
     * A convenience method for retrieving the SAML metadata provider from the relying party manager.
     * 
     * @return the metadata provider or null
     */
    public MetadataProvider getMetadataProvider() {
        if (rpConfigManager instanceof SAMLMDRelyingPartyConfigurationManager) {
            return ((SAMLMDRelyingPartyConfigurationManager) rpConfigManager).getMetadataProvider();
        } else {
            return null;
        }
    }
}