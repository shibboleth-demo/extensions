/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid;

import java.io.IOException;
import java.io.Writer;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.openid.Configuration;
import edu.internet2.middleware.openid.common.ParameterMap;
import edu.internet2.middleware.openid.message.Message;
import edu.internet2.middleware.openid.message.encoding.EncodingException;
import edu.internet2.middleware.openid.message.encoding.EncodingUtils;
import edu.internet2.middleware.openid.message.encoding.impl.KeyValueFormCodec;
import edu.internet2.middleware.openid.message.io.MarshallingException;
import edu.internet2.middleware.openid.message.io.MessageMarshaller;

/**
 * OpenID Utilities.
 */
public final class OpenIDProviderHelper {

    /** Name of the key to the current OpenID request: {@value} . */
    public static final String OPENID_REQ_KEY_NAME = "_idp_openid_req_key";

    /** Logger. */
    private static final Logger log = LoggerFactory.getLogger(OpenIDProviderHelper.class);

    /** Constructor. */
    private OpenIDProviderHelper() {
    }

    /**
     * Send the OpenID direct response message.
     * 
     * @param message OpenID message to send
     * @param output writer to write message to
     * 
     * @throws ServletException if unable to build the response message
     * @throws IOException if unable to write the servlet response
     */
    public static void writeDirectResponse(Message message, Writer output) throws ServletException, IOException {

        try {
            MessageMarshaller<Message> marshaller = Configuration.getMessageMarshallers().getMarshaller(message);
            ParameterMap responseParameters = marshaller.marshall(message);
            output.write(KeyValueFormCodec.getInstance().encode(responseParameters));
        } catch (MarshallingException e) {
            throw new ServletException("Unable to marshall response", e);
        } catch (EncodingException e) {
            throw new ServletException("Unable to encode response", e);
        }
    }

    /**
     * Send an OpenID indirect response message.
     * 
     * @param message OpenID message to send
     * @param response HTTP servlet response to send message on
     * @param url URL to send message to
     * 
     * @throws ServletException if unable to build the response message
     * @throws IOException if unable to write the servlet response
     */
    public static void sendIndirectResponse(Message message, HttpServletResponse response, URL url)
            throws ServletException, IOException {
        // TODO the indirect response should be an HTML form post if it is too large for an HTTP redirect

        URL redirectURL = EncodingUtils.encodeMessage(url, message);

        if (redirectURL == null) {
            throw new ServletException("Unable to encode response");
        }

        log.info("redirecting to: {}", redirectURL);
        response.sendRedirect(redirectURL.toString());
        return;
    }

}