/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.internet2.middleware.openid.message.Message;

/**
 * OpenID Request context.
 */
public interface OpenIDRequestContext {

    /**
     * Get the servlet request.
     * 
     * @return Returns the httpServletRequest.
     */
    public HttpServletRequest getServletRequest();

    /**
     * Set the servlet request.
     * 
     * @param request The httpServletRequest to set.
     */
    public void setServletRequest(HttpServletRequest request);

    /**
     * Get the servlet response.
     * 
     * @return Returns the httpServletResponse.
     */
    public HttpServletResponse getServletResponse();

    /**
     * Set the servlet response.
     * 
     * @param response The httpServletResponse to set.
     */
    public void setServletResponse(HttpServletResponse response);

    /**
     * Get the OpenID request.
     * 
     * @return Returns the openidRequest.
     */
    public Message getOpenIDRequest();

    /**
     * Set the OpenID request.
     * 
     * @param request The openidRequest to set.
     */
    public void setOpenIDRequest(Message request);

    /**
     * Get the OpenID response.
     * 
     * @return Returns the openidResponse.
     */
    public Message getOpenIDResponse();

    /**
     * Set the OpenID response.
     * 
     * @param response The openidResponse to set.
     */
    public void setOpenIDResponse(Message response);

    /**
     * Check if this request was made over a secure channel, such as SSL or TLS.
     * 
     * @return true if the request is secure
     */
    public boolean isSecure();

    /**
     * Set if this request was made over a secure channel, such as SSL or TLS.
     * 
     * @param secure if the request is secure
     */
    public void setSecure(boolean secure);

}