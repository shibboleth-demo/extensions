/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.profile.provider;

import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.openid.Configuration;
import edu.internet2.middleware.openid.message.AuthenticationRequest;
import edu.internet2.middleware.openid.message.Message;
import edu.internet2.middleware.openid.message.io.MessageMarshallerFactory;
import edu.internet2.middleware.openid.security.AssociationManager;
import edu.internet2.middleware.shibboleth.idp.openid.OpenIDProviderHelper;
import edu.internet2.middleware.shibboleth.idp.openid.profile.OpenIDRequestContext;
import edu.internet2.middleware.shibboleth.idp.openid.profile.RequestHandler;

/**
 * Base class for implementations of {@link RequestHandler}.
 * 
 * @param <RequestType> type of message this handler processes
 */
public abstract class BaseRequestHandler<RequestType extends Message> implements RequestHandler<RequestType> {

    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(BaseRequestHandler.class);

    /** Message marshaller factory. */
    private MessageMarshallerFactory messageMarshallers;

    /** Association Manager. */
    private AssociationManager associationManager;

    /** Constructor. */
    public BaseRequestHandler() {
        super();
        messageMarshallers = Configuration.getMessageMarshallers();
    }

    /**
     * Get the message marshaller factory.
     * 
     * @return the message marshaller factory
     */
    public MessageMarshallerFactory getMarshallerFactory() {
        return messageMarshallers;
    }

    /**
     * Get the association manager.
     * 
     * @return the association manager
     */
    public AssociationManager getAssociationManager() {
        return associationManager;
    }

    /**
     * Set the association manager.
     * 
     * @param newManager the association manager
     */
    public void setAssociationManager(AssociationManager newManager) {
        associationManager = newManager;
    }

    /**
     * Send the OpenID direct response message.
     * 
     * @param context OpenID request context
     * @throws ServletException if unable to build the response message
     * @throws IOException if unable to write the servlet response
     */
    public void sendDirectResponse(OpenIDRequestContext context) throws ServletException, IOException {
        OpenIDProviderHelper.writeDirectResponse(context.getOpenIDResponse(), context.getServletResponse().getWriter());
    }

    /**
     * Send an OpenID indirect response message.
     * 
     * @param context request context
     * @throws ServletException if unable to build the response message
     * @throws IOException if unable to write the servlet response
     */
    public void sendIndirectResponse(OpenIDRequestContext context) throws ServletException, IOException {
        URL url = new URL(((AuthenticationRequest) context.getOpenIDRequest()).getReturnTo());
        OpenIDProviderHelper.sendIndirectResponse(context.getOpenIDResponse(), context.getServletResponse(), url);
    }

}