/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.profile.provider;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.util.StringResourceRepository;
import org.opensaml.xml.util.DatatypeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.openid.common.OpenIDConstants;
import edu.internet2.middleware.shibboleth.common.util.StringResourceLoader;
import edu.internet2.middleware.shibboleth.idp.profile.IdPProfileHandlerManager;

/**
 *
 */
public class DiscoveryHandler {

    /** Classpath location of the template to use to render the user page. */
    private static final String PAGE_TEMPLATE_PATH = "/openid-user.vm";

    /** Classpath location of the template to use to render the user XRDS. */
    private static final String XRDS_TEMPLATE_PATH = "/openid-xrds.vm";

    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(DiscoveryHandler.class);

    /** Velocity engine used to render the templates. */
    private VelocityEngine velocityEngine;

    /** Profile handler manager. */
    private IdPProfileHandlerManager profileHandlerManager;

    /** Constructor. */
    public DiscoveryHandler() {

        // initialize Velocity template repository
        StringResourceRepository repository = StringResourceLoader.getRepository();

        try {
            String templateString = DatatypeHelper.inputstreamToString(getClass().getResourceAsStream(
                    XRDS_TEMPLATE_PATH), null);
            repository.putStringResource(XRDS_TEMPLATE_PATH, templateString);
        } catch (IOException e) {
            log.error("Unable to load velocity template {}", XRDS_TEMPLATE_PATH);
        }

        try {
            String templateString = DatatypeHelper.inputstreamToString(getClass().getResourceAsStream(
                    PAGE_TEMPLATE_PATH), null);
            repository.putStringResource(PAGE_TEMPLATE_PATH, templateString);
        } catch (IOException e) {
            log.error("Unable to load velocity template {}", PAGE_TEMPLATE_PATH);
        }
    }

    /**
     * Get the velocity engine used to render the templates.
     * 
     * @return velocity engine
     */
    public VelocityEngine getVelocityEngine() {
        return velocityEngine;
    }

    /**
     * Set the velocity engine used to render the templates.
     * 
     * @param engine velocity engine
     */
    public void setVelocityEngine(VelocityEngine engine) {
        velocityEngine = engine;
    }

    /**
     * Get the profile handler manager.
     * 
     * @return the profile handler manager
     */
    public IdPProfileHandlerManager getProfileHandlerManager() {
        return profileHandlerManager;
    }

    /**
     * Set the profile handler manager.
     * 
     * @param manager the profile handler manager
     */
    public void setProfileHandlerManager(IdPProfileHandlerManager manager) {
        profileHandlerManager = manager;
    }

    /**
     * Process request for user page or XRDS descriptor.
     * 
     * @param request HTTP request
     * @param response HTTP response
     * @throws IOException if unable to send response
     */
    public void processUserRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String[] parts = request.getPathInfo().substring(1).split("/");
        if (parts.length < 2) {
            return;
        }

        String principalName = parts[1];
        String template;

        if (parts.length == 3 && "xrds".equals(parts[2])) {
            template = XRDS_TEMPLATE_PATH;
        } else {
            template = PAGE_TEMPLATE_PATH;
        }

        VelocityContext context = new VelocityContext();
        context.put("type", OpenIDConstants.IDENTIFIER_DISCOVERY_TYPE);
        context.put("userURL", buildURL(request, request.getServletPath() + "/user/" + principalName));
        context.put("serverURL", buildURL(request, request.getServletPath() + "/provider"));
        context.put("papePolicies", profileHandlerManager.getLoginHandlers().keySet());

        processTemplate(context, template, response.getOutputStream());
    }

    /**
     * Process a request for the OpenID Provider's XRDS descriptor.
     * 
     * @param request HTTP request
     * @param response HTTP response
     * @throws IOException if unable to send response
     */
    public void processProviderRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String[] parts = request.getPathInfo().substring(1).split("/");
        if (parts.length < 1) {
            return;
        }
        
        String template;

        if (parts.length == 2 && "xrds".equals(parts[1])) {
            template = XRDS_TEMPLATE_PATH;
        } else {
            template = PAGE_TEMPLATE_PATH;
        }
        
        VelocityContext context = new VelocityContext();
        context.put("type", OpenIDConstants.PROVIDER_DISCOVERY_TYPE);
        context.put("serverURL", buildURL(request, request.getServletPath() + "/provider"));
        context.put("papePolicies", profileHandlerManager.getLoginHandlers().keySet());

        processTemplate(context, template, response.getOutputStream());
    }

    /**
     * Process a velocity template using the given context and write it to the output stream.
     * 
     * @param context velocity context used to populate the template
     * @param templateName classpath location of the velocity template to use
     * @param output output stream to write to
     */
    private void processTemplate(VelocityContext context, String templateName, OutputStream output) {
        try {
            OutputStreamWriter responseWriter = new OutputStreamWriter(output);
            Template template = velocityEngine.getTemplate(templateName);
            template.merge(context, responseWriter);
            responseWriter.flush();
        } catch (Throwable t) {
            log.error("Unable to evaluate velocity user template: {}", t);
        }
    }

    private URL buildURL(HttpServletRequest request, String path) {
        try {
            URL requestURL = new URL(request.getRequestURL().toString());
            return new URL(requestURL, request.getContextPath() + path);
        } catch (MalformedURLException e) {
            return null;
        }
    }

}