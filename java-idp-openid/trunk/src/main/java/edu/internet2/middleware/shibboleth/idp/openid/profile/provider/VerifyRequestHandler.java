/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.openid.profile.provider;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.openid.Configuration;
import edu.internet2.middleware.openid.common.OpenIDConstants;
import edu.internet2.middleware.openid.message.VerifyRequest;
import edu.internet2.middleware.openid.message.VerifyResponse;
import edu.internet2.middleware.openid.security.Association;
import edu.internet2.middleware.openid.security.NonceValidator;
import edu.internet2.middleware.openid.security.SecurityException;
import edu.internet2.middleware.openid.security.SecurityUtils;
import edu.internet2.middleware.shibboleth.idp.openid.profile.OpenIDRequestContext;

/**
 * Association Manager.
 */
public class VerifyRequestHandler extends BaseRequestHandler<VerifyRequest> {

    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(VerifyRequestHandler.class);

    /** Nonce validator. */
    private NonceValidator nonceValidator;

    /** Constructor. */
    public VerifyRequestHandler() {
    }

    /**
     * Get the nonce validator.
     * 
     * @return nonce validator
     */
    public NonceValidator getNonceValidator() {
        return nonceValidator;
    }

    /**
     * Set the nonce validator.
     * 
     * @param validator nonce validator
     */
    public void setNonceValidator(NonceValidator validator) {
        nonceValidator = validator;
    }

    /** {@inheritDoc} */
    public void processRequest(OpenIDRequestContext context) throws ServletException, IOException {
        log.info("handling verify request");

        VerifyRequest request = (VerifyRequest) context.getOpenIDRequest();

        VerifyResponse response = (VerifyResponse) Configuration.getMessageBuilders().getBuilder(
                new QName(OpenIDConstants.OPENID_20_NS, OpenIDConstants.VERIFICATION_RESPONSE_MODE)).buildObject();

        if (!nonceValidator.isValid(request.getEndpoint(), request.getResponseNonce())) {
            response.setValid(false);
        } else {
            Association association = getAssociationManager().getAssociation(request.getAssociationHandle());
            if (association == null) {
                log.info("unable to find assertion with handle: {}", request.getAssociationHandle());
                response.setValid(false);
                context.setOpenIDResponse(response);
                return;
            }

            try {
                if (SecurityUtils.signatureIsValid(request, association)) {
                    log.info("request verified to be valid");
                    response.setValid(true);
                } else {
                    log.info("request signature is not valid");
                    response.setValid(false);
                }
            } catch (SecurityException e) {
                log.error(e.getMessage());
            }
        }

        if (response.isValid() && request.getInvalidateHandle() != null) {
            response.setInvalidateHandle(request.getInvalidateHandle());
        }

        context.setOpenIDResponse(response);
        sendDirectResponse(context);
    }

    /** {@inheritDoc} */
    public Class getRequestType() {
        return VerifyRequest.class;
    }

}