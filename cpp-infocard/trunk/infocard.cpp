/*
 *  Copyright 2001-2008 Internet2
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * infocard.cpp
 *
 * Infocard extension library
 */

#if defined (_MSC_VER) || defined(__BORLANDC__)
# include "config_win32.h"
#else
# include "config.h"
#endif

#ifdef WIN32
# define _CRT_NONSTDC_NO_DEPRECATE 1
# define _CRT_SECURE_NO_DEPRECATE 1
# define Infocard_EXPORTS __declspec(dllexport)
#else
# define Infocard_EXPORTS
#endif

#include <shibsp/base.h>
#include <shibsp/exceptions.h>
#include <shibsp/Application.h>
#include <shibsp/ServiceProvider.h>
#include <shibsp/SPConfig.h>
#include <shibsp/handler/AssertionConsumerService.h>
#include <shibsp/handler/LogoutHandler.h>
#include <shibsp/handler/SessionInitiator.h>
#include <xmltooling/util/NDC.h>
#include <xmltooling/util/URLEncoder.h>
#include <xmltooling/util/XMLHelper.h>
#include <xmltooling/XMLObjectBuilder.h>
#include <log4shib/Category.hh>
#include <xercesc/util/XMLUniDefs.hpp>

#include <xmltooling/encryption/Decrypter.h>
#ifndef SHIBSP_LITE
#include <fstream>
#include <sstream>

#include <xsec/framework/XSECProvider.hpp>
#include <xsec/framework/XSECException.hpp>
#include <xsec/xenc/XENCCipher.hpp>
#include <xsec/enc/OpenSSL/OpenSSLCryptoKeyRSA.hpp>
#include <xsec/enc/XSECKeyInfoResolverDefault.hpp>


#include <xsec/dsig/DSIGKeyInfoX509.hpp>
#include <xmltooling/signature/KeyInfo.h>
#include <xmltooling/security/KeyInfoResolver.h>
#include <xmltooling/security/X509Credential.h>
#include <xsec/dsig/DSIGSignature.hpp>
#include <xsec/enc/XSECCryptoUtils.hpp>
#include <xsec/framework/XSECDefs.hpp>
#include <xsec/framework/XSECError.hpp>


#include <shibsp/SessionCache.h>
#include <shibsp/attribute/Attribute.h>
#include <shibsp/attribute/SimpleAttribute.h>
#include <shibsp/attribute/filtering/AttributeFilter.h>
#include <shibsp/attribute/filtering/BasicFilteringContext.h>
#include <shibsp/attribute/resolver/AttributeExtractor.h>
#include <shibsp/attribute/resolver/ResolutionContext.h>
#include <saml/SAMLConfig.h>
#include <saml/saml1/core/Assertions.h>
#include <saml/saml1/profile/AssertionValidator.h>
#include <saml/saml2/core/Assertions.h>
#include <saml/saml2/metadata/Metadata.h>
#include <saml/saml2/metadata/EndpointManager.h>
#include <saml/saml2/profile/AssertionValidator.h>
#include <xmltooling/impl/AnyElement.h>
#include <xmltooling/validation/ValidatorSuite.h>

#include "xmltooling/security/CredentialResolver.h"
#include "xmltooling/security/Credential.h"
#include "xmltooling/util/TemplateEngine.h"

#include <openssl/rsa.h>

// using namespace opensaml::saml1;
using namespace opensaml::saml2md;
using namespace opensaml;
using namespace xmlsignature;
#endif
using namespace shibsp;
using namespace xmltooling;
using namespace xercesc;
using namespace log4shib;
using namespace std;


#define WSFED_NS "http://schemas.xmlsoap.org/ws/2003/07/secext"
#define WSTRUST_NS "http://schemas.xmlsoap.org/ws/2005/02/trust"
#define WSENC "http://www.w3.org/2001/04/xmlenc#"
#define ICARD_BINDING "urn:mace:shibboleth:2.0:infocard"

namespace {

#if defined (_MSC_VER)
    #pragma warning( push )
    #pragma warning( disable : 4250 )
#endif


/* Infocard session initiator */

    class SHIBSP_DLLLOCAL InfocardSessionInitiator : public SessionInitiator, public AbstractHandler, public RemotedHandler
    {
    public:
        InfocardSessionInitiator(const DOMElement* e, const char* appId)
                : AbstractHandler(e, Category::getInstance(SHIBSP_LOGCAT".SessionInitiator.Infocard")), m_appId(appId), m_binding(ICARD_BINDING) {
            // If Location isn't set, defer address registration until the setParent call.
            pair<bool,const char*> loc = getString("Location");
            if (loc.first) {
                string address = m_appId + loc.second + "::run::InfocardSI";
                setAddress(address.c_str());
            }
            form_template = getString("template");
            if (form_template.first) m_log.debug(" .. template: %s", form_template.second );
        }
        virtual ~InfocardSessionInitiator() {}
        
        void setParent(const PropertySet* parent) {
           DOMPropertySet::setParent(parent);
           pair<bool,const char*> loc = getString("Location");
           if (loc.first) {
               string address = m_appId + loc.second + "::run::InfocardSI";
               setAddress(address.c_str());
           }
           else {
               m_log.warn("no Location property in Infocard SessionInitiator (or parent), can't register as remoted handler");
           }
        }

        void receive(DDF& in, ostream& out);
        pair<bool,long> run(SPRequest& request, string& entityID, bool isHandler=true) const;

    private:
        pair<bool,long> doRequest(
            const Application& application,
            HTTPResponse& httpResponse,
            const char* acsLocation,
            string& relayState,
            const char* tplate,
            const char* entityId
            ) const;
        string m_appId;
        auto_ptr_XMLCh m_binding;
        pair<bool,const char*> form_template;
    };



    class SHIBSP_DLLLOCAL InfocardConsumer : public shibsp::AssertionConsumerService
    {
    public:
        InfocardConsumer(const DOMElement* e, const char* appId)
            : shibsp::AssertionConsumerService(e, appId, Category::getInstance(SHIBSP_LOGCAT".SSO.Infocard"))
#ifndef SHIBSP_LITE
                ,m_protocol(ICARD_BINDING)
#endif
            {}
        virtual ~InfocardConsumer() {}

#ifndef SHIBSP_LITE
        void generateMetadata(SPSSODescriptor& role, const char* handlerURL) const {
            AssertionConsumerService::generateMetadata(role, handlerURL);
            role.addSupport(m_protocol.get());
        }

        auto_ptr_XMLCh m_protocol;

    private:
        void implementProtocol(
            const Application& application,
            const HTTPRequest& httpRequest,
            HTTPResponse& httpResponse,
            SecurityPolicy& policy,
            const PropertySet* settings,
            const XMLObject& xmlObject
            ) const;
#endif
    };

#if defined (_MSC_VER)
    #pragma warning( pop )
#endif



#ifndef SHIBSP_LITE
    class InfocardDecoder : public MessageDecoder
    {
        auto_ptr_XMLCh m_ns;
    public:
        InfocardDecoder() : m_ns(WSENC) {}
        virtual ~InfocardDecoder() {}
        
        XMLObject* decode(string& relayState, const GenericRequest& genericRequest, SecurityPolicy& policy) const;

    protected:
        void extractMessageDetails(
            const XMLObject& message, const GenericRequest& req, const XMLCh* protocol, SecurityPolicy& policy
            ) const {
        }
    };

    MessageDecoder* InfocardDecoderFactory(const pair<const DOMElement*,const XMLCh*>& p)
    {
        return new InfocardDecoder();
    }
#endif

    SessionInitiator* InfocardSessionInitiatorFactory(const pair<const DOMElement*,const char*>& p)
    {
        return new InfocardSessionInitiator(p.first, p.second);
    }

    Handler* InfocardConsumerFactory(const pair<const DOMElement*,const char*>& p)
    {
        return new InfocardConsumer(p.first, p.second);
    }

    const XMLCh EncryptedData[] =      UNICODE_LITERAL_13(E,n,c,r,y,p,t,e,d,D,a,t,a);
};

extern "C" int Infocard_EXPORTS xmltooling_extension_init(void*)
{
    SPConfig& conf=SPConfig::getConfig();
    conf.SessionInitiatorManager.registerFactory("Infocard", InfocardSessionInitiatorFactory);
    conf.AssertionConsumerServiceManager.registerFactory(ICARD_BINDING, InfocardConsumerFactory);
#ifndef SHIBSP_LITE
    SAMLConfig::getConfig().MessageDecoderManager.registerFactory(ICARD_BINDING, InfocardDecoderFactory);
    XMLObjectBuilder::registerBuilder(QName(WSTRUST_NS,"EncryptedData"), new AnyElementBuilder());
#endif
    return 0;
}

extern "C" void Infocard_EXPORTS xmltooling_extension_term()
{
    /* should get unregistered during normal shutdown...
    SPConfig& conf=SPConfig::getConfig();
    conf.SessionInitiatorManager.deregisterFactory("Infocard");
    conf.AssertionConsumerServiceManager.deregisterFactory(ICARD_BINDING);
#ifndef SHIBSP_LITE
    SAMLConfig::getConfig().MessageDecoderManager.deregisterFactory(ICARD_BINDING);
#endif
    */
}

// SESSION INITIALIZER

pair<bool,long> InfocardSessionInitiator::run(SPRequest& request, string& entityID, bool isHandler) const
{
    string target;
    const Handler* ACS=NULL;
    const char* option;
    const Application& app=request.getApplication();

    if (isHandler) {
        option = request.getParameter("target");
        if (option) target = option;
        recoverRelayState(request.getApplication(), request, request, target, false);
    } else {
        target=request.getRequestURL();
    }

    // Don't know what we would do with more than one ACS
    if (!ACS) {
        // Get the Infocard endpoint.
        const vector<const Handler*>& handlers = app.getAssertionConsumerServicesByBinding(m_binding.get());

        // Index comes from request, or default set in the handler, or we just pick the first endpoint.
        pair<bool,unsigned int> index = make_pair(false,0);
        if (isHandler) {
            option = request.getParameter("acsIndex");
            if (option)
                index = make_pair(true, atoi(option));
        }
        if (!index.first) index = getUnsignedInt("defaultACSIndex");
        if (index.first) {
            for (vector<const Handler*>::const_iterator h = handlers.begin(); !ACS && h!=handlers.end(); ++h) {
                if (index.second == (*h)->getUnsignedInt("index").second)
                    ACS = *h;
            }
        } else if (!handlers.empty()) {
            ACS = handlers.front();
        }
    }
    if (!ACS) throw ConfigurationException("Unable to locate Infocard response endpoint.");

    // Compute the ACS URL. Add the ACS location to the base handlerURL.

    string ACSloc=request.getHandlerURL(target.c_str());
    pair<bool,const char*> loc=ACS ? ACS->getString("Location") : pair<bool,const char*>(false,NULL);
    if (loc.first) ACSloc += loc.second;

    if (isHandler) {
        // We may already have RelayState set if we looped back here,
        // but just in case target is a resource, we reset it back.
        target.erase();
        option = request.getParameter("target");
        if (option) target = option;
    }

    m_log.debug("attempting to initiate session using Infocard initiator.");

    if (SPConfig::getConfig().isEnabled(SPConfig::OutOfProcess))
        return doRequest(app, request, ACSloc.c_str(), target, NULL, NULL);

    // Remote the call.
    DDF out,in = DDF(m_address.c_str()).structure();
    DDFJanitor jin(in), jout(out);
    in.addmember("application_id").string(app.getId());
    if (form_template.first) {
       cout << "adding template:" << form_template.second << XERCES_STD_QUALIFIER endl;
       in.addmember("template").string(form_template.second);
    }
    in.addmember("acsLocation").string(ACSloc.c_str());
    if (!target.empty()) in.addmember("RelayState").string(target.c_str());
    if (!entityID.empty()) in.addmember("entity_id").string(entityID.c_str());

    // Remote the processing.
    out = request.getServiceProvider().getListenerService()->send(in);
    return unwrap(request, out);
}


void InfocardSessionInitiator::receive(DDF& in, ostream& out)
{
    // Find application.
    const char* aid=in["application_id"].string();
    const Application* app=aid ? SPConfig::getConfig().getServiceProvider()->getApplication(aid) : NULL;
    if (!app) {
        m_log.error("couldn't find application (%s) to generate Infocard request", aid ? aid : "(missing)");
        throw ConfigurationException("Unable to locate application for new session, deleted?");
    }

    const char* acsLocation = in["acsLocation"].string();
    if (!acsLocation) throw ConfigurationException("No acsLocation parameter supplied to remoted SessionInitiator.");

    DDF ret(NULL);
    DDFJanitor jout(ret);

    // Wrap the outgoing object with a Response facade.
    auto_ptr<HTTPResponse> http(getResponse(ret));

    string relayState(in["RelayState"].string() ? in["RelayState"].string() : "");

    const char* tplate = in["template"].string();
    const char* entityId = in["entity_id"].string();

    // Since we're remoted, the result should either be a throw, which we pass on,
    // a false/0 return, which we just return as an empty structure, or a response/redirect,
    // which we capture in the facade and send back.
    doRequest(*app, *http.get(), acsLocation, relayState, tplate, entityId);
    out << ret;
}

pair<bool,long> InfocardSessionInitiator::doRequest(
    const Application& app,
    HTTPResponse& httpResponse,
    const char* acsLocation,
    string& relayState,
    const char* tplate,
    const char* entityId
    ) const
{
#ifndef SHIBSP_LITE

    // Here's where we load the infocard form 

    // Use metadata to invoke the SSO service directly.
    MetadataProvider* m=app.getMetadataProvider();
    Locker locker(m);

    preserveRelayState(app, httpResponse, relayState);

    char timebuf[32];
    sprintf(timebuf,"%lu",time(NULL));

    // const URLEncoder* urlenc = XMLToolingConfig::getConfig().getURLEncoder();

    TemplateEngine* engine = XMLToolingConfig::getConfig().getTemplateEngine();
    if (!engine) throw BindingException("Infocard initiation requires a TemplateEngine instance.");

    ifstream infile(form_template.second);
    if (!infile) throw BindingException("Failed to open HTML template for Infocard post ($1).", params(1,form_template.second));

    TemplateEngine::TemplateParameters pmap;
    pmap.m_map["wctx"] = relayState;     // final destination
    pmap.m_map["action_target"] = acsLocation;   // ACS destination
    if (entityId) pmap.m_map["entity_id"] = entityId;   // If entity given
    stringstream s;
    engine->run(infile, s, pmap);

    httpResponse.setContentType("text/html");
    httpResponse.setResponseHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    httpResponse.setResponseHeader("Expires", "Sat, 1 Jan 2000 01:01:01 GMT");
    return make_pair(true, httpResponse.sendResponse(s));

#else
    return make_pair(false,0);
#endif
}

#ifndef SHIBSP_LITE


// DECODE

XMLObject* InfocardDecoder::decode(string& relayState, const GenericRequest& genericRequest, SecurityPolicy& policy) const
{
#ifdef _DEBUG
    xmltooling::NDC ndc("decode");
#endif
    Category& log = Category::getInstance(SHIBSP_LOGCAT".MessageDecoder.Infocard");

    log.debug("validating input");
    const HTTPRequest* httpRequest=dynamic_cast<const HTTPRequest*>(&genericRequest);
    if (!httpRequest) throw BindingException("Unable to cast request object to HTTPRequest type.");
    if (strcmp(httpRequest->getMethod(),"POST")) throw BindingException("Invalid HTTP method ($1).", params(1, httpRequest->getMethod()));

    const char *wctx = httpRequest->getParameter("wctx");
    if (wctx) relayState = wctx;

    const char* infocardToken = httpRequest->getParameter("xmlToken");
    if (!infocardToken) throw BindingException("Missing xmlToken.");

    // Parse and bind the document into an XMLObject.
    istringstream is(infocardToken);

    DOMDocument* doc = (XMLToolingConfig::getConfig().getParser()).parse(is); 

    XercesJanitor<DOMDocument> janitor(doc);
    auto_ptr<XMLObject> xmlObject(XMLObjectBuilder::buildOneFromElement(doc->getDocumentElement(), true));
    janitor.release();

#ifdef MAYBENEVERNEEDTHIS
    // Run through the policy.
    policy.evaluate(*xmlObject.get(), &genericRequest);
#endif
    
    return xmlObject.release();
}


void addAttribute(ResolutionContext &ctx, const char *id, XMLCh *txt)
{
   vector<string> ids;
   ids.push_back(id);
   auto_ptr<SimpleAttribute> simple(new SimpleAttribute(ids));
   vector<string>& dest = simple->getValues();
   stringstream ctout;
   ctout << XMLString::transcode(txt);
   dest.push_back(ctout.str().c_str());
   ctx.getResolvedAttributes().push_back(simple.release());
}


void InfocardConsumer::implementProtocol(
    const Application& application,
    const HTTPRequest& httpRequest,
    HTTPResponse& httpResponse,
    SecurityPolicy& policy,
    const PropertySet* settings,
    const XMLObject& xmlObject
    ) const
{

    // Implementation of Infocard profile.
    m_log.debug("processing message against Infocard profile");

    // With Infocard, all the security comes from the assertion, which is encrypted with our public key
    // signed by the issuer, as has us as the target

    DOMElement *eadom = xmlObject.getDOM();
    if (!eadom) throw FatalProfileException("Incoming message was not a infocard response.");

    XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument *tdoc = NULL;
    try {
       XSECProvider prov;
       XENCCipher *cipher;

       // Our Cipher doesn't deal with encrypted keyinfo, so find that element and remove it
       // (must be a better way)
    
       const XMLCh kifo[] = UNICODE_LITERAL_7(K,e,y,I,n,f,o);
       const XMLCh enck[] = UNICODE_LITERAL_12(E,n,c,r,y,p,t,e,d,K,e,y);

       // m_log.debug(" .. looking for encrypted key info");
       DOMElement *ki = XMLHelper::getFirstChildElement(eadom, kifo );
       if (ki) {
            DOMElement *ek = XMLHelper::getFirstChildElement(ki, enck );
            if (ek) {
                 DOMElement *ki2 = XMLHelper::getFirstChildElement(ek, kifo );
                 if (ki2) {
                    m_log.debug( ".. removing encrypted keyinfo");
                    // might sometime check the signature against our cert.
                    ek->removeChild(ki2);
                 }
            }
       }

       cipher = prov.newCipher(eadom->getOwnerDocument());
       CredentialResolver *cr=application.getCredentialResolver();
       const Credential *cred = cr->resolve(NULL);
       XSECCryptoKey *k = cred->getPrivateKey()->clone();
       cipher->setKEK(k);

       m_log.debug(" .. decrypting");
       tdoc = cipher->decryptElement((DOMElement *) eadom);

    }  catch (XSECException &e) {
        m_log.warn("An error occurred decryption the asserton\n");
        throw FatalProfileException("An error occurred decryption the asserton");
    }

    // Process the decrypted message

    DOMElement *assn = (DOMElement*) tdoc->getFirstChild();

    string ebuf;
    XMLHelper::serialize(tdoc->getFirstChild(), ebuf, true);

    const XMLObjectBuilder* b=XMLObjectBuilder::getBuilder(assn);
    XMLObject *ass = b->buildFromElement(assn, true);

    vector<const Assertion*> tokens;
    time_t now = time(NULL);

    Signature *tok_sig = NULL;

    if (const saml1::Assertion* token1 = dynamic_cast<const saml1::Assertion*>(ass)) {
       m_log.debug(" .. is saml1 assertion");
       if (!token1->getSignature()) throw FatalProfileException("Incoming SAML1 message not signed.");
       extractMessageDetails(*token1, m_protocol.get(), policy);
       policy.evaluate(*token1);

       if (!policy.isAuthenticated()) {
          m_log.debug ("Treating this as a personal card.");
          // save the signature we can pull keyinfo later 
          tok_sig =  token1->getSignature();
          tokens.push_back((const Assertion*)token1);
       } else {
          const EntityDescriptor* entity = policy.getIssuerMetadata() ?
                         dynamic_cast<const EntityDescriptor*>(policy.getIssuerMetadata()->getParent()) : NULL;
          saml1::AssertionValidator sso1Validator(application.getRelyingParty(entity)->getXMLString("entityID").second,
                         application.getAudiences(), now);
          // add this back when we can edit the audience
          // sso1Validator.validateAssertion(*token1);
          tokens.push_back((const Assertion*)token1);
      }

    } else if (const saml2::Assertion* token2 = dynamic_cast<const saml2::Assertion*>(ass)) {
       m_log.debug(" .. is saml2 assertion");
       if (!token2->getSignature()) throw FatalProfileException("Incoming SAML2 message not signed.");
       extractMessageDetails(*token2, m_protocol.get(), policy);
       policy.evaluate(*token2);

       if (!policy.isAuthenticated()) {
          m_log.debug ("Treating this as a personal card.");
          // save the signature we can pull keyinfo later 
          tok_sig = token2->getSignature();
          tokens.push_back((const Assertion*)token1);
       } else {
          const EntityDescriptor* entity = policy.getIssuerMetadata() ?
                         dynamic_cast<const EntityDescriptor*>(policy.getIssuerMetadata()->getParent()) : NULL;
          saml2::AssertionValidator sso2Validator(application.getRelyingParty(entity)->getXMLString("entityID").second,
                         application.getAudiences(), now);
          // sso2Validator.validateAssertion(*token2);
          tokens.push_back((const Assertion*)token2);
       }

    } else throw FatalProfileException("Incoming message not saml1 or saml2.");


    // m_log.debug("Infocard profile processing completed successfully");

    // Now we have to extract the authentication details for attribute and session setup.

    // Session expiration for Infocard is purely SP-driven, and the method is mapped to a ctx class.
    const PropertySet* sessionProps = application.getPropertySet("Sessions");
    pair<bool,unsigned int> lifetime = sessionProps ? sessionProps->getUnsignedInt("lifetime") : pair<bool,unsigned int>(true,28800);
    if (!lifetime.first || lifetime.second == 0) lifetime.second = 28800;

    // We've successfully "accepted" the SSO token.
    // To complete processing, we need to extract and resolve attributes and then create the session.

    // Normalize the SAML 1.x NameIdentifier...
    // (What if we have a saml2 assertion?)

    auto_ptr<saml2::NameID> nameid(NULL);

    auto_ptr<ResolutionContext> ctx(
        resolveAttributes(
            application,
            policy.getIssuerMetadata(),
            m_protocol.get(),
            NULL, // n,
            nameid.get(),
            NULL, // ssoStatement->getAuthenticationMethod(),
            NULL,
            &tokens
            )
        );

    if (ctx.get()) {
        // Copy over any new tokens, but leave them in the context for cleanup.
        tokens.insert(tokens.end(), ctx->getResolvedAssertions().begin(), ctx->getResolvedAssertions().end());

        // if this is a personal card, put the keyinfo in an attribute
        if (tok_sig!=NULL) {

           // get the keyinfo's public key
           DSIGKeyInfoList *sig_ki = tok_sig->getXMLSignature()->getKeyInfoList();
           XSECKeyInfoResolverDefault def;
           XSECCryptoKeyRSA *rkey = static_cast<XSECCryptoKeyRSA*>(def.resolveKey(sig_ki));
           RSA *rsa = static_cast<OpenSSLCryptoKeyRSA*>(rkey)->getOpenSSLRSA();

            
           // make an sha1 hash of it and store in an attribute
           auto_ptr<XSECCryptoHash> h(XSECPlatformUtils::g_cryptoProvider->hashSHA1());
           char *rsatext = BN_bn2hex(rsa->n);
           m_log.debug("rsa pubkey: %s", rsatext);
           int l = strlen(rsatext);
           h->hash((unsigned char*)rsatext, l);
           unsigned char buf[31];
           if (h->finish(buf,30)==20) {
              XMLCh *rch = EncodeToBase64XMLCh(buf, 20); 
              addAttribute(*ctx.get(), "Shib_Infocard_Key", rch);
           }
           OPENSSL_free(rsatext);
        }
    }

    application.getServiceProvider().getSessionCache()->insert(
            application,
            httpRequest,
            httpResponse,
            now + lifetime.second,
            policy.getIssuerMetadata() ? dynamic_cast<const EntityDescriptor*>(policy.getIssuerMetadata()->getParent()) : NULL,
            m_protocol.get(),
            nameid.get(),
            NULL, // ssoStatement->getAuthenticationInstant() ? ssoStatement->getAuthenticationInstant()->getRawData() : NULL,
            NULL,
            NULL, // ssoStatement->getAuthenticationMethod(),
            NULL,
            &tokens,
            ctx.get() ? &ctx->getResolvedAttributes() : NULL
		    );

}


#endif
