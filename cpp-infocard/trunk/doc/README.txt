December 11, 2007
Version 1.0 - alpha

This extension adds information card support
to a Shibboleth 2.0 Service Provider.

Build and install:

Most configure parameters are same as for shib.
You must provide shib's install path.

e.g. 

./configure --prefix=/usr/local/shib-R20 \
  --with-shibboleth=/usr/local/shib-R20 \
  --with-saml=/usr/local/ \
  --with-xmlsec=/usr/local \
  --with-log4shib=/usr/local \
  --with-xerces=/usr/local \
  --with-xmltooling=/usr/local \
  --with-curl=/usr/local \
  --with-openssl=/usr/local 


then make and make install ought to do it.


Configure:

1) Install one or more templates in your shib etc/shibboleth directory

2) Add extension paths to shibboleth2.xml

   e.g.

      <OutOfProcess logger="/usr/local/shib-R20/etc/shibboleth/shibd.logger">
        <Extensions>
          <Library path="/usr/local/shib-R20/lib/infocard/infocard.so" fatal="true"/>
        </Extensions>
      </OutOfProcess>

   and

      <InProcess logger="/usr/local/shib-R20/etc/shibboleth/native.logger">
        <Extensions>
          <Library path="/usr/local/shib-R20/lib/infocard/infocard-lite.so" fatal="true"/>
        </Extensions>
        ...
      </InProcess>

3) Add an infocard session initiator (uses a template)

      <!-- Infocard service. -->
        <SessionInitiator type="Chaining" Location="/SHIB/Infocard" id="Infocard" isDefault="false" relayState="cookie">
            <SessionInitiator type="Infocard" template="/usr/local/shib-R20/etc/shibboleth/infocard-shib.html"/>
        </SessionInitiator>


4) Add a binding

      <!-- Infocard consumer -->
      <md:AssertionConsumerService Location="/SAML/Infocard" index="5"
                Binding="urn:mace:shibboleth:2.0:infocard"/>



Issues:

1) Relying parties

   The IdP needs to know the SP's relying party identity, both
   to lookup metadata and for the AudienceRestrictionCondition element
   in its response.  The only information it gets, at present,
   from a request is the URL that initiated authentication.
  
   The shib IdP truncates the URL just prior to the path part
   and calls that the relying party. So, for instance,
 
     https://example.edu/demos/test1/index.cgi

   would translate to a rp of

     https://example.edu/


   Not sure what other IdPs do. Probably trouble there.


2) Encryption certificate

   The Shib IdP needs a cert with which to encrypt the assertion.
   If it doesn't already have that in metadata it gets one
   by doing a GET of the SP's base address and using that cert.
   

More documentation will be available on the spaces web site

