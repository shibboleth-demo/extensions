Shibboleth IdP 2 Audit Log Analysis Tool

This analysis tool operates over one, or more, IdP audit log files and 
provides the follow statistics:
 * Entity IDs of unique relying parties
 * Number of unique relying parties
 * Number of authentication events
 * Number of unique authenticated principals
 * Number of authentication events per relying party
 * SAML profiles used per relying party
 
 Requirements:
 * Python 2.3+
 * Shibboleth 2 IdP Audit Log Files
 
 Usage:
   python loganalysis.py [options] <log_files>
   
   Options:
     -r, --relyingparties - list of unique relying parties, sorted by name
     -c, --rpcount - number of unique relying parties
     -u, --users - number of unique principals
     -l, --logins - number of logins
     -p, --rplogins - number of events per relying party, by name
     -n, --rploginssort - number of events per relying party, sorted numerically
     -m, --msgprofiles - usage of SAML message profiles per relying party 
     -q, --quiet - suppress all descriptive or decorative output
     
   More than one option may be used at a time.