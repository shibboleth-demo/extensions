<?xml version="1.0" encoding="UTF-8"?>

<schema targetNamespace="urn:mace:shibboleth:2.0:idp:ext:delegation:relying-party" xmlns="http://www.w3.org/2001/XMLSchema"
    xmlns:samlrp="urn:mace:shibboleth:2.0:relying-party:saml" 
    xmlns:sec="urn:mace:shibboleth:2.0:security"
    xmlns:delrp="urn:mace:shibboleth:2.0:idp:ext:delegation:relying-party"
    elementFormDefault="qualified">

    <import namespace="urn:mace:shibboleth:2.0:relying-party:saml"
        schemaLocation="classpath:/schema/shibboleth-2.0-relying-party-saml.xsd" />

    <import namespace="urn:mace:shibboleth:2.0:security"
        schemaLocation="classpath:/schema/shibboleth-2.0-security.xsd" />

    <complexType name="SAML2SSOProfile">
        <annotation>
            <documentation>SAML 2.0 delegation-aware single sign-on communication profile configuration.</documentation>
        </annotation>
        <complexContent>
            <extension base="samlrp:SAML2SSOProfile">
                <attribute name="allowTokenDelegation" type="boolean" use="optional">
                    <annotation>
                        <documentation>
                            A boolean flag indicating whether an Assertion evaluated as invalid should be
                            treated as a fatal error.
                        </documentation>
                    </annotation>
                </attribute>
            </extension>
        </complexContent>
    </complexType>
    
    <complexType name="LibertyIDWSFSSOSProfile">
        <annotation>
            <documentation>Liberty ID-WSF SSOS communication profile configuration.</documentation>
        </annotation>
        <complexContent>
            <extension base="delrp:SAML2SSOProfile">
                <sequence>
                    <element name="DelegationRestriction" type="string" minOccurs="0" maxOccurs="unbounded">
                        <annotation>
                            <documentation>
                                Controls to which requesting service providers a delegate token may be issued.
                                
                                The effective policy is applied based on the entity actor which is functioning
                                in the SAML presenter role, which presents the AuthnRequest on behalf of the SAML
                                relying party which issued the AuthnRequest. The requester must appear in the
                                DelegationRestriction list of the presenter's effective profile configuration
                                in order for the request to be honored.
                            </documentation>
                        </annotation>
                    </element>
                </sequence>
                <attribute name="maximumTokenDelegationChainLength" type="positiveInteger" use="optional">
                    <annotation>
                        <documentation>
                            Limits the total number of delegates that may be derived from the initial SAML token.
                            The identity provider will not accept, and therefore will also not issue, Assertion tokens
                            with a delegation chain length greater than this value.
                            
                            The length of a delegation chain is evaluated as the number of Delegate children within an
                            Assertion's DelegationRestrictionType Condition element.
                            
                            The value used when applying the policy is the policy value in effect for the first Delegate in the
                            delegation chain.
                            
                            Defaults to 1. 
                        </documentation>
                    </annotation>
                </attribute>
            </extension>
        </complexContent>
    </complexType>
    
    <!-- Security rules -->
    <complexType name="WSSSAML20AssertionTokenRule">
        <annotation>
            <documentation>WS-Security SAML 2.0 Assertion token security policy rule.</documentation>
        </annotation>
        <complexContent>
            <extension base="sec:SecurityPolicyRuleType">
                <attribute name="trustEngineRef" type="string" use="required">
                    <annotation>
                        <documentation>
                            Reference to the trust engine used to validate the signature. 
                        </documentation>
                    </annotation>
                </attribute>
                <attribute name="isInvalidFatal" type="boolean" use="optional">
                    <annotation>
                        <documentation>
                            A boolean flag indicating whether an Assertion evaluated as invalid should be
                            treated as a fatal error.
                        </documentation>
                    </annotation>
                </attribute>
            </extension>
        </complexContent>
    </complexType>
    
    <complexType name="ClientCertAuth">
        <annotation>
            <documentation>
                Client certificate auth rule that uses the value of the Liberty sb:Sender SOAP header
                as the client certificate presenter entity ID for the basis of trust evaluation.
            </documentation>
        </annotation>
        <complexContent>
            <extension base="sec:ClientCertAuth" />
        </complexContent>
    </complexType>

</schema>