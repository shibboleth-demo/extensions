/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.soap;

import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.soap.soap11.Fault;
import org.opensaml.ws.soap.util.SOAPHelper;
import org.opensaml.xml.XMLObject;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.message.SubContextContainer;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.soap.soap11.SOAP11Context;

/**
 * Helper class for processing SOAP message based on a SOAP message context.
 */
public final class SOAPProcessingHelper {
    
    //TODO need to make this more generic, support SOAP 1.1 and 1.2 transparently
    
    /** Constructor. */
    private SOAPProcessingHelper() {}
    
    /**
     * Get the inbound header blocks of the specified name, targeted to the node
     * as specified in the SOAP11Context.
     * 
     * @param msgContext the current message context
     * @param headerName the header name to resolve
     * @return list of headers matching the name and target node parameters
     */
    public static List<XMLObject> getInboundHeaderBlock(MessageContext msgContext, QName headerName) {
        SOAP11Context soap11Context = getSOAP11Context(msgContext);
        
        return SOAPHelper.getInboundHeaderBlock(msgContext, headerName, soap11Context.getNodeActors(),
                soap11Context.isFinalDestination());
    }
    
    /**
     * Register a header as understood.
     * 
     * @param msgContext the current message context
     * @param header the header that was understood
     */
    public static void registerUnderstoodHeader(MessageContext msgContext, XMLObject header) {
        SOAP11Context soap11Context = getSOAP11Context(msgContext);
        
        soap11Context.getUnderstoodHeaders().add(header);
    }
    
    /**
     * Check whether a header was understood.
     * 
     * @param msgContext the current message context
     * @param header the header that is to be checked for understanding
     * @return true if header was understood, false otherwise
     */
    public static boolean checkUnderstoodHeader(MessageContext msgContext, XMLObject header) {
        SOAP11Context soap11Context = getSOAP11Context(msgContext);
        
        return soap11Context.getUnderstoodHeaders().contains(header);
    }
    
    /**
     * Register a SOAP fault based on it's constituent information items.
     * 
     * @param msgContext the current message context
     * @param faultCode the fault code QName (required)
     * @param faultString the fault message string value (required)
     * @param faultActor the fault actor value (may be null)
     * @param detailChildren the detail child elements
     * @param detailAttributes the detail element attributes
     */
    public static void registerFault(MessageContext msgContext, QName faultCode, String faultString,
            String faultActor, List<XMLObject> detailChildren, Map<QName, String> detailAttributes) {
        SOAP11Context soap11Context = getSOAP11Context(msgContext);
        
        soap11Context.setFault(SOAPHelper.buildSOAP11Fault(faultCode, faultString, faultActor,
                detailChildren, detailAttributes));
    }
    
    /**
     * Register a SOAP 1.1 fault.
     * 
     * @param msgContext the current message context
     * @param fault the fault to register
     */
    public static void registerFault(MessageContext msgContext, Fault fault) {
        SOAP11Context soap11Context = getSOAP11Context(msgContext);
        
        soap11Context.setFault(fault);
    }
    
    /**
     * Get the registered SOAP 1.1 fault, if any.
     * 
     * @param msgContext the current message context
     * @return the registered fault, or null
     */
    public static Fault getFault(MessageContext msgContext) {
        SOAP11Context soap11Context = getSOAP11Context(msgContext);
        
        return soap11Context.getFault();
    }
    
    
    /**
     * Get the SOAP 1.1 sub-context from the main message context.
     * @param msgContext the current message context
     * @return the SOAP 1.1 sub-context
     */
    public static SOAP11Context getSOAP11Context(MessageContext msgContext) {
        if (!(msgContext instanceof SubContextContainer)) {
            throw new IllegalArgumentException("Message context was not a sub-context container");
        }
        SubContextContainer contextContainer = (SubContextContainer) msgContext;
        
        SOAP11Context soap11Context = (SOAP11Context) contextContainer.getContext(SOAP11Context.CONTEXT_ID);
        if (soap11Context == null) {
            throw new IllegalStateException("SOAP 1.1 sub-context was not available from the sub-context container");
        }
        return soap11Context;
    }

}
