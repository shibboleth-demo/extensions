/*
 * Copyright [2007] [University Corporation for Advanced Internet Development, Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.config.relyingparty;

import javax.xml.namespace.QName;

import org.opensaml.xml.util.DatatypeHelper;
import org.opensaml.xml.util.XMLHelper;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.AbstractSingleBeanDefinitionParser;
import org.w3c.dom.Element;

/**
 * Spring bean definition parser for SAML 2 Delegation WS-Security SAML 2.0 Assertion token rule.
 */
public class WSSecuritySAML20AssertionTokenRuleBeanDefinitionParser extends AbstractSingleBeanDefinitionParser {
    
    /** Schema type. */
    public static final QName TYPE_NAME = new QName(DelegationRelyingPartyNamespaceHandler.NAMESPACE,
            "WSSSAML20AssertionTokenRule");

    /** {@inheritDoc} */
    protected Class getBeanClass(Element element) {
        return WSSecuritySAML20AssertionTokenRuleFactoryBean.class;
    }

    /** {@inheritDoc} */
    protected boolean shouldGenerateId() {
        return true;
    }

    /** {@inheritDoc} */
    protected void doParse(Element element, BeanDefinitionBuilder builder) {
        
        builder.addPropertyReference("trustEngine",
                DatatypeHelper.safeTrimOrNullString(element.getAttributeNS(null, "trustEngineRef")));
        
        if (element.hasAttributeNS(null, "isInvalidFatal")) {
            builder.addPropertyValue("invalidFatal", XMLHelper.getAttributeValueAsBoolean(element
                    .getAttributeNodeNS(null, "isInvalidFatal")));
        } else {
            builder.addPropertyValue("invalidFatal", true);
        }
        
    }
}