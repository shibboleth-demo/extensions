/*
 * Copyright [2007] [University Corporation for Advanced Internet Development, Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.config.profile;

import javax.xml.namespace.QName;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.w3c.dom.Element;

import edu.internet2.middleware.shibboleth.idp.config.profile.saml2.SAML2SSOProfileHandlerBeanDefinitionParser;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.profile.DelegationAwareSAML2SSOProfileHandler;

/**
 * Spring bean definition parser for {@link DelegationAwareSAML2SSOProfileHandler} profile handlers.
 */
public class DelegationAwareSAML2SSOProfileHandlerBeanDefinitionParser 
        extends SAML2SSOProfileHandlerBeanDefinitionParser {

    /** Schema type. */
    public static final QName SCHEMA_TYPE = new QName(DelegationProfileHandlerNamespaceHandler.NAMESPACE, "SAML2SSO");

    /** {@inheritDoc} */
    protected Class getBeanClass(Element arg0) {
        return DelegationAwareSAML2SSOProfileHandler.class;
    }

    /** {@inheritDoc} */
    protected void doParse(Element config, BeanDefinitionBuilder builder) {
        super.doParse(config, builder);
        
    }
}