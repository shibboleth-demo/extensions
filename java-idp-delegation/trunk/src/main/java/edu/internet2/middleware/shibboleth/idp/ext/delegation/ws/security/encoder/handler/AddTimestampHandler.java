/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security.encoder.handler;

import java.util.List;

import org.joda.time.DateTime;
import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.MessageContextEvaluatingFunctor;
import org.opensaml.ws.message.MessageException;
import org.opensaml.ws.message.handler.Handler;
import org.opensaml.ws.message.handler.HandlerException;
import org.opensaml.ws.soap.util.SOAPHelper;
import org.opensaml.ws.wssecurity.Created;
import org.opensaml.ws.wssecurity.Expires;
import org.opensaml.ws.wssecurity.Security;
import org.opensaml.ws.wssecurity.Timestamp;
import org.opensaml.ws.wssecurity.WSSecurityObjectBuilder;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  Handler implementation that adds a wsse:Timestamp header to the wsse:Security header
 *  of the outbound SOAP envelope.
 */
public class AddTimestampHandler implements Handler {
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(AddTimestampHandler.class);
    
    /** Builder of Security object. */
    private WSSecurityObjectBuilder<Security> securityBuilder;
    
    /** Builder of Timestamp object. */
    private WSSecurityObjectBuilder<Timestamp> timestampBuilder;
    
    /** Builder of Created object. */
    private WSSecurityObjectBuilder<Created> createdBuilder;
    
    /** Builder of Expires object. */
    private WSSecurityObjectBuilder<Expires> expiresBuilder;
    
    /** Message context functor which produces the Created value. */
    private MessageContextEvaluatingFunctor<DateTime> createdValueSource;
    
    /** Message context functor which produces the Expires value. */
    private MessageContextEvaluatingFunctor<DateTime> expiresValueSource;

    /** Flag indicating whether to use the current time as the Created time, if no value
     * is produced by the Created value source. */
    private boolean useCurrentTimeAsDefaultCreated;
    
    /** Parameter indicating the offset, in seconds, used to calculate the Expires time, if no value
     * is produced by the Expires value source. */
    private Integer expiresOffsetFromCreated;


    /** Constructor. */
    public AddTimestampHandler() {
        XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
        securityBuilder = (WSSecurityObjectBuilder<Security>) builderFactory.getBuilder(Security.ELEMENT_NAME);
        timestampBuilder = (WSSecurityObjectBuilder<Timestamp>) builderFactory.getBuilder(Timestamp.ELEMENT_NAME);
        createdBuilder = (WSSecurityObjectBuilder<Created>) builderFactory.getBuilder(Created.ELEMENT_NAME);
        expiresBuilder = (WSSecurityObjectBuilder<Expires>) builderFactory.getBuilder(Expires.ELEMENT_NAME);
    }
    
    /**
     * Get whether to use the current time as the value for the Created element,
     * if no value is produced by the Created value source.
     * @return Returns the useCurrentTimeAsDefaultCreated.
     */
    public boolean isUseCurrentTimeAsDefaultCreated() {
        return useCurrentTimeAsDefaultCreated;
    }

    /**
     * Set whether to use the current time as the value for the Created element,
     * if no value is produced by the Created value source.
     * @param newUseCurrentTimeAsDefaultCreated The useCurrentTimeAsDefaultCreated to set.
     */
    public void setUseCurrentTimeAsDefaultCreated(boolean newUseCurrentTimeAsDefaultCreated) {
        useCurrentTimeAsDefaultCreated = newUseCurrentTimeAsDefaultCreated;
    }
    
    /**
     * Get the parameter indicating the offset, in seconds, used to calculate the Expires time, if no value
     * is produced by the Expires value source.
     * @return Returns the expiresOffsetFromCreated.
     */
    public Integer getExpiresOffsetFromCreated() {
        return expiresOffsetFromCreated;
    }

    /**
     * Set the parameter indicating the offset, in seconds, used to calculate the Expires time, if no value
     * is produced by the Expires value source.
     * @param newExpiresOffsetFromCreated The expiresOffsetFromCreated to set.
     */
    public void setExpiresOffsetFromCreated(Integer newExpiresOffsetFromCreated) {
        expiresOffsetFromCreated = newExpiresOffsetFromCreated;
    }
    
    /**
     * Set the functor which produces the Created value from the message context.
     * 
     * @param functor the new message context functor
     */
    public void setCreatedValueSource(MessageContextEvaluatingFunctor<DateTime> functor) {
        createdValueSource = functor;
    }

    /** {@inheritDoc} */
    public void invoke(MessageContext msgContext) throws HandlerException {
        log.debug("Processing addition of outbound WS-Security Timestamp");
        Timestamp timestamp = null;
        
        DateTime createdValue = getCreatedValue(msgContext);
        if (createdValue != null) {
            log.debug("WS-Security Timestamp Created value added was: {}", createdValue);
            Created created = createdBuilder.buildObject();
            created.setDateTime(createdValue);
            
            timestamp = timestampBuilder.buildObject();
            timestamp.setCreated(created);
        }
            
        DateTime expiresValue = getExpiresValue(msgContext, createdValue);
        if (expiresValue != null) {
            log.debug("WS-Security Timestamp Expires value added was: {}", createdValue);
            Expires expires = expiresBuilder.buildObject();
            expires.setDateTime(expiresValue);
            
            if (timestamp == null) {
                timestamp = timestampBuilder.buildObject();
            }
            timestamp.setExpires(expires);
        }
        
        if (timestamp != null) {
            log.debug("Resulting WS-Security Timestamp was non-null, adding to outbound envelope");
            Security security = getSecurityHeader(msgContext);
            if (security == null) {
                log.debug("Security header was null, building");
                security = securityBuilder.buildObject();
                //TODO probably should add Security/@mustUnderstand=1,
                // but need helper and/or other config (e.g. core SOAP processing context) to know SOAP version
                SOAPHelper.addHeaderBlock(msgContext, security);
            }
            security.getUnknownXMLObjects().add(timestamp);
        }
            
    }

    /**
     * Get the Created value.
     * @param msgContext the current message context
     * @return the Created datetime value to use
     * @throws HandlerException if there is a problem obtaining the Created value from the context
     */
    protected DateTime getCreatedValue(MessageContext msgContext) throws HandlerException {
        if (createdValueSource != null) {
            try {
                return createdValueSource.evaluate(msgContext);
            } catch (MessageException e) {
                throw new HandlerException("Error obtaining Created timestamp value from the context");
            }
        } else {
            if (useCurrentTimeAsDefaultCreated) {
                return new DateTime();
            } else {
                return null;
            }
        }
    }
    
    /**
     * Get the Expires value.
     * @param msgContext the current message context
     * @param createdValue  the created value, if any
     * @return the Expires datetime value to use
     * @throws HandlerException if there is a problem obtaining the 
     */
    private DateTime getExpiresValue(MessageContext msgContext, DateTime createdValue) throws HandlerException {
        if (expiresValueSource != null) {
            try {
                return expiresValueSource.evaluate(msgContext);
            } catch (MessageException e) {
                throw new HandlerException("Error obtaining Expires timestamp value from the context");
            }
        } else {
            if (expiresOffsetFromCreated != null && createdValue != null) {
                return createdValue.plusSeconds(expiresOffsetFromCreated);
            } else {
                return null;
            }
        }
    }

    /**
     * Get the Security header block.
     * @param msgContext  the current message context
     * @return the Security header block if present, or null
     */
    protected Security getSecurityHeader(MessageContext msgContext) {
        List<XMLObject> securityHeaders = SOAPHelper.getOutboundHeaderBlock(msgContext,
                Security.ELEMENT_NAME, null, true);
        if (securityHeaders != null && !securityHeaders.isEmpty()) {
            return (Security) securityHeaders.get(0);
        }
        return null; 
    }

}
