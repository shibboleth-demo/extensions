/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.authn;

import net.jcip.annotations.ThreadSafe;

import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.SubjectConfirmation;

/**
 * Validates a bearer subject confirmation.
 * 
 * This validator does not expect any parameters in the {@link ValidationContext#getStaticParameters()} or
 * {@link ValidationContext#getDynamicParameters()}.
 * 
 * This validator does not populate any parameters in the {@link ValidationContext#getDynamicParameters()}.
 */
@ThreadSafe
public class BearerSubjectConfirmationValidator extends AbstractSubjectConfirmationValidator {

    /** {@inheritDoc} */
    public String getServicedMethod() {
        return SubjectConfirmation.METHOD_BEARER;
    }

    /** {@inheritDoc} */
    protected ValidationResult doValidate(SubjectConfirmation confirmation, Assertion assertion,
            ValidationContext context) {
        return ValidationResult.VALID;
    }
}