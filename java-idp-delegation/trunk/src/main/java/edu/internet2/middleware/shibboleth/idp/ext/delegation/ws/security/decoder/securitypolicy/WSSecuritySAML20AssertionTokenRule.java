/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security.decoder.securitypolicy;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.security.SecurityPolicyException;
import org.opensaml.ws.security.SecurityPolicyRule;
import org.opensaml.ws.soap.soap11.FaultCode;
import org.opensaml.ws.soap.util.SOAPHelper;
import org.opensaml.ws.transport.InTransport;
import org.opensaml.ws.transport.http.HTTPInTransport;
import org.opensaml.ws.transport.http.HttpServletRequestAdapter;
import org.opensaml.ws.wssecurity.Security;
import org.opensaml.ws.wssecurity.WSSecurityConstants;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.security.CriteriaSet;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.credential.UsageType;
import org.opensaml.xml.security.criteria.EntityIDCriteria;
import org.opensaml.xml.security.criteria.UsageCriteria;
import org.opensaml.xml.security.x509.X509Credential;
import org.opensaml.xml.util.DatatypeHelper;
import org.opensaml.xml.util.LazyList;
import org.opensaml.xml.util.LazySet;
import org.opensaml.xml.validation.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.authn.AbstractSubjectConfirmationValidator;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.authn.AudienceRestrictionConditionValidator;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.authn.HolderOfKeySubjectConfirmationValidator;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.authn.SAML20AssertionTokenValidator;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.authn.ValidationContext;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.authn.ValidationResult;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.message.SubContextContainer;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.decoder.ShibbolethAssertionValidationContext;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security.WSSecurityContext;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security.token.Token.ValidationStatus;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security.token.impl.SAML20AssertionToken;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.soap.SOAPProcessingHelper;

/**
 * A security policy rule which resolves SAML 2.0 Assertion tokens from a SOAP envelope's
 * wsse:Security header, validates them, and makes them available via via the
 * {@link WSSecurityContext}.
 */
public class WSSecuritySAML20AssertionTokenRule implements SecurityPolicyRule {

    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(WSSecuritySAML20AssertionTokenRule.class);
    
    /** Flag which indicates whether a failure of Assertion validation should be considered fatal. */
    private boolean invalidFatal;
    
    /** The SAML 2.0 Assertion validator.*/
    private SAML20AssertionTokenValidator assertionTokenValidator;
    
    /**
     * Constructor.
     * 
     * @param newTokenValidator the SAML 2.0 Assertion token validator to use
     *
     */
    public WSSecuritySAML20AssertionTokenRule(SAML20AssertionTokenValidator newTokenValidator) {
        if (newTokenValidator == null) {
            throw new IllegalArgumentException("SAML20AssertionTokenValidator may not be null");
        }
        assertionTokenValidator = newTokenValidator;
        setInvalidFatal(true);
    }
    
    /**
     * Get flag which indicates whether a failure of Assertion validation should be considered a fatal processing error.
     * @return Returns the invalidFatal.
     */
    public boolean isInvalidFatal() {
        return invalidFatal;
    }

    /**
     * Set flag which indicates whether a failure of Assertion validation should be considered a fatal processing error.
     * @param newInvalidFatal The invalidFatal to set.
     */
    public void setInvalidFatal(boolean newInvalidFatal) {
        invalidFatal = newInvalidFatal;
    }
    
    /** {@inheritDoc} */
    public void evaluate(MessageContext messageContext) throws SecurityPolicyException {
        if (!SOAPHelper.isInboundSOAPMessage(messageContext)) {
            log.info("Message context does not contain a SOAP envelope. Skipping rule...");
            return;
        }
        
        List<Assertion> assertions = resolveAssertions(messageContext);
        if (assertions == null || assertions.isEmpty()) {
            log.info("Inbound SOAP envelope contained no Assertion tokens. Skipping further processing");
            return;
        }
        
        WSSecurityContext wsContext = null;
        if (!(messageContext instanceof SubContextContainer)) {
            log.warn("Message context is not a subcontext container.  "
                    + "SAML 2 Assertion tokens will be validated but not otherwise made available for consumption");
        } else {
            SubContextContainer contextContainer = (SubContextContainer) messageContext;
            wsContext = (WSSecurityContext) contextContainer.getContext(WSSecurityContext.CONTEXT_ID);
            if (wsContext == null) {
                log.warn("Context container did not contain a WS-Security sub context. Assertions will not be stored");
            }
        }
        
        for (Assertion assertion : assertions) {
            ValidationContext validationContext = buildValidationContext(messageContext, assertion);
            
            SAML20AssertionToken token = new SAML20AssertionToken(assertion);
            
            SAML20AssertionTokenValidator tokenValidator = getTokenValidator(messageContext);
            try { 
                ValidationResult validationResult = tokenValidator.validate(assertion, validationContext);
                processResult(validationContext, validationResult, token, messageContext);
            } catch (ValidationException e) {
                log.warn("There was a problem determining Assertion validity: {}", e.getMessage());
                SOAPProcessingHelper.registerFault(messageContext, FaultCode.SERVER, 
                        "Internal security token processing error", null, null, null);
                throw new SecurityPolicyException("Error determining SAML 2.0 Assertion validity", e);
            }
            
            if (wsContext != null) {
                wsContext.getTokens().add(token);
            }
            
        }
        
    }

    /**
     * Process the result of the token validation.
     * 
     * @param validationContext the Assertion validation context
     * @param validationResult the Assertion validation result
     * @param token the token being produced
     * @param messageContext the current message context
     * 
     * @throws SecurityPolicyException if the Assertion was invalid or indeterminate and idInvalidFatal is true
     */
    protected void processResult(ValidationContext validationContext, ValidationResult validationResult,
            SAML20AssertionToken token, MessageContext messageContext)
            throws SecurityPolicyException {
        
        log.debug("Assertion token validation result was: {}", validationResult);
        
        String validationMsg = validationContext.getValidationFailureMessage();
        if (DatatypeHelper.isEmpty(validationMsg)) {
            validationMsg  = "unspecified";
        }
                    
        // TODO re fault reporting: per WS-S spec there are various codes for more specific processing failures.
        // We would need the Assertion validator to report that more specific error in some fashion
        switch (validationResult) {
            case VALID:
                token.setValidationStatus(ValidationStatus.VALID);
                token.setSubjectConfirmation((SubjectConfirmation) validationContext.getDynamicParameters()
                        .get(SAML20AssertionTokenValidator.CONFIRMED_SUBJECT_CONFIRMATION));
                break;
            case INVALID:
                log.warn("Assertion token validation was INVALID.  Reason: {}", validationMsg);
                if (isInvalidFatal()) {
                    SOAPProcessingHelper.registerFault(messageContext,
                            WSSecurityConstants.SOAP_FAULT_INVALID_SECURITY_TOKEN, 
                            "The SAML 2.0 Assertion token was invalid", null, null, null);
                    throw new SecurityPolicyException("Assertion token validation result was INVALID"); 
                } else {
                    token.setValidationStatus(ValidationStatus.INVALID);
                    token.setSubjectConfirmation((SubjectConfirmation) validationContext.getDynamicParameters()
                            .get(SAML20AssertionTokenValidator.CONFIRMED_SUBJECT_CONFIRMATION));
                }
                break;
            case INDETERMINATE:
                log.warn("Assertion token validation was INDETERMINATE. Reason: {}", validationMsg);
                if (isInvalidFatal()) {
                    SOAPProcessingHelper.registerFault(messageContext,
                            WSSecurityConstants.SOAP_FAULT_INVALID_SECURITY_TOKEN, 
                            "The SAML 2.0 Assertion token's validity could not be determined", null, null, null);
                    throw new SecurityPolicyException("Assertion token validation result was INDETERMINATE"); 
                } else {
                    token.setValidationStatus(ValidationStatus.INDETERMINATE);
                    token.setSubjectConfirmation((SubjectConfirmation) validationContext.getDynamicParameters()
                            .get(SAML20AssertionTokenValidator.CONFIRMED_SUBJECT_CONFIRMATION));
                }
                break;
            default:
                log.warn("Assertion validation result indicated an unknown value: {}", validationResult);
        }
        
    }

    /**
     * Get the Assertion token validator.
     * 
     * @param messageContext the current message context
     * @return the token validator
     */
    protected SAML20AssertionTokenValidator getTokenValidator(MessageContext messageContext) {
        return assertionTokenValidator;
    }

    /**
     * Build the Assertion ValidationContext.
     * 
     * @param messageContext the current message context
     * @param assertion the assertion which is to be validated
     * @return the new Assertion validation context to use
     */
    protected ValidationContext buildValidationContext(MessageContext messageContext, Assertion assertion) {
        HashMap<String, Object> staticParams = new HashMap<String, Object>();
        
        //For signature validation
        staticParams.put(SAML20AssertionTokenValidator.SIGNATURE_REQUIRED, Boolean.TRUE);
        staticParams.put(SAML20AssertionTokenValidator.SIGNATURE_VALIDATION_CRITERIA_SET, 
                getSignatureCriteriaSet(messageContext, assertion));
        
        // For HoK subject confirmation
        staticParams.put(HolderOfKeySubjectConfirmationValidator.PRESENTER_CERT_PARAM, 
                getAttesterCertificate(messageContext));
        
        // For SubjectConfirmationData
        HashSet<String> validRecipients = new HashSet<String>();
        validRecipients.addAll(getValidRecipients(messageContext));
        staticParams.put(AbstractSubjectConfirmationValidator.VALID_RECIPIENTS_PARAM, validRecipients);
        
        try {
            InetAddress[] addresses = null;
            String attesterIPAddress = getAttesterIPAddress(messageContext);
            if (attesterIPAddress != null) {
                addresses = InetAddress.getAllByName(getAttesterIPAddress(messageContext));
                HashSet<InetAddress> validAddresses = new HashSet<InetAddress>();
                validAddresses.addAll(Arrays.asList(addresses));
                staticParams.put(AbstractSubjectConfirmationValidator.VALID_ADDRESSES_PARAM, validAddresses);
            } else {
                log.warn("Could not determine attester IP address. Validation of Assertion may or may not succeed");
            }
        } catch (UnknownHostException e) {
            log.warn("Processing of attester IP address failed. Validation of Assertion may or may not succeed", e);
        }
        
        // For Audience Condition
        HashSet<String> validAudiences = new HashSet<String>();
        validAudiences.addAll(getValidAudiences(messageContext));
        staticParams.put(AudienceRestrictionConditionValidator.VALID_AUDIENCES_PARAM, validAudiences);
        
        
        return new ValidationContext(staticParams);
    }

    /**
     * Get the attesting entity's certificate.
     * @param messageContext the current message context.
     * @return the entity certificate
     */
    protected X509Certificate getAttesterCertificate(MessageContext messageContext) {
        InTransport inTransport = messageContext.getInboundMessageTransport();
        Credential credential = inTransport.getPeerCredential();
        if (credential == null) {
            log.warn("Peer transport credential was not present. " 
                    + "Holder-of-key proof-of-possession via client TLS cert will not be possible");
            return null;
        }
        if (!(credential instanceof X509Credential))  {
            log.warn("Peer credential was not an X509Certificate. "
                    + "Holder-of-key proof-of-possession via client TLS cert will not be possible");
            return null;
        }
        return ((X509Credential)credential).getEntityCertificate();
    }

    /**
     * Get the attester's IP address.
     * @param messageContext the current message context.
     * @return the IP address of the attester
     */
    protected String getAttesterIPAddress(MessageContext messageContext) {
        InTransport inTransport = messageContext.getInboundMessageTransport();
        if (!(inTransport instanceof HTTPInTransport)) {
            log.warn("Could not determine attester's IP address");
            return null;
        }
        return ((HTTPInTransport)inTransport).getPeerAddress();
    }
    
    /**
     * Get the signature validation criteria set.
     * @param messageContext the current message context.
     * @param assertion the assertion whose signature is to be verified
     * @return the criteria set based on the message context data
     */
    protected CriteriaSet getSignatureCriteriaSet(MessageContext messageContext, Assertion assertion) {
        CriteriaSet criteriaSet = new CriteriaSet();
        
        //TODO need to be able to add statically-injected criteria?
        
        ShibbolethAssertionValidationContext shibAssertionValidationContext = null;
        if (!(messageContext instanceof SubContextContainer)) {
            log.warn("Message context is not a subcontext container. Unable to resolve set of valid audiences");
        } else {
            SubContextContainer contextContainer = (SubContextContainer) messageContext;
            shibAssertionValidationContext = (ShibbolethAssertionValidationContext) contextContainer.getContext(
                    ShibbolethAssertionValidationContext.CONTEXT_ID);
            if (shibAssertionValidationContext == null) {
                log.debug("Context container did not contain a Shibboleth assertion validation sub context." 
                        + "Will use internally-generated signature validation criteria only");
            }
        }
        
        if (shibAssertionValidationContext != null) {
            criteriaSet.addAll(shibAssertionValidationContext.getSignatureValidationCriteriaSet());
        }
        
        if (!criteriaSet.contains(EntityIDCriteria.class)) {
            String issuer = null;
            if (assertion.getIssuer() != null) {
                issuer = DatatypeHelper.safeTrimOrNullString(assertion.getIssuer().getValue());
            }
            if (issuer != null) {
                log.debug("Adding internally-generated EntityIDCriteria with value of: {}", issuer);
                criteriaSet.add(new EntityIDCriteria(issuer));
            }
        }
        
        if (!criteriaSet.contains(UsageCriteria.class)) {
            log.debug("Adding internally-generated UsageCriteria with value of: {}", UsageType.SIGNING);
            criteriaSet.add(new UsageCriteria(UsageType.SIGNING));
        }
        
        return criteriaSet;
    }

    /**
     * Get the valid recipient endpoints for attestation.
     * @param messageContext the current message context.
     * @return set of recipient endpoint URI's
     */
    protected Set<String> getValidRecipients(MessageContext messageContext) {
        LazySet<String> validRecipients = new LazySet<String>();
        InTransport inTransport = messageContext.getInboundMessageTransport();
        if (!(inTransport instanceof HttpServletRequestAdapter)) {
            log.warn("Could not determine recipient endpoint at which Assertion is being attested");
            return validRecipients;
        }
        String endpoint = ((HttpServletRequestAdapter)inTransport).getWrappedRequest().getRequestURL().toString();
        validRecipients.add(endpoint);
        return validRecipients;
    }

    /**
     * Get the valid audiences for attestation.
     * @param messageContext the current message context.
     * @return set of audience URI's
     */
    protected Set<String> getValidAudiences(MessageContext messageContext) {
        LazySet<String> validAudiences = new LazySet<String>();
        
        if (!(messageContext instanceof SubContextContainer)) {
            log.warn("Message context is not a subcontext container. Unable to resolve set of valid audiences");
            return validAudiences;
        }
        
        //TODO for future library inclusion this probably needs to be more generalized and/or pluggable
        
        SubContextContainer contextContainer = (SubContextContainer) messageContext;
        ShibbolethAssertionValidationContext shibAssertionValidationContext =
            (ShibbolethAssertionValidationContext) contextContainer.getContext(
                    ShibbolethAssertionValidationContext.CONTEXT_ID);
        if (shibAssertionValidationContext == null) {
            log.warn("Context container did not contain a Shibboleth assertion validation sub context." 
                    + "Unable to resolve set of valid audiences. Validation of Assertion may or may not succeed");
            return validAudiences;
        }
        
        log.debug("Obtained set of valid audiences from Shibboleth assertion validation context: {}",
                shibAssertionValidationContext.getValidAudiences());
        
        validAudiences.addAll(shibAssertionValidationContext.getValidAudiences());
        
        return validAudiences;
    }

    /**
     * Resolve the SAML 2.0 Assertions token from the SOAP envelope.
     * 
     * @param messageContext the current message context
     * @return the list of resolved Assertions, or an empty list
     */
    protected List<Assertion> resolveAssertions(MessageContext messageContext) {
        LazyList<Assertion> assertions = new LazyList<Assertion>();
        
        List<XMLObject> securityHeaders = SOAPProcessingHelper.getInboundHeaderBlock(messageContext,
                Security.ELEMENT_NAME);
        if (securityHeaders == null || securityHeaders.isEmpty()) {
            log.debug("No WS-Security found in inbound SOAP message. Skipping further processing.");
            return Collections.emptyList();
        }
        
        // There could be multiple Security headers targeted to this node, so process all of them
        for (XMLObject header : securityHeaders) {
            Security securityHeader = (Security) header;
            List<XMLObject> xmlObjects = securityHeader.getUnknownXMLObjects(Assertion.DEFAULT_ELEMENT_NAME);
            if (xmlObjects != null && !xmlObjects.isEmpty()) {
                for (XMLObject xmlObject : xmlObjects) {
                    assertions.add((Assertion) xmlObject);
                }
            }
        }
        
        return assertions;
    }

}
