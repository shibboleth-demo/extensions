/*
 * Copyright 2007 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.config.relyingparty;

import edu.internet2.middleware.shibboleth.common.config.relyingparty.saml.SAML2SSOProfileConfigurationFactoryBean;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.relyingparty.DelegationAwareSSOConfiguration;

/** Spring factory for delegation-aware SAML 2 SSO profile configurations. */
public class DelegationAwareSAML2SSOProfileConfigurationFactoryBean extends SAML2SSOProfileConfigurationFactoryBean {
    
    /** Flag which indicates whether a request for a delegation token will be fulfilled. */
    private Boolean allowTokenDelegation;
    
    /**
     * Get the flag which indicates whether a request for a delegation token will 
     * be fulfilled.
     * 
     * @return the flag value
     */
    public Boolean getAllowTokenDelegation() {
        return allowTokenDelegation;
    }
    
    /**
     * Set the flag which indicates whether a request for a delegation token will 
     * be fulfilled.
     * 
     * @param newValue the new flag value
     */
    public void setAllowTokenDelegation(Boolean newValue) {
        allowTokenDelegation = newValue;
    }

    /** {@inheritDoc} */
    public Class getObjectType() {
        return DelegationAwareSSOConfiguration.class;
    }

    /** {@inheritDoc} */
    protected Object createInstance() throws Exception {
        DelegationAwareSSOConfiguration configuration = (DelegationAwareSSOConfiguration) getObjectType().newInstance();
        populateBean(configuration);
        
        // TODO fixme (in main IdP code)
        // This is ugly, have to copy from the super class, because it does not 
        // expose either: 1) its own own population logic or 2) (even better) allow the subclass to just
        // call super.createInstance(). For the latter super would need to use getObjectType() to get the 
        // right type to reflectively instantiate, rather than just new the instance directly. See above.
        configuration.setIncludeAttributeStatement(includeAttributeStatement());
        configuration.setMaximumSPSessionLifetime(getMaximumSPSessionLifetime());
        
        configuration.setAllowTokenDelegation(getAllowTokenDelegation());

        return configuration;
    }
}