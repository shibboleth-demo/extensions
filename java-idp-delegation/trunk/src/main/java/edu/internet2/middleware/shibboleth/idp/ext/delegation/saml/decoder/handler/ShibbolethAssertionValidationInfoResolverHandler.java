/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.decoder.handler;

import java.util.Set;

import org.opensaml.saml2.metadata.EntitiesDescriptor;
import org.opensaml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.handler.Handler;
import org.opensaml.ws.message.handler.HandlerException;
import org.opensaml.xml.util.DatatypeHelper;
import org.opensaml.xml.util.LazySet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.message.SubContextContainer;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.decoder.ShibbolethAssertionValidationContext;

/**
 * Handler implementation which resolves dynamic request-specific information
 * needed for Assertion token validation, and populates in the message context's
 * {@link ShibbolethAssertionValidationContext} sub-context.
 */
public class ShibbolethAssertionValidationInfoResolverHandler extends BaseShibbolethLocalEntityInfoResolverHandler
        implements Handler {
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(ShibbolethAssertionValidationInfoResolverHandler.class);

    /** Flag indicating whether local entity ID as produced by the relyingPartyIDSource
     * is to be included as a valid audience. Defaults to true. */
    private boolean localIDIsAudience;
    
    /** Flag indicating whether the EntitiesDescriptor group parents of the local entity ID
     * as produced by the relyingPartyIDSource are to be included as valid audiences.
     * Defaults to false.*/
    private boolean localIDGroupsAreAudience;
    
    /** Constructor. */
    public ShibbolethAssertionValidationInfoResolverHandler() {
        localIDIsAudience = true;
        localIDGroupsAreAudience = false;
    }
    
    /**
     * Get the flag indicating whether local entity ID as produced by the relyingPartyIDSource
     * is to be included as a valid audience. Defaults to true.
     * @param value The localIDIsAudience to set.
     */
    public void setLocalIDIsAudience(boolean value) {
        localIDIsAudience = value;
    }

    /** Flag indicating whether the EntitiesDescriptor group parents of the local entity ID
     * as produced by the relyingPartyIDSource are to be included as valid audiences.
     * Defaults to false.
     * @param value The localIDGroupsAreAudience to set.
     */
    public void setLocalIDGroupsAreAudience(boolean value) {
        localIDGroupsAreAudience = value;
    }

    /** {@inheritDoc} */
    public void invoke(MessageContext msgContext) throws HandlerException {
        if (!(msgContext instanceof SubContextContainer)) {
            log.warn("Message context is not a subcontext container. " 
                    + "Unable to resolve assertion validation information");
            return;
        }
        
        SubContextContainer contextContainer = (SubContextContainer) msgContext;
        ShibbolethAssertionValidationContext shibAssertionValidationContext =
            (ShibbolethAssertionValidationContext) contextContainer.getContext(
                    ShibbolethAssertionValidationContext.CONTEXT_ID);
        if (shibAssertionValidationContext == null) {
            log.warn("Context container did not contain a Shibboleth assertion validation sub context." 
                    + "Unable to resolve assertion validation information.");
            return;
        }
        
        String localEntityId = resolveLocalEntityId(msgContext);
        
        if (localEntityId != null) {
            log.debug("Resolving valid audiences based on local entity ID: {}", localEntityId);
            shibAssertionValidationContext.getValidAudiences().addAll(resolveValidAudiences(msgContext, localEntityId));
        } else {
            log.warn("Local entity ID could not be resolved. " 
                    + "Failed to populate valid audiences for assertion validation");
        }

    }

    /**
     * Resolve the valid Assertion AudienceRestriction Condition Audience values for the given
     * entity ID.
     * @param msgContext the current message context
     * @param entityID the entity ID for which to resolve the valid audiences
     * @return the set of valid audience URI's
     * @throws HandlerException if there was a problem resolving the value
     */
    protected Set<String> resolveValidAudiences(MessageContext msgContext, String entityID)
            throws HandlerException{
        LazySet<String> validAudiences = new LazySet<String>();
        
        // Add specified entity ID
        if (localIDIsAudience) {
            validAudiences.add(entityID);
        }
        
        // Resolve names of all enclosing EntitiesDescriptors groups
        if (localIDGroupsAreAudience && getMetadataProvider() != null) {
            EntityDescriptor entityDescriptor = null;
            try {
                entityDescriptor = getMetadataProvider().getEntityDescriptor(entityID);
            } catch (MetadataProviderException e) {
                throw new HandlerException(
                        String.format("Error looking up entity descriptor for entity ID: %s", entityID), e);
            }
            if (entityDescriptor != null) {
                EntitiesDescriptor currentParent = (EntitiesDescriptor) entityDescriptor.getParent();
                while (currentParent != null) {
                    String parentName = DatatypeHelper.safeTrimOrNullString(currentParent.getName());
                    if (parentName != null) {
                        validAudiences.add(parentName);
                    }
                    currentParent = (EntitiesDescriptor) currentParent.getParent();
                }
            }
        }
        
        return validAudiences;
    }

}
