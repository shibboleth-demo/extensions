/*
 * Copyright 2007 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.config.relyingparty;

import java.util.List;

import javax.xml.namespace.QName;

import org.opensaml.xml.util.DatatypeHelper;
import org.opensaml.xml.util.LazySet;
import org.opensaml.xml.util.XMLHelper;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

/** Spring configuration parser for Liberty ID-WSF SSOS profile configurations. */
public class LibertyIDWSFSSOSProfileConfigurationBeanDefinitionParser extends
        DelegationAwareSAML2SSOProfileConfigurationBeanDefinitionParser {

    /** Schema type name. */
    public static final QName TYPE_NAME =
        new QName(DelegationRelyingPartyNamespaceHandler.NAMESPACE, "LibertyIDWSFSSOSProfile");

    /** {@inheritDoc} */
    protected Class getBeanClass(Element element) {
        return LibertyIDWSFSSOSProfileConfigurationFactoryBean.class;
    }

    /** {@inheritDoc} */
    protected void doParse(Element element, ParserContext parserContext, BeanDefinitionBuilder builder) {
        super.doParse(element, parserContext, builder);
        
        if (element.hasAttributeNS(null, "maximumTokenDelegationChainLength")) {
            builder.addPropertyValue("maximumTokenDelegationChainLength", 
                    Integer.valueOf(element.getAttributeNS(null, "maximumTokenDelegationChainLength")));
        } else {
            builder.addPropertyValue("maximumTokenDelegationChainLength", 1);
        }
        
        LazySet<String> allowedDelegates = new LazySet<String>();
        
        List<Element> delegateChildList = XMLHelper.getChildElementsByTagNameNS(element,
                DelegationRelyingPartyNamespaceHandler.NAMESPACE, "DelegationRestriction");
        
        if (delegateChildList != null && ! delegateChildList.isEmpty()) {
            for (Element delegateChild : delegateChildList) {
                if (delegateChild != null) {
                    String delegateURI = DatatypeHelper.safeTrimOrNullString(delegateChild.getTextContent());
                    if (delegateURI != null) {
                        allowedDelegates.add(delegateURI);
                    }
                }
            }
        }
        
        builder.addPropertyValue("allowedDelegates", allowedDelegates);
        
    }
}