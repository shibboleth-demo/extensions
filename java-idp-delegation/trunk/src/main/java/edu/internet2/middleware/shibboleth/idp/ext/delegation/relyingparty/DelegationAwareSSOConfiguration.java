/*
 * Copyright 2007 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.relyingparty;

import edu.internet2.middleware.shibboleth.common.relyingparty.provider.saml2.SSOConfiguration;

/** Delegation-aware SAML 2 SSO configuration settings. */
public class DelegationAwareSSOConfiguration extends SSOConfiguration {
    
    /** ID for this profile configuration. */
    public static final String PROFILE_ID = "urn:mace:shibboleth:2.0:ext:delegation:profiles:saml2:sso";
    
    /** Flag which indicates whether a request for a delegation token will be fulfilled. */
    private Boolean allowTokenDelegation;
    
    /** {@inheritDoc} */
    public String getProfileId() {
        return PROFILE_ID;
    }
    
    /**
     * Get the flag which indicates whether a request for a delegation token will 
     * be fulfilled.
     * 
     * @return the flag value
     */
    public Boolean getAllowTokenDelegation() {
        return allowTokenDelegation;
    }
    
    /**
     * Set the flag which indicates whether a request for a delegation token will 
     * be fulfilled.
     * 
     * @param newValue the new flag value
     */
    public void setAllowTokenDelegation(Boolean newValue) {
        allowTokenDelegation = newValue;
    }

}