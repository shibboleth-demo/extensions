/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.soap.decoder.handler;

import java.util.List;

import org.openliberty.xmltooling.soapbinding.Sender;
import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.MessageContextMutatingFunctor;
import org.opensaml.ws.message.MessageException;
import org.opensaml.ws.message.handler.Handler;
import org.opensaml.ws.message.handler.HandlerException;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.util.DatatypeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.LibertyConstants;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.soap.SOAPProcessingHelper;

/**
 * Handler implementation that handles sb:Sender header on the inbound SOAP envelope.
 */
public class SenderHandler implements Handler {
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(SenderHandler.class);
    
    /** Message context functor which handles processing/mutating the message context
     * based on the Sender providerId value. */
    private MessageContextMutatingFunctor<String> providerIdValueHandler;
    
    /**
     * Set the functor which handles processing the Sender providerId value.
     * 
     * @param functor the new message context functor
     */
    public void setProviderIdValueHandler(MessageContextMutatingFunctor<String> functor) {
        providerIdValueHandler = functor;
    }

    /** {@inheritDoc} */
    public void invoke(MessageContext msgContext) throws HandlerException {
        log.debug("Handling message Liberty ID-WSF Sender");
        Sender sender = getSender(msgContext);
        if (sender != null) {
            String providerId = DatatypeHelper.safeTrimOrNullString(sender.getProviderID());
            if (providerId != null) {
                log.debug("Message contained Liberty ID-WSF Sender providerId: {}", providerId);
                handleProviderIdValue(msgContext, providerId);
            } else {
                log.debug("Message contained no Liberty ID-WSF Sender providerId");
            }
        } else {
            log.debug("Message contained no Liberty ID-WSF Sender header");
        }
    }

    /**
     * Handle the Sender providerId value.
     * @param msgContext the current message context
     * @param providerId the message ID value
     * @throws HandlerException if there is a problem handling the message value
     */
    protected void handleProviderIdValue(MessageContext msgContext, String providerId) throws HandlerException {
        if (providerIdValueHandler != null) {
          try {
            providerIdValueHandler.mutate(msgContext, providerId);
          } catch (MessageException e) {
              throw new HandlerException("Error handling Sender providerId value", e);
          }
        }
    }

    /**
     * Get message Sender header block.
     * 
     * @param msgContext the current message context
     * @return the message Sender value
     */
    protected Sender getSender(MessageContext msgContext) {
        List<XMLObject> senders = SOAPProcessingHelper.getInboundHeaderBlock(msgContext,
                LibertyConstants.SOAP_BINDING_SENDER_ELEMENT_NAME);
        if (senders != null && !senders.isEmpty()) {
            return (Sender) senders.get(0);
        }
        return null; 
    }

}
