/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.soap.soap11;

import java.util.Set;

import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.soap.soap11.Fault;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.util.LazySet;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.message.BaseSubContext;

/**
 * Sub context implementation related to SOAP 1.1 processing state.
 */
public class SOAP11Context extends BaseSubContext {
    
    /** The subcontext identifier. */
    public static final String CONTEXT_ID = SOAP11Context.class.getName();
    
    /** SOAP 1.1 Fault related to the current message processing context. */
    private Fault fault;
    
    /** The set of actor URI's under which this SOAP node is operating. */
    private LazySet<String> nodeActors;
    
    /** Flag indicating whether the node is the final destination for the current 
     * message processing context. */
    private boolean finalDestination;
    
    /** The set of headers that have been understood. */
    private LazySet<XMLObject> understoodHeaders;

    /**
     * Constructor.
     *
     * @param owningContext the owning message context
     */
    public SOAP11Context(MessageContext owningContext) {
        super(owningContext);
        nodeActors = new LazySet<String>();
        understoodHeaders = new LazySet<XMLObject>();
    }

    /**
     * Get the SOAP Fault related to the current message processing context.
     * @return Returns the fault.
     */
    public Fault getFault() {
        return fault;
    }

    /**
     * Set the SOAP Fault related to the current message processing context.
     * @param newFault The fault to set.
     */
    public void setFault(Fault newFault) {
        fault = newFault;
    }
    
    /** 
     * Get the (modifiable) set of actor URI's under which this SOAP node is operating.
     * @return the set of node actor URI's
     * */
    public Set<String> getNodeActors() {
        return nodeActors;
    }
    
    /** 
     * Get the (modifiable) set of headers which have been understood.
     * @return the set of node actor URI's
     * */
    public Set<XMLObject> getUnderstoodHeaders() {
        return understoodHeaders;
    }
    
    /**
     * Get the flag indicating whether the node is the final destination for the current 
     * message processing context.
     * @return true if is the final destination, false otherwise
     * 
     */
    public boolean isFinalDestination() {
        return finalDestination;
    }
    
    /**
     * Set the flag indicating whether the node is the final destination for the current 
     * message processing context.
     * @param newValue the new flag value
     * 
     */
    public void setFinalDestination(boolean newValue) {
        finalDestination = newValue;
    }

}
