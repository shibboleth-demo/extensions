/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security.token.impl;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security.token.Token;

/**
 * Base abstract implementation of {@link Token}.
 * 
 * @param <TokenType> the type of token  represented
 */
public abstract class BaseToken<TokenType> implements Token<TokenType> {
    
    /** The wrapped token object instance. */
    private TokenType wrappedToken;
    
    /** Token validation status. */
    private ValidationStatus validationStatus;
    
    /**
     * Constructor.
     *
     * @param token the wrapped token instance
     */
    public BaseToken(TokenType token) {
        if (token == null) {
            throw new IllegalArgumentException("Wrapped token may not be null");
        }
        wrappedToken = token;
        validationStatus = ValidationStatus.VALIDATION_NOT_ATTEMPTED;
    }
    
    /** {@inheritDoc} */
    public TokenType getWrappedToken() {
        return wrappedToken;
    }

    /** {@inheritDoc} */
    public ValidationStatus getValidationStatus() {
        return validationStatus;
    }
    
    /**
     * Get the token validation status.
     * 
     * @param newValidationStatus the new token validation status
     */
    public void setValidationStatus(ValidationStatus newValidationStatus) {
        validationStatus = newValidationStatus;
    }

}
