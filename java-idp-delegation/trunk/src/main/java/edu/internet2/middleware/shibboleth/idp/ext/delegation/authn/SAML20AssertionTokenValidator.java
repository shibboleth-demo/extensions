/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.authn;

import java.util.Collection;
import java.util.List;

import javax.xml.namespace.QName;

import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;
import org.opensaml.common.SAMLVersion;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Condition;
import org.opensaml.saml2.core.Conditions;
import org.opensaml.saml2.core.Statement;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.security.SAMLSignatureProfileValidator;
import org.opensaml.xml.security.CriteriaSet;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.security.credential.UsageType;
import org.opensaml.xml.security.criteria.EntityIDCriteria;
import org.opensaml.xml.security.criteria.UsageCriteria;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.SignatureTrustEngine;
import org.opensaml.xml.util.DatatypeHelper;
import org.opensaml.xml.util.LazyMap;
import org.opensaml.xml.validation.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A validator used to evaluate version 2.0 {@link Assertion}s. */
public class SAML20AssertionTokenValidator {

    /**
     * The name of the {@link ValidationContext#getStaticParameters()} carrying the clock skew, in milliseconds as a
     * {@link Long}. If not present the default clock skew of {@value #DEFAULT_CLOCK_SKEW} milliseconds will be used.
     */
    public static final String CLOCK_SKEW_PARAM = SAML20AssertionTokenValidator.class.getName() + ".ClockSkew";

    /**
     * The name of the {@link ValidationContext#getDynamicParameters()} carrying the {@link SubjectConfirmation} that
     * confirmed the subject.
     */
    public static final String CONFIRMED_SUBJECT_CONFIRMATION = SAML20AssertionTokenValidator.class.getName()
            + ".ConfirmedSubjectConfirmation";
    
    /**
     * The name of the {@link ValidationContext#getDynamicParameters()} carrying the flag which indicates whether
     * the Assertion is required to be signed.  If not supplied, defaults to 'true'. If an Assertion is signed,
     * the signature is always evaluated and the result factored into the overall validation result, regardless
     * of the value of this setting.
     */
    public static final String SIGNATURE_REQUIRED = SAML20AssertionTokenValidator.class.getName()
            + ".SignatureRequired";
    
    /**
     * The name of the {@link ValidationContext#getDynamicParameters()} which carries the {@link CriteriaSet}
     * which will be used as the input to the signature trust engine.  If not supplied, a minimal criteria
     * set will be constructed which contains an {@link EntityIDCriteria} containing the Assertion Issuer entityID,
     * and a {@link UsageCriteria} of {@link UsageType#SIGNING}. If it is supplied, but either of those criteria
     * are absent from the criteria set, they will be added with the above values.
     */
    public static final String SIGNATURE_VALIDATION_CRITERIA_SET =
        SAML20AssertionTokenValidator.class.getName() + ".SignatureValidationCriteriaSet";


    /** Default clock skew; {@value} milliseconds. */
    public static final long DEFAULT_CLOCK_SKEW = 5 * 60 * 1000;

    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(SAML20AssertionTokenValidator.class);

    /** Registered {@link Condition} validators. */
    private LazyMap<QName, ConditionValidator> conditionValidators;

    /** Registered {@link SubjectConfirmation} validators. */
    private LazyMap<String, SubjectConfirmationValidator> subjectConfirmationValidators;

    /** Registered {@link Statement} validators. */
    private LazyMap<QName, StatementValidator> statementValidators;
    
    /** Trust engine for signature evaluation. */
    private SignatureTrustEngine trustEngine;
    
    /** SAML signature profile validator.*/
    private SAMLSignatureProfileValidator samlSigProfileValidator;

    /**
     * Constructor.
     * 
     * @param newConditionValidators validators used to validate the {@link Condition}s within the assertion
     * @param newConfirmationValidators validators used to validate {@link SubjectConfirmation} methods within the
     *            assertion
     * @param newStatementValidators validators used to validate {@link Statement}s within the assertion
     * @param newTrustEngine the trust used to validate the Assertion signature
     */
    public SAML20AssertionTokenValidator(Collection<ConditionValidator> newConditionValidators,
            Collection<SubjectConfirmationValidator> newConfirmationValidators,
            Collection<StatementValidator> newStatementValidators, SignatureTrustEngine newTrustEngine) {
        conditionValidators = new LazyMap<QName, ConditionValidator>();
        if (newConditionValidators != null) {
            for (ConditionValidator validator : newConditionValidators) {
                if (validator != null) {
                    conditionValidators.put(validator.getServicedCondition(), validator);
                }
            }
        }

        subjectConfirmationValidators = new LazyMap<String, SubjectConfirmationValidator>();
        if (newConfirmationValidators != null) {
            for (SubjectConfirmationValidator validator : newConfirmationValidators) {
                if (validator != null) {
                    subjectConfirmationValidators.put(validator.getServicedMethod(), validator);
                }
            }
        }

        statementValidators = new LazyMap<QName, StatementValidator>();
        if (newStatementValidators != null) {
            for (StatementValidator validator : newStatementValidators) {
                if (validator != null) {
                    statementValidators.put(validator.getServicedStatement(), validator);
                }
            }
        }
        
        trustEngine = newTrustEngine;
        samlSigProfileValidator = new SAMLSignatureProfileValidator();
    }

    /**
     * Gets the clock skew from the {@link ValidationContext#getStaticParameters()} parameters. If the parameter is not
     * set or is not a positive {@link Long} then the {@link #DEFAULT_CLOCK_SKEW} is used.
     * 
     * @param context current validation context
     * 
     * @return the clock skew
     */
    public static long getClockSkew(ValidationContext context) {
        long clockSkew = DEFAULT_CLOCK_SKEW;

        if (context.getStaticParameters().containsKey(CLOCK_SKEW_PARAM)) {
            try {
                clockSkew = (Long) context.getStaticParameters().get(CLOCK_SKEW_PARAM);
                if (clockSkew < 1) {
                    clockSkew = DEFAULT_CLOCK_SKEW;
                }
            } catch (ClassCastException e) {
                clockSkew = DEFAULT_CLOCK_SKEW;
            }
        }

        return clockSkew;
    }

    /** {@inheritDoc} */
    public ValidationResult validate(Assertion assertion, ValidationContext context) throws ValidationException {
        ValidationResult result = validateVersion(assertion, context);
        if (result != ValidationResult.VALID) {
            return result;
        }

        result = validateSignature(assertion, context);
        if (result != ValidationResult.VALID) {
            return result;
        }

        result = validateConditions(assertion, context);
        if (result != ValidationResult.VALID) {
            return result;
        }

        result = validateSubjectConfirmation(assertion, context);
        if (result != ValidationResult.VALID) {
            return result;
        }

        return validateStatements(assertion, context);
    }

    /**
     * Validates that the assertion is a {@link SAMLVersion#VERSION_20} assertion.
     * 
     * @param assertion the assertion to validate
     * @param context current validation context
     * 
     * @return result of the validation evaluation
     */
    protected ValidationResult validateVersion(Assertion assertion, ValidationContext context) {
        if (assertion.getVersion() != SAMLVersion.VERSION_20) {
            context.setValidationFailureMessage(String.format(
                    "Assertion '%s' is not a SAML 2.0 version Assertion", assertion.getID()));
            return ValidationResult.INVALID;
        }
        return ValidationResult.VALID;
    }

    /**
     * Validates the signature of the assertion, if it is signed.
     * 
     * @param token assertion whose signature will be validated
     * @param context current validation context
     * 
     * @return the result of the signature validation
     * 
     * @throws ValidationException thrown if there is a problem determining the validity of the signature
     */
    protected ValidationResult validateSignature(Assertion token, ValidationContext context)
            throws ValidationException {
        
        Boolean signatureRequired = (Boolean) context.getStaticParameters().get(SIGNATURE_REQUIRED);
        if (signatureRequired == null) {
            signatureRequired = Boolean.TRUE;
        }
        
        // Validate params and requirements
        if (!token.isSigned()) {
            if (signatureRequired) {
                context.setValidationFailureMessage("Assertion was required to be signed, but was not");
                return ValidationResult.INVALID;
            } else {
                log.debug("Assertion was not required to be signed, and was not signed.  " 
                        + "Skipping further signature evaluation");
                return ValidationResult.VALID;
            }
        }
        
        if (trustEngine == null) {
            log.warn("Signature validation was necessary, but no signature trust engine was available");
            context.setValidationFailureMessage("Assertion signature could not be evaluated due to internal error");
            return ValidationResult.INDETERMINATE;
        }
        
        return performSignatureValidation(token, context);
    }
    
    /**
     * Handle the actual signature validation.
     * 
     * @param token assertion whose signature will be validated
     * @param context current validation context
     * 
     * @return the validation result
     */
    private ValidationResult performSignatureValidation(Assertion token, ValidationContext context) {
        Signature signature = token.getSignature();
        
        String tokenIssuer = null;
        if (token.getIssuer() != null) {
            tokenIssuer = token.getIssuer().getValue();
        }
        
        log.debug("Attempting signature validation on Assertion '{}' from Issuer '{}'",
                token.getID(), tokenIssuer);
        
        try {
            samlSigProfileValidator.validate(signature);
        } catch (ValidationException e) {
            String msg = String.format("Assertion Signature failed pre-validation: %s", e.getMessage());
            log.warn(msg);
            context.setValidationFailureMessage(msg);
            return ValidationResult.INVALID;
        }
        
        CriteriaSet criteriaSet = getSignatureValidationCriteriaSet(token, context);
        
        try {
            if (trustEngine.validate(signature, criteriaSet)) {
                log.debug("Validation of signature of Assertion '{}' from Issuer '{}' was successful",
                        token.getID(), tokenIssuer);
                return ValidationResult.VALID;
            } else {
                String msg = String.format(
                        "Signature of Assertion '%s' from Issuer '%s' was not valid", token.getID(), tokenIssuer);
                log.warn(msg);
                context.setValidationFailureMessage(msg);
                return ValidationResult.INVALID;
            }
        } catch (SecurityException e) {
            String msg = String.format(
                    "A problem was encountered evaluating the signature over Assertion with ID '%s': %s",
                    token.getID(), e.getMessage());
            log.warn(msg);
            context.setValidationFailureMessage(msg);
            return ValidationResult.INDETERMINATE;
        }
        
    }

    /**
     * Get the criteria set that will be used in evaluating the Assertion signature via the supplied trust engine.
     * 
     * @param token assertion whose signature will be validated
     * @param context current validation context
     * @return the criteria set to use
     */
    protected CriteriaSet getSignatureValidationCriteriaSet(Assertion token, ValidationContext context) {
        CriteriaSet criteriaSet = (CriteriaSet) context.getStaticParameters().get(SIGNATURE_VALIDATION_CRITERIA_SET);
        if (criteriaSet == null)  {
            criteriaSet = new CriteriaSet();
        }
        
        if (!criteriaSet.contains(EntityIDCriteria.class)) {
            String issuer =  null;
            if (token.getIssuer() != null) {
                issuer = DatatypeHelper.safeTrimOrNullString(token.getIssuer().getValue());
            }
            if (issuer != null) {
                criteriaSet.add(new EntityIDCriteria(issuer));
            }
        }
        
        if (!criteriaSet.contains(UsageCriteria.class)) {
            criteriaSet.add(new UsageCriteria(UsageType.SIGNING));
        }
        
        return criteriaSet;
    }

    /**
     * Validates the conditions on the assertion. Condition validators are looked up by the element QName and, if
     * present, the schema type of the condition. If no validator can be found for the Condition the validation process
     * fails.
     * 
     * @param assertion the assertion whose conditions will be validated
     * @param context current validation context
     * 
     * @return the result of the validation evaluation
     * 
     * @throws ValidationException thrown if there is a problem determining the validity of the conditions
     */
    protected ValidationResult validateConditions(Assertion assertion, ValidationContext context)
            throws ValidationException {
        Conditions conditions = assertion.getConditions();
        
        DateTime now = new DateTime(ISOChronology.getInstanceUTC());
        long clockSkew = getClockSkew(context);
            

        DateTime notBefore = conditions.getNotBefore();
        log.debug("Evaluating Conditions NotBefore '{}' against 'skewed now' time '{}'",
                notBefore, now.plus(clockSkew));
        if (notBefore != null && notBefore.isAfter(now.plus(clockSkew))) {
            context.setValidationFailureMessage(String.format(
                    "Assertion '%s' with NotBefore condition of '%s' is not yet valid", assertion.getID(), notBefore));
            return ValidationResult.INVALID;
        }

        DateTime notOnOrAfter = conditions.getNotOnOrAfter();
        log.debug("Evaluating Conditions NotOnOrAfter '{}' against 'skewed now' time '{}'",
                notOnOrAfter, now.minus(clockSkew));
        if (notOnOrAfter != null && notOnOrAfter.isBefore(now.minus(clockSkew))) {
            context.setValidationFailureMessage(String.format(
                    "Assertion '%s' with NotOnOrAfter condition of '%s' is no longer valid", assertion.getID(),
                    notOnOrAfter));
            return ValidationResult.INVALID;
        }

        ConditionValidator validator;
        for (Condition condition : conditions.getConditions()) {
            validator = conditionValidators.get(condition.getElementQName());
            if (validator == null && condition.getSchemaType() != null) {
                validator = conditionValidators.get(condition.getSchemaType());
            }

            if (validator == null) {
                String msg = String.format(
                        "Unknown Condition '%s' of type '%s' in assertion '%s'", 
                                condition.getElementQName(), condition.getSchemaType(), assertion.getID());
                log.debug(msg);
                context.setValidationFailureMessage(msg);
                return ValidationResult.INDETERMINATE;
            }
            if (validator.validate(condition, assertion, context) != ValidationResult.VALID) {
                String msg = String.format(
                        "Condition '%s' of type '%s' in assertion '%s' was not valid.",
                                condition.getElementQName(), condition.getSchemaType(), assertion.getID());
                if (context.getValidationFailureMessage() != null) {
                    msg = msg + ": " + context.getValidationFailureMessage();
                }
                log.debug(msg);
                context.setValidationFailureMessage(msg);
                return ValidationResult.INVALID;
            }
        }

        return ValidationResult.VALID;
    }

    /**
     * Validates the subject confirmations of the assertion. Validators are looked up by the subject confirmation
     * method. If any one subject confirmation is met the subject is considered confirmed per the SAML specification.
     * 
     * @param assertion assertion whose subject is being confirmed
     * @param context current validation context
     * 
     * @return the result of the validation
     * 
     * @throws ValidationException thrown if there is a problem determining the validity the subject
     */
    protected ValidationResult validateSubjectConfirmation(Assertion assertion, ValidationContext context)
            throws ValidationException {
        Subject assertionSubject = assertion.getSubject();
        if (assertionSubject == null) {
            return ValidationResult.VALID;
        }

        List<SubjectConfirmation> confirmations = assertionSubject.getSubjectConfirmations();
        if (confirmations == null || confirmations.isEmpty()) {
            return ValidationResult.VALID;
        }

        for (SubjectConfirmation confirmation : confirmations) {
            SubjectConfirmationValidator validator = subjectConfirmationValidators.get(confirmation.getMethod());
            if (validator != null) {
                try {
                    if (validator.validate(confirmation, assertion, context) == ValidationResult.VALID) {
                        context.getDynamicParameters().put(CONFIRMED_SUBJECT_CONFIRMATION, confirmation);
                        return ValidationResult.VALID;
                    }
                } catch (ValidationException e) {
                    log.warn("Error while executing subject confirmation validation " + validator.getClass().getName(),
                            e);
                }
            }
        }

        String msg = String.format(
                "No subject confirmation methods were met for assertion with ID '%s'", assertion.getID());
        log.debug(msg);
        context.setValidationFailureMessage(msg);
        return ValidationResult.INVALID;
    }

    /**
     * Validates the statements within the assertion. Validators are looked up by the Statement's element QName or, if
     * present, its schema type. Any statement for which a validator can not be found is simply ignored.
     * 
     * @param assertion assertion whose statements are being validated
     * @param context current validation context
     * 
     * @return result of the validation
     * 
     * @throws ValidationException thrown if there is a problem determining the validity the statements
     */
    protected ValidationResult validateStatements(Assertion assertion, ValidationContext context)
            throws ValidationException {
        List<Statement> statements = assertion.getStatements();
        if (statements == null || statements.isEmpty()) {
            return ValidationResult.VALID;
        }

        ValidationResult result;
        StatementValidator validator;
        for (Statement statement : statements) {
            validator = statementValidators.get(statement.getElementQName());
            if (validator == null && statement.getSchemaType() != null) {
                validator = statementValidators.get(statement.getSchemaType());
            }

            if (validator != null) {
                result = validator.validate(statement, assertion, context);
                if (result != ValidationResult.VALID) {
                    return result;
                }
            }
        }

        return ValidationResult.VALID;
    }
}