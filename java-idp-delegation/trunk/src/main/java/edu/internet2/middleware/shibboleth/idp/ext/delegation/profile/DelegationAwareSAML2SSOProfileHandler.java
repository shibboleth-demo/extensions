/*
 * Copyright 2007 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.profile;

import java.util.Iterator;
import java.util.List;

import org.openliberty.xmltooling.disco.MetadataAbstract;
import org.openliberty.xmltooling.disco.ProviderID;
import org.openliberty.xmltooling.disco.SecurityContext;
import org.openliberty.xmltooling.disco.SecurityMechID;
import org.openliberty.xmltooling.disco.ServiceType;
import org.openliberty.xmltooling.security.Token;
import org.openliberty.xmltooling.soapbinding.Framework;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.binding.BasicEndpointSelector;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.AttributeValue;
import org.opensaml.saml2.core.Audience;
import org.opensaml.saml2.core.AudienceRestriction;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.Conditions;
import org.opensaml.saml2.core.KeyInfoConfirmationDataType;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.StatusCode;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.saml2.core.SubjectConfirmationData;
import org.opensaml.saml2.metadata.AttributeConsumingService;
import org.opensaml.saml2.metadata.Endpoint;
import org.opensaml.saml2.metadata.RequestedAttribute;
import org.opensaml.saml2.metadata.SPSSODescriptor;
import org.opensaml.saml2.metadata.SingleSignOnService;
import org.opensaml.security.MetadataCredentialResolver;
import org.opensaml.security.MetadataCriteria;
import org.opensaml.ws.transport.http.HTTPInTransport;
import org.opensaml.ws.transport.http.HTTPOutTransport;
import org.opensaml.ws.wsaddressing.Address;
import org.opensaml.ws.wsaddressing.EndpointReference;
import org.opensaml.ws.wsaddressing.Metadata;
import org.opensaml.ws.wsaddressing.WSAddressingObjectBuilder;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.XMLObjectBuilder;
import org.opensaml.xml.schema.XSAny;
import org.opensaml.xml.security.CriteriaSet;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.credential.UsageType;
import org.opensaml.xml.security.criteria.EntityIDCriteria;
import org.opensaml.xml.security.criteria.UsageCriteria;
import org.opensaml.xml.security.keyinfo.KeyInfoGenerator;
import org.opensaml.xml.security.keyinfo.KeyInfoGeneratorFactory;
import org.opensaml.xml.security.keyinfo.KeyInfoGeneratorManager;
import org.opensaml.xml.signature.KeyInfo;
import org.opensaml.xml.util.DatatypeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.common.profile.ProfileException;
import edu.internet2.middleware.shibboleth.idp.authn.Saml2LoginContext;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.LibertyConstants;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.relyingparty.DelegationAwareSSOConfiguration;
import edu.internet2.middleware.shibboleth.idp.profile.saml2.BaseSAML2ProfileRequestContext;
import edu.internet2.middleware.shibboleth.idp.profile.saml2.SSOProfileHandler;

/** SAML 2.0 SSO request profile handler. */
public class DelegationAwareSAML2SSOProfileHandler extends SSOProfileHandler {
    
    /** Internal error message constant. */
    public static final String INTERNAL_ERR_MSG = "Internal IdP processing error";
    
    /** Enum which represents the state of the request presenter's indication of whether
     * a delegation token is requested. */
    private static enum DelegationRequest {
        /** Delegation was not requested. */
        NOT_REQUESTED,
        /** Delegation was requested, as optional. */
        REQUESTED_OPTIONAL,
        /** Delegation was requested, as required. */
        REQUESTED_REQUIRED,
    };
    
    /** Default delegation request value. */
    private DelegationRequest defaultDelegationRequested = DelegationRequest.REQUESTED_OPTIONAL;
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(DelegationAwareSAML2SSOProfileHandler.class);
    
    /** For building subject confirmation. */
    private SAMLObjectBuilder<SubjectConfirmation> subjectConfirmationBuilder;

    /** For building subject confirmation data. */
    private SAMLObjectBuilder<KeyInfoConfirmationDataType> keyInfoSubjectConfirmationDataBuilder;
    
    /** For building NameID. */
    private SAMLObjectBuilder<NameID> nameIDBuilder;
    
    /** For building subject. */
    private SAMLObjectBuilder<Subject> subjectBuilder;
    
    /** For building audience. */
    private SAMLObjectBuilder<Audience> audienceBuilder;

    /** For building audience restriction. */
    private SAMLObjectBuilder<AudienceRestriction> audienceRestrictionBuilder;
    
    /** For building AttributeValue elements with XMLObject content. */
    private XMLObjectBuilder<XSAny> xsAnyBuilder;
    
    /** For building attribute. */
    private SAMLObjectBuilder<Attribute> attributeBuilder;
    
    /** For building attribute statement. */
    private SAMLObjectBuilder<AttributeStatement> attributeStatementBuilder;
    
    /** For building wsa:EndpointReference. */
    private WSAddressingObjectBuilder<EndpointReference> wsaEPRBuilder;

    /** For building wsa:Address. */
    private WSAddressingObjectBuilder<Address> wsaAddressBuilder;
    
    /** For building wsa:Metadata. */
    private WSAddressingObjectBuilder<Metadata> wsaMetadataBuilder;
    
    /** For building Liberty ServiceType. */
    private XMLObjectBuilder<ServiceType> serviceTypeBuilder;
    
    /** For building Liberty Abstract. */
    private XMLObjectBuilder<MetadataAbstract> abstractBuilder;
    
    /** For building Liberty ProviderID. */
    private XMLObjectBuilder<ProviderID> providerIDBuilder;
    
    /** For building Liberty Framework. */
    private XMLObjectBuilder<Framework> frameworkBuilder;
    
    /** For building Liberty SecurityContext. */
    private XMLObjectBuilder<SecurityContext> securityContextBuilder;
    
    /** For building Liberty SecurityMechID. */
    private XMLObjectBuilder<SecurityMechID> securityMechIDBuilder;
    
    /** For building Liberty Token. */
    private XMLObjectBuilder<Token> tokenBuilder;
    
    /**
     * Constructor.
     * 
     * @param authnManagerPath path to the authentication manager servlet
     */
    @SuppressWarnings("unchecked")
    public DelegationAwareSAML2SSOProfileHandler(String authnManagerPath) {
        super(authnManagerPath);
        subjectConfirmationBuilder = (SAMLObjectBuilder<SubjectConfirmation>) getBuilderFactory()
            .getBuilder(SubjectConfirmation.DEFAULT_ELEMENT_NAME);
        keyInfoSubjectConfirmationDataBuilder = (SAMLObjectBuilder<KeyInfoConfirmationDataType>) 
            getBuilderFactory().getBuilder(KeyInfoConfirmationDataType.TYPE_NAME);
        nameIDBuilder = (SAMLObjectBuilder<NameID>) getBuilderFactory().getBuilder(NameID.DEFAULT_ELEMENT_NAME);
        subjectBuilder = (SAMLObjectBuilder<Subject>) getBuilderFactory().getBuilder(Subject.DEFAULT_ELEMENT_NAME);
        audienceBuilder = (SAMLObjectBuilder<Audience>) getBuilderFactory().getBuilder(Audience.DEFAULT_ELEMENT_NAME);
        audienceRestrictionBuilder = (SAMLObjectBuilder<AudienceRestriction>) getBuilderFactory()
            .getBuilder(AudienceRestriction.DEFAULT_ELEMENT_NAME);
        xsAnyBuilder = getBuilderFactory().getBuilder(XSAny.TYPE_NAME);
        attributeBuilder = (SAMLObjectBuilder<Attribute>) getBuilderFactory()
            .getBuilder(Attribute.DEFAULT_ELEMENT_NAME);
        attributeStatementBuilder = (SAMLObjectBuilder<AttributeStatement>) getBuilderFactory()
            .getBuilder(AttributeStatement.DEFAULT_ELEMENT_NAME);
        wsaEPRBuilder = (WSAddressingObjectBuilder<EndpointReference>) getBuilderFactory()
            .getBuilder(EndpointReference.ELEMENT_NAME);
        wsaAddressBuilder = (WSAddressingObjectBuilder<Address>) getBuilderFactory().getBuilder(Address.ELEMENT_NAME);
        wsaMetadataBuilder = (WSAddressingObjectBuilder<Metadata>) getBuilderFactory()
            .getBuilder(Metadata.ELEMENT_NAME);
        serviceTypeBuilder = getBuilderFactory().getBuilder(LibertyConstants.DISCO_SERVICE_TYPE_ELEMENT_NAME);
        abstractBuilder = getBuilderFactory().getBuilder(LibertyConstants.DISCO_ABSTRACT_ELEMENT_NAME);
        providerIDBuilder = getBuilderFactory().getBuilder(LibertyConstants.DISCO_PROVIDERID_ELEMENT_NAME);
        frameworkBuilder = getBuilderFactory().getBuilder(Framework.DEFAULT_ELEMENT_NAME);
        securityContextBuilder = getBuilderFactory().getBuilder(LibertyConstants.DISCO_SECURITY_CONTEXT_ELEMENT_NAME);
        securityMechIDBuilder = getBuilderFactory().getBuilder(LibertyConstants.DISCO_SECURITY_MECH_ID_ELEMENT_NAME);
        tokenBuilder = getBuilderFactory().getBuilder(LibertyConstants.SECURITY_TOKEN_ELEMENT_NAME);
        
        // This default value of REQUESTED_OPTIONAL effectively allows the policy 
        // at the IdP to directly control whether a delegation token will be issued. */
        defaultDelegationRequested = DelegationRequest.REQUESTED_OPTIONAL;
    }
    
    /** {@inheritDoc} */
    public String getProfileId() {
        return DelegationAwareSSOConfiguration.PROFILE_ID;
    }

    /**
     * Get the effective default value for whether request processing should proceed 
     * with issuance of a delegation token.
     * 
     * @return the default value
     */
    public DelegationRequest getDefaultDelegationRequested() {
        return defaultDelegationRequested;
    }
    
    /**
     * Set the effective default value for whether request processing should proceed 
     * with issuance of a delegation token.
     * 
     * @param delegationRequest the default delegation requested value
     */
    public void setDefaultDelegationRequested(DelegationRequest delegationRequest) {
        defaultDelegationRequested = delegationRequest;
    }

    /** {@inheritDoc} */
    protected void postProcessAssertion(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext,
            Assertion assertion) throws ProfileException {
        
        DelegationRequest delegationRequestState = getDelegationRequested(requestContext);
        
        switch (delegationRequestState) {
            case NOT_REQUESTED:
                log.debug("Delegation was not requested, skipping delegation decoration");
                break;
            case REQUESTED_OPTIONAL:
                if (isDelegationAllowed(requestContext)) {
                    log.debug("Delegation token issuance was requested (optional) and allowed");
                    decorateDelegatedAssertion(requestContext, assertion);
                } else {
                    log.debug("Delegation token issuance was requested (optional), but not allowed, skipping delegation decoration");
                    return;
                }
                break;
            case REQUESTED_REQUIRED:
                if (isDelegationAllowed(requestContext)) {
                    log.debug("Delegation token issuance was requested (required) and allowed");
                    decorateDelegatedAssertion(requestContext, assertion);
                } else {
                    log.warn("Delegation token issuance was requested (required), but disallowed by policy");
                    requestContext.setFailureStatus(buildStatus(StatusCode.REQUESTER_URI, StatusCode.REQUEST_DENIED_URI,
                            "A delegation token was requested but was disallowed by policy"));
                    throw new ProfileException("Delegation was requested and required, but disallowed by policy");
                }
                break;
            default:
                log.error("Unknown value '{}' for delegation request state", delegationRequestState);
        }
        
    }

    /**
     * Decorate the Assertion to allow use as a delegated security token by the SAML requester.
     * 
     * @param requestContext the current request context
     * @param assertion the delegated assertion being issued
     * 
     * @throws ProfileException if there was a problem handling the assertion
     */
    private void decorateDelegatedAssertion(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext,
            Assertion assertion) throws ProfileException {
        
        addSAMLPeerSubjectConfirmation(requestContext, assertion);
        addIdPAudienceRestriction(requestContext, assertion);
        addLibertySSOSEPRAttribute(requestContext, assertion);
    }

    /**
     * Add Liberty SSOS service Endpoint Reference (EPR) attribute to Assertion's AttributeStatement.
     * 
     * @param requestContext the current request context
     * @param assertion the delegated assertion being issued
     * 
     * @throws ProfileException if EPR attribute can not be generated
     */
    private void addLibertySSOSEPRAttribute(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext,
            Assertion assertion) throws ProfileException {
        Attribute attribute = attributeBuilder.buildObject();
        attribute.setName(LibertyConstants.SERVICE_TYPE_SSOS);
        attribute.setNameFormat(Attribute.URI_REFERENCE);
        attribute.getAttributeValues().add(buildLibertSSOSEPRAttributeValue(requestContext, assertion));
        
        List<AttributeStatement> attributeStatements = assertion.getAttributeStatements();
        AttributeStatement attributeStatement = null;
        if (attributeStatements.isEmpty()) {
            attributeStatement = attributeStatementBuilder.buildObject();
            assertion.getAttributeStatements().add(attributeStatement);
        } else {
            attributeStatement = attributeStatements.get(0);
        }
        attributeStatement.getAttributes().add(attribute);
    }

    /**
     * Build the Liberty SSOS EPR AttributeValue object.
     * 
     * @param requestContext the current request context
     * @param assertion the delegated assertion being issued
     * @return the AttributeValue object containing the EPR
     * 
     * @throws ProfileException if there is a problem building the EPR
     */
    private XMLObject buildLibertSSOSEPRAttributeValue(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext,
            Assertion assertion) throws ProfileException {
        
        Address address = wsaAddressBuilder.buildObject();
        address.setValue(getIdPEPRAddress(requestContext));
        
        MetadataAbstract libertyAbstract = abstractBuilder.buildObject(LibertyConstants.DISCO_ABSTRACT_ELEMENT_NAME);
        libertyAbstract.setValue(LibertyConstants.SSOS_EPR_METADATA_ABSTRACT);
        
        ServiceType serviceType = serviceTypeBuilder.buildObject(LibertyConstants.DISCO_SERVICE_TYPE_ELEMENT_NAME);
        serviceType.setValue(LibertyConstants.SERVICE_TYPE_SSOS);
        
        ProviderID providerID = providerIDBuilder.buildObject(LibertyConstants.DISCO_PROVIDERID_ELEMENT_NAME);
        providerID.setValue(requestContext.getLocalEntityId());
        
        Framework framework = frameworkBuilder.buildObject(Framework.DEFAULT_ELEMENT_NAME);
        framework.setVersion("2.0");
        
        SecurityMechID securityMechID = securityMechIDBuilder
            .buildObject(LibertyConstants.DISCO_SECURITY_MECH_ID_ELEMENT_NAME);
        securityMechID.setValue(LibertyConstants.SECURITY_MECH_ID_CLIENT_TLS_PEER_SAML_V2);
        
        Token token = tokenBuilder.buildObject(LibertyConstants.SECURITY_TOKEN_ELEMENT_NAME);
        token.setUsage(LibertyConstants.TOKEN_USAGE_SECURITY_TOKEN);
        token.setRef("#" + assertion.getID());
        
        SecurityContext securityContext = securityContextBuilder
            .buildObject(LibertyConstants.DISCO_SECURITY_CONTEXT_ELEMENT_NAME);
        securityContext.getSecurityMechIDs().add(securityMechID);
        securityContext.getTokens().add(token);
        
        Metadata metadata = wsaMetadataBuilder.buildObject();
        metadata.getUnknownXMLObjects().add(libertyAbstract);
        metadata.getUnknownXMLObjects().add(serviceType);
        metadata.getUnknownXMLObjects().add(providerID);
        metadata.getUnknownXMLObjects().add(framework);
        metadata.getUnknownXMLObjects().add(securityContext);
        
        EndpointReference epr = wsaEPRBuilder.buildObject();
        epr.setAddress(address);
        epr.setMetadata(metadata);
        
        XSAny attributeValue = xsAnyBuilder.buildObject(AttributeValue.DEFAULT_ELEMENT_NAME);
        attributeValue.getUnknownXMLObjects().add(epr);
        
        return attributeValue;
    }

    /**
     * Get the endpoint address for the IdP's Liberty ID-WSF SSOS service endpoint.
     * 
     * @param requestContext the current request context
     * @return the ID-WSF SSOS endpoint address
     * 
     * @throws ProfileException if the endpoint can not be resolved
     */
    private String getIdPEPRAddress(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext) 
            throws ProfileException {
        BasicEndpointSelector endpointSelector = new BasicEndpointSelector();
        endpointSelector.setEndpointType(SingleSignOnService.DEFAULT_ELEMENT_NAME);
        endpointSelector.setEntityRoleMetadata(requestContext.getLocalEntityRoleMetadata());
        endpointSelector.getSupportedIssuerBindings().add(LibertyConstants.SOAP_BINDING_20_URI);
        
        Endpoint endpoint = endpointSelector.selectEndpoint();
        if (endpoint == null || DatatypeHelper.isEmpty(endpoint.getLocation())) {
            log.error("The IdP's Liberty ID-WSF SSOS service endpoint address could not be resolved");
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, INTERNAL_ERR_MSG));
            throw new ProfileException("IdP Liberty ID-WSF SSOS service endpoint address could not be resolved");
        }
        return endpoint.getLocation();
    }

    /**
     * An an AudienceRestriction condition indicating the IdP as an acceptable Audience.
     * 
     * @param requestContext the current request context
     * @param assertion the assertion being isued
     */
    private void addIdPAudienceRestriction(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext,
            Assertion assertion) {
        
        List<AudienceRestriction> audienceRestrictions = assertion.getConditions().getAudienceRestrictions();
        AudienceRestriction audienceRestriction = null;
        if (audienceRestrictions.isEmpty()) {
            audienceRestriction = audienceRestrictionBuilder.buildObject();
            assertion.getConditions().getAudienceRestrictions().add(audienceRestriction);
        } else {
            audienceRestriction = audienceRestrictions.get(0);
        }
        
        String localEntityId = requestContext.getLocalEntityId();
        
        // Sanity check that IdP audience has not already been added by other code.
        for (Audience audience : audienceRestriction.getAudiences()) {
            if (localEntityId.equals(audience.getAudienceURI())) {
                log.debug("Local entity ID '{}' already present in assertion AudienceRestriction set, skipping",
                        localEntityId);
                return;
            }
        }
        
        Audience idpAudience = audienceBuilder.buildObject();
        idpAudience.setAudienceURI(localEntityId);
        audienceRestriction.getAudiences().add(idpAudience);
    }

    /**
     * Add SubjectConfirmation to the Assertion Subject to allow confirmation when wielded by the SAML requester.
     * 
     * @param requestContext the current request context
     * @param assertion the assertion being issued
     * @throws ProfileException if there is a problem resolving the peer's keys for HoK confirmation
     */
    private void addSAMLPeerSubjectConfirmation(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext,
            Assertion assertion) throws ProfileException {
        
        String peerEntityId = requestContext.getPeerEntityId();
        
        // Add holder-of-key confirmation for all signing keys present for SP in metadata
        MetadataCredentialResolver credentialResolver = new MetadataCredentialResolver(getMetadataProvider());
        CriteriaSet criteriaSet = new CriteriaSet();
        criteriaSet.add(new EntityIDCriteria(peerEntityId));
        criteriaSet.add(new MetadataCriteria(SPSSODescriptor.DEFAULT_ELEMENT_NAME, SAMLConstants.SAML20P_NS));
        criteriaSet.add(new UsageCriteria(UsageType.SIGNING));
        
        try {
            addHoKSubjectConfirmation(assertion, peerEntityId, credentialResolver.resolve(criteriaSet).iterator());
        } catch (SecurityException e) {
            log.error("Error resolving holder-of-key credentials for SP '{}': {})", peerEntityId, e.getMessage());
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, INTERNAL_ERR_MSG));
            throw new ProfileException("Error resolving holder-of-key credentials", e);
        } catch (ProfileException e) {
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, INTERNAL_ERR_MSG));
            throw e;
        }
        
    }

    /**
     * Add a holder-of-key SubjectConfirmation for the SAML peer entity ID. The KeyInfoConfirmationDataType
     * SubjectConfirmationData will contain a KeyInfo for each resolved credential;
     * 
     * @param assertion the assertion to be updated
     * @param peerEntityId the SAML peer ID
     * @param credIterator the iterator of resolved credentials
     * @throws ProfileException if no credentials were resolved or there is an error generating KeyInfo data 
     *              from resolved credentials
     */
    private void addHoKSubjectConfirmation(Assertion assertion, String peerEntityId, Iterator<Credential> credIterator) 
            throws ProfileException {
        if (!credIterator.hasNext()) {
            log.error("No credentials were available from SP '{}' for HoK assertion", peerEntityId);
            throw new ProfileException("No credentials were available for creating HoK subject confirmation");
        }
        
        KeyInfoConfirmationDataType scData = keyInfoSubjectConfirmationDataBuilder.
            buildObject(SubjectConfirmationData.DEFAULT_ELEMENT_NAME, KeyInfoConfirmationDataType.TYPE_NAME);
        
        KeyInfoGeneratorManager kigm = 
            Configuration.getGlobalSecurityConfiguration().getKeyInfoGeneratorManager().getDefaultManager();
        while (credIterator.hasNext()) {
            Credential cred = credIterator.next();
            KeyInfoGeneratorFactory kigf = kigm.getFactory(cred);
            KeyInfoGenerator kig = kigf.newInstance();
            try {
                KeyInfo keyInfo = kig.generate(cred);
                scData.getKeyInfos().add(keyInfo);
            } catch (SecurityException e) {
                log.error("Error generating KeyInfo from peer credential: {}", e.getMessage());
                throw new ProfileException("Error generating KeyInfo from credential", e);
            }
        }
        
        NameID nameID = nameIDBuilder.buildObject();
        nameID.setValue(peerEntityId);
        nameID.setFormat(NameID.ENTITY);
        
        SubjectConfirmation sc = subjectConfirmationBuilder.buildObject();
        sc.setMethod("urn:oasis:names:tc:SAML:2.0:cm:holder-of-key");
        sc.setNameID(nameID);
        sc.setSubjectConfirmationData(scData);
        
        Subject subject = assertion.getSubject();
        if (subject==null) {
            subject = subjectBuilder.buildObject();
            assertion.setSubject(subject);
        }
        subject.getSubjectConfirmations().add(sc);
    }

    /**
     * Check whether issuance of a delegated token has been requested.
     * 
     * @param requestContext the current request context
     * @return true if delegation is requested, false otherwise
     */
    private DelegationRequest getDelegationRequested(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext) {
        DelegationAwareSSORequestContext delegationContext = (DelegationAwareSSORequestContext) requestContext;
        // The effective value will be stored off in the context so we don't do this
        // potentially semi-expensive evaluation more than once.
        if (delegationContext.getDelegationRequested() != null) {
            log.debug("Context delegationRequested was non-null, returning stored value: {}",
                    delegationContext.getDelegationRequested());
            return delegationContext.getDelegationRequested();
        } else {
            log.debug("Context delegationRequested was null, evaluating and storing.");
            if (isDelegationRequestedByAudience(requestContext)) {
                delegationContext.setDelegationRequested(DelegationRequest.REQUESTED_REQUIRED);
            } else {
                DelegationRequest requestedByMetadata = getDelegationRequestedByMetadata(requestContext);
                if (requestedByMetadata != DelegationRequest.NOT_REQUESTED) {
                    delegationContext.setDelegationRequested(requestedByMetadata);
                }
            }
        }
        
        if (delegationContext.getDelegationRequested() == null) {
            log.debug("Delegation request was not explicitly indicated, using default value");
            delegationContext.setDelegationRequested(getDefaultDelegationRequested());
        }
        
        log.debug("Context delegationRequested value was evaluated and stored as: {}",
                delegationContext.getDelegationRequested());
        
        return delegationContext.getDelegationRequested();
    }
    

    /**
     * Determine whether a delegation token was requested via the SP's SPSSODescriptor AttributeConsumingService.
     * 
     * @param requestContext the current request context
     * @return DelegationRequest enum value as appropriate
     */
    private DelegationRequest getDelegationRequestedByMetadata(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext) {
        AttributeConsumingService attribCS = findAttributeConsumingService(requestContext);
        if (attribCS == null) {
            return DelegationRequest.NOT_REQUESTED;
        }
        
        for (RequestedAttribute requestedAttribute : attribCS.getRequestAttributes()) {
            if (DatatypeHelper.safeEquals(LibertyConstants.SERVICE_TYPE_SSOS, requestedAttribute.getName())) {
                log.debug("Saw requested attribute '{}' in metadata AttributeConsumingService for SP: {}",
                        LibertyConstants.SERVICE_TYPE_SSOS, requestContext.getPeerEntityId());
                if (requestedAttribute.isRequired()) {
                    log.debug("Metadata delegation request attribute indicated it was required");
                    return DelegationRequest.REQUESTED_REQUIRED;
                } else {
                    log.debug("Metadata delegation request attribute indicated it was NOT required");
                    return DelegationRequest.REQUESTED_OPTIONAL;
                }
            }
        }
        
        return DelegationRequest.NOT_REQUESTED;
    }

    /**
     * Find the relevant AttributeConsumingService entry from the SP's SPSSODescriptor in metadata.
     * 
     * @param requestContext the current request context
     * @return the AttributeConsumingService object, or null if not present
     */
    private AttributeConsumingService findAttributeConsumingService(
            BaseSAML2ProfileRequestContext<?, ?, ?> requestContext) {
        
        AuthnRequest authnRequest = (AuthnRequest) requestContext.getInboundSAMLMessage();
        Integer requestAttribCSIndex = authnRequest.getAttributeConsumingServiceIndex();
        boolean useDefault = false;
        if (requestAttribCSIndex == null) {
            useDefault = true;
        }
        
        SPSSODescriptor spssoDesc = (SPSSODescriptor) requestContext.getPeerEntityRoleMetadata();
        AttributeConsumingService firstNoDefaultACS = null;
        for (AttributeConsumingService attribCS : spssoDesc.getAttributeConsumingServices()) {
            if ( (useDefault && attribCS.isDefault()) || 
                    (requestAttribCSIndex != null && requestAttribCSIndex == attribCS.getIndex()) ) {
                return attribCS;
            }
            if (firstNoDefaultACS == null && attribCS.isDefaultXSBoolean() == null) {
                firstNoDefaultACS = attribCS;
            }
        }
        
        if (firstNoDefaultACS != null) {
            return firstNoDefaultACS;
        }
        
        if (spssoDesc.getAttributeConsumingServices() != null && 
                spssoDesc.getAttributeConsumingServices().size() > 0) {
            return spssoDesc.getAttributeConsumingServices().get(0);
        }
        
        return null;
    }

    /**
     * Determine whether a delegation token was requested via the inbound AuthnRequest's
     * Conditions' AudienceRestriction.
     * 
     * @param requestContext the current request context
     * @return true if the AudienceRestrictions condition contained the local entity Id, false otherwise
     */
    private boolean isDelegationRequestedByAudience(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext) {
        AuthnRequest authnRequest = (AuthnRequest) requestContext.getInboundSAMLMessage();
        if (authnRequest.getConditions() != null) {
            Conditions conditions = authnRequest.getConditions();
            String localEntityId = requestContext.getLocalEntityId();
            for (AudienceRestriction ar : conditions.getAudienceRestrictions()) {
                for (Audience audience : ar.getAudiences()) {
                    String audienceValue = DatatypeHelper.safeTrimOrNullString(audience.getAudienceURI());
                    if (DatatypeHelper.safeEquals(audienceValue, localEntityId)) {
                        log.debug("Saw an AuthnRequest/Conditions/AudienceRestriction/Audience with value of '{}'",
                                localEntityId);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Check whether Assertions may be delegated to the SAML requester.
     * 
     * @param requestContext the current request context
     * @return true if delegation is allowed for the SAML requester, otherwise false
     */
    private boolean isDelegationAllowed(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext) {
        DelegationAwareSSOConfiguration profileConfig = 
            (DelegationAwareSSOConfiguration) requestContext.getProfileConfiguration();
        return profileConfig.getAllowTokenDelegation();
    }

    /** {@inheritDoc} */
    protected boolean isEncryptNameID(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext) throws ProfileException {
        if (isDelegationIssuanceActive(requestContext)) {
            if (isRequestRequiresEncryptNameID(requestContext)) {
                log.warn("Issuance of a delegation token is active, and request indicated an encrypted NameID, ",
                        "which is currently unsupported");
                throw new ProfileException("Unable to issue delegation token with encrypted NameID");
            } else {
                log.debug("Issuance of a delegation token is active, " 
                        + "overriding eval as to encrypting NameID to: false");
                return false;
            }
        } else {
            log.debug("Issuance of a delegation token is not active, " 
                    + "proceeding with normal eval as to encrypting NameID");
            return super.isEncryptNameID(requestContext);
        }
    }

    /** {@inheritDoc} */
    protected boolean isSignAssertion(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext) throws ProfileException {
        if (isDelegationIssuanceActive(requestContext)) {
            log.debug("Issuance of a delegation token is active, " 
                    + "overriding eval as to signing assertion to: true");
            return true;
        } else {
            log.debug("Issuance of a delegation token is not active, " 
                    + "proceeding with normal eval as to signing assertion");
            return super.isSignAssertion(requestContext);
        }
    }
    
    /**
     * Determine whether issuance of a delegation token is effectively active.
     * @param requestContext the current request context
     * @return true if issuance of a delegation token is in effect for the current request, false otherwise
     */
    protected boolean isDelegationIssuanceActive(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext) {
        return getDelegationRequested(requestContext) != DelegationRequest.NOT_REQUESTED
            && isDelegationAllowed(requestContext);
    }
    
    /** {@inheritDoc} */
    protected SSORequestContext buildRequestContext(Saml2LoginContext loginContext, HTTPInTransport in,
            HTTPOutTransport out) throws ProfileException {
        // We have to duplicate this logic from the superclass b/c it doesn't give anyway
        // to build a different profile context impl subtype
        DelegationAwareSSORequestContext requestContext = new DelegationAwareSSORequestContext();
        requestContext.setCommunicationProfileId(getProfileId());

        requestContext.setMessageDecoder(getInboundMessageDecoder(requestContext));

        requestContext.setLoginContext(loginContext);

        requestContext.setInboundMessageTransport(in);
        requestContext.setInboundSAMLProtocol(SAMLConstants.SAML20P_NS);

        requestContext.setOutboundMessageTransport(out);
        requestContext.setOutboundSAMLProtocol(SAMLConstants.SAML20P_NS);

        requestContext.setMetadataProvider(getMetadataProvider());

        String relyingPartyId = loginContext.getRelyingPartyId();
        requestContext.setPeerEntityId(relyingPartyId);
        requestContext.setInboundMessageIssuer(relyingPartyId);

        populateRequestContext(requestContext);

        return requestContext;
    }
    
    /** Represents the internal state of a delegation-aware SAML 2.0 SSO Request 
     * while it's being processed by the IdP. */
    protected class DelegationAwareSSORequestContext extends SSORequestContext {
        
        /** The current known state of whether issuance of a delegation token was effectively requested. */
        private DelegationRequest delegationRequested;

        /**
         * Get the current known state of whether issuance of a delegation token was effectively requested.
         * @return Returns the delegationRequest.
         */
        public DelegationRequest getDelegationRequested() {
            return delegationRequested;
        }

        /**
         * Set the current known state of whether issuance of a delegation token was effectively requested.
         * @param newDelegationRequested The delegationRequest to set.
         */
        public void setDelegationRequested(DelegationRequest newDelegationRequested) {
            delegationRequested = newDelegationRequested;
        }

    }

}