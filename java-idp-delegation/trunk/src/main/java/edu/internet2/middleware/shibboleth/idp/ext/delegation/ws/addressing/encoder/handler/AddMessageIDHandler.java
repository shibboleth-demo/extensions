/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.addressing.encoder.handler;


import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.MessageContextEvaluatingFunctor;
import org.opensaml.ws.message.MessageException;
import org.opensaml.ws.message.handler.Handler;
import org.opensaml.ws.message.handler.HandlerException;
import org.opensaml.ws.soap.util.SOAPHelper;
import org.opensaml.ws.wsaddressing.MessageID;
import org.opensaml.ws.wsaddressing.WSAddressingObjectBuilder;
import org.opensaml.xml.Configuration;

/**
 * Handler implementation that adds a wsa:MessageID header to the outbound SOAP envelope.
 */
public class AddMessageIDHandler implements Handler {
    
    /** Builder of MessageID object. */
    private WSAddressingObjectBuilder<MessageID> messageIDBuilder;
    
    /** Message context functor which produces the MessageID value. */
    private MessageContextEvaluatingFunctor<String> messageIDValueSource;

    /** Constructor. */
    @SuppressWarnings("unchecked")
    public AddMessageIDHandler() {
        messageIDBuilder = (WSAddressingObjectBuilder<MessageID>) Configuration.getBuilderFactory()
            .getBuilder(MessageID.ELEMENT_NAME);
    }
    
    /**
     * Set the functor which produces the MessageID value from the message context.
     * 
     * @param functor the new message context functor
     */
    public void setMessageIDValueSource(MessageContextEvaluatingFunctor<String> functor) {
        messageIDValueSource = functor;
    }

    /** {@inheritDoc} */
    public void invoke(MessageContext msgContext) throws HandlerException {
        String value = getMessageIDValue(msgContext);
        if (value != null) {
            MessageID messageID = messageIDBuilder.buildObject();
            messageID.setValue(getMessageIDValue(msgContext));
            
            SOAPHelper.addHeaderBlock(msgContext, messageID);
        }
    }

    /**
     * Get the value of the MessageID.
     * 
     * @param msgContext the current message context
     * @return the MessageID value.
     * 
     * @throws HandlerException if there is a problem obtaining the message ID from the context
     */
    protected String getMessageIDValue(MessageContext msgContext) throws HandlerException {
        if (messageIDValueSource != null) {
            try {
                return messageIDValueSource.evaluate(msgContext);
            } catch (MessageException e) {
                throw new HandlerException("Error obtaining the message ID from the context", e);
            }
        }
        return new UUIDMessageIDGenerator().evaluate(msgContext);
    }

}
