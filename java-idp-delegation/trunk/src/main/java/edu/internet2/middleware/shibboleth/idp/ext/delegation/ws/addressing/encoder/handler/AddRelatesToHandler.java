/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.addressing.encoder.handler;

import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.MessageContextEvaluatingFunctor;
import org.opensaml.ws.message.MessageException;
import org.opensaml.ws.message.handler.Handler;
import org.opensaml.ws.message.handler.HandlerException;
import org.opensaml.ws.soap.util.SOAPHelper;
import org.opensaml.ws.wsaddressing.RelatesTo;
import org.opensaml.ws.wsaddressing.WSAddressingObjectBuilder;
import org.opensaml.xml.Configuration;

/**
 * Handler implementation that adds a wsa:RelatesTo header to the outbound SOAP envelope.
 */
public class AddRelatesToHandler implements Handler {
    
    /** Builder of MessageID object. */
    private WSAddressingObjectBuilder<RelatesTo> relatesToBuilder;
    
    /** Message context functor which produces the RelatesTo value. */
    private MessageContextEvaluatingFunctor<String> relatesToValueSource;

    /** Constructor. */
    @SuppressWarnings("unchecked")
    public AddRelatesToHandler() {
        relatesToBuilder = (WSAddressingObjectBuilder<RelatesTo>) Configuration.getBuilderFactory()
            .getBuilder(RelatesTo.ELEMENT_NAME);
    }
    
    /**
     * Set the functor which produces the Action value from the message context.
     * 
     * @param functor the new message context functor
     */
    public void setRelatesToValueSource(MessageContextEvaluatingFunctor<String> functor) {
        relatesToValueSource = functor;
    }

    /** {@inheritDoc} */
    public void invoke(MessageContext msgContext) throws HandlerException {
        String value = getRelatesToValue(msgContext);
        if (value != null) {
            RelatesTo relatesTo = relatesToBuilder.buildObject();
            relatesTo.setValue(value);
            
            SOAPHelper.addHeaderBlock(msgContext, relatesTo);
        }
    }

    /**
     * Get the value of the RelatesTo.
     * 
     * @param msgContext the current message context
     * @return the RelatesTo value.
     * 
     * @throws HandlerException if there is a problem obtaining the RelatesTo value
     */
    protected String getRelatesToValue(MessageContext msgContext) throws HandlerException {
        if (relatesToValueSource != null) {
            try {
                return relatesToValueSource.evaluate(msgContext);
            } catch (MessageException e) {
                throw new HandlerException("Error obtaining the RelatesTo value from the context", e);
            }
        }
        return null;
    }
    

}
