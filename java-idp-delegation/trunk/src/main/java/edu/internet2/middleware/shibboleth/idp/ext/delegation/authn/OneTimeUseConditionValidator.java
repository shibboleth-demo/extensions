/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.authn;

import javax.xml.namespace.QName;

import net.jcip.annotations.ThreadSafe;

import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Condition;
import org.opensaml.saml2.core.OneTimeUse;
import org.opensaml.util.storage.ReplayCache;

/**
 * {@link ConditionValidator} used for {@link OneTimeUse} conditions.
 * 
 * This validator does not expect any parameters in the {@link ValidationContext#getStaticParameters()} or
 * {@link ValidationContext#getDynamicParameters()}.
 * 
 * This validator does not populate any parameters in the {@link ValidationContext#getDynamicParameters()}.
 */
@ThreadSafe
public class OneTimeUseConditionValidator implements ConditionValidator {

    /** Replay cache used to track which assertions have been used. */
    private ReplayCache replayCache;

    /**
     * Constructor.
     * 
     * @param replay reply cache used to track which assertions have been used
     */
    public OneTimeUseConditionValidator(ReplayCache replay) {
        if (replay == null) {
            throw new IllegalArgumentException("Replay cache may not be null");
        }
        replayCache = replay;
    }

    /** {@inheritDoc} */
    public QName getServicedCondition() {
        return OneTimeUse.DEFAULT_ELEMENT_NAME;
    }

    /** {@inheritDoc} */
    public ValidationResult validate(Condition condition, Assertion assertion, ValidationContext context) {
        if (replayCache.isReplay(assertion.getIssuer().getValue(), assertion.getID())) {
            context.setValidationFailureMessage(String.format(
                    "Assertion '%s' has a one time use condition and has been used before", assertion.getID()));
            return ValidationResult.INVALID;
        }

        return ValidationResult.VALID;
    }
}