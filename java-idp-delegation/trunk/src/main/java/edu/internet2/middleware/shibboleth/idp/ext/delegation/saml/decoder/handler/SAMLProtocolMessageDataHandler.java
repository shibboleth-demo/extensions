/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.decoder.handler;

import org.opensaml.common.SAMLObject;
import org.opensaml.common.binding.SAMLMessageContext;
import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.MessageContextEvaluatingFunctor;
import org.opensaml.ws.message.MessageException;
import org.opensaml.ws.message.decoder.MessageDecodingException;
import org.opensaml.ws.message.handler.Handler;
import org.opensaml.ws.message.handler.HandlerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.decoder.SAMLDecoderHelper;

/**
 * Handler which populates data from the inbound SAML protocol message on a
 * {@link SAMLMessageContext}.
 */
public class SAMLProtocolMessageDataHandler implements Handler {
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(SAMLProtocolMessageDataHandler.class);
    
    /** Functor which produces the SAML protocol message. */
    private MessageContextEvaluatingFunctor<SAMLObject> protocolMessageSource;
    
    /**
     * Set the functor which produces the SAML protocol message from the message context.
     * 
     * @param functor the new message context functor
     */
    public void setProtocolMessageSource(MessageContextEvaluatingFunctor<SAMLObject> functor) {
        protocolMessageSource = functor;
    }

    /** {@inheritDoc} */
    public void invoke(MessageContext msgContext) throws HandlerException {
        if (! (msgContext instanceof SAMLMessageContext)) {
            log.error("Invalid message context type, this handler only supports SAMLMessageContext");
            throw new HandlerException("Invalid message context type, this handler only support SAMLMessageContext");
        }
        
        SAMLMessageContext samlMsgCtx = (SAMLMessageContext) msgContext;
        
        SAMLObject inboundMessage = getSAMLProtocolMessage(msgContext);
        samlMsgCtx.setInboundSAMLMessage(inboundMessage);

        populateMessageContext(samlMsgCtx);
    }

    /**
     * Get the SAML protocol message.
     * 
     * @param msgContext the current message context
     * @return the SAML protocol message
     * 
     * @throws HandlerException if the SAML protocol message can not be obtained
     *              from the message context
     */
    protected SAMLObject getSAMLProtocolMessage(MessageContext msgContext) throws HandlerException {
        if (protocolMessageSource != null) {
            try {
                return protocolMessageSource.evaluate(msgContext);
            } catch (MessageException e) {
                throw new HandlerException("Error extracting SAML protocol message", e);
            }
        } else {
            throw new HandlerException("SAML protocol message evaluating functor was null");
        }
    }
    
    /**
     * Populate the SAML message context with data from the SAML protocol message.
     * 
     * @param messageContext the current message context
     * 
     * @throws HandlerException thrown if there is a problem populating the message context
     */
    protected void populateMessageContext(SAMLMessageContext messageContext) throws HandlerException {
        try {
            SAMLDecoderHelper.populateMessageContext(messageContext);
        } catch (MessageDecodingException e) {
            throw new HandlerException("Error populating SAML message context", e);
        }
    }
    


}
