/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.authn;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Set;

import net.jcip.annotations.ThreadSafe;

import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.saml2.core.SubjectConfirmationData;
import org.opensaml.xml.util.DatatypeHelper;
import org.opensaml.xml.validation.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A base class for {@link SubjectConfirmationValidator} implementations. This class takes care of processing the the
 * <code>NotBefore</code>, <code>NotOnOrAfter</code>, <code>Recipient</code>, and <code>Address</code> checks.
 * 
 * This validator expects the {@link ValidationContext#getStaticParameters()} to contain the properties
 * {@link #VALID_ADDRESSES_PARAM} and {@link #VALID_RECIPIENTS_PARAM} to be present during evaluation.
 * 
 * This validator does not populate any parameters in the {@link ValidationContext#getDynamicParameters()}.
 */
@ThreadSafe
public abstract class AbstractSubjectConfirmationValidator implements SubjectConfirmationValidator {

    /**
     * The name of the {@link ValidationContext#getStaticParameters()} carrying a {@link Set<String>} whose values are
     * the acceptable subject confirmation data recipient endpoints.
     */
    public static final String VALID_RECIPIENTS_PARAM = AbstractSubjectConfirmationValidator.class.getName()
            + ".ValidRecipients";

    /**
     * The name of the {@link ValidationContext#getStaticParameters()} carrying a {@link Set<InetAddress>} whose values
     * are the acceptable subject confirmation data addresses.
     */
    public static final String VALID_ADDRESSES_PARAM = AbstractSubjectConfirmationValidator.class.getName()
            + ".ValidAddresses";

    /** Class logger. */
    private Logger log = LoggerFactory.getLogger(AbstractSubjectConfirmationValidator.class);

    /** Constructor. */
    public AbstractSubjectConfirmationValidator() {
    }

    /** {@inheritDoc} */
    public ValidationResult validate(SubjectConfirmation confirmation, Assertion assertion, ValidationContext context)
            throws ValidationException {

        if (confirmation.getSubjectConfirmationData() != null) {
            ValidationResult result = validateNotBefore(confirmation, assertion, context);
            if (result != ValidationResult.VALID) {
                return result;
            }

            result = validateNotOnOrAfter(confirmation, assertion, context);
            if (result != ValidationResult.VALID) {
                return result;
            }

            result = validateRecipient(confirmation, assertion, context);
            if (result != ValidationResult.VALID) {
                return result;
            }

            result = validateAddress(confirmation, assertion, context);
            if (result != ValidationResult.VALID) {
                return result;
            }
        }

        return doValidate(confirmation, assertion, context);
    }

    /**
     * Validates the <code>NotBefore</code> condition of the {@link SubjectConfirmationData}, if any is present.
     * 
     * @param confirmation confirmation method, with {@link SubjectConfirmationData}, being validated
     * @param assertion assertion bearing the confirmation method
     * @param context current validation context
     * 
     * @return the result of the validation evaluation
     */
    protected ValidationResult validateNotBefore(SubjectConfirmation confirmation, Assertion assertion,
            ValidationContext context) {
        DateTime skewedNow = new DateTime(ISOChronology.getInstanceUTC()).plus(SAML20AssertionTokenValidator
                .getClockSkew(context));
        DateTime notBefore = confirmation.getSubjectConfirmationData().getNotBefore();
        
        log.debug("Evaluating SubjectConfirmationData NotBefore '{}' against 'skewed now' time '{}'",
                notBefore, skewedNow);
        if (notBefore != null && notBefore.isAfter(skewedNow)) {
            context.setValidationFailureMessage(String.format(
                    "Subject confirmation, in assertion '%s', with NotBefore condition of '%s' is not yet valid",
                    assertion.getID(), notBefore));
            return ValidationResult.INVALID;
        }

        return ValidationResult.VALID;
    }

    /**
     * Validates the <code>NotOnOrAfter</code> condition of the {@link SubjectConfirmationData}, if any is present.
     * 
     * @param confirmation confirmation method, with {@link SubjectConfirmationData}, being validated
     * @param assertion assertion bearing the confirmation method
     * @param context current validation context
     * 
     * @return the result of the validation evaluation
     */
    protected ValidationResult validateNotOnOrAfter(SubjectConfirmation confirmation, Assertion assertion,
            ValidationContext context) {
        DateTime skewedNow = new DateTime(ISOChronology.getInstanceUTC()).minus(SAML20AssertionTokenValidator
                .getClockSkew(context));
        DateTime notOnOrAfter = confirmation.getSubjectConfirmationData().getNotOnOrAfter();
        
        log.debug("Evaluating SubjectConfirmationData NotOnOrAfter '{}' against 'skewed now' time '{}'",
                notOnOrAfter, skewedNow);
        if (notOnOrAfter != null && notOnOrAfter.isBefore(skewedNow)) {
            context.setValidationFailureMessage(String.format(
                    "Subject confirmation, in assertion '%s', with NotOnOrAfter condition of '%s' is no longer valid",
                    assertion.getID(), notOnOrAfter));
            return ValidationResult.INVALID;
        }

        return ValidationResult.VALID;
    }

    /**
     * Validates the <code>Recipient</code> condition of the {@link SubjectConfirmationData}, if any is present.
     * 
     * @param confirmation confirmation method being validated
     * @param assertion assertion bearing the confirmation method
     * @param context current validation context
     * 
     * @return the result of the validation evaluation
     */
    protected ValidationResult validateRecipient(SubjectConfirmation confirmation, Assertion assertion,
            ValidationContext context) {
        String recipient = DatatypeHelper
                .safeTrimOrNullString(confirmation.getSubjectConfirmationData().getRecipient());
        if (recipient == null) {
            return ValidationResult.VALID;
        }
        
        log.debug("Evaluating SubjectConfirmationData@Recipient of : {}", recipient);

        Set<String> validRecipients;
        try {
            validRecipients = (Set<String>) context.getStaticParameters().get(VALID_RECIPIENTS_PARAM);
        } catch (ClassCastException e) {
            log.warn("The value of the static validation parameter '{}' was not java.util.Set<String>",
                    VALID_RECIPIENTS_PARAM);
            context.setValidationFailureMessage(
                    "Unable to determine list of valid subject confirmation recipient endpoints");
            return ValidationResult.INDETERMINATE;
        }
        if (validRecipients == null || validRecipients.isEmpty()) {
            log.warn("Set of valid recipient URI's was not available from the validation context, " 
                    + "unable to evaluate SubjectConfirmationData@Recipient");
            context.setValidationFailureMessage(
                    "Unable to determine list of valid subject confirmation recipient endpoints");
            return ValidationResult.INDETERMINATE;
        }
        
        

        if (validRecipients.contains(recipient)) {
            log.debug("Matched valid recipient: {}", recipient);
            return ValidationResult.VALID;
        }
        
        log.debug("Failed to match SubjectConfirmationData@Recipient to any supplied valid recipients: {}",
                validRecipients);

        context.setValidationFailureMessage(String.format(
                "Subject confirmation recipient for asertion '%s' did not match any valid recipients", assertion
                        .getID()));
        return ValidationResult.INVALID;
    }

    /**
     * Validates the <code>Address</code> condition of the {@link SubjectConfirmationData}, if any is present.
     * 
     * @param confirmation confirmation method being validated
     * @param assertion assertion bearing the confirmation method
     * @param context current validation context
     * 
     * @return the result of the validation evaluation
     * 
     * @throws ValidationException thrown if address of assertion sender does is not the address listed in the subject
     *             confirmation data
     */
    protected ValidationResult validateAddress(SubjectConfirmation confirmation, Assertion assertion,
            ValidationContext context) throws ValidationException {
        String address = DatatypeHelper.safeTrimOrNullString(confirmation.getSubjectConfirmationData().getAddress());
        if (address == null) {
            return ValidationResult.VALID;
        }
        
        log.debug("Evaluating SubjectConfirmationData@Address of : {}", address);

        InetAddress[] confirmingAddresses;
        try {
            confirmingAddresses = InetAddress.getAllByName(address);
        } catch (UnknownHostException e) {
            log.warn("The subject confirmation address '{}' in assetion '{}' can not be resolved " 
                    + "to a valid set of IP address(s)", address, assertion.getID());
            context.setValidationFailureMessage(String.format(
                    "Subject confirmation address '%s' is not resolvable hostname or IP address", address));
            return ValidationResult.INDETERMINATE;
        }
        
        if (log.isDebugEnabled()) {
            log.debug("SubjectConfirmationData/@Address was resolved to addresses: {}",
                    Arrays.asList(confirmingAddresses));
        }

        Set<InetAddress> validAddresses;
        try {
            validAddresses = (Set<InetAddress>) context.getStaticParameters().get(VALID_ADDRESSES_PARAM);
        } catch (ClassCastException e) {
            log.warn("The value of the static validation parameter '{}' was not java.util.Set<InetAddress>",
                    VALID_ADDRESSES_PARAM);
            context.setValidationFailureMessage("Unable to determine list of valid subject confirmation addresses");
            return ValidationResult.INDETERMINATE;
        }
        if (validAddresses == null || validAddresses.isEmpty()) {
            log.warn("Set of valid addresses was not available from the validation context, " 
                    + "unable to evaluate SubjectConfirmationData@Address");
            context.setValidationFailureMessage("Unable to determine list of valid subject confirmation addresses");
            return ValidationResult.INDETERMINATE;
        }

        for (InetAddress confirmingAddress : confirmingAddresses) {
            if (validAddresses.contains(confirmingAddress)) {
                log.debug("Matched SubjectConfirmationData address '{}' to valid address",
                        confirmingAddress.getHostAddress());
                return ValidationResult.VALID;
            }
        }
        
        log.debug("Failed to match SubjectConfirmationData@Address to any supplied valid addresses", validAddresses);

        context.setValidationFailureMessage(String.format(
                "Subject confirmation address for asertion '%s' did not match any valid addresses", assertion
                        .getID()));
        return ValidationResult.INVALID;
    }

    /**
     * Performs any further validation required for the specific confirmation method implementation.
     * 
     * @param confirmation confirmation method being validated
     * @param assertion assertion bearing the confirmation method
     * @param context current validation context
     * 
     * @return the result of the validation evaluation
     * 
     * @throws ValidationException thrown if further validation finds the confirmation method to be invalid
     */
    protected abstract ValidationResult doValidate(SubjectConfirmation confirmation, Assertion assertion,
            ValidationContext context) throws ValidationException;
}