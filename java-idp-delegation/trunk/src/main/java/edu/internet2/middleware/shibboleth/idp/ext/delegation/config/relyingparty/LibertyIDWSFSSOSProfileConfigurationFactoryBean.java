/*
 * Copyright 2007 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.config.relyingparty;

import java.util.Set;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.relyingparty.LibertyIDWSFSSOSConfiguration;

/** Spring factory for Liberty ID-WSF SSOS profile configurations. */
public class LibertyIDWSFSSOSProfileConfigurationFactoryBean 
        extends DelegationAwareSAML2SSOProfileConfigurationFactoryBean {
    
    /** maximumTokenDelegationChainLength policy value. */
    private Integer maximumTokenDelegationChainLength;
    
    /** The set of allowed delegates. */
    private Set<String> allowedDelegates;

    /** {@inheritDoc} */
    public Class getObjectType() {
        return LibertyIDWSFSSOSConfiguration.class;
    }
    
    /**
     * Get the maximumTokenDelegationChainLength policy value.
     * 
     * @return the policy value
     */
    public Integer getMaximumTokenDelegationChainLength() {
        return maximumTokenDelegationChainLength;
    }
    
    /**
     * Set the maximumTokenDelegationChainLength policy value.
     * 
     * @param newValue the new policy value
     */
    public void setMaximumTokenDelegationChainLength(Integer newValue) {
        maximumTokenDelegationChainLength = newValue;
    }
    
    /**
     * Get the set of allowed delegates.
     * @return the set of allowed delegates
     */
    public Set<String> getAllowedDelegates() {
        return allowedDelegates;
    }
    
    /**
     * Get the set of allowed delegates.
     * @param newSet the new set of allowed delegates
     */
    public void setAllowedDelegates(Set<String> newSet) {
        allowedDelegates = newSet;
    }


    /** {@inheritDoc} */
    protected Object createInstance() throws Exception {
        LibertyIDWSFSSOSConfiguration configuration = (LibertyIDWSFSSOSConfiguration) super.createInstance();
        
        configuration.setMaximumTokenDelegationChainLength(getMaximumTokenDelegationChainLength());
        configuration.setAllowedDelegates(getAllowedDelegates());

        return configuration;
    }
}