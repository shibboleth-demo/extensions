/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.soap.decoder.handler;

import java.util.List;

import org.openliberty.xmltooling.soapbinding.Framework;
import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.handler.Handler;
import org.opensaml.ws.message.handler.HandlerException;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.util.DatatypeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.soap.SOAPProcessingHelper;

/**
 * Handler implementation that handles sbf:Framework header on the inbound SOAP envelope.
 * 
 * Framework version, if present, must be "2.0".
 */
public class FrameworkHandler implements Handler {
    
    /** Supported Framework version. */
    public static final String SUPPORTED_VERSION = "2.0";
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(FrameworkHandler.class);

    /** {@inheritDoc} */
    public void invoke(MessageContext msgContext) throws HandlerException {
        log.debug("Handling message Liberty ID-WSF Framework");
        Framework framework = getFramework(msgContext);
        if (framework != null) {
            String version = DatatypeHelper.safeTrimOrNullString(framework.getVersion());
            if (version != null) {
                if (!SUPPORTED_VERSION.equals(version)) {
                    String msg =
                        String.format("Liberty ID-WSF Framwork indicated version '%s', which is unsupported",
                                version);
                    log.warn(msg);
                    throw new HandlerException(msg);
                }
            }
        }
    }
    
    /**
     * Get message Framework header block.
     * 
     * @param msgContext the current message context
     * @return the message Framework element
     */
    protected Framework getFramework(MessageContext msgContext) {
        List<XMLObject> frameworks = SOAPProcessingHelper.getInboundHeaderBlock(msgContext, 
                Framework.DEFAULT_ELEMENT_NAME);
        if (frameworks != null && !frameworks.isEmpty()) {
            return (Framework) frameworks.get(0);
        }
        return null; 
    }

}
