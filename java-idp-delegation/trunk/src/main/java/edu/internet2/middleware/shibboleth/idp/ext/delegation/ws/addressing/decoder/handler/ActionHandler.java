/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.addressing.decoder.handler;

import java.util.List;

import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.MessageContextEvaluatingFunctor;
import org.opensaml.ws.message.MessageException;
import org.opensaml.ws.message.handler.Handler;
import org.opensaml.ws.message.handler.HandlerException;
import org.opensaml.ws.wsaddressing.Action;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.util.DatatypeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.soap.SOAPProcessingHelper;

/**
 * Handler implementation that checks an wsa:Action header against an expected value.
 */
public class ActionHandler implements Handler {
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(ActionHandler.class);
    
    /** Message context functor which produces the expected Action value. */
    private MessageContextEvaluatingFunctor<String> actionValueSource;
    
    /**
     * Set the functor which produces the expected Action value from the message context.
     * 
     * @param functor the new message context functor
     */
    public void setActionValueSource(MessageContextEvaluatingFunctor<String> functor) {
        actionValueSource = functor;
    }

    /** {@inheritDoc} */
    public void invoke(MessageContext msgContext) throws HandlerException {
        log.debug("Checking inbound message WS-Addressing Action URI value");
        String expectedValue = getExpectedActionValue(msgContext);
        if (expectedValue != null) {
            String messageValue = getMessageActionValue(msgContext);
            if (messageValue != null) {
                log.debug("Inbound message contains WS-Addressing Action URI: {}", messageValue);
            } else {
                log.debug("Inbound message contained no WS-Addressing Action URI value");
            }
            if (!expectedValue.equals(messageValue)) {
                String msg  =
                    String.format("Message WS-Addressing Action URI '%s' did not match expected value '%s'",
                        messageValue, expectedValue);
                log.warn(msg);
                throw new HandlerException(msg);
            }
        } else {
            log.info("No expected WS-Addressing Action value available from message context, skipping action check");
        }
    }

    /**
     * Get message Action URI value.
     * 
     * @param msgContext the current message context
     * @return the message Action URI value
     */
    protected String getMessageActionValue(MessageContext msgContext) {
        List<XMLObject> actions = SOAPProcessingHelper.getInboundHeaderBlock(msgContext, Action.ELEMENT_NAME);
        if (actions != null && !actions.isEmpty()) {
            return DatatypeHelper.safeTrimOrNullString(((Action)actions.get(0)).getValue());
        }
        return null; 
    }

    /**
     * Get the expected value of the Action.
     * 
     * @param msgContext the current message context
     * @return the Action value.
     * 
     * @throws HandlerException if there is a problem obtaining the action value from the context
     */
    protected String getExpectedActionValue(MessageContext msgContext) throws HandlerException {
        if (actionValueSource != null) {
            try {
                return actionValueSource.evaluate(msgContext);
            } catch (MessageException e) {
                throw new HandlerException("Error obtaining Action value from the context", e);
            }
        }
        return null;
    }

}
