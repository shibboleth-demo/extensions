/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.addressing;

import org.opensaml.ws.message.MessageContext;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.message.BaseSubContext;

/**
 * Sub context implementation related to WS-Addressing processing state.
 */
public class WSAddressingContext extends BaseSubContext {
    
    /** The subcontext identifier. */
    public static final String CONTEXT_ID = WSAddressingContext.class.getName();

    //TODO implement support for remaining items of WS-Addressing data model
    
    /** The inbound Action URI value. */
    private String inboundActionURI;
    
    /** The outbound Action URI value. */
    private String outboundActionURI;
    
    /** The inbound MessageID URI value. */
    private String inboundMessageIDURI;
    
    /** The outbound MessageID URI value. */
    private String outboundMessageIDURI;
    
    /**
     * Constructor.
     *
     * @param owningContext the owning message context
     */
    public WSAddressingContext(MessageContext owningContext) {
        super(owningContext);
    }
    
    /**
     * Get the inbound Action URI value.
     * @return Returns the inboundActionURI.
     */
    public String getInboundActionURI() {
        return inboundActionURI;
    }

    /**
     * Set the inbound Action URI value.
     * @param newInboundActionURI The inboundActionURI to set.
     */
    public void setInboundActionURI(String newInboundActionURI) {
        inboundActionURI = newInboundActionURI;
    }

    /**
     * Get the outbound Action URI value.
     * @return Returns the outboundActionURI.
     */
    public String getOutboundActionURI() {
        return outboundActionURI;
    }

    /**
     * Set the outbound Action URI value.
     * @param newOutboundActionURI The outboundActionURI to set.
     */
    public void setOutboundActionURI(String newOutboundActionURI) {
        outboundActionURI = newOutboundActionURI;
    }

    /**
     * Get the inbound MessageID URI value.
     * @return Returns the inboundMessageIDURI.
     */
    public String getInboundMessageIDURI() {
        return inboundMessageIDURI;
    }

    /**
     * Set the inbound MessageID URI value.
     * @param newInboundMessageIDURI The inboundMessageIDURI to set.
     */
    public void setInboundMessageIDURI(String newInboundMessageIDURI) {
        inboundMessageIDURI = newInboundMessageIDURI;
    }

    /**
     * Get the outbound MessageID URI value.
     * @return Returns the outboundMessageIDURI.
     */
    public String getOutboundMessageIDURI() {
        return outboundMessageIDURI;
    }

    /**
     * Set the outbound MessageID URI value.
     * @param newOutboundMessageIDURI The outboundMessageIDURI to set.
     */
    public void setOutboundMessageIDURI(String newOutboundMessageIDURI) {
        outboundMessageIDURI = newOutboundMessageIDURI;
    }

}
