/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.soap.decoder.securitypolicy;

import java.util.List;

import org.openliberty.xmltooling.soapbinding.Sender;
import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.security.SecurityPolicyException;
import org.opensaml.ws.security.provider.CertificateNameOptions;
import org.opensaml.ws.soap.soap11.FaultCode;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.security.trust.TrustEngine;
import org.opensaml.xml.security.x509.X509Credential;
import org.opensaml.xml.util.DatatypeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.common.binding.security.ShibbolethClientCertAuthRule;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.LibertyConstants;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.profile.LibertyIDWSFSSOSProfileHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.soap.SOAPProcessingHelper;

/**
 * Extension of {@link ShibbolethClientCertAuthRule}
 * that overrides the selection of the client TLS certificate presenter to be the 
 * value exposed by
 * {@link LibertyIDWSFSSOSProfileHandler.LibertySSOSRequestContext#getPresenterEntityId()}.
 */
public class ClientCertAuthRule extends ShibbolethClientCertAuthRule {
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(ClientCertAuthRule.class);

    /**
     * Constructor.
     *
     * @param engine Trust engine used to verify the request X509Credential
     * @param nameOptions options for deriving certificate presenter entity ID's from an X.509 certificate
     */
    public ClientCertAuthRule(TrustEngine<X509Credential> engine, CertificateNameOptions nameOptions) {
        super(engine, nameOptions);
    }

    /** {@inheritDoc} */
    public void evaluate(MessageContext messageContext) throws SecurityPolicyException {
        try {
            super.evaluate(messageContext);
        } catch (SecurityPolicyException e) {
            SOAPProcessingHelper.registerFault(messageContext, FaultCode.CLIENT, 
                    "Client TLS certificate failed trust evaluation", null, null, null);
            throw e;
        }
    }

    /** {@inheritDoc} */
    protected String getCertificatePresenterEntityID(MessageContext messageContext) {
        log.debug("Resolving certificate presenter entity ID as Liberty SOAP Sender header providerId");
        Sender sender = getSender(messageContext);
        if (sender != null) {
            String value = DatatypeHelper.safeTrimOrNullString(sender.getProviderID());
            log.debug("Found Liberty SOAP Sender header providerId value of: {}", value);
            return value;
        } else {
            log.debug("Could not find Liberty SOAP Sender header, returning null");
            return null;
        }
    }

    /** {@inheritDoc} */
    protected void setAuthenticatedCertificatePresenterEntityID(MessageContext messageContext, String entityID) {
        log.warn("Setting of derived certificate presenter entity ID is unsupported");
    }
    
    /**
     * Get message Sender header block.
     * 
     * @param msgContext the current message context
     * @return the message Sender value
     */
    protected Sender getSender(MessageContext msgContext) {
        List<XMLObject> senders = SOAPProcessingHelper.getInboundHeaderBlock(msgContext,
                LibertyConstants.SOAP_BINDING_SENDER_ELEMENT_NAME);
        if (senders != null && !senders.isEmpty()) {
            return (Sender) senders.get(0);
        }
        return null; 
    }

}
