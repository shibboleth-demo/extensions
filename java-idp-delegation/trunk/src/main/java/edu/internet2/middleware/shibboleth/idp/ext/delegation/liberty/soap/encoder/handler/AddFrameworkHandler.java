/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.soap.encoder.handler;

import org.openliberty.xmltooling.soapbinding.Framework;
import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.MessageContextEvaluatingFunctor;
import org.opensaml.ws.message.MessageException;
import org.opensaml.ws.message.handler.Handler;
import org.opensaml.ws.message.handler.HandlerException;
import org.opensaml.ws.soap.util.SOAPHelper;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.XMLObjectBuilder;

/**
 * Handler implementation that adds a Liberty sbf:Framework header to the outbound SOAP envelope.
 */
public class AddFrameworkHandler implements Handler {
    
    /** Default framework version. */
    private static final String DEFAULT_VERSION = "2.0";

    /** Builder of Framework object. */
    private XMLObjectBuilder<Framework> frameworkBuilder;
    
    /** Message context functor which produces the Framework version attribute value. */
    private MessageContextEvaluatingFunctor<String> versionSource;

    /** Constructor. */
    @SuppressWarnings("unchecked")
    public AddFrameworkHandler() {
        frameworkBuilder = (XMLObjectBuilder<Framework>) Configuration.getBuilderFactory()
            .getBuilder(Framework.DEFAULT_ELEMENT_NAME);
    }
    
    /**
     * Set the functor which produces the framework version from the message context.
     * 
     * @param functor the new message context functor
     */
    public void setVersionSource(MessageContextEvaluatingFunctor<String> functor) {
        versionSource = functor;
    }

    /** {@inheritDoc} */
    public void invoke(MessageContext msgContext) throws HandlerException {
        String version = getVersionValue(msgContext);
        if (version != null) {
            Framework framework = frameworkBuilder.buildObject(Framework.DEFAULT_ELEMENT_NAME);
            framework.setVersion(version);
            
            SOAPHelper.addHeaderBlock(msgContext, framework);
        }
    }

    /**
     * Get the value of the Framework version attribute value.
     * 
     * @param msgContext the current message context
     * @return the Framework version attribute value.
     * 
     * @throws HandlerException if version can not be determined
     */
    protected String getVersionValue(MessageContext msgContext) throws HandlerException {
        if (versionSource != null) {
            try {
                return versionSource.evaluate(msgContext);
            } catch (MessageException e) {
                throw new HandlerException("Error obtaining the framework version", e);
            }
        }
        return DEFAULT_VERSION;
    }

}
