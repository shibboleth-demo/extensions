/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.addressing.decoder.handler;

import java.util.List;

import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.MessageContextMutatingFunctor;
import org.opensaml.ws.message.MessageException;
import org.opensaml.ws.message.handler.Handler;
import org.opensaml.ws.message.handler.HandlerException;
import org.opensaml.ws.wsaddressing.MessageID;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.util.DatatypeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.soap.SOAPProcessingHelper;

/**
 * Handler implementation that handles wsa:MessageID header on the inbound SOAP envelope.
 */
public class MessageIDHandler implements Handler {
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(MessageIDHandler.class);
    
    /** Message context functor which handles processing/mutating the message context
     * based on the MessageID value. */
    private MessageContextMutatingFunctor<String> messageIDValueHandler;
    
    /**
     * Set the functor which handles processing the MessageID value.
     * 
     * @param functor the new message context functor
     */
    public void setMessageIDValueHandler(MessageContextMutatingFunctor<String> functor) {
        messageIDValueHandler = functor;
    }

    /** {@inheritDoc} */
    public void invoke(MessageContext msgContext) throws HandlerException {
        log.debug("Handling message WS-Addressing MessageID");
        String messageID = getMessageIDValue(msgContext);
        if (messageID != null) {
            log.debug("Message contained WS-Addressing MessageID: {}", messageID);
            handleValue(msgContext, messageID);
        } else {
            log.debug("Message contained no WS-Addressing MessageID");
        }
    }

    /**
     * Handle the message ID value.
     * @param msgContext the current message context
     * @param messageID the message ID value
     * @throws HandlerException if there is a problem handling the message value
     */
    protected void handleValue(MessageContext msgContext, String messageID) throws HandlerException {
        if (messageIDValueHandler != null) {
          try {
            messageIDValueHandler.mutate(msgContext, messageID);
          } catch (MessageException e) {
              throw new HandlerException("Error handling MessageID value", e);
          }
        }
    }

    /**
     * Get message MessageID URI value.
     * 
     * @param msgContext the current message context
     * @return the message MessageID URI value
     */
    protected String getMessageIDValue(MessageContext msgContext) {
        List<XMLObject> messageIDs = SOAPProcessingHelper.getInboundHeaderBlock(msgContext, MessageID.ELEMENT_NAME);
        if (messageIDs != null && !messageIDs.isEmpty()) {
            return DatatypeHelper.safeTrimOrNullString(((MessageID)messageIDs.get(0)).getValue());
        }
        return null; 
    }

}
