/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.message;

/**
 * Interface for components that act as container of {@link SubContext} instances.
 */
public interface SubContextContainer {
    
    /**
     * Get the subcontext.
     * @param contextID the context ID
     * @return the context retrieved by context ID, or null if not present
     */
    public SubContext getContext(String contextID);
    
    /**
     * Store the subcontext.
     * @param contextID the context ID
     * @param context the context to store
     */
    public void setContext(String contextID, SubContext context);

}
