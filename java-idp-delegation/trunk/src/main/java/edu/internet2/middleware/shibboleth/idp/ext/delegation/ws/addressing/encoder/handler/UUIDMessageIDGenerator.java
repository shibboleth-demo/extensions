package edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.addressing.encoder.handler;

import java.util.UUID;

import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.MessageContextEvaluatingFunctor;

/**
 * This implementation generates a unique message ID value using {@link UUID#randomUUID()}
 * and returns it in the form of a UUID URN.
 */
public class UUIDMessageIDGenerator implements MessageContextEvaluatingFunctor<String> {

    /** {@inheritDoc} */
    public String evaluate(MessageContext msgContext) {
        return "urn:uuid:" + UUID.randomUUID().toString();
    }
    
}