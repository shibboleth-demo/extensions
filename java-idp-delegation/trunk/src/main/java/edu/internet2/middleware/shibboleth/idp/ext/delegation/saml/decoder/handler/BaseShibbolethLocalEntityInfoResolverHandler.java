/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.decoder.handler;

import org.opensaml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml2.metadata.provider.MetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.MessageContextEvaluatingFunctor;
import org.opensaml.ws.message.MessageException;
import org.opensaml.ws.message.handler.Handler;
import org.opensaml.ws.message.handler.HandlerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.common.relyingparty.RelyingPartyConfiguration;
import edu.internet2.middleware.shibboleth.common.relyingparty.RelyingPartyConfigurationManager;

/**
 * Base class for handlers which need to resolve entity ID information based
 * on a {@link RelyingPartyConfigurationManager} and {@link MetadataProvider}.
 */
public abstract class BaseShibbolethLocalEntityInfoResolverHandler implements Handler {
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(BaseShibbolethLocalEntityInfoResolverHandler.class);
    
    /** The relying party configuration manager. */
    private RelyingPartyConfigurationManager relyingPartyManager;
    
    /** Metadata provider. */
    private MetadataProvider metadataProvider;
    
    /** Message context functor which produces the relying party ID. */
    private MessageContextEvaluatingFunctor<String> relyingPartyIDSource;

    /**
     * Get the relying party manager.
     * @return Returns the relyingPartyManager.
     */
    public RelyingPartyConfigurationManager getRelyingPartyManager() {
        return relyingPartyManager;
    }

    /**
     * Set the relying party manager.
     * @param newRelyingPartyManager The relyingPartyManager to set.
     */
    public void setRelyingPartyManager(RelyingPartyConfigurationManager newRelyingPartyManager) {
        relyingPartyManager = newRelyingPartyManager;
    }

    /**
     * Get the metadata provider.
     * @return Returns the metadataProvider.
     */
    public MetadataProvider getMetadataProvider() {
        return metadataProvider;
    }

    /**
     * Set the metadata provider.
     * @param newMetadataProvider The metadataProvider to set.
     */
    public void setMetadataProvider(MetadataProvider newMetadataProvider) {
        metadataProvider = newMetadataProvider;
    }

    /**
     * Set the functor used to obtain the relying party entity ID.
     * @param functor The relyingPartyIDSource to set.
     */
    public void setRelyingPartyIDSource(MessageContextEvaluatingFunctor<String> functor) {
        relyingPartyIDSource = functor;
    }

    /**
     * Resolve the local entity ID based on the dynamically-determined relying party ID.
     * @param msgContext the current message context
     * @return the local entity ID relative to the resolved relying party
     * @throws HandlerException if there was a problem resolving the value
     */
    protected String resolveLocalEntityId(MessageContext msgContext) throws HandlerException {
        String relyingPartyID = resolveRelyingPartyID(msgContext);
        if (relyingPartyID != null) {
            if (relyingPartyManager == null) {
                throw new IllegalStateException("Relying party manager was null, but is required");
            }
            RelyingPartyConfiguration rpConfig = relyingPartyManager.getRelyingPartyConfiguration(relyingPartyID);
            if (rpConfig == null) {
                log.error("Unable to retrieve relying party configuration data for entity ID: {}",
                        relyingPartyID);
                return null;
            }
            return rpConfig.getProviderId();
        } else {
            log.warn("Relying party entity ID could not be resolved, " 
                    + "and could therefore not resolve local entity ID based upon it");
        }
        
        return null;
    }

    /**
     * Resolve the relying party entity ID using the functor supplied via
     * {@link #setRelyingPartyIDSource(MessageContextEvaluatingFunctor).
     * @param msgContext the current message context
     * @return the relying party entity ID, or null if no functor was supplied or the relying party ID
     *              could not be resolved
     * @throws HandlerException if there was a problem resolving the value
     */
    protected String resolveRelyingPartyID(MessageContext msgContext) throws HandlerException {
        if (relyingPartyIDSource != null) {
            try {
                return relyingPartyIDSource.evaluate(msgContext);
            } catch (MessageException e) {
                throw new HandlerException("Error retrieving relying party ID value", e);
            }
        }
        return null;
    }
    
    /**
     * Resolve the metadata for the specified entity ID.
     * @param entityID the entity ID whose metadata is to be resolved
     * @return the entity ID's metadata
     * @throws HandlerException if there was a problem resolving the value
     */
    protected EntityDescriptor resolveEntityMetadata(String entityID) throws HandlerException {
        if (entityID == null) {
            log.warn("Entity ID was null, unable to resolve metadata");
            return null;
        }
        if (metadataProvider == null) {
            log.warn("Metadata provider was null, unable to resolve metadata for entity ID: {}",
                    entityID);
            return null;
        }
        
        EntityDescriptor entityDescriptor = null;
        try {
            entityDescriptor = metadataProvider.getEntityDescriptor(entityID);
        } catch (MetadataProviderException e) {
            throw new HandlerException(
                    String.format("Error looking up entity descriptor for entity ID: %s", entityID), e);
        }
        if (entityDescriptor == null) {
            log.warn("Could not resolve metadata for local entity ID: {}", entityID);
        }
        return entityDescriptor;
    }

}
