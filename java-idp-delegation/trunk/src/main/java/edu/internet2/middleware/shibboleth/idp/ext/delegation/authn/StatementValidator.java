/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.authn;

import javax.xml.namespace.QName;

import net.jcip.annotations.ThreadSafe;

import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Statement;
import org.opensaml.xml.validation.ValidationException;

/** Validator used to validate {@link Statement}s within a given {@link Assertion}. */
@ThreadSafe
public interface StatementValidator {

    /**
     * Gets the element or schema type QName of the statement handled by this validator.
     * 
     * @return element or schema type QName of the statement handled by this validator
     */
    public QName getServicedStatement();

    /**
     * Validates the given statement.
     * 
     * @param statement statement to be validated
     * @param assertion assertion bearing the statement
     * @param context current Assertion validation context
     * 
     * @return the validation result
     * 
     * @throws ValidationException if there is a problem processing the validation operation
     */
    public ValidationResult validate(Statement statement, Assertion assertion, ValidationContext context)
            throws ValidationException;
}