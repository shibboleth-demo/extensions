/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.decoder.handler;

import java.util.List;

import org.opensaml.common.SAMLObject;
import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.MessageContextEvaluatingFunctor;
import org.opensaml.ws.message.MessageException;
import org.opensaml.ws.soap.soap11.Envelope;
import org.opensaml.xml.XMLObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Functor which extracts a SAML protocol message from a SOAP 1.1 Envelope Body.
 */
public class SOAP11BodySAMLMessageExtractor implements MessageContextEvaluatingFunctor<SAMLObject> {
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(SOAP11BodySAMLMessageExtractor.class);

    /** {@inheritDoc} */
    public SAMLObject evaluate(MessageContext msgContext) throws MessageException {
        Envelope soapMessage = (Envelope) msgContext.getInboundMessage();
        List<XMLObject> soapBodyChildren = soapMessage.getBody().getUnknownXMLObjects();
        if (soapBodyChildren.size() != 1) {
            log.error("Unexpected number of children in the SOAP body, " + soapBodyChildren.size()
                    + ".  Unable to extract SAML message");
            throw new MessageException(
                    "Unexpected number of children in the SOAP body, unable to extract SAML message");
        }

        XMLObject incommingMessage = soapBodyChildren.get(0);
        if (!(incommingMessage instanceof SAMLObject)) {
            log.error("Unexpected SOAP body content.  Expected a SAML request but recieved {}",
                    incommingMessage.getElementQName());
            throw new MessageException("Unexpected SOAP body content.  Expected a SAML request but recieved "
                    + incommingMessage.getElementQName());
        }
        
        return (SAMLObject) incommingMessage;
    }
    
}
