/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.decoder.handler;

import org.opensaml.common.binding.SAMLMessageContext;
import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.handler.Handler;
import org.opensaml.ws.message.handler.HandlerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handler implementation which resolves dynamic request-specific information
 * about the responding local provider, and populates it in the {@link SAMLMessageContext}.
 */
public class ShibbolethLocalEntityInfoResolverHandler extends BaseShibbolethLocalEntityInfoResolverHandler
        implements Handler {
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(ShibbolethLocalEntityInfoResolverHandler.class);

    /** {@inheritDoc} */
    public void invoke(MessageContext msgContext) throws HandlerException {
        if (!(msgContext instanceof SAMLMessageContext)) {
            log.warn("Message context is not a SAMLMessageContext. " 
                    + "Unable to store local entity ID information");
            return;
        }
        SAMLMessageContext samlMsgContext = (SAMLMessageContext) msgContext;
        
        String localEntityId = resolveLocalEntityId(msgContext);
        
        if (localEntityId != null) {
            log.debug("Resolved local entity ID: {}", localEntityId);
            samlMsgContext.setLocalEntityId(localEntityId);
            samlMsgContext.setLocalEntityMetadata(resolveEntityMetadata(localEntityId));
        } else {
            log.warn("Local entity ID could not be resolved. Failed to populate local entity ID information");
        }

    }

}
