/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.authn;

import net.jcip.annotations.ThreadSafe;

import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.xml.validation.ValidationException;

/**
 * Validator that confirms the {@link Subject} of the issuer by evaluating the {@link SubjectConfirmation} within that
 * subject.
 */
@ThreadSafe
public interface SubjectConfirmationValidator {

    /**
     * Gets the subject confirmation method handled by this validator.
     * 
     * @return subject confirmation method handled by this validator
     */
    public String getServicedMethod();

    /**
     * Confirms the {@link Subject} by means of the given {@link SubjectConfirmation}.
     * 
     * @param confirmation the subject confirmation information
     * @param assertion the assertion bearing the subject
     * @param context the current Assertion validation context
     * 
     * @return the validation result
     * 
     * @throws ValidationException if there is a problem processing the validation operation
     */
    public ValidationResult validate(SubjectConfirmation confirmation, Assertion assertion, ValidationContext context)
            throws ValidationException;
}