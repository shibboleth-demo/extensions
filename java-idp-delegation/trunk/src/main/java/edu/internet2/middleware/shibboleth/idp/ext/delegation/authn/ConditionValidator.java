/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.authn;

import javax.xml.namespace.QName;

import net.jcip.annotations.ThreadSafe;

import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Condition;
import org.opensaml.xml.validation.ValidationException;


/** A validator that evaluates a {@link Condition} within an {@link Assertion}. */
@ThreadSafe
public interface ConditionValidator {

    /**
     * Gets the element or schema type QName of the condition handled by this validator.
     * 
     * @return element or schema type QName of the statement handled by this validator
     */
    public QName getServicedCondition();

    /**
     * Validates the given condition.
     * 
     * @param condition condition to be evaluated
     * @param assertion assertion bearing the condition
     * @param context current Assertion validation context
     * 
     * @return the result of the condition evaluation
     * 
     * @throws ValidationException if there is a problem processing the validation operation
     */
    public ValidationResult validate(Condition condition, Assertion assertion, ValidationContext context)
            throws ValidationException;
}