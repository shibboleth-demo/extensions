/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security;

import java.util.List;

import org.opensaml.ws.message.MessageContext;
import org.opensaml.xml.util.LazyList;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.message.BaseSubContext;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security.token.Token;

/**
 * Sub context implementation related to WS-Security processing state.
 */
public class WSSecurityContext extends BaseSubContext {
    
    /** The subcontext identifier. */
    public static final String CONTEXT_ID = WSSecurityContext.class.getName();
    
    /** List of known WS-Security tokens. */
    private LazyList<Token> tokens;
 
    /**
     * Constructor.
     *
     * @param owningContext the owning message context
     */
    public WSSecurityContext(MessageContext owningContext) {
        super(owningContext);
        tokens = new LazyList<Token>();
    }
    
    /**
     * Get the list of WS-Security tokens.
     * 
     * @return the list of tokens
     */
    public List<Token> getTokens() {
        return tokens;
    }

}
