/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security.token.impl;

import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.SubjectConfirmation;

/**
 * Implementation of SAML 2.0 Assertion token.
 */
public class SAML20AssertionToken extends BaseToken<Assertion> {
    
    /** Get the successfully attested SubjectConfirmation. */
    private SubjectConfirmation subjectConfirmation;

    /**
     * Constructor.
     *
     * @param token the wrapped token
     */
    public SAML20AssertionToken(Assertion token) {
        super(token);
    }
    
    /**
     * Get the successfully attested SubjectConfirmation.
     * 
     * @return the attested SubjectConfirmation
     */
    public SubjectConfirmation getSubjectConfirmation() {
        return subjectConfirmation;
    }
    
    /**
     * Set the successfully attested SubjectConfirmation.
     * 
     * @param newSubjectConfirmation the new attested SubjectConfirmation
     */
    public void setSubjectConfirmation(SubjectConfirmation newSubjectConfirmation) {
        subjectConfirmation = newSubjectConfirmation;
    }

}
