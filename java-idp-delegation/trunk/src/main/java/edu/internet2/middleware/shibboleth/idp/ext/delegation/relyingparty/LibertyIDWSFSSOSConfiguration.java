/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.relyingparty;

import java.util.Collections;
import java.util.Set;

import org.opensaml.xml.util.LazySet;

/** Delegation-aware Liberty ID-WSF 2.0 SSOS configuration settings. */
public class LibertyIDWSFSSOSConfiguration extends DelegationAwareSSOConfiguration {
    
    /** ID for this profile configuration. */
    public static final String PROFILE_ID = "urn:mace:shibboleth:2.0:ext:delegation:profiles:liberty:ssos";
    
    /** maximumTokenDelegationChainLength policy value. */
    private Integer maximumTokenDelegationChainLength;
    
    /** The set of allowed delegates. */
    private Set<String> allowedDelegates;
    
    /** {@inheritDoc} */
    public String getProfileId() {
        return PROFILE_ID;
    }
    
    /**
     * Get the maximumTokenDelegationChainLength policy value.
     * 
     * @return the policy value
     */
    public Integer getMaximumTokenDelegationChainLength() {
        return maximumTokenDelegationChainLength;
    }
    
    /**
     * Set the maximumTokenDelegationChainLength policy value.
     * 
     * @param newValue the new policy value
     */
    public void setMaximumTokenDelegationChainLength(Integer newValue) {
        maximumTokenDelegationChainLength = newValue;
    }
    
    /**
     * Get the set of allowed delegates.
     * @return the set of allowed delegates
     */
    public Set<String> getAllowedDelegates() {
        return Collections.unmodifiableSet(allowedDelegates);
    }
    
    /**
     * Get the set of allowed delegates.
     * @param newSet the new set of allowed delegates
     */
    public void setAllowedDelegates(Set<String> newSet) {
        LazySet<String> temp = new LazySet<String>();
        if (newSet != null) {
            temp.addAll(newSet);
        }
        allowedDelegates = temp;
    }

}
