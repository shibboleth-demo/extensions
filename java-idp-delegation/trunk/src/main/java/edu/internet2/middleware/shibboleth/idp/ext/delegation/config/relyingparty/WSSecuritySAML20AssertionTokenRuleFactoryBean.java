/*
 * Copyright [2007] [University Corporation for Advanced Internet Development, Inc.]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.config.relyingparty;

import java.util.ArrayList;

import org.opensaml.xml.signature.SignatureTrustEngine;
import org.springframework.beans.factory.config.AbstractFactoryBean;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.authn.AudienceRestrictionConditionValidator;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.authn.ConditionValidator;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.authn.DelegationRestrictionConditionValidator;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.authn.HolderOfKeySubjectConfirmationValidator;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.authn.SAML20AssertionTokenValidator;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.authn.StatementValidator;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.authn.SubjectConfirmationValidator;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security.decoder.securitypolicy.WSSecuritySAML20AssertionTokenRule;

/**
 * Spring factory for SAML 2 Delegation WS-Security SAML 2.0 Assertion token rule.
 */
public class WSSecuritySAML20AssertionTokenRuleFactoryBean extends AbstractFactoryBean {
    
    /** The trust engine. */
    private SignatureTrustEngine trustEngine;
    
    /** Flag which indicates whether a failure of Assertion validation should be considered a fatal processing error. */
    private boolean invalidFatal;

    /** {@inheritDoc} */
    public Class getObjectType() {
        return WSSecuritySAML20AssertionTokenRule.class;
    }
    
    /**
     * Get the signature trust engine.
     * @return the signature trust engine
     */
    public SignatureTrustEngine getTrustEngine() {
        return trustEngine;
    }
    
    /**
     * Set the signature trust engine.
     * @param newTrustEngine the new trust engine
     */
    public void setTrustEngine(SignatureTrustEngine newTrustEngine) {
        trustEngine = newTrustEngine;
    }
    
    /**
     * Get flag which indicates whether a failure of Assertion validation should be considered a fatal processing error.
     * @return Returns the invalidFatal.
     */
    public boolean isInvalidFatal() {
        return invalidFatal;
    }

    /**
     * Set flag which indicates whether a failure of Assertion validation should be considered a fatal processing error.
     * @param newInvalidFatal The invalidFatal to set.
     */
    public void setInvalidFatal(boolean newInvalidFatal) {
        invalidFatal = newInvalidFatal;
    }
    
    /** {@inheritDoc} */
    protected Object createInstance() throws Exception {
        //TODO just hardcoding these for now
        ArrayList<ConditionValidator> conditionValidators = new ArrayList<ConditionValidator>();
        conditionValidators.add(new AudienceRestrictionConditionValidator());
        conditionValidators.add(new DelegationRestrictionConditionValidator());
        
        ArrayList<SubjectConfirmationValidator> scValidators = new ArrayList<SubjectConfirmationValidator>();
        scValidators.add(new HolderOfKeySubjectConfirmationValidator());
        
        ArrayList<StatementValidator> statementValidators = new ArrayList<StatementValidator>();
        
        SAML20AssertionTokenValidator validator = new SAML20AssertionTokenValidator(conditionValidators, scValidators,
                statementValidators, getTrustEngine());
        
        WSSecuritySAML20AssertionTokenRule rule = new WSSecuritySAML20AssertionTokenRule(validator);
        rule.setInvalidFatal(isInvalidFatal());
        
        return rule;
    }


}