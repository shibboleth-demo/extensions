/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.profile;

import java.util.ArrayList;
import java.util.HashMap;

import org.joda.time.DateTime;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.binding.SAMLMessageContext;
import org.opensaml.common.binding.decoding.SAMLMessageDecoder;
import org.opensaml.common.binding.encoding.SAMLMessageEncoder;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.AuthnStatement;
import org.opensaml.saml2.core.Condition;
import org.opensaml.saml2.core.Conditions;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.Statement;
import org.opensaml.saml2.core.StatusCode;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.saml2.metadata.SPSSODescriptor;
import org.opensaml.samlext.saml2delrestrict.Delegate;
import org.opensaml.samlext.saml2delrestrict.DelegationRestrictionType;
import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.MessageContextEvaluatingFunctor;
import org.opensaml.ws.message.MessageContextMutatingFunctor;
import org.opensaml.ws.message.MessageException;
import org.opensaml.ws.message.decoder.MessageDecodingException;
import org.opensaml.ws.message.encoder.MessageEncodingException;
import org.opensaml.ws.message.handler.BasicHandlerChain;
import org.opensaml.ws.message.handler.Handler;
import org.opensaml.ws.message.handler.HandlerChain;
import org.opensaml.ws.message.handler.HandlerChainResolver;
import org.opensaml.ws.message.handler.HandlerException;
import org.opensaml.ws.message.handler.StaticHandlerChainResolver;
import org.opensaml.ws.soap.soap11.Envelope;
import org.opensaml.ws.soap.soap11.FaultCode;
import org.opensaml.ws.transport.http.HTTPInTransport;
import org.opensaml.ws.transport.http.HTTPOutTransport;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.io.UnmarshallingException;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.util.DatatypeHelper;
import org.opensaml.xml.util.XMLObjectHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.internet2.middleware.shibboleth.common.profile.ProfileException;
import edu.internet2.middleware.shibboleth.common.profile.provider.BaseSAMLProfileRequestContext;
import edu.internet2.middleware.shibboleth.common.relyingparty.ProfileConfiguration;
import edu.internet2.middleware.shibboleth.common.relyingparty.RelyingPartyConfiguration;
import edu.internet2.middleware.shibboleth.common.relyingparty.provider.saml2.SSOConfiguration;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.LibertyConstants;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.soap.decoder.LibertyHTTPSOAP11Decoder;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.soap.decoder.handler.FrameworkHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.soap.decoder.handler.SenderHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.soap.encoder.LibertyHTTPSOAP11Encoder;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.soap.encoder.handler.AddFrameworkHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.soap.encoder.handler.AddSenderHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.message.SubContext;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.message.SubContextContainer;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.relyingparty.LibertyIDWSFSSOSConfiguration;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.decoder.ShibbolethAssertionValidationContext;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.decoder.handler.SAMLProtocolMessageDataHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.decoder.handler.SOAP11BodySAMLMessageExtractor;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.decoder.handler.ShibbolethAssertionValidationInfoResolverHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.decoder.handler.ShibbolethLocalEntityInfoResolverHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.encoder.SAMLEncoderHelper;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.encoder.handler.AddECPResponseHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.encoder.handler.SAMLPeerEntityEndpointLocationExtractor;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.addressing.WSAddressingContext;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.addressing.decoder.handler.ActionHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.addressing.decoder.handler.MessageIDHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.addressing.encoder.handler.AddActionHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.addressing.encoder.handler.AddMessageIDHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.addressing.encoder.handler.AddRelatesToHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.addressing.encoder.handler.UUIDMessageIDGenerator;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security.WSSecurityContext;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security.encoder.handler.AddTimestampHandler;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security.token.Token;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.security.token.impl.SAML20AssertionToken;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.soap.SOAPProcessingHelper;
import edu.internet2.middleware.shibboleth.idp.ext.delegation.ws.soap.soap11.SOAP11Context;
import edu.internet2.middleware.shibboleth.idp.profile.saml2.BaseSAML2ProfileRequestContext;

/**
 * An implementation of the Liberty ID-WSF 2.0 Single Sign-On Service (SSOS).
 */
public class LibertyIDWSFSSOSProfileHandler extends DelegationAwareSAML2SSOProfileHandler {
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(LibertyIDWSFSSOSProfileHandler.class);
    
    /** For building NameID. */
    private SAMLObjectBuilder<NameID> nameIDBuilder;
    
    /** For building Delegate. */
    private SAMLObjectBuilder<Delegate> delegateBuilder;

    /** For building DelegationRestrictionType Condition. */
    private SAMLObjectBuilder<DelegationRestrictionType> delegationRestrictionBuilder;
    
    /** Liberty SOAP message decoder to use. */
    private SAMLMessageDecoder messageDecoder;
    
    /** Liberty SOAP message encoder to use. */
    private SAMLMessageEncoder messageEncoder;
    
    /** Static inbound handler chain resolver. */
    private StaticHandlerChainResolver inboundHandlerChainResolver;
    
    /** Static outbound handler chain resolver. */
    private StaticHandlerChainResolver outboundHandlerChainResolver;

    /** Constructor. */
    public LibertyIDWSFSSOSProfileHandler() {
        // Note: this isn't actually used in this class, but needs to be non-null due to superclass checks introduced in IdP 2.2.x.
        super("/AuthnEngine");
        nameIDBuilder = (SAMLObjectBuilder<NameID>) getBuilderFactory().getBuilder(NameID.DEFAULT_ELEMENT_NAME); 
        delegateBuilder = (SAMLObjectBuilder<Delegate>) getBuilderFactory().getBuilder(Delegate.DEFAULT_ELEMENT_NAME);
        delegationRestrictionBuilder = (SAMLObjectBuilder<DelegationRestrictionType>)
            getBuilderFactory().getBuilder(DelegationRestrictionType.TYPE_NAME);
    }
    
    /** Initialize the profile handler. */
    public void initialize() {
        messageDecoder = new LibertyHTTPSOAP11Decoder(getParserPool());
        messageEncoder = new LibertyHTTPSOAP11Encoder();
        
        inboundHandlerChainResolver = new StaticHandlerChainResolver(buildInboundHandlerChain());
        outboundHandlerChainResolver = new StaticHandlerChainResolver(buildOutboundHandlerChain());
        
        // This is needed by the endpoint selector. We're obviously not actually responding over this binding.
        // In this profile handler, outbound binding selection is not determined by the product of the 
        // EndpointSelector.
        ArrayList<String> libertyOutboundBindings = new ArrayList<String>();
        libertyOutboundBindings.add(SAMLConstants.SAML2_PAOS_BINDING_URI);
        setSupportedOutboundBindings(libertyOutboundBindings);
    }

    /** {@inheritDoc} */
    public String getProfileId() {
        return LibertyIDWSFSSOSConfiguration.PROFILE_ID;
    }
    
    /** {@inheritDoc} */
    public void processRequest(HTTPInTransport inTransport, HTTPOutTransport outTransport)
            throws ProfileException {
        LibertySSOSRequestContext requestContext = buildRequestContext(inTransport, outTransport);
        
        try {
            decodeRequest(requestContext);
        } catch (Throwable t) {
            handleDecodingError(requestContext, t);
            encodeResponse(requestContext);
            return;
        }
        
        try {
            checkPreconditions(requestContext);
            
            populateRequestContext(requestContext);
            
            checkContextData(requestContext);
            
            checkProfileConfiguration(requestContext);
            
            checkProfilePolicy(requestContext);
            
            resolvePrincipalFromToken(requestContext);
            
            processRequest(requestContext);
        } catch (Throwable t) {
            handleProfileError(requestContext, t);
            return;
        } finally {
            encodeResponse(requestContext);
        }
        writeAuditLogEntry(requestContext);
    }

    /**
     * Handle a thrown decoding exception by emitting a SOAP fault response.
     * 
     * @param requestContext the current request context
     * @param t the Throwable that has been thrown
     */
    protected void handleDecodingError(LibertySSOSRequestContext requestContext, Throwable t) {
        if (!(t instanceof ProfileException)) {
            log.error("Error occured while processing request", t);
        }
        if (SOAPProcessingHelper.getFault(requestContext) == null) {
            SOAPProcessingHelper.registerFault(requestContext, FaultCode.SERVER, INTERNAL_ERR_MSG,
                    null, null, null);
        }
    }

    /**
     * Handle a thrown exception by populating the message context appropriately.
     * 
     * @param requestContext the current request context
     * @param t the Throwable that has been thrown
     */
    protected void handleProfileError(LibertySSOSRequestContext requestContext, Throwable t) {
        if (!(t instanceof ProfileException)) {
            log.error("Error occured while processing request", t);
        }
        if (requestContext.getFailureStatus() == null) {
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null,
                    INTERNAL_ERR_MSG));
        }
        Response samlResponse = buildErrorResponse(requestContext);
        requestContext.setOutboundSAMLMessage(samlResponse);
        requestContext.setOutboundSAMLMessageId(samlResponse.getID());
        requestContext.setOutboundSAMLMessageIssueInstant(samlResponse.getIssueInstant());
    }

    /**
     * Process the request message.
     * 
     * @param requestContext the current request context
     */
    protected void processRequest(LibertySSOSRequestContext requestContext) {
        Response samlResponse;
        try {
            if (requestContext.getSubjectNameIdentifier() != null) {
                log.debug("Authentication request contained a subject with a name identifier, "  
                        + "resolving principal from NameID");
                resolvePrincipal(requestContext);
                String requestedPrincipalName = requestContext.getPrincipalName();
                if (!DatatypeHelper.safeEquals(requestContext.getTokenPrincipalName(), requestedPrincipalName)) {
                    log.warn("Authentication request identified principal '{}', " 
                            + "but authentication mechanism identified principal '{}'",
                            requestedPrincipalName, requestContext.getTokenPrincipalName());
                    requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, StatusCode.AUTHN_FAILED_URI,
                            null));
                    throw new ProfileException("User failed authentication");
                }
            } else {
                requestContext.setPrincipalName(requestContext.getTokenPrincipalName());
            }

            resolveAttributes(requestContext);

            ArrayList<Statement> statements = new ArrayList<Statement>();
            statements.add(buildAuthnStatement(requestContext));
            if (requestContext.getProfileConfiguration().includeAttributeStatement()) {
                AttributeStatement attributeStatement = buildAttributeStatement(requestContext);
                if (attributeStatement != null) {
                    requestContext.setReleasedAttributes(requestContext.getAttributes().keySet());
                    statements.add(attributeStatement);
                }
            }

            samlResponse = buildResponse(requestContext, "urn:oasis:names:tc:SAML:2.0:cm:bearer", statements);
        } catch (ProfileException e) {
            samlResponse = buildErrorResponse(requestContext);
        }

        requestContext.setOutboundSAMLMessage(samlResponse);
        requestContext.setOutboundSAMLMessageId(samlResponse.getID());
        requestContext.setOutboundSAMLMessageIssueInstant(samlResponse.getIssueInstant());
    }
    
    /**
     * Check pre-conditions for the profile handler to run.
     * 
     * @param requestContext the current request context
     * 
     * @throws ProfileException if pre-conditions are not met
     */
    protected void checkPreconditions(LibertySSOSRequestContext requestContext) throws ProfileException {
        checkInboundMessage(requestContext);
        checkSamlVersion(requestContext);
    }

    /**
     * Check that all necessary context specific data is available.
     * 
     * @param requestContext the current request context
     * @throws ProfileException if any required context data is not populated
     */
    protected void checkContextData(LibertySSOSRequestContext requestContext) throws ProfileException {
        if (requestContext.getPresenterEntityId() == null) {
            log.warn("Required context property presenterEntityId was not populated");
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, INTERNAL_ERR_MSG));
            throw new ProfileException("Required context property presenterEntityID was not populated");
        }
        if (requestContext.getAttestedToken() == null) {
            log.warn("Required context property attestedToken was not populated");
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, INTERNAL_ERR_MSG));
            throw new ProfileException("Required context property attestedToken was not populated");
        }
    }

    /**
     * Check the profile configuration availability and validity.
     * 
     * @param requestContext the current request context
     * @throws ProfileException if the current profile is not configured for the relying party
     */
    protected void checkProfileConfiguration(LibertySSOSRequestContext requestContext) throws ProfileException {
        String samlRequester = requestContext.getInboundMessageIssuer();
        RelyingPartyConfiguration rpConfigRequester = getRelyingPartyConfiguration(samlRequester);
        ProfileConfiguration ssosConfigRequester = rpConfigRequester.getProfileConfiguration(getProfileId());
        if (ssosConfigRequester == null) {
            String msg =
                String.format("Liberty ID-WSF SSOS profile is not configured for SAML requester '%s'",
                        samlRequester);
            log.warn(msg);
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, msg));
            throw new ProfileException(msg);
        }
        
        String requestPresenter = requestContext.getPresenterEntityId();
        RelyingPartyConfiguration rpConfigPresenter = getRelyingPartyConfiguration(requestPresenter);
        ProfileConfiguration ssosConfigPresenter = rpConfigPresenter.getProfileConfiguration(getProfileId());
        if (ssosConfigPresenter == null) {
            String msg =
                String.format("Liberty ID-WSF SSOS profile is not configured for requester presenter '%s'",
                   requestPresenter);
            log.warn(msg);
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, msg));
            throw new ProfileException(msg);
        }
    }
    
    /**
     * Check the inbound message for non-security-related validity.
     * 
     * @param requestContext the current request context
     * @throws ProfileException if the current profile is not configured for the relying party
     */
    protected void checkInboundMessage(LibertySSOSRequestContext requestContext) throws ProfileException {
        if (!(requestContext.getInboundSAMLMessage() instanceof AuthnRequest)) {
            log.warn("Incomming message was not a AuthnRequest, it was a '{}'",
                    requestContext.getInboundSAMLMessage().getClass().getName());
            requestContext.setFailureStatus(buildStatus(StatusCode.REQUESTER_URI, null,
            "Invalid SAML AuthnRequest message."));
            throw new ProfileException("Invalid SAML AuthnRequest message.");
        }
    }
    
    /**
     * Evaluate profile configuration policy controls for the request.
     * 
     * @param requestContext the current request context
     * @throws ProfileException if the request can not be processed due to policy control
     *          evaluation
     */
    protected void checkProfilePolicy(LibertySSOSRequestContext requestContext) throws ProfileException {
        checkAllowedDelegate(requestContext);
        checkTokenDelegationChainLength(requestContext);
    }

    /**
     * Apply policy control {@link LibertyIDWSFSSOSConfiguration#getAllowedDelegates()}.
     * @param requestContext the current request context
     * @throws ProfileException if the SAML requester is not an allowed delegate with respect to 
     *              the request presenter
     */
    protected void checkAllowedDelegate(LibertySSOSRequestContext requestContext) throws ProfileException {
        String samlRequester = requestContext.getInboundMessageIssuer();
        String requestPresenter = requestContext.getPresenterEntityId();
        log.debug("Checking whether request presenter '{}' allows delegate token issuance to requester '{}'",
                requestPresenter, samlRequester);
        
        RelyingPartyConfiguration rpConfigPresenter = getRelyingPartyConfiguration(requestPresenter);
        LibertyIDWSFSSOSConfiguration ssosConfigPresenter =
            (LibertyIDWSFSSOSConfiguration) rpConfigPresenter.getProfileConfiguration(getProfileId());
        
        if (!ssosConfigPresenter.getAllowedDelegates().contains(samlRequester)) {
            log.warn("SAML requester '{}' is not an allowed delegate based on relying party configuration",
                    samlRequester);
            requestContext.setFailureStatus(buildStatus(StatusCode.REQUESTER_URI, null,
                    "SAML requester is not an allowed delegate for request presenter"));
            throw new ProfileException("SAML requester is not an allowed delegate for request presenter");
        }
    }
    
    /**
     * Apply policy control {@link LibertyIDWSFSSOSConfiguration#getMaximumTokenDelegationChainLength()}.
     * @param requestContext the current request context
     * @throws ProfileException if the SAML token presented for authentication contains a delegation
     *              chain exceeding the allowed maximum length, or there is an error in evaluation
     */
    protected void checkTokenDelegationChainLength(LibertySSOSRequestContext requestContext) throws ProfileException {
        Integer tokenLength = getTokenDelegationChainLength(requestContext);
        if (tokenLength == null || tokenLength <= 0) {
            log.debug("Token did not have delegation chain, this must be initial delegation request");
            return;
        }
        
        String initialDelegate = getInitialDelegateFromToken(requestContext);
        if (initialDelegate == null) {
            String msg = String.format(
                "DelegationRestrictionType chain in Assertion '%s' is malformed, missing initial Delegate NameID value",
                requestContext.getAttestedToken().getID());
            log.warn(msg);
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, msg));
            throw new ProfileException(msg);
        }
        
        Integer policyMaxLength = getEffectiveMaxDelegationChainLength(requestContext, initialDelegate);
        
        log.debug("Token delegation chain length was '{}', policy max was '{}' based on initial delegate '{}'",
                new Object[] {tokenLength, policyMaxLength, initialDelegate,});
        
        if (tokenLength < policyMaxLength) {
            log.debug("Token delegation chain length is OK");
            return;
        }
        
        if (tokenLength > policyMaxLength) {
            log.warn("Presented token delegation chain length exceeds policy max, and fails acceptance");
            requestContext.setFailureStatus(buildStatus(StatusCode.REQUESTER_URI, null,
                    "Presented Assertion authentication token's delegation chain length exceeds policy max"));
            throw new ProfileException("Presented token delegation chain length exceeds policy max");
        }
        
        if (tokenLength.equals(policyMaxLength)) {
            log.warn("Token delegation chain length is equal to policy max, can't issue a new token from this token");
            requestContext.setFailureStatus(buildStatus(StatusCode.REQUESTER_URI, null,
                    "Requested Assertion token would exceed maximum policy length of delegation chain"));
            throw new ProfileException("Requested token would exceed maximum policy length of delegation chain");
        }
        
    }

    /**
     * Get the length of the delegation chain in the presented token.
     * 
     * @param requestContext the current request context
     * @return the token delegation chain length
     */
    protected Integer getTokenDelegationChainLength(LibertySSOSRequestContext requestContext) {
        DelegationRestrictionType delRestrict = getDelegationRestrictionCondition(requestContext
                .getAttestedToken().getConditions());
        
        if (delRestrict != null && delRestrict.getDelegates() != null) {
            return delRestrict.getDelegates().size();
        }
        return null;
    }

    /**
     * Get the entityID of the initial delegate in the presented token's delegation chain.  This is
     * used in policy evaluation.
     * 
     * @param requestContext the current request context
     * @return the initial delegate's entityID, or null if the chain is not present
     */
    protected String getInitialDelegateFromToken(LibertySSOSRequestContext requestContext) {
        DelegationRestrictionType delRestrict = getDelegationRestrictionCondition(requestContext
                .getAttestedToken().getConditions());
        
        if (delRestrict != null && delRestrict.getDelegates() != null && ! delRestrict.getDelegates().isEmpty()) {
            Delegate delegate = delRestrict.getDelegates().get(0);
            if (delegate != null) {
                NameID nameID = delegate.getNameID();
                if (nameID != null) {
                    return DatatypeHelper.safeTrimOrNullString(nameID.getValue());
                }
            }
        }
        
        return null;
    }

    /**
     * Get the effective maximum delegation chain length allowed by policy.
     * 
     * @param requestContext the current request context
     * @param initialDelegate the entityID of the initial chain delegate, which will determine effective policy
     * @return the effective max delegation chain policy length
     * @throws ProfileException if the profile configuration for the delegate can not be resolved
     */
    protected Integer getEffectiveMaxDelegationChainLength(LibertySSOSRequestContext requestContext,
            String initialDelegate) throws ProfileException {
        
        RelyingPartyConfiguration rpConfigDelegate = getRelyingPartyConfiguration(initialDelegate);
        LibertyIDWSFSSOSConfiguration ssosConfigDelegate =
            (LibertyIDWSFSSOSConfiguration) rpConfigDelegate.getProfileConfiguration(getProfileId());
        
        if (ssosConfigDelegate == null) {
            String msg =
                String.format("Liberty ID-WSF SSOS profile is not configured for initial chain delegate '%s'",
                   initialDelegate);
            log.warn(msg);
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, msg));
            throw new ProfileException(msg);
        }
        
        return ssosConfigDelegate.getMaximumTokenDelegationChainLength();
    }

    /**
     * Decode the inbound request.
     * 
     * @param requestContext the current request context
     * @throws ProfileException if there is a problem decoding the message, or if the message 
     *              does not pass required security requirements
     */
    protected void decodeRequest(LibertySSOSRequestContext requestContext) throws ProfileException {
        if (log.isDebugEnabled()) {
            log.debug("Decoding message with decoder binding '{}'",
                    getInboundMessageDecoder(requestContext).getBindingURI());
        }

        try {
            SAMLMessageDecoder decoder = getInboundMessageDecoder(requestContext);
            requestContext.setMessageDecoder(decoder);
            decoder.decode(requestContext);
            log.debug("Decoded request from relying party '{}'", requestContext.getInboundMessageIssuer());

        } catch (MessageDecodingException e) {
            String msg = "Error decoding authentication request message"; 
            log.warn(msg + ": " +   e.getMessage());
            if (SOAPProcessingHelper.getFault(requestContext) == null) {
                SOAPProcessingHelper.registerFault(requestContext, FaultCode.SERVER,
                        INTERNAL_ERR_MSG, null, null, null);
            }
            throw new ProfileException(msg, e);
        } catch (SecurityException e) {
            String msg = "Message did not meet security requirements"; 
            log.warn(msg + ": " +   e.getMessage());
            if (SOAPProcessingHelper.getFault(requestContext) == null) {
                SOAPProcessingHelper.registerFault(requestContext, FaultCode.CLIENT,
                        "Message did not meet security requirements", null, null, null);
            }
            throw new ProfileException(msg, e);
        }
        
    }
    
    /**
     * Build the request context that will be used in processing this request.
     * 
     * @param in HTTP inbound transport
     * @param out HTTP outbound transport
     * @return the new request context
     * @throws ProfileException if the request context can not be constructed
     */
    protected LibertySSOSRequestContext buildRequestContext(HTTPInTransport in, HTTPOutTransport out)
            throws ProfileException {
        
        LibertySSOSRequestContext requestContext = new LibertySSOSRequestContext();
        
        requestContext.setCommunicationProfileId(getProfileId());

        requestContext.setInboundMessageTransport(in);
        requestContext.setInboundSAMLProtocol(SAMLConstants.SAML20P_NS);

        requestContext.setOutboundMessageTransport(out);
        requestContext.setOutboundSAMLProtocol(SAMLConstants.SAML20P_NS);

        requestContext.setMetadataProvider(getMetadataProvider());
        
        requestContext.setPreSecurityInboundHandlerChainResolver(getInboundHandlerChainResolver());
        requestContext.setSecurityPolicyResolver(getSecurityPolicyResolver());
        requestContext.setOutboundHandlerChainResolver(getOutboundHandlerChainResolver());

        requestContext.setPeerEntityRole(SPSSODescriptor.DEFAULT_ELEMENT_NAME);
        
        //TODO perhaps these should be produced by a factory on-demand, inside the SubContextContainer
        SOAP11Context soap11Context = new SOAP11Context(requestContext);
        soap11Context.setFinalDestination(true);
        requestContext.setContext(SOAP11Context.CONTEXT_ID, soap11Context);
        
        WSAddressingContext wsAddressingContext = new WSAddressingContext(requestContext);
        wsAddressingContext.setInboundActionURI(LibertyConstants.SSOS_AUTHN_REQUEST_WSA_ACTION_URI);
        wsAddressingContext.setOutboundActionURI(LibertyConstants.SSOS_RESPONSE_WSA_ACTION_URI);
        requestContext.setContext(WSAddressingContext.CONTEXT_ID, wsAddressingContext);
        
        WSSecurityContext wsSecurityContext = new WSSecurityContext(requestContext);
        requestContext.setContext(WSSecurityContext.CONTEXT_ID, wsSecurityContext);
        
        ShibbolethAssertionValidationContext shibAssertionValidationContext = 
            new ShibbolethAssertionValidationContext(requestContext);
        requestContext.setContext(ShibbolethAssertionValidationContext.CONTEXT_ID, shibAssertionValidationContext);

        return requestContext;
    }
    
    /** {@inheritDoc} */
    protected SAMLMessageDecoder getInboundMessageDecoder(BaseSAMLProfileRequestContext requestContext)
            throws ProfileException {
        return messageDecoder;
    }

    /** {@inheritDoc} */
    protected SAMLMessageEncoder getOutboundMessageEncoder(BaseSAMLProfileRequestContext requestContext)
            throws ProfileException {
        return messageEncoder;
    }
    
    /**
     * Build the inbound handler chain.
     * 
     * @return the handler chain
     */
    protected HandlerChain buildInboundHandlerChain() {
        BasicHandlerChain handlerChain = new BasicHandlerChain();
        
        SAMLProtocolMessageDataHandler samlHandler = new SAMLProtocolMessageDataHandler();
        samlHandler.setProtocolMessageSource(new SOAP11BodySAMLMessageExtractor());
        handlerChain.getHandlers().add(samlHandler);
        
        FrameworkHandler frameworkHandler = new FrameworkHandler();
        handlerChain.getHandlers().add(frameworkHandler);
        
        SenderHandler senderHandler = new SenderHandler();
        senderHandler.setProviderIdValueHandler(new MessageContextMutatingFunctor<String>() {
            public void mutate(MessageContext msgContext, String input) throws MessageException {
                LibertySSOSRequestContext libertyContext = (LibertySSOSRequestContext) msgContext;
                libertyContext.setPresenterEntityId(input);
            }
        });
        handlerChain.getHandlers().add(senderHandler);
        
        ActionHandler actionHandler = new ActionHandler();
        //This has to be anon inner class until can move subcontexts to MessageContext interface
        actionHandler.setActionValueSource(new MessageContextEvaluatingFunctor<String>() {
            public String evaluate(MessageContext msgContext) throws MessageException {
                LibertySSOSRequestContext libertyContext = (LibertySSOSRequestContext) msgContext;
                WSAddressingContext wsAddressingContext =
                    (WSAddressingContext) libertyContext.getContext(WSAddressingContext.CONTEXT_ID);
                return wsAddressingContext.getInboundActionURI();
            }
        });
        handlerChain.getHandlers().add(actionHandler);
        
        MessageIDHandler messageIDHandler = new MessageIDHandler();
        //This has to be anon inner class until can move subcontexts to MessageContext interface
        messageIDHandler.setMessageIDValueHandler(new MessageContextMutatingFunctor<String>() {
            public void mutate(MessageContext msgContext, String input) throws MessageException {
                LibertySSOSRequestContext libertyContext = (LibertySSOSRequestContext) msgContext;
                WSAddressingContext wsAddressingContext =
                    (WSAddressingContext) libertyContext.getContext(WSAddressingContext.CONTEXT_ID);
                wsAddressingContext.setInboundMessageIDURI(input);
            }
        });
        handlerChain.getHandlers().add(messageIDHandler);
        
        ShibbolethAssertionValidationInfoResolverHandler shibAssertionValidationInfoResolver =
            new ShibbolethAssertionValidationInfoResolverHandler();
        shibAssertionValidationInfoResolver.setRelyingPartyManager(getRelyingPartyConfigurationManager());
        shibAssertionValidationInfoResolver.setMetadataProvider(getMetadataProvider());
        //This has to be anon inner class until can move subcontexts to MessageContext interface
        shibAssertionValidationInfoResolver.setRelyingPartyIDSource(new MessageContextEvaluatingFunctor<String>() {
            public String evaluate(MessageContext msgContext) throws MessageException {
                LibertySSOSRequestContext libertyContext = (LibertySSOSRequestContext) msgContext;
                return libertyContext.getPresenterEntityId();
            }
        });
        handlerChain.getHandlers().add(shibAssertionValidationInfoResolver);
        
        ShibbolethLocalEntityInfoResolverHandler shibLocalEntityInfoResolver = 
            new ShibbolethLocalEntityInfoResolverHandler();
        shibLocalEntityInfoResolver.setRelyingPartyManager(getRelyingPartyConfigurationManager());
        shibLocalEntityInfoResolver.setMetadataProvider(getMetadataProvider());
        //This has to be anon inner class until can move subcontexts to MessageContext interface
        shibLocalEntityInfoResolver.setRelyingPartyIDSource(new MessageContextEvaluatingFunctor<String>() {
            public String evaluate(MessageContext msgContext) throws MessageException {
                return ((SAMLMessageContext)msgContext).getPeerEntityId();
            }
        });
        handlerChain.getHandlers().add(shibLocalEntityInfoResolver);
        
        return handlerChain;
    }
    
    /**
     * Build the outbound handler chain.
     * 
     * @return the handler chain
     */
    protected HandlerChain buildOutboundHandlerChain() {
        BasicHandlerChain handlerChain = new BasicHandlerChain();
        
        // Possibly move this to a more generalized mechanism in some fashion
        handlerChain.getHandlers().add( new Handler() {
            public void invoke(MessageContext msgContext) throws HandlerException {
                XMLObject bodyObj = SOAPProcessingHelper.getFault(msgContext);
                if (bodyObj == null) {
                    SAMLMessageContext samlMsgCtx = (SAMLMessageContext) msgContext;
                    bodyObj = samlMsgCtx.getOutboundSAMLMessage();
                }
                Envelope envelope = (Envelope) msgContext.getOutboundMessage();
                envelope.getBody().getUnknownXMLObjects().add(bodyObj);
            }
        });
        
        handlerChain.getHandlers().add( new Handler() {
            public void invoke(MessageContext msgContext) throws HandlerException {
                SAMLMessageContext samlMsgCtx = (SAMLMessageContext) msgContext;
                if (samlMsgCtx.getOutboundSAMLMessage() != null) {
                    try {
                        SAMLEncoderHelper.signMessage(samlMsgCtx);
                    } catch (MessageEncodingException e) {
                        throw new HandlerException("Error signing SAML message", e);
                    }
                }
            }
        });
        
        AddECPResponseHandler ecpHandler = new AddECPResponseHandler();
        ecpHandler.setACSURLValueSource(new SAMLPeerEntityEndpointLocationExtractor());
        handlerChain.getHandlers().add(ecpHandler);
        
        handlerChain.getHandlers().add(new AddFrameworkHandler());
        
        AddSenderHandler senderHandler = new AddSenderHandler();
        senderHandler.setProviderIdSource( new MessageContextEvaluatingFunctor<String>() {
            public String evaluate(MessageContext msgContext) throws MessageException {
                return ((SAMLMessageContext)msgContext).getLocalEntityId();
            }
        });
        handlerChain.getHandlers().add(senderHandler);
        
        AddMessageIDHandler messageIDHandler = new AddMessageIDHandler();
        messageIDHandler.setMessageIDValueSource(new UUIDMessageIDGenerator());
        handlerChain.getHandlers().add(messageIDHandler);
        
        AddActionHandler actionHandler = new AddActionHandler();
        //This has to be anon inner class until can move subcontexts to MessageContext interface
        actionHandler.setActionValueSource(new MessageContextEvaluatingFunctor<String>() {
            public String evaluate(MessageContext msgContext) throws MessageException {
                LibertySSOSRequestContext libertyContext = (LibertySSOSRequestContext) msgContext;
                WSAddressingContext wsAddressingContext =
                    (WSAddressingContext) libertyContext.getContext(WSAddressingContext.CONTEXT_ID);
                return wsAddressingContext.getOutboundActionURI();
            }
        });
        handlerChain.getHandlers().add(actionHandler);
        
        AddRelatesToHandler relatesToHandler = new AddRelatesToHandler();
        //This has to be anon inner class until can move subcontexts to MessageContext interface
        relatesToHandler.setRelatesToValueSource(new MessageContextEvaluatingFunctor<String>() {
            public String evaluate(MessageContext msgContext) throws MessageException {
                LibertySSOSRequestContext libertyContext = (LibertySSOSRequestContext) msgContext;
                WSAddressingContext wsAddressingContext =
                    (WSAddressingContext) libertyContext.getContext(WSAddressingContext.CONTEXT_ID);
                return wsAddressingContext.getInboundMessageIDURI();
            }
        });
        handlerChain.getHandlers().add(relatesToHandler);
        
        AddTimestampHandler timestampHandler = new AddTimestampHandler();
        timestampHandler.setCreatedValueSource(new MessageContextEvaluatingFunctor<DateTime>() {
            public DateTime evaluate(MessageContext msgContext) throws MessageException {
                DateTime samlIssueInstant =  ((SAMLMessageContext)msgContext).getOutboundSAMLMessageIssueInstant();
                if (samlIssueInstant != null) {
                    return samlIssueInstant;
                } else {
                    return new DateTime();
                }
            }
        });
        handlerChain.getHandlers().add(timestampHandler);
        
        return handlerChain;
    }

    
    /**
     * Get the resolver used to resolve the inbound handler chain.
     * 
     * @return the handler chain resolver
     */
    protected HandlerChainResolver getInboundHandlerChainResolver() {
        return inboundHandlerChainResolver;
    }
    
    /**
     * Get the resolver used to resolve the outbound handler chain.
     * 
     * @return the handler chain resolver
     */
    protected HandlerChainResolver getOutboundHandlerChainResolver() {
        return outboundHandlerChainResolver;
    }

    /** {@inheritDoc} */
    protected void populateSAMLMessageInformation(BaseSAMLProfileRequestContext requestContext)
            throws ProfileException {
        AuthnRequest authnRequest = (AuthnRequest) requestContext.getInboundSAMLMessage();
        Subject authnSubject = authnRequest.getSubject();
        if (authnSubject != null) {
            requestContext.setSubjectNameIdentifier(authnSubject.getNameID());
        }
    }

    /** {@inheritDoc} */
    protected void populateProfileInformation(BaseSAMLProfileRequestContext requestContext) throws ProfileException {
        super.populateProfileInformation(requestContext);
        populateWSSTokenInformation(requestContext);
    }
    
    /**
     * Extract WS-Security SAML token and related info and populate on the profile context.
     * 
     * @param requestContext the current message context
     * @throws ProfileException if the mandatory profile information can not be populated
     */
    protected void populateWSSTokenInformation(BaseSAMLProfileRequestContext requestContext) throws ProfileException {
        LibertySSOSRequestContext libertyContext = (LibertySSOSRequestContext) requestContext;
        WSSecurityContext wsSecurityContext = (WSSecurityContext) libertyContext
            .getContext(WSSecurityContext.CONTEXT_ID);
        
        if (wsSecurityContext == null) {
            libertyContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, INTERNAL_ERR_MSG));
            log.warn("WS-Security context was not available");
            throw new ProfileException("WS-Security context was not available");
        }
        
        SAML20AssertionToken assertionToken = null;
        for (Token token : wsSecurityContext.getTokens()) {
            if (token instanceof SAML20AssertionToken) {
                assertionToken = (SAML20AssertionToken) token;
                break;
            }
        }
        
        if (assertionToken == null) {
            log.warn("No SAML 2.0 Assertion token was available in the WS-Security context");
            libertyContext.setFailureStatus(buildStatus(StatusCode.REQUESTER_URI, null,
                    "No SAML 2.0 Assertion token was presented"));
            throw new ProfileException("No SAML 2.0 Assertion token was available ");
        }
        
        if (assertionToken.getValidationStatus() != Token.ValidationStatus.VALID) {
            log.warn("SAML 2.0 Assertion token was found, but its validation status was other than valid: {}",
                    assertionToken.getValidationStatus());
            libertyContext.setFailureStatus(buildStatus(StatusCode.REQUESTER_URI, null,
                    "The SAML 2.0 Assertion token was not successfully validated"));
            throw new ProfileException("SAML 2.0 Assertion token was not successfully validated");
        }
        
        libertyContext.setAttestedToken(assertionToken.getWrappedToken());
        
        if (assertionToken.getSubjectConfirmation() != null) {
            libertyContext.setAttestedSubjectConfirmationMethod(assertionToken.getSubjectConfirmation().getMethod());
        }
        
    }

    /** {@inheritDoc} */
    protected void populateUserInformation(BaseSAMLProfileRequestContext requestContext) {
    }
    
    /**
     * Resolve the principal name from the presented token and store in the message context.
     * 
     * @param requestContext the current request context
     * @throws ProfileException if the principal name can not be resolved from the presented token
     */
    protected void resolvePrincipalFromToken(LibertySSOSRequestContext requestContext) throws ProfileException {
        String relyingPartyId = requestContext.getPresenterEntityId();
        Assertion token = requestContext.getAttestedToken();
        if (token.getSubject() == null || token.getSubject().getNameID() == null) {
            log.warn("Invalid Assertion token: Subject or NameID was null");
            requestContext.setFailureStatus(buildStatus(StatusCode.REQUESTER_URI, null,
                    "The presented SAML 2.0 Assertion token was missing either Subject or NameID"));
            throw new ProfileException("Invalid Assertion token: Subject or NameID was null");
        }
        NameID tokenNameID = token.getSubject().getNameID();
        
        ProfileConfiguration profileConfiguration = getProfileConfiguration(relyingPartyId, getProfileId());
        if (profileConfiguration == null) {
            log.warn("Couldn't resolve profile configuration for token presenter '{}' for profile ID '{}'",
                    relyingPartyId, getProfileId());
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, INTERNAL_ERR_MSG));
            throw new ProfileException("Couldn't resolve profile configuration for token presenter");
        }
        
        if (log.isDebugEnabled()) {
            log.debug("Attempting to resolve principal name from assertion token presented by '{}',"
                    + " with NameID format '{}' and value '{}'",
                    new Object[] {relyingPartyId, tokenNameID.getFormat(), tokenNameID.getValue()});
        }
        
        LibertySSOSRequestContext principalResolutionContext = new LibertySSOSRequestContext();
        principalResolutionContext.setInboundMessageIssuer(relyingPartyId);
        principalResolutionContext.setSubjectNameIdentifier(tokenNameID);
        principalResolutionContext.setProfileConfiguration((SSOConfiguration) profileConfiguration);
        
        try {
            resolvePrincipal(principalResolutionContext);
        } catch (ProfileException e) {
            log.warn("Error resolving principal name from assertion token presented by '{}',"
                    + " with NameID format '{}' and value '{}'",
                    new Object[] {relyingPartyId, tokenNameID.getFormat(), tokenNameID.getValue()});
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, INTERNAL_ERR_MSG));
            throw e;
        }
        requestContext.setTokenPrincipalName(principalResolutionContext.getPrincipalName());
    }

    /** {@inheritDoc} */
    protected SubjectConfirmation buildSubjectConfirmation(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext,
            String confirmationMethod, DateTime issueInstant) {
        SubjectConfirmation sc = super.buildSubjectConfirmation(requestContext, confirmationMethod, issueInstant);
        
        LibertySSOSRequestContext libertyContext = (LibertySSOSRequestContext) requestContext;
        
        NameID nameID = nameIDBuilder.buildObject();
        nameID.setValue(libertyContext.getPresenterEntityId());
        nameID.setFormat(NameID.ENTITY);
        
        sc.setNameID(nameID);
        
        return sc;
    }
    
    /** {@inheritDoc} */
    protected AuthnStatement buildAuthnStatement(SSORequestContext requestContext) {
        LibertySSOSRequestContext libertyContext = (LibertySSOSRequestContext) requestContext;
        Assertion attestedAssertion = libertyContext.getAttestedToken();
        AuthnStatement authnStatement = attestedAssertion.getAuthnStatements().get(0);
        try {
            return XMLObjectHelper.cloneXMLObject(authnStatement);
        } catch (MarshallingException e) {
            log.error("There was a marshalling error while cloning the original AuthnStatement");
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, INTERNAL_ERR_MSG));
            throw new RuntimeException("Marshalling problem while cloning AuthnStatement", e);
        } catch (UnmarshallingException e) {
            log.error("There was an unmarshalling error while cloning the original AuthnStatement");
            requestContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, INTERNAL_ERR_MSG));
            throw new RuntimeException("Unmarshalling problem while cloning AuthnStatement", e);
        }
    }

    /** {@inheritDoc} */
    protected Conditions buildConditions(BaseSAML2ProfileRequestContext<?, ?, ?> requestContext, 
            DateTime issueInstant) {
        LibertySSOSRequestContext libertyContext = (LibertySSOSRequestContext) requestContext;
        DelegationRestrictionType drt = buildDelegationRestriction(libertyContext, issueInstant);
        
        Conditions conditions = super.buildConditions(requestContext, issueInstant);
        conditions.getConditions().add(drt);
        return conditions;
    }
    
    /**
     * Using the existing attested Assertion from the presenter as a context, build the 
     * appropriate DelegationRestrictionType Condition.
     * 
     * @param libertyContext the current request context
     * @param issueInstant the issue instant of the containing assertion
     * 
     * @return new DelegationRestrictionType Condition 
     */
    private DelegationRestrictionType buildDelegationRestriction(LibertySSOSRequestContext libertyContext,
            DateTime issueInstant) {
        Assertion attestedAssertion = libertyContext.getAttestedToken();
        DelegationRestrictionType drt = null;
        
        Delegate newDelegate = buildDelegate(libertyContext, issueInstant);
        
        drt = getDelegationRestrictionCondition(attestedAssertion.getConditions());
        
        if (drt != null) {
            try {
                drt = XMLObjectHelper.cloneXMLObject(drt);
            } catch (MarshallingException e) {
                log.error("There was a marshalling error while cloning the original DelegationRestrictionType");
                libertyContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, INTERNAL_ERR_MSG));
                throw new RuntimeException("Marshalling problem while cloning DelegationRestrictionType", e);
            } catch (UnmarshallingException e) {
                log.error("There was an unmarshalling error while cloning the original DelegationRestrictionType");
                libertyContext.setFailureStatus(buildStatus(StatusCode.RESPONDER_URI, null, INTERNAL_ERR_MSG));
                throw new RuntimeException("Unmarshalling problem while cloning DelegationRestrictionType", e);
            }
        } else {
            drt = delegationRestrictionBuilder.buildObject();
        }
        
        drt.getDelegates().add(newDelegate);
        
        return drt;
    }
    
    /**
     * Get the DelegationRestrictionType Condition from the supplied Conditions, if present.
     * 
     * @param conditions the Assertion Conditions to process
     * @return the DelegationRestrictionType Condition object, or null if not present
     */
    protected DelegationRestrictionType getDelegationRestrictionCondition(Conditions conditions) {
        if (conditions == null) {
            return null;
        }
        
        for (Condition conditionChild : conditions.getConditions()) {
            if (DelegationRestrictionType.TYPE_NAME.equals(conditionChild.getSchemaType())) {
                if (conditionChild instanceof DelegationRestrictionType) {
                    return (DelegationRestrictionType) conditionChild;
                } else {
                    log.warn("Saw Condition of xsi:type DelegationRestrictionType, but incorrect class instance: {}",
                            conditionChild.getClass().getName());
                }
            }
        }
        return null;
    }

    /**
     * Build the Delegate child for the DelegationRestrictionType Condition,
     * based on the current request context.
     * 
     * @param libertyContext the current request context
     * @param issueInstant the issue instant of the containing assertion
     * 
     * @return the new Delegate instance
     */
    private Delegate buildDelegate(LibertySSOSRequestContext libertyContext, DateTime issueInstant) {
        NameID delegateNameID = nameIDBuilder.buildObject();
        delegateNameID.setValue(libertyContext.getPresenterEntityId());
        delegateNameID.setFormat(NameID.ENTITY);
        
        Delegate newDelegate = delegateBuilder.buildObject();
        newDelegate.setNameID(delegateNameID);
        newDelegate.setConfirmationMethod(libertyContext.getAttestedSubjectConfirmationMethod());
        newDelegate.setDelegationInstant(issueInstant);
        
        return newDelegate;
    }


    /** Represents the internal state of a Liberty ID-WSF 2.0 SSOS Request while it's being processed by the IdP. */
    protected class LibertySSOSRequestContext extends DelegationAwareSSORequestContext implements SubContextContainer {
        
        /** The principal name represented by the attested token. */
        private String tokenPrincipalName;
        
        /** The entityID of the presenter of the AuthnRequest. */
        private String presenterEntityId;
        
        /** The SAML 2 Assertion which serves as the authentication token for the AuthnRequest and
         * has been successfully attested by the AuthnRequest presenter. */
        private Assertion attestedToken;
        
        /** Get the confirmation method that was successfully used by the attesting entity. */
        private String attestedSubjectConfirmationMethod;
        
        // TODO working idea is to eventually move subcontexts or something similar to
        // main MessageContext interface
        
        /** Storage for subcontexts. */
        private HashMap<String, SubContext> subcontexts;
        
        /** Constructor. */
        public LibertySSOSRequestContext() {
            subcontexts = new HashMap<String, SubContext>();
        }

        /**
         * Get the principal name represented by the attested token.
         * 
         * @return Returns the token principal name
         */
        public String getTokenPrincipalName() {
            return tokenPrincipalName;
        }

        /**
         * Set the principal name represented by the attested token.
         * 
         * @param newPrincipalName  the new token principal name
         */
        public void setTokenPrincipalName(String newPrincipalName) {
            tokenPrincipalName = newPrincipalName;
        }
        /**
         * Get the entityID of the presenter of the AuthnRequest.
         * 
         * @return Returns the presenter entityId
         */
        public String getPresenterEntityId() {
            return presenterEntityId;
        }

        /**
         * Set the entityID of the presenter of the AuthnRequest.
         * 
         * @param newPresenterEntityId The presenter entityId to set
         */
        public void setPresenterEntityId(String newPresenterEntityId) {
            presenterEntityId = newPresenterEntityId;
        }

        
        /**
         * Get the SAML 2 Assertion which serves as the authentication token for the AuthnRequest and
         * which has been successfully attested by the AuthnRequest presenter.
         * 
         * @return Returns the attestedToken.
         */
        public Assertion getAttestedToken() {
            return attestedToken;
        }

        /**
         * Set the SAML 2 Assertion which serves as the authentication token for the AuthnRequest and
         * which has been successfully attested by the AuthnRequest presenter.
         * 
         * @param newAttestedToken The attestedToken to set.
         */
        public void setAttestedToken(Assertion newAttestedToken) {
            attestedToken = newAttestedToken;
        }
        
        /**
         * Get the SAML 2 SubjectConfirmation method which was used by the presenter in
         * the attestation of the authentication token.
         * 
         * @return Returns the attestedSubjectConfirmationMethod.
         */
        public String getAttestedSubjectConfirmationMethod() {
            return attestedSubjectConfirmationMethod;
        }

        /**
         * Set the SAML 2 SubjectConfirmation method which was used by the presenter in
         * the attestation of the authentication token.
         * 
         * @param newAttestedSubjectConfirmationMethod The attestedSubjectConfirmationMethod to set.
         */
        public void setAttestedSubjectConfirmationMethod(String newAttestedSubjectConfirmationMethod) {
            attestedSubjectConfirmationMethod = newAttestedSubjectConfirmationMethod;
        }
        
        // TODO working idea is to eventually move subcontexts or something similar to
        // main MessageContext interface
        
        /**
         * Get the subcontext.
         * @param contextID the context ID
         * @return the context retrieved by context ID, or null if not present
         */
        public SubContext getContext(String contextID) {
            return subcontexts.get(contextID);
        }
        
        /**
         * Store the subcontext.
         * @param contextID the context ID
         * @param context the context to store
         */
        public void setContext(String contextID, SubContext context) {
            subcontexts.put(contextID, context);
        }

    }

}
