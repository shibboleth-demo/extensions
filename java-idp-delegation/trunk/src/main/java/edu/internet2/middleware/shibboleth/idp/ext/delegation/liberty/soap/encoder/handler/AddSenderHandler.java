/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.soap.encoder.handler;

import org.openliberty.xmltooling.soapbinding.Sender;
import org.opensaml.ws.message.MessageContext;
import org.opensaml.ws.message.MessageContextEvaluatingFunctor;
import org.opensaml.ws.message.MessageException;
import org.opensaml.ws.message.handler.Handler;
import org.opensaml.ws.message.handler.HandlerException;
import org.opensaml.ws.soap.util.SOAPHelper;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.XMLObjectBuilder;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.liberty.LibertyConstants;

/**
 * Handler implementation that adds a Liberty sb:Sender header to the outbound SOAP envelope.
 */
public class AddSenderHandler implements Handler {
    
    /** Builder of Sender object. */
    private XMLObjectBuilder<Sender> senderBuilder;
    
    /** Message context functor which produces the Sender providerId attribute value. */
    private MessageContextEvaluatingFunctor<String> providerIdSource;

    /** Constructor. */
    @SuppressWarnings("unchecked")
    public AddSenderHandler() {
        senderBuilder = (XMLObjectBuilder<Sender>) Configuration.getBuilderFactory()
            .getBuilder(LibertyConstants.SOAP_BINDING_SENDER_ELEMENT_NAME);
    }
    
    /**
     * Set the functor which produces the providerId from the message context.
     * 
     * @param functor the new message context functor
     */
    public void setProviderIdSource(MessageContextEvaluatingFunctor<String> functor) {
        providerIdSource = functor;
    }

    /** {@inheritDoc} */
    public void invoke(MessageContext msgContext) throws HandlerException {
        String providerId = getProviderIdValue(msgContext);
        if (providerId != null) {
            Sender sender = senderBuilder.buildObject(LibertyConstants.SOAP_BINDING_SENDER_ELEMENT_NAME);
            sender.setProviderID(providerId);
            
            SOAPHelper.addHeaderBlock(msgContext, sender);
        }
    }

    /**
     * Get the value of the Sender providerId attribute value.
     * 
     * @param msgContext the current message context
     * @return the Sender providerId attribute value.
     * 
     * @throws HandlerException if providerId can not be determined
     */
    protected String getProviderIdValue(MessageContext msgContext) throws HandlerException {
        if (providerIdSource != null) {
            try {
                return providerIdSource.evaluate(msgContext);
            } catch (MessageException e) {
                throw new HandlerException("Error obtaining the providerId value", e);
            }
        }
        return null;
    }

}
