/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.saml.decoder;

import java.util.Set;

import org.opensaml.ws.message.MessageContext;
import org.opensaml.xml.security.CriteriaSet;
import org.opensaml.xml.util.LazySet;

import edu.internet2.middleware.shibboleth.idp.ext.delegation.message.BaseSubContext;

/**
 * Sub context implementation related to Shibboleth IdP-specific processing of presented Assertion tokens.
 */
public class ShibbolethAssertionValidationContext extends BaseSubContext {
    
    /** The subcontext identifier. */
    public static final String CONTEXT_ID = ShibbolethAssertionValidationContext.class.getName();
    
    /** The set of valid audiences to use when validating an Assertion. */
    private LazySet<String> validAudiences;
    
    /** The set of criteria to use when validating an Assertion's signature. */
    private CriteriaSet sigValCriteriaSet;

    /**
     * Constructor.
     *
     * @param owningContext the owning message context
     */
    public ShibbolethAssertionValidationContext(MessageContext owningContext) {
        super(owningContext);
        validAudiences = new LazySet<String>();
        sigValCriteriaSet = new CriteriaSet();
    }
    
    /**
     * Get the set of valid audiences to use when validating an assertion.
     * @return the set of valid audiences
     */
    public Set<String> getValidAudiences() {
        return validAudiences;
    }
    
    /**
     * Get the criteria set containing criteria used as input to the signature trust engine
     * when the validation of an Assertion's signature is performed.
     * @return the criteria set
     */
    public CriteriaSet getSignatureValidationCriteriaSet() {
        return sigValCriteriaSet;
    }

}
