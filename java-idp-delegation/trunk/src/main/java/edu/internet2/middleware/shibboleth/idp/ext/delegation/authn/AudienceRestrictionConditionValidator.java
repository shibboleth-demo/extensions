/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.authn;

import java.util.List;
import java.util.Set;

import javax.xml.namespace.QName;

import net.jcip.annotations.ThreadSafe;

import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Audience;
import org.opensaml.saml2.core.AudienceRestriction;
import org.opensaml.saml2.core.Condition;
import org.opensaml.xml.util.DatatypeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link ConditionValidator} implementation for {@link AudienceRestriction} conditions.
 * 
 * This validator requires that the {@value #VALID_AUDIENCES_PARAM} parameter by set in the
 * {@link ValidationContext#getStaticParameters()} parameters. It does not require any parameters in
 * {@link ValidationContext#getDynamicParameters()}.
 * 
 * This validator does not populate any parameters in the {@link ValidationContext#getDynamicParameters()}.
 */
@ThreadSafe
public class AudienceRestrictionConditionValidator implements ConditionValidator {

    /**
     * The name of the {@link ValidationContext#getStaticParameters()} carrying a {@link Set<String>} whose values are
     * the acceptable Audience values for evaluating the Assertion.
     */
    public static final String VALID_AUDIENCES_PARAM = AudienceRestrictionConditionValidator.class.getName()
            + ".IntendedAudiences";

    /** Logger. */
    private Logger log = LoggerFactory.getLogger(AudienceRestrictionConditionValidator.class);

    /** {@inheritDoc} */
    public QName getServicedCondition() {
        return AudienceRestriction.DEFAULT_ELEMENT_NAME;
    }

    /** {@inheritDoc} */
    public ValidationResult validate(Condition condition, Assertion assertion, ValidationContext context) {
        
        Set<String> validAudiences =  (Set<String>) context.getStaticParameters().get(VALID_AUDIENCES_PARAM);
        if (validAudiences == null || validAudiences.isEmpty()) {
            log.warn("Set of valid audiences was not available from the validation context, " 
                    + "unable to evaluate AudienceRestriction Condition");
            context.setValidationFailureMessage("Unable to determine list of valid audiences");
            return ValidationResult.INDETERMINATE;
        }
        log.debug("Evaluating the Assertion's AudienceRestriction/Audience values " 
                + "against the list of valid audiences: {}",
                validAudiences.toString());

        if (!(condition instanceof AudienceRestriction)) {
            log.warn("Condition '{}' of type '{}' in assertion '{}' was not an '{}' condition.  Unable to process.",
                    new Object[] { condition.getElementQName(), condition.getSchemaType(), assertion.getID(),
                            getServicedCondition(), });
            return ValidationResult.INDETERMINATE;
        }
        AudienceRestriction audienceRestriction = (AudienceRestriction) condition;
        List<Audience> audiences = audienceRestriction.getAudiences();
        if (audiences == null || audiences.isEmpty()) {
            context.setValidationFailureMessage(String.format(
                    "'%s' condition in assertion '%s' is malformed as it does not contain any audiences",
                    getServicedCondition(), assertion.getID()));
            return ValidationResult.INVALID;
        }

        for (Audience audience : audiences) {
            String audienceURI = DatatypeHelper.safeTrimOrNullString(audience.getAudienceURI());
            if (validAudiences.contains(audienceURI)) {
                log.debug("Matched valid audience: {}", audienceURI);
                return ValidationResult.VALID;
            }
        }

        String msg = String.format(
                "None of the audiences within Assertion '%s' matched the list of valid audiances", assertion.getID());
        log.debug(msg);
        context.setValidationFailureMessage(msg);
        return ValidationResult.INVALID;
    }
}