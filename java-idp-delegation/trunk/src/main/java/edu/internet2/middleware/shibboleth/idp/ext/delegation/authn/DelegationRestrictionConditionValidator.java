/*
 * Copyright 2009 University Corporation for Advanced Internet Development, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.idp.ext.delegation.authn;

import javax.xml.namespace.QName;

import net.jcip.annotations.ThreadSafe;

import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Condition;
import org.opensaml.samlext.saml2delrestrict.DelegationRestrictionType;

/**
 * {@link ConditionValidator} implementation for <code>DelegationRestrictionType</code> style conditions. Note, as
 * delegation restriction conditions do not place any conditions on the use of an assertion this condition always
 * evaluates as valid with the assumption that further processing of this information will be done later by the invoker
 * of the validation process.
 * 
 * This validator does not expect any parameters in the {@link ValidationContext#getStaticParameters()} or
 * {@link ValidationContext#getDynamicParameters()}.
 * 
 * This validator does not populate any parameters in the {@link ValidationContext#getDynamicParameters()}.
 */
@ThreadSafe
public class DelegationRestrictionConditionValidator implements ConditionValidator {

    /** {@inheritDoc} */
    public QName getServicedCondition() {
        return DelegationRestrictionType.TYPE_NAME;
    }

    /** {@inheritDoc} */
    public ValidationResult validate(Condition condition, Assertion assertion, ValidationContext context) {
        // Delegation restriction information is a 'condition of use' type condition so we always return valid.
        return ValidationResult.VALID;
    }
}