from net.grinder.script import Test
from net.grinder.script.Grinder import grinder

from java.util import List, ArrayList
from java.net import URL

from com.gargoylesoftware.htmlunit import WebClient, WebRequestSettings, WebResponse, HttpMethod, DefaultCredentialsProvider

from org.apache.commons.httpclient import NameValuePair, UsernamePasswordCredentials

props = grinder.getProperties()

log = grinder.logger.output
stdout = grinder.logger.TERMINAL
debug = props.getBoolean('log.debug', False)

httpScheme = props.get('shib.idp.scheme')
idpHost = props.get('shib.idp.host')
idpPort = props.getInt('shib.idp.port', 0)
if idpPort == 0:
    if httpScheme == 'http':
        idpPort = 80
    if httpScheme == 'https':
        idpPort = 443
idpPath =  grinder.getProperties().get('shib.idp.path')

idpBaseUrl = httpScheme + '://' + idpHost + ':' + str(idpPort)

def executeSSOTest():    
    idpSSOEndpoint = idpBaseUrl + idpPath + '/profile/Shibboleth/SSO'
    webClient=WebClient()
    webClient.setUseInsecureSSL(True)
    listreq = ArrayList()
    listreq.add(NameValuePair("providerId", props.get('shib.sp.id')))
    listreq.add(NameValuePair("shire", props.get('shib.sp.acsURL')))
    listreq.add(NameValuePair("target", props.get('shib.sp.acsURL')))
    wrs = WebRequestSettings(URL(idpSSOEndpoint), HttpMethod.GET)
    wrs.setRequestParameters(listreq)

    if props.get('shib.idp.auth') == 'FORM':
        webClient.setRedirectEnabled(True);

        if debug: log("sending SSO request to: " + idpSSOEndpoint, stdout)
        wr = webClient.loadWebResponse(wrs)
        if debug: log("Response content: \n"+wr.getContentAsString(), stdout)
		
        if wr.getStatusCode() != 200:
            raise Exception('Invalid HTTP response code: ' + str(wr.getStatusCode()))
        wrs.setHttpMethod(HttpMethod.POST)
        listauth = ArrayList()
        listauth.add(NameValuePair("j_username", props.get('shib.user.name')))
        listauth.add(NameValuePair("j_password", props.get('shib.user.pass')))
        wrs.setRequestParameters(listauth)
        wrs.setUrl(wr.getRequestUrl())
    else:
        # BASIC auth
        webClient.setRedirectEnabled(False);

        if debug: log("sending SSO request to: " + idpSSOEndpoint, stdout)
        wr = webClient.loadWebResponse(wrs)
        if debug: log("Response content: \n"+wr.getContentAsString(), stdout)

        if wr.getStatusCode() != 302:
            raise Exception('Invalid HTTP response code: ' + str(wr.getStatusCode()))
        wrs.setHttpMethod(HttpMethod.GET)
        creds = DefaultCredentialsProvider()
        creds.addCredentials(props.get('shib.user.name'), props.get('shib.user.pass'), idpHost, idpPort, props.get('shib.idp.realm'))
        wrs.setCredentialsProvider(creds);

        if debug: log("sending BASIC auth to: " + wr.getResponseHeaderValue('Location'), stdout)
        wrs.setUrl(URL(wr.getResponseHeaderValue('Location')))
	
    if debug: log("sending login request to: " + str(wr.getRequestUrl()), stdout)
    wr = webClient.loadWebResponse(wrs)
    
    if debug:
        log("Request URL = "+wr.getRequestUrl().toString(), stdout)
        log(wr.getContentAsString())
    
    if wr.getStatusCode() not in [ 200, 301, 302 ]:
        if debug: log("error response: " + response.getText(), stdout)
        raise Exception('Invalid HTTP response code: ' + str(wr.getStatusCode()))
    return wr.getContentAsString()


def validateReturn(responseString):
    return """<input type=\"hidden\" name=\"SAMLResponse\"""" in responseString


ssoTest = Test(1, 'Shibboleth SSO Profile').wrap(executeSSOTest)


class TestRunner:    
    def __call__(self):
        grinder.statistics.delayReports = 1
        
        strResponse = ssoTest()
        if validateReturn(strResponse):
            log("Success!")
            grinder.statistics.forLastTest.setSuccess(1)
        else:
            log("Failure!")
            grinder.statistics.forLastTest.setSuccess(0)
