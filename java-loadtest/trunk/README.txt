Grinder Load Testing Framework
==================================
A simple framework, based on Grinder, used for load testing.

Requirements
----------------------------------
- Java 5

Package Contents
----------------------------------
/grinder-framework
   - bin/        - scripts for starting and stopping the test console and agents
   - lib/        - libraries for running grinder
   - log/        - log files produced by grinder
   - test/       - directory where tests "packets" are put
   - logging.xml - logging configuration file

Test Packets
----------------------------------
A test packet is single self-contained test.  It is made up of a directory, of any name, 
with the following structure.

<testdir>/
   - test.properties  - grinder properties used for the test
   - test.py          - python script describing the test
   - lib/             - directory containing any jars needed in order to run the script
   
Additional files or directories may be included.  They will simply be ignored by the framework.
It is recommended that you include a README file that describes the tests, system requirements, 
and properties which must/may be set in the 'test.properties' file.

To install a test packets simply place it in the 'grinder-framework/tests' directory.

Quick Start
----------------------------------
Assuming you have already installed your test packet you simply need to do the following.

1. Start the console:  'grinder-framwork/bin/console.sh'
2. Start the test agent:  'grinder-framwork/bin/agent.sh <testPacketName>'