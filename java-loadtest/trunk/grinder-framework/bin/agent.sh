#! /bin/bash

if [ $# -lt 1 ] ; then
   echo "Usage:  agent.sh <TEST_DIR>"
   exit 0
fi

HOME="$(cd "${0%/*}/.." && pwd)"
TEST_LIB="$HOME/tests/$1/lib"
TEST_CONF="$HOME/tests/$1/test.properties"
TEST_SCRIPT="$HOME/tests/$1/test.py"
TEST_JVM_OPTS="-Djava.endorsed.dirs=$HOME/lib/endorsed -Dgrinder.script=$TEST_SCRIPT -Dgrinder.jvm.arguments=-Dlogback.configurationFile=$HOME/logging.xml"

. $HOME/bin/java_env.sh

LIBS="$TEST_LIB/*.jar"
for i in $LIBS
do
    if [ "$i" != "${LIBS}" ] ; then
        LOCALCLASSPATH="$LOCALCLASSPATH":"$i"
    fi
done

echo "Grinder Home:        $HOME"
echo "Grinder Properties:  $TEST_CONF"
echo "Grinder Script File: $TEST_SCRIPT"

$JAVACMD $JVMOPTS '-classpath' $LOCALCLASSPATH $TEST_JVM_OPTS 'net.grinder.Grinder' "$TEST_CONF"