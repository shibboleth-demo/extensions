#! /bin/bash

HOME="$(cd "${0%/*}/.." && pwd)"

. $HOME/bin/java_env.sh

$JAVACMD $JVMOPTS '-classpath' $LOCALCLASSPATH 'net.grinder.Console'