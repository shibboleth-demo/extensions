package edu.internet2.middleware.shibboleth.jaas.securid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.Principal;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import org.apache.log4j.Logger;
import org.apache.commons.codec.binary.Base64;

import com.sun.security.auth.callback.TextCallbackHandler;

public class RemoteSecurIDLoginModule implements LoginModule {
    private String roleName = null;
    private String protectedURL = "/protected";
    private Subject subject;
    private CallbackHandler callbackHandler;

    // optional regex patterns to apply to input
    private Pattern userPattern = null;
    private Pattern passPattern = null;

    // Authentication status
    private boolean loginSucceeded; // have we successfully logged in?
    private boolean principalsInSubject;
    // if so, what principals did we add to the subject
    // (so we can remove the principals we added if the login is aborted)
    private Vector<Principal> principalsForSubject = new Vector<Principal>();

    private static Logger log = Logger.getLogger(RemoteSecurIDLoginModule.class.getName());

    /**
     * Initialize a login attempt.
     * 
     * @param subject
     *            the Subject this login attempt will populate.
     * 
     * @param callbackhandler
     *            the CallbackHandler that can be used to get the user name, and
     *            in authentication mode, the user's password
     * 
     * @param sharedState
     *            A Map containing data shared between login modules when there
     *            are multiple authenticators configured. This SecurID does not
     *            use this parameter.
     * 
     * @param options
     *            A Map containing options that the authenticator's
     *            authentication provider impl wants to pass to its login module
     *            impl. For example, it can be used to pass in configuration
     *            data (where is the sessionFactory holding user and group info)
     *            and to pass in whether the login module is used for
     *            authentication or to complete identity assertion. The
     *            SecurIDAuthenticationProviderImpl adds an option named
     *            "sessionFactory". The value is a AuthSessionFactory object. It
     *            gives the login module access to the user and group
     *            definitions define in Authentication Manager.
     * 
     */
    public void initialize(Subject subject, CallbackHandler callbackHandler, Map sharedState, Map options) {
        // only called (once!) after the constructor and before login
        log.debug("initializing SecurID login module");

        this.subject = subject;
        this.callbackHandler = callbackHandler;

        try {
            Object name = options.get("roleName");
            if (name != null)
                roleName = name.toString();
            name = options.get("protectedURL");
            if (name != null)
            	protectedURL = name.toString();
            name = options.get("userRegex");
            if (name != null)
                userPattern = Pattern.compile(name.toString());
            name = options.get("pwRegex");
            if (name != null)
                passPattern = Pattern.compile(name.toString());
        } catch (Exception e) {
            log.error("error creating SecurID SessionFactory: " + e);
        }
    }

    /**
     * Attempt to login.
     * 
     * extract the user name and password from the callback handler. Call check
     * to find out if the user and the password matches, then populate the
     * subject with the user and the user's group using the value from the
     * Shell. Otherwise, the login fails.
     * 
     * @return A boolean indicating whether or not the login for this login
     *         module succeeded.
     */
    public boolean login() throws LoginException {
        // only called (once!) after initialize

        log.debug("login attempt with SecurID");

        loginSucceeded = false;
        principalsInSubject = false;

        // Call a method to get the callbacks.
        // For authentication mode, it will have one for the
        // username and one for the password.
        Callback[] callbacks = getCallbacks();

        // Get the user name and passcode.
        String userName = getUserName(callbacks);
        String Passcode = getPasscode(userName, callbacks);

        // Optional short-circuit of login attempt?
        if (userPattern != null) {
            Matcher m = userPattern.matcher(userName);
            if (!m.matches()) {
                log.debug("username (" + userName + ") did not match required regex, bypassing attempt");
                return false;
            }
        }
        if (passPattern != null) {
            Matcher m = passPattern.matcher(Passcode);
            if (!m.matches()) {
                log.debug("passcode did not match required regex, bypassing attempt");
                return false;
            }
        }

		try {
			String authString = userName + ":" + Passcode;
			String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
			
			URL url = new URL(protectedURL);
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("Authorization", "Basic " + authStringEnc);
			conn.connect();

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String inputLine = in.readLine();
			if (inputLine != null && inputLine.trim().equals(userName)) {
                loginSucceeded = true;
                principalsForSubject.add(new SecurIDPrincipal(userName));
                if (roleName != null)
                    principalsForSubject.add(new SecurIDRole(roleName));
			}
			
		} catch (Exception ex) {
			log.error("Exception while verifying token code remotely :", ex);
			throw new LoginException(ex.getMessage());
		}
        
        log.debug("SecurID login for (" + userName + ") " + (loginSucceeded ? "succeeded" : "failed"));
        return loginSucceeded;
    }

    /**
     * Completes the login by adding the user and the user's groups to the
     * subject.
     * 
     * @return A boolean indicating whether or not the commit succeeded.
     */
    public boolean commit() throws LoginException {
        // only called (once!) after login

        log.debug("SecurID login.commit");
        if (loginSucceeded) {
            // put the user and the user's groups (computed during the
            // login method and stored in the principalsForSubject object)
            // into the subject.
            subject.getPrincipals().addAll(principalsForSubject);
            principalsInSubject = true;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Aborts the login attempt. Remove any principals we put into the subject
     * during the commit method from the subject.
     * 
     * @return A boolean indicating whether or not the abort succeeded.
     */
    public boolean abort() throws LoginException {
        // only called (once!) after login or commit
        // or may be? called (n times) after abort

        log.debug("SecurID login.abort");
        if (principalsInSubject) {
            subject.getPrincipals().removeAll(principalsForSubject);
            principalsInSubject = false;
        }
        return true;
    }

    /**
     * Logout. This should never be called.
     * 
     * @return A boolean indicating whether or not the logout succeeded.
     */
    public boolean logout() throws LoginException {
        // should never be called

        log.debug("SecurID logout");
        return true;
    }

    /**
     * Get the list of callbacks needed by the login module.
     * 
     * @return The array of Callback objects by the login module. Returns one
     *         for the user name and password if in authentication mode. Returns
     *         one for the user name if in identity assertion mode.
     */
    private Callback[] getCallbacks() throws LoginException {
        if (callbackHandler == null) {
            throw new LoginException("No CallbackHandler specified.");
        }

        Callback[] callbacks = new Callback[2]; // need one for the user name
                                                // and one for the password

        // add in the password callback
        callbacks[1] = new PasswordCallback("SecurIDPasscode: ", false);

        // add in the user name callback
        callbacks[0] = new NameCallback("SecurIDUser: ");

        // Call the callback handler, who in turn, calls back to the
        // callback objects, handing them the user name and password.
        // These callback objects hold onto the user name and password.
        // The login module retrieves the user name and password from them
        // later.
        try {
            callbackHandler.handle(callbacks);
        } catch (IOException e) {
            throw new LoginException(e.toString());
        } catch (UnsupportedCallbackException e) {
            throw new LoginException(e.toString() + " " + e.getCallback().toString());
        }

        return callbacks;
    }

    /**
     * Get the user name from the callbacks (that the callback handler has
     * already handed the user name to).
     * 
     * @param callbacks
     *            The array of Callback objects used by this login module. The
     *            first in the list must be the user name callback object.
     * 
     * @return A String containing the user name (from the user name callback
     *         object)
     */
    private String getUserName(Callback[] callbacks) throws LoginException {
        String userName = ((NameCallback) callbacks[0]).getName();
        if (userName == null) {
            throw new LoginException("Username not supplied.");
        }
        log.debug("authenticating with SecurID as (" + userName + ")");
        return userName;
    }

    /**
     * Get the password from the callbacks (that the callback handler has
     * already handed the password to) - that is, the password from the login
     * attempt. Must only be used for authentication mode, not for identity
     * assertion mode.
     * 
     * @param useName
     *            A String containing the name of the user (already retrieved
     *            from the callbacks). Only passed in so that we can print a
     *            better error message if the password is bogus.
     * 
     * @param callbacks
     *            The array of Callback objects used by this login module. The
     *            second in the list must be the password callback object.
     * 
     * @return A String containing the password from the login attempt
     * 
     * @throws LoginException
     *             if no password was supplied in the login attempt.
     */
    private String getPasscode(String userName, Callback[] callbacks) throws LoginException {
        PasswordCallback passwordCallback = (PasswordCallback) callbacks[1];
        char[] password = passwordCallback.getPassword();
        passwordCallback.clearPassword();
        if (password == null || password.length < 1) {
            throw new LoginException("Password not supplied for user (" + userName + ").");
        }
        return new String(password);
    }
 
    public static void main(final String[] args) throws Exception {
    	String name = "RemoteSecurID";
    	if (args.length > 0) {
    		name = args[0];
    	}

    	final LoginContext lc = new LoginContext(name, new TextCallbackHandler());
    	lc.login();
    	System.out.println("Authentication/Authorization succeeded");

    	final Set<Principal> principals = lc.getSubject().getPrincipals();
    	System.out.println("Subject Principal(s): ");

    	final Iterator<Principal> i = principals.iterator();
    	while (i.hasNext()) {
    		final Principal p = i.next();
    		System.out.println("  " + p);
    	}
    	lc.logout();
    }

}