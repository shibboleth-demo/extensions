package edu.internet2.middleware.shibboleth.jaas.securid;


import java.security.Principal;


public class SecurIDPrincipal implements Principal {

    protected String name;

    public SecurIDPrincipal(String name) {
        this.name = name;
    }


    public String getName() {
        return (this.name);
    }


    /**
     * Return a String representation of this object, which exposes only
     * information that should be public.
     */
    public String toString() {

        StringBuffer sb = new StringBuffer("SecurIDPrincipal[");
        sb.append(this.name);
        sb.append("]");
        return (sb.toString());

    }

}
