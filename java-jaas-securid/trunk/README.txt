This package contains a stand-alone JAAS LoginModule that authenticates
users against an RSA SecurID Authentication Server. The PIN code and
time-based token code are combined to form the password accepted by the
module.

As a JAAS module, it can be plugged into any environment into which
password-based LoginModules can be configured. The JAAS wrapper must
supply Name and Password callbacks of the following classes:

javax.security.auth.callback.NameCallback;
javax.security.auth.callback.PasswordCallback;

Upon a successful login, the JAAS Subject will contain a SecurIDPrincipal
object containing the username used.

To build this module, you will need to obtain the RSA SecurID engine
library. This code is not redistributable but should be available at
no charge if you have a SecurID license. The file "authapi.jar" should
be placed into the lib folder to build the project. The resulting
jarfile and the jarfiles in lib must be available to the runtime
environment in which the module is used.

Configuration depends on how your JAAS environment is controlled.
In a typical JAAS configuration file, the LoginModule is activated
as follows:

    edu.internet2.middleware.shibboleth.jaas.securid.SecurIDLoginModule required
        properties="/usr/local/tomcat/conf/securid.properties";

The properties file contains a set of configuration properties controlling
the underlying SecurID library code and can be found in the SecurID SDK.

Optional JAAS configuration properties include:

roleName
    If set, the LoginModule adds a SecurIDRole object with the supplied
    value to the Subject upon successful login.

userRegex
    If set, the username must match the regular expression or the module
    will not attempt a login. Used to avoid wasted trips to the server.

pwRegex
    If set, the password must match the regular expression or the module
    will not attempt a login. Used to avoid wasted trips to the server.


Acknowledgment:

This module is based on source code supplied by RSA Security, Inc.
carrying a Copyright (c) 2002 by BEA Systems.
